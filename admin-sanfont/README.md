### Environment requirement

> Operating System: `Ubuntu v20.04.4`
>
> NodeJS: `v20.11.1`

### Install Dependencies``

Install project dependencies: `npm install```

### Development (For Developer)

> Run: `npm start`
>
> Open browser at: `http://localhost:8080`
>
> Please install flug-in ESLINT into your IDE for code convention auto checking

### Development Build

> Run: `npm build:development`
>
> Copy all content in `./build` for deployment

### Staging Build

> Run: `npm build:staging`
>
> Copy all content in `./build` for deployment

### Production Build

> Run: `npm build:production`
>
> Copy all content in `./build` for deployment

### Check Convention

> Javascript: `npm lint`
