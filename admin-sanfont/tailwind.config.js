/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        './src/**/*.{js,jsx,ts,tsx}',
        './node_modules/react-tailwindcss-select/dist/index.esm.js',
        './node_modules/flowbite/**/*.js',
    ],
    theme: {
        extend: {
            colors: {
                'main-color': '#ff5e3a',
            },
        },
    },
    plugins: [require('daisyui', 'flowbite/plugin')],
};
