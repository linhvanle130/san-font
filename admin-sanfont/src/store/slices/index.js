import accountSlice from './accountSlice';
import commonSlice from './commonSlice';
import cartItemsReducer from './cartItemsSlide';

export const rootReducer = () => ({
    account: accountSlice,
    common: commonSlice,
    cartItems: cartItemsReducer,
});
