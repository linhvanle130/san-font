import React from 'react';
import { BrowserRouter, Navigate, Outlet, Route, Routes, useLocation } from 'react-router-dom';
import { useSelector } from 'react-redux';
import axiosClient from '@/api/axiosClient';
import LocalStorage from '@/utils/storage';
import { STORAGE_KEY } from '@/constants/storage-key';
import Init from './init';

import Home from '@/pages/home';
import Login from '@/pages/auth/login';
import Product from '@/pages/product';
import User from '@/pages/user';
import Layout from '@/components/layout';
import NotFound from '@/pages/404';
import ProductCategory from '@/pages/product-category';
import ProductElement from '@/pages/product-element';
import ProductCreate from '@/pages/product/product-create';
import ProductUpdate from '@/pages/product/product-update';
import Contact from '@/pages/contact';
import Blog from '@/pages/blog';
import BlogCreate from '@/pages/blog/blog-create';
import BlogCategoryCreate from '@/pages/blog-category/blog-category-create';
import Profile from '@/pages/profile';
import Order from '@/pages/order';
import OrderPackage from '@/pages/order-package';
import OrderCreate from '@/pages/order/order-create';
import OrderView from '@/pages/order/order-view';
import ProductSave from '@/pages/product-save';
import OrderUpdate from '@/pages/order/order-update';
import Comment from '@/pages/comment';
import ConfigData from '@/pages/config-data';
import ConfigRole from '@/pages/config-role';
import DownloadFont from './../components/download-font';

axiosClient.defaults.headers.common = {
    Authorization: `Bearer ${LocalStorage.get(STORAGE_KEY.TOKEN)}`,
};

function RequireAuth({ children }) {
    const { token } = useSelector((state) => state.account);
    const location = useLocation();

    if (!token) {
        return <Navigate to="/login" state={{ from: location }} replace />;
    }

    return children;
}

function NotRequireAuth({ children }) {
    const { token } = useSelector((state) => state.account);
    const location = useLocation();

    if (token) {
        return <Navigate to="/" state={{ from: location }} replace />;
    }

    return children;
}

const MainApp = () => {
    return (
        <Init>
            <BrowserRouter>
                <Routes>
                    <Route
                        element={
                            <RequireAuth>
                                <Layout />
                            </RequireAuth>
                        }
                    >
                        <Route path="/" element={<Home />} />
                        <Route path="/product" element={<Product />} />
                        <Route path="/product/create" element={<ProductCreate />} />
                        <Route path="/product/update/:id" element={<ProductUpdate />} />
                        <Route path="/product-category" element={<ProductCategory />} />
                        <Route path="/product-element" element={<ProductElement />} />
                        <Route path="/product-save" element={<ProductSave />} />
                        <Route path="/user" element={<User />} />
                        <Route path="/contact" element={<Contact />} />
                        <Route path="/blog" element={<Blog />} />
                        <Route path="/blog/create" element={<BlogCreate />} />
                        <Route path="/blog/update/:id" element={<BlogCreate />} />
                        <Route path="/blog-category/create" element={<BlogCategoryCreate />} />
                        <Route path="/blog-category/update/:id" element={<BlogCategoryCreate />} />
                        <Route path="/profile" element={<Profile />} />
                        <Route path="/order" element={<Order />} />
                        <Route path="/order-new" element={<OrderCreate />} />
                        <Route path="/order-update/:id" element={<OrderUpdate />} />
                        <Route path="/order-package" element={<OrderPackage />} />
                        <Route path="/order-detail/:orderCode" element={<OrderView />} />
                        <Route path="/comment" element={<Comment />} />
                        <Route path="/config-data" element={<ConfigData />} />
                        <Route path="/config-role" element={<ConfigRole />} />
                        <Route path="/down" element={<DownloadFont />} />
                    </Route>
                    <Route
                        element={
                            <NotRequireAuth>
                                <Outlet />
                            </NotRequireAuth>
                        }
                    >
                        <Route path="/login" element={<Login />} />
                    </Route>
                    <Route path="/not-found" element={<NotFound />} />
                    <Route path="*" element={<Navigate to="/not-found" />} />
                </Routes>
            </BrowserRouter>
        </Init>
    );
};

export default MainApp;
