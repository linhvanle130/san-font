import { ToastContainer, Zoom } from 'react-toastify';
import React, { useEffect, useState } from 'react';
import LocalStorage from '@/utils/storage';
import { STORAGE_KEY } from '@/constants/storage-key';
import { MODE_THEME } from '@/constants';
import { useSelector } from 'react-redux';

const Init = ({ children }) => {
    const { theme } = useSelector((state) => state.common);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        switch (theme) {
            case MODE_THEME.DARK:
                document.documentElement.setAttribute('data-theme', 'dark');
                LocalStorage.set(STORAGE_KEY.THEME, MODE_THEME.DARK);
                break;
            case MODE_THEME.LIGHT:
            default:
                document.documentElement.setAttribute('data-theme', 'light');
                LocalStorage.set(STORAGE_KEY.THEME, MODE_THEME.LIGHT);
                break;
        }
    }, [theme]);

    useEffect(() => {
        setLoading(false);
    }, []);

    return (
        <>
            {!loading ? children : null}
            <ToastContainer
                position="top-right"
                transition={Zoom}
                autoClose={3000}
                hideProgressBar
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
        </>
    );
};

export default Init;
