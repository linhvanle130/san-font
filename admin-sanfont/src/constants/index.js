export const MODE_THEME = {
    LIGHT: 'light',
    DARK: 'dark',
};

export const PATHS_LOCAL = {
    CITIES: '/locations/cities.json',
    DISTRICTS: '/locations/districts',
    WARDS: '/locations/wards',
    LOCATION: '/locations/location.json',
};

export const STATUS_REQUEST = {
    UNAUTHORIZED: 401,
};

export const GLOBAL_STATUS = {
    INACTIVE: 0,
    ACTIVE: 1,
};

export const GLOBAL_FEATURED = {
    INACTIVE: 0,
    ACTIVE: 1,
};

export const ACCOUNT_STATUS = {
    ACTIVATE: 1,
    INACTIVATE: 2,
};

export const IMAGE_TYPE = {
    SUB: 0,
    MAIN: 1,
};

export const FONT_TYPE = {
    SUB: 0,
    MAIN: 1,
};

export const MODULE = {
    DASHBOARD: 1,
    PRODUCT: 2,
    USER: 3,
    ORDER: 4,
    BLOG: 5,
    CONFIG: 6,
};

export const MODULE_MAP = [
    { id: 1, name: 'Dashboard' },
    { id: 2, name: 'Sản phẩm' },
    { id: 3, name: 'Người dùng' },
    { id: 4, name: 'Đơn hàng' },
    { id: 5, name: 'Blog' },
    { id: 6, name: 'Cấu hình' },
];

export const ROLE = {
    ADMIN: 1,
    SALE: 2,
    USER: 3,
};

export const MASTER_DATA_NAME = {
    LEVEL_USER: 1,
    STATUS_ORDER: 2,
    ROLE: 3,
    STATUS_FONT: 4,
};

export const MASTER_DATA = [
    { value: 1, nameMaster: 'Cấp bậc người dùng' },
    { value: 2, nameMaster: 'Tình trạng đơn hàng' },
    { value: 3, nameMaster: 'Phần quyền' },
];

export const PAYMENT_METHOD_MAP = [
    { value: 1, label: 'Thanh toán bằng Vietcombank' },
    { value: 2, label: 'Thanh toán bằng tiền mặt' },
];

export const STATUS_ORDER = {
    PENDING: 1,
    SUCCESS: 2,
};

export const STATUS_ORDER_DATA = [
    { value: 0, label: 'Tải các font bình thường' },
    { value: 1, label: 'Tải tất cả các font' },
];
