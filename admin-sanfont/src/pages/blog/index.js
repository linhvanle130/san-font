import React, { useEffect, useRef, useState } from 'react';
import { Tooltip, Table, Input, Space, Button } from 'antd';
import { useTranslation } from 'react-i18next';
import { Link, useNavigate } from 'react-router-dom';
import { useForm, Controller } from 'react-hook-form';
import { IoIosSearch } from 'react-icons/io';
import { AiOutlineDelete, AiOutlineEdit } from 'react-icons/ai';
import { FiChevronDown, FiChevronRight } from 'react-icons/fi';
import moment from 'moment';

import { GLOBAL_STATUS } from '@/constants';
import { yupResolver } from '@hookform/resolvers/yup';
import yup from '@/utils/yup';
import errorHelper from '@/utils/error-helper';
import Loading from '@/components/loading';
import ModalBlogChangeStatus from './modal-blog-change-status';
import ModalBlogDelete from './modal-blog-delete';
import ModalBlogCategoryDelete from '../blog-category/modal-blog-category-delete';
import blogApis from '@/api/blogApis';
import blogCategoryApis from '@/api/blogCategoryApis';

const LIMIT = 1000000;

const Blog = () => {
    const { t } = useTranslation();
    const [search, setSearch] = useState(true);
    const account = JSON.parse(localStorage.getItem('info'));

    const [listBlogs, setListBlogs] = useState([]);
    const [listDetailBlogs, setListDetailBlogs] = useState([]);
    const [loading, setLoading] = useState(true);
    const [page, setPage] = useState(1);

    console.log(listBlogs);

    const refModalAccountDetail = useRef();
    const refModalBlogDelete = useRef();
    const refModalBlogCategoryDelete = useRef();
    const searchInput = useRef(null);
    const navigate = useNavigate();

    const [searchText, setSearchText] = useState('');
    const [searchedColumn, setSearchedColumn] = useState('');

    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        setSearchText(selectedKeys[0]);
        setSearchedColumn(dataIndex);
    };

    const onSubmit = (values) => {
        const params = {
            search: values?.title,
            size: LIMIT,
        };
        setPage(1);
        return onGetListBlogs(params);
    };

    const schema = yup.object({
        search: yup.string().max(255),
    });

    const {
        control,
        handleSubmit,
        getValues,
        formState: { errors },
    } = useForm({
        resolver: yupResolver(schema),
    });

    const handleEditBlog = (id) => {
        navigate(`/blog/update/${id}`);
    };

    const handleEditBogCategory = (id) => {
        navigate(`/blog-category/update/${id}`);
    };

    const getColumnSearchProps = (dataIndex) => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm }) => (
            <div
                style={{
                    padding: 8,
                }}
            >
                <Input
                    ref={searchInput}
                    placeholder={`${t('search')} ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{
                        marginBottom: 8,
                        display: 'block',
                    }}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<IoIosSearch />}
                        size="small"
                        style={{
                            width: 100,
                            background: '#ff5e3a',
                        }}
                    >
                        {t('search')}
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: (filtered) => (
            <IoIosSearch
                style={{
                    color: filtered ? '#1890ff' : undefined,
                }}
            />
        ),
        onFilter: (value, record) => record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
        onFilterDropdownOpenChange: (visible) => {
            if (visible) {
                setTimeout(() => searchInput.current?.select(), 100);
            }
        },
        render: (text) => text,
    });

    const onGetListBlogs = (values) => {
        const params = {
            ...values,
            admin: true,
            size: LIMIT,
        };

        setLoading(true);

        blogApis
            .getListBlog(params)
            .then(({ rows }) => {
                const detailBlogs = [];
                setListBlogs(
                    rows.map((e) => {
                        e.blogCategory.map((a) => detailBlogs.push(a));
                        return {
                            ...e,
                            countCategory: e.blogCategory?.length,
                        };
                    }),
                );
                setListDetailBlogs(detailBlogs);

                window.scroll({
                    top: 0,
                    behavior: 'smooth',
                });
            })
            .catch((err) => {
                console.log(err);
                errorHelper(err);
            })
            .finally(() => {
                setLoading(false);
            });
    };

    useEffect(() => {
        onGetListBlogs();
    }, []);

    const expandedRowRender = (row) => {
        const blogDetail = listDetailBlogs
            .filter((ft) => ft.blogId === row.id)
            .map((e) => {
                return {
                    id: e.id,
                    thumbnail: e.thumbnail,
                    blogId: e.blogId,
                    title: e.title,
                    createdAt: e.createdAt,
                };
            });
        const columns = [
            {
                title: t('image'),
                key: 'thumbnail',
                dataIndex: 'thumbnail',
                width: 150,
                align: 'center',
                render: (item) => (
                    <div className="flex items-center justify-center">
                        <img width={100} src={item} alt="" />
                    </div>
                ),
            },
            {
                title: t('article_name'),
                dataIndex: 'title',
                key: 'title',
                width: 150,
                sorter: (a, b) => a.title?.localeCompare(b.title),
            },
            {
                title: t('created_at'),
                dataIndex: 'createdAt',
                key: 'created_at',
                render: (item) => moment(item).format('YYYY-MM-DD HH:mm:ss'),
                width: 100,
                sorter: (a, b) => a.createdAt?.localeCompare(b.createdAt),
            },
            {
                title: t('action'),
                key: 'action',
                width: 100,
                render: (item, record) => (
                    <div className="p-4 flex items-center justify-center gap-3">
                        <Tooltip title={t('update')}>
                            <button
                                onClick={() => handleEditBogCategory(record.id)}
                                className="px-3 mt-2 py-[6px] bg-main-color text-white rounded-2xl cursor-pointer hover:shadow-4xl hover:opacity-80"
                            >
                                <AiOutlineEdit />
                            </button>
                        </Tooltip>
                        {account.role === 1 && (
                            <Tooltip title={t('Delete')}>
                                <button
                                    className=" px-3 mt-2 py-[6px] bg-red-500 text-white rounded-2xl cursor-pointer hover:shadow-4xl hover:opacity-80"
                                    onClick={() => refModalBlogCategoryDelete?.current?.onOpen(record.id, record)}
                                >
                                    <AiOutlineDelete />
                                </button>
                            </Tooltip>
                        )}
                    </div>
                ),
                align: 'center',
            },
        ];
        return <Table columns={columns} dataSource={blogDetail} pagination={false} rowKey="id" size="small" />;
    };

    const columns = [
        {
            title: t('image'),
            key: 'thumbnail',
            dataIndex: 'thumbnail',
            width: 200,
            align: 'center',
            render: (item) => (
                <div className="flex items-center justify-center">
                    <img width={100} src={item} alt="" />
                </div>
            ),
        },
        {
            title: t('article_name'),
            dataIndex: 'title',
            key: 'title',
            width: 300,
            render: (text, record) => <Tooltip title={text}>{text}</Tooltip>,
            sorter: (a, b) => a.title?.localeCompare(b.title),
            ...getColumnSearchProps('title'),
        },
        {
            title: t('article_description'),
            dataIndex: 'description',
            key: 'description',
            width: 450,
            render: (text, record) => <Tooltip title={text}>{text}</Tooltip>,
            sorter: (a, b) => a.description?.localeCompare(b.description),
        },
        {
            title: t('blog_total_item'),
            key: 'countCategory',
            dataIndex: 'countCategory',
            width: 150,
            align: 'center',
            sorter: (a, b) => a.countCategory - b.countCategory,
        },
        {
            title: t('active'),
            dataIndex: 'status',
            key: 'status',
            with: 250,
            render: (item, record) => (
                <div className="w-full flex justify-center hover:opacity-80">
                    <div className="relative">
                        <input
                            type="checkbox"
                            className="sr-only peer"
                            checked={item === GLOBAL_STATUS.ACTIVE}
                            onChange={() => {}}
                        />
                        <div className="w-11 h-6 bg-gray-300 rounded-full peer peer-focus:ring-4  dark:peer-focus:ring-main-color  peer-checked:after:translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-0.5 after:left-[2px] after:bg-white  after:border after:rounded-full after:h-5 after:w-5 after:transition-all peer-checked:bg-main-color"></div>
                        <div
                            onClick={() => refModalAccountDetail.current.onOpen(record.id, record)}
                            className="absolute pt-[16px] pb-[25px] pl-[24px] pr-[35px] top-[-10px] cursor-pointer"
                        ></div>
                    </div>
                </div>
            ),
            align: 'center',
            sorter: (a, b) => a.status - b.status,
        },
        {
            title: t('created_at'),
            dataIndex: 'createdAt',
            key: 'created_at',
            render: (item) => moment(item).format('YYYY-MM-DD HH:mm:ss'),
            width: 200,
            sorter: (a, b) => a.createdAt?.localeCompare(b.createdAt),
        },
        {
            title: t('action'),
            key: 'action',
            render: (item, record) => (
                <div className="p-4 flex items-center justify-center gap-3">
                    <Tooltip title={t('update')}>
                        <button
                            onClick={() => handleEditBlog(record.id)}
                            className="px-3 mt-2 py-[6px] bg-main-color text-white rounded-2xl cursor-pointer hover:shadow-4xl hover:opacity-80"
                        >
                            <AiOutlineEdit />
                        </button>
                    </Tooltip>
                    {account.role === 1 && (
                        <Tooltip title={t('Delete')}>
                            <button
                                className=" px-3 mt-2 py-[6px] bg-red-500 text-white rounded-2xl cursor-pointer hover:shadow-4xl hover:opacity-80"
                                onClick={() => refModalBlogDelete?.current?.onOpen(record.id, record)}
                            >
                                <AiOutlineDelete />
                            </button>
                        </Tooltip>
                    )}
                </div>
            ),
            fixed: 'right',
            align: 'center',
        },
    ];

    const renderSearch = () => (
        <div className="mb-4">
            <div className="bg-white rounded-sm shadow-3xl dark-container">
                <button
                    className="flex items-center px-4 py-3 cursor-pointer transition"
                    onClick={() => setSearch(!search)}
                >
                    {search ? <FiChevronDown /> : <FiChevronRight />}
                    <h3 className="pl-2 text-base font-bold">{t('search')}</h3>
                </button>
                {search && (
                    <div className="px-4 py-3">
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <div className="w-full flex gap-5 max-lg:flex-col max-lg:gap-3">
                                <div className="w-1/3 max-sm:w-full">
                                    <Controller
                                        name="title"
                                        control={control}
                                        render={({ field }) => (
                                            <Input {...field} type="text" placeholder={t('search_dot3')} />
                                        )}
                                    />
                                    {errors?.search?.message && (
                                        <p className="text-error mt-1">{errors?.search?.message}</p>
                                    )}
                                </div>
                            </div>

                            <div className="mt-5">
                                <button
                                    type="submit"
                                    className="flex items-center justify-center bg-main-color py-2 px-4  hover:shadow-4xl max-sm:w-[100px] max-sm:mt-0 hover:opacity-80"
                                >
                                    <div> {loading && <Loading Loading />}</div>
                                    <span className="text-white text-sm">{t('search')}</span>
                                </button>
                            </div>
                        </form>
                    </div>
                )}
            </div>
        </div>
    );

    const handleTableChange = (pagination) => {
        setPage(pagination.current);
    };

    return (
        <div>
            {renderSearch()}
            <div className="flex justify-between py-4 max-sm:flex-col">
                <h2 className="mb-3 text-[20px] md:text-[24px] lg:text-[30px] text-black font-medium">
                    {t('blog_all')}
                </h2>
                <div className="flex gap-3">
                    <Link to={'/blog/create'}>
                        <button
                            type="submit"
                            className="bg-main-color py-2 px-4 hover:shadow-4xl max-sm:float-right hover:opacity-80 transition-all"
                        >
                            <span className="text-white text-sm">{t('blog_create')}</span>
                        </button>
                    </Link>
                    <Link to={'/blog-category/create'}>
                        <button
                            type="submit"
                            className="bg-main-color py-2 px-4 hover:shadow-4xl max-sm:float-right hover:opacity-80 transition-all"
                        >
                            <span className="text-white text-sm">{t('blog_create_item')}</span>
                        </button>
                    </Link>
                </div>
            </div>
            <Table
                rowSelection={{
                    type: 'checkbox',
                }}
                columns={columns}
                dataSource={listBlogs}
                rowKey="id"
                expandable={{
                    expandedRowRender,
                    expandRowByClick: true,
                }}
                scroll={{ x: 'max-content' }}
                pagination={{
                    current: page,
                    position: ['bottomCenter'],
                }}
                loading={loading}
                onChange={handleTableChange}
            />
            <ModalBlogChangeStatus
                ref={refModalAccountDetail}
                onAfterChangeStatus={() => {
                    onGetListBlogs(getValues());
                }}
                account={account}
            />
            <ModalBlogDelete
                ref={refModalBlogDelete}
                onSubmit={(id) => blogApis.deleteBlog(id)}
                onAfterDelete={() => {
                    onGetListBlogs(getValues());
                }}
            />
            <ModalBlogCategoryDelete
                ref={refModalBlogCategoryDelete}
                onSubmit={(id) => blogCategoryApis.deleteBlogCategory(id)}
                onAfterDelete={() => {
                    onGetListBlogs(getValues());
                }}
            />
        </div>
    );
};

export default Blog;
