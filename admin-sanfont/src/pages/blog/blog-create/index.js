import React, { useRef, useState, useEffect } from 'react';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm, Controller, useWatch } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router-dom';
import { Image, Spin, Input } from 'antd';
import { useTranslation } from 'react-i18next';
import { AiOutlineArrowLeft, AiOutlineCheck, AiOutlineReload } from 'react-icons/ai';
import { CiCamera } from 'react-icons/ci';
import Compressor from 'compressorjs';

import blogApis from '@/api/blogApis';
import commonApis from '@/api/commonApis';
import errorHelper from '@/utils/error-helper';
import successHelper from '@/utils/success-helper';
import Loading from '@/components/loading';
import yup from '@/utils/yup';

const BlogCreate = () => {
    const { t } = useTranslation();
    const { id } = useParams();
    const { TextArea } = Input;
    const account = JSON.parse(localStorage.getItem('info'));

    const [loading, setLoading] = useState(false);
    const [isUpdateThumbnail, setIsUpdateThumbnail] = useState(false);
    const refThumbnail = useRef();
    const navigate = useNavigate();

    const schema = yup.object({
        title: yup.string().max(100).required(),
        description: yup.string().max(500).required(),
        thumbnail: yup.string().required().trim(),
    });

    const {
        control,
        reset,
        handleSubmit,
        formState: { errors },
        setValue,
    } = useForm({
        resolver: yupResolver(schema),
    });
    const { thumbnail } = useWatch({ control });

    const submitCreate = (values) => {
        const { title, description, thumbnail } = values;

        const payload = {
            title: title,
            description: description,
            thumbnail: thumbnail,
        };

        if (id) {
            return blogApis
                .updateBlog({ ...payload, id })
                .then(() => {
                    successHelper(t('update_success'));
                    navigate('/blog');
                })
                .catch((err) => {
                    console.log(err);
                    errorHelper(err);
                })
                .finally(() => {
                    setLoading(false);
                });
        } else {
            return blogApis
                .createBlog(payload)
                .then(() => {
                    successHelper(t('create_success'));
                    navigate('/blog');
                })
                .catch((err) => {
                    console.log(err);
                    errorHelper(err);
                })
                .finally(() => {
                    setLoading(false);
                });
        }
    };

    const getBlogs = async () => {
        const detailBlogs = await blogApis.getDetailBlog(id);
        setValue('title', detailBlogs.title, {
            shouldValidate: true,
            shouldDirty: true,
        });
        setValue('description', detailBlogs.description, {
            shouldValidate: true,
            shouldDirty: true,
        });
        setValue('thumbnail', detailBlogs.thumbnail, {
            shouldValidate: true,
            shouldDirty: true,
        });
    };

    useEffect(() => {
        if (id) {
            getBlogs();
        }
    }, []);

    const handleChangeTitle = (value) => {
        setValue('title', value, {
            shouldValidate: true,
            shouldDirty: true,
        });
    };

    const handleChangeDescription = (value) => {
        setValue('description', value, {
            shouldValidate: true,
            shouldDirty: true,
        });
    };

    const onchangeThumbnail = (e) => {
        const files = e.target.files;
        if (files.length > 0) {
            new Compressor(files[0], {
                quality: 0.8,
                success: (compressedImg) => {
                    // compressedResult has the compressed file.
                    // Use the compressed file to upload the images to your server.
                    onUploadThumbnail(compressedImg)
                        .then((response) => {
                            setValue('thumbnail', response, {
                                shouldValidate: true,
                                shouldDirty: true,
                            });
                        })
                        .finally(() => {
                            setIsUpdateThumbnail(false);
                        });
                },
            });
        }
        e.target.value = null;
    };

    const onUploadThumbnail = async (file) => {
        setIsUpdateThumbnail(true);
        const formData = new FormData();
        formData.append('file', file);
        formData.append('fileName', file.name);
        return commonApis
            .preUploadFile(formData)
            .then(async (response) => {
                return response;
            })
            .catch((err) => {
                errorHelper(err);
            });
    };

    const goBackListBlog = () => {
        navigate(`/blog`);
    };

    const customStyle = {
        border: '1px solid red',
    };

    return (
        <div className="w-full max-sm:px-0 max-sm:py-2">
            <form onSubmit={handleSubmit(submitCreate)}>
                <div className="flex justify-between max-lg:pr-3">
                    <div className="flex items-center">
                        <div onClick={() => goBackListBlog()}>
                            <AiOutlineArrowLeft className="mr-4 text-black hover:text-main-color text-lg cursor-pointer" />
                        </div>
                        <h2 className="text-[20px] md:text-[24px] lg:text-[30px] text-black font-medium">
                            {id ? t('article_update') : t('article_add')}
                        </h2>
                    </div>
                    <div className="flex gap-2 max-sm:hidden">
                        {account.role === 1 && (
                            <button
                                type="submit"
                                className=" bg-main-color py-2 px-4 hover:shadow-4xl rounded-[2px] hover:opacity-80"
                            >
                                <div className="flex gap-2 items-center text-white">
                                    <div>{loading && <Loading Loading />}</div>
                                    <AiOutlineCheck />
                                    <span className="text-sm">{id ? t('update') : t('submit')}</span>
                                </div>
                            </button>
                        )}
                        {id ? (
                            <button
                                onClick={() => goBackListBlog()}
                                className="bg-white py-2 px-4 border-[1px] border-[#ccc] rounded-[2px] hover:border-main-color hover:text-main-color"
                            >
                                <div className="flex gap-2 items-center">
                                    <AiOutlineReload className="text-black" />
                                    <span className="text-sm text-black">{t('back_list')}</span>
                                </div>
                            </button>
                        ) : (
                            <button
                                onClick={() => {
                                    reset();
                                }}
                                type="reset"
                                className="bg-white py-2 px-4 border-[1px] border-[#ccc] rounded-[2px] hover:border-main-color hover:text-main-color"
                            >
                                <div className="flex gap-2 items-center">
                                    <AiOutlineReload className="text-black" />
                                    <span className="text-sm text-black">{t('reset')}</span>
                                </div>
                            </button>
                        )}
                    </div>
                </div>

                <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 mt-5 mb-[50px] md:mb-0 lg:mb-0">
                    <div className="md:col-span-1 lg:col-span-2 px-0 md:px-1 lg:px-3">
                        <div className="bg-white p-3 md:p-4 lg:p-6 rounded dark-container">
                            <div className="mb-3">
                                <h4 className="mb-2">{t('article_name')}</h4>
                                <Controller
                                    name="title"
                                    control={control}
                                    render={({ field }) => (
                                        <Input
                                            {...field}
                                            placeholder={t('article_name')}
                                            onChange={(event) => handleChangeTitle(event.target.value)}
                                            status={errors?.title?.message}
                                            style={errors?.title?.message && customStyle}
                                        />
                                    )}
                                />
                                {errors?.title?.message && <p className="text-error mt-1">{errors?.title?.message}</p>}
                            </div>
                            <div className="mb-3">
                                <h4 className="mb-2">{t('article_description')}</h4>
                                <Controller
                                    name="description"
                                    control={control}
                                    render={({ field }) => (
                                        <TextArea
                                            {...field}
                                            placeholder={t('article_description')}
                                            onChange={(event) => handleChangeDescription(event.target.value)}
                                            status={errors?.description?.message}
                                            style={errors?.title?.message && customStyle}
                                            rows={6}
                                        />
                                    )}
                                />
                                {errors?.description?.message && (
                                    <p className="text-error mt-1">{errors?.description?.message}</p>
                                )}
                            </div>
                        </div>
                    </div>
                    <div className="md:col-span-1 lg:col-span-1 px-0 md:px-1 lg:px-3">
                        <div className="bg-white rounded mt-3 md:mt-0 lg:mt-0 dark-container">
                            <div className="p-3 md:p-4 lg:p-6 border-b-[1px] border-b-[#ccc]">
                                <h3 className="font-medium text-base">{t('thumbnail')}</h3>
                            </div>
                            <div className="flex flex-col items-center justify-center">
                                <div className="my-5">
                                    <div
                                        className={
                                            errors?.thumbnail?.message
                                                ? 'border-[1px] border-error border-dashed'
                                                : 'border-[1px] border-[#ccc] border-dashed'
                                        }
                                    >
                                        <div className="thumbnail-upload">
                                            <Image
                                                className="img-thumbnail"
                                                src={
                                                    thumbnail ||
                                                    'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg=='
                                                }
                                                alt=""
                                            />
                                            <button
                                                type={'button'}
                                                disabled={isUpdateThumbnail}
                                                onClick={() => refThumbnail.current.click()}
                                                className="button-camera tooltip tooltip-left"
                                                data-tip={t('thumbnail_action_note')}
                                            >
                                                {isUpdateThumbnail ? <Spin /> : <CiCamera />}
                                                <input
                                                    ref={refThumbnail}
                                                    type="file"
                                                    accept="image/*"
                                                    onChange={onchangeThumbnail}
                                                    style={{ display: 'none' }}
                                                />
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                {errors?.thumbnail?.message && (
                                    <p className="text-error mt-1">{errors?.thumbnail?.message}</p>
                                )}
                            </div>
                        </div>
                    </div>
                </div>

                <div className="flex justify-end p-3 max-sm:fixed max-sm:bottom-0 max-sm:left-0 max-sm:right-0 max-sm:justify-center max-sm:bg-white z-50 rounded ">
                    <div className="flex gap-2">
                        {account.role === 1 && (
                            <button
                                type="submit"
                                className=" bg-main-color py-2 px-4 hover:shadow-4xl rounded-[2px] hover:opacity-80"
                            >
                                <div className="flex gap-2 items-center text-white">
                                    <div>{loading && <Loading Loading />}</div>
                                    <AiOutlineCheck />
                                    <span className="text-sm">{id ? t('update') : t('submit')}</span>
                                </div>
                            </button>
                        )}
                        {id ? (
                            <button
                                onClick={() => goBackListBlog()}
                                className="bg-white py-2 px-4 border-[1px] border-[#ccc] rounded-[2px] hover:border-main-color hover:text-main-color"
                            >
                                <div className="flex gap-2 items-center">
                                    <AiOutlineReload className="text-black" />
                                    <span className="text-sm text-black">{t('back_list')}</span>
                                </div>
                            </button>
                        ) : (
                            <button
                                onClick={() => {
                                    reset();
                                }}
                                type="reset"
                                className="bg-white py-2 px-4 border-[1px] border-[#ccc] rounded-[2px] hover:border-main-color hover:text-main-color"
                            >
                                <div className="flex gap-2 items-center">
                                    <AiOutlineReload className="text-black" />
                                    <span className="text-sm text-black">{t('reset')}</span>
                                </div>
                            </button>
                        )}
                    </div>
                </div>
            </form>
        </div>
    );
};

export default BlogCreate;
