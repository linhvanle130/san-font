import React, { forwardRef, useImperativeHandle, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import { yupResolver } from '@hookform/resolvers/yup';
import { Modal, Input } from 'antd';
import yup from '@/utils/yup';
import contactApis from '@/api/contactApis';
import errorHelper from '@/utils/error-helper';
import successHelper from '@/utils/success-helper';
import Loading from '@/components/loading';

const schema = yup.object({
    email: yup.string().required().max(255),
    fullName: yup.string().trim().required().max(255),
    phoneNumber: yup.string().required(),
    content: yup.string().trim().required().max(255),
});

const ModalContactDetail = ({ onAfterUpdate }, ref) => {
    const { t } = useTranslation();

    const {
        control,
        handleSubmit,
        formState: { errors },
        setValue,
        reset,
    } = useForm({
        resolver: yupResolver(schema),
    });

    const [visible, setVisible] = useState(false);
    const [loading, setLoading] = useState(false);
    const [idDataContact, setIdDataContact] = useState(null);
    const [errorContact, setErrorContact] = useState('');

    const onOpen = (id) => {
        setVisible(true);
        setLoading(true);
        setIdDataContact(id);
        contactApis
            .getDetailContact(id)
            .then((res) => {
                const { email, fullName, phoneNumber, content } = res;
                setValue('email', email, {
                    shouldValidate: true,
                    shouldDirty: true,
                });
                setValue('fullName', fullName?.trim(), {
                    shouldValidate: true,
                    shouldDirty: true,
                });
                setValue('phoneNumber', phoneNumber?.trim(), {
                    shouldValidate: true,
                    shouldDirty: true,
                });
                setValue('content', content?.trim(), {
                    shouldValidate: true,
                    shouldDirty: true,
                });
            })
            .catch((err) => {
                errorHelper(err.message);
            })
            .finally(() => setLoading(false));
    };

    useImperativeHandle(ref, () => ({
        onOpen,
    }));

    const onClose = () => {
        setVisible(false);
        setLoading(false);
        reset();
        setIdDataContact(null);
        setErrorContact('');
    };

    const submitUpdateProductCategory = (values) => {
        const { email, fullName, phoneNumber, content } = values;
        const payload = {
            id: idDataContact,
            email: email,
            fullName: fullName.trim(),
            phoneNumber: phoneNumber,
            content: content?.trim(),
        };

        return contactApis
            .updateStatusContact(payload)
            .then(() => {
                successHelper(t('update_success'));
                onClose();
                onAfterUpdate();
            })
            .catch((err) => {
                errorHelper(err);
            })
            .finally(() => setLoading(false));
    };

    const customStyle = {
        border: '1px solid red',
    };

    return (
        <Modal footer={null} open={visible} onCancel={onClose} maskClosable={false} destroyOnClose>
            <form onSubmit={handleSubmit(submitUpdateProductCategory)}>
                <h4 className="mb-3 font-semibold text-black">{t('contact_details')}</h4>
                <div className="mb-3">
                    <h4 className="mb-1">{t('email')}</h4>
                    <Controller
                        name="email"
                        control={control}
                        render={({ field }) => (
                            <Input
                                {...field}
                                status={errors?.email?.message ? 'error' : null}
                                placeholder={t('email')}
                                style={errors?.email?.message && customStyle}
                                disabled
                            />
                        )}
                    />
                    {errors?.email?.message && <p className="text-error mt-1">{errors?.email?.message}</p>}
                </div>
                <div className="mb-3">
                    <h4 className="mb-1">{t('full_name')}</h4>
                    <Controller
                        name="fullName"
                        control={control}
                        render={({ field }) => (
                            <Input
                                {...field}
                                status={errors?.fullName?.message ? 'error' : null}
                                placeholder={t('fullName')}
                                style={errors?.fullName?.message && customStyle}
                                disabled
                            />
                        )}
                    />
                    {errors?.fullName?.message && <p className="text-error mt-1">{errors?.fullName?.message}</p>}
                </div>
                <div className="mb-3">
                    <h4 className="mb-1">{t('phone')}</h4>
                    <Controller
                        name="phoneNumber"
                        control={control}
                        render={({ field }) => (
                            <Input
                                {...field}
                                status={errors?.phoneNumber?.message ? 'error' : null}
                                placeholder={t('phone')}
                                style={errors?.phoneNumber?.message && customStyle}
                                disabled
                            />
                        )}
                    />
                    {errors?.phoneNumber?.message && <p className="text-error mt-1">{errors?.phoneNumber?.message}</p>}
                </div>
                <div className="mb-3">
                    <h4 className="mb-1">{t('content')}</h4>
                    <Controller
                        name="content"
                        control={control}
                        render={({ field }) => (
                            <Input.TextArea
                                {...field}
                                status={errors?.content?.message ? 'error' : null}
                                placeholder={t('note')}
                                style={errors?.content?.message && customStyle}
                                disabled
                            />
                        )}
                    />
                    {errors?.content?.message && <p className="text-error mt-1">{errors?.content?.message}</p>}
                </div>
            </form>
        </Modal>
    );
};

export default forwardRef(ModalContactDetail);
