import React, { useEffect, useRef, useState } from 'react';
import { Table, Tooltip } from 'antd';
import { useTranslation } from 'react-i18next';
import { AiOutlineEdit } from 'react-icons/ai';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import moment from 'moment';

import yup from '@/utils/yup';
import { GLOBAL_STATUS } from '@/constants';
import ModalContactView from './modal-contact-view';
import ModalContactChangeStatus from './modal-contact-change-status';
import contactApis from '@/api/contactApis';

const LIMIT = 1000000;

const Contact = () => {
    const { t } = useTranslation();
    const account = JSON.parse(localStorage.getItem('info'));

    const schema = yup.object({
        search: yup.string().max(255),
    });

    const { getValues } = useForm({
        resolver: yupResolver(schema),
    });

    const refModalContactChangeStatus = useRef();
    const refModalContactDetailStatus = useRef();

    const [page, setPage] = useState(1);
    const [loading, setLoading] = useState(true);
    const [listContacts, setListContacts] = useState([]);

    const onGetListContacts = (values) => {
        const params = {
            ...values,
            page,
            size: LIMIT,
        };

        setLoading(true);

        contactApis
            .getListContacts(params)
            .then(({ rows }) => {
                setListContacts(rows);

                window.scroll({
                    top: 0,
                    behavior: 'smooth',
                });
            })
            .catch((err) => {
                errorHelper(err);
            })
            .finally(() => {
                setLoading(false);
            });
    };

    useEffect(() => {
        onGetListContacts();
    }, []);

    const columns = [
        {
            title: t('email'),
            dataIndex: 'email',
            key: 'email',
            width: 300,
            render: (text) => <Tooltip title={text}>{text}</Tooltip>,
            sorter: (a, b) => a.email?.localeCompare(b.email),
        },
        {
            title: t('full_name'),
            dataIndex: 'fullName',
            key: 'fullName',
            width: 300,
            sorter: (a, b) => a.fullName?.localeCompare(b.fullName),
        },
        {
            title: t('phone'),
            dataIndex: 'phoneNumber',
            key: 'phoneNumber',
            width: 250,
            sorter: (a, b) => a.phoneNumber?.localeCompare(b.phoneNumber),
        },
        {
            title: t('content'),
            dataIndex: 'content',
            key: 'content',
            width: 300,
        },
        {
            title: t('active'),
            dataIndex: 'status',
            key: 'status',
            width: 200,
            render: (item, record) => (
                <div className="w-full flex justify-center hover:opacity-80">
                    <div className="relative">
                        <input
                            type="checkbox"
                            className="sr-only peer"
                            checked={item === GLOBAL_STATUS.ACTIVE}
                            onChange={() => {}}
                        />
                        <div className="w-11 h-6 bg-gray-300 rounded-full peer peer-focus:ring-4  dark:peer-focus:ring-main-color  peer-checked:after:translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-0.5 after:left-[2px] after:bg-white  after:border after:rounded-full after:h-5 after:w-5 after:transition-all peer-checked:bg-main-color"></div>
                        <div
                            onClick={() => refModalContactChangeStatus.current.onOpen(record.id, record)}
                            className="absolute pt-[16px] pb-[25px] pl-[24px] pr-[35px] top-[-10px] cursor-pointer"
                        ></div>
                    </div>
                </div>
            ),
            align: 'center',
            sorter: (a, b) => a.status - b.status,
        },
        {
            title: t('created_at'),
            dataIndex: 'createdAt',
            key: 'created_at',
            width: 200,
            render: (item) => moment(item).format('YYYY-MM-DD HH:mm:ss'),
            sorter: (a, b) => a.createdAt?.localeCompare(b.createdAt),
        },
        {
            title: t('action'),
            key: 'action',
            render: (item, record) => (
                <div className="p-4 flex items-center justify-center gap-3">
                    <Tooltip title={t('update')}>
                        <button
                            onClick={() => refModalContactDetailStatus.current.onOpen(record.id, record)}
                            className="px-3 mt-2 py-[6px] bg-main-color text-white rounded-2xl cursor-pointer hover:shadow-4xl hover:opacity-80"
                        >
                            <AiOutlineEdit />
                        </button>
                    </Tooltip>
                </div>
            ),
            fixed: 'right',
            align: 'center',
        },
    ];

    const handleTableChange = (pagination) => {
        setPage(pagination.current);
    };

    return (
        <>
            <div className="flex py-4 max-sm:flex-col">
                <h2 className="mb-3 text-[20px] md:text-[24px] lg:text-[30px] text-black font-medium">
                    {t('contact_info')}
                </h2>
            </div>
            <Table
                rowSelection={{
                    type: 'checkbox',
                }}
                dataSource={listContacts}
                rowKey="id"
                scroll={{ x: 'max-content' }}
                columns={columns}
                pagination={{
                    current: page,
                    position: ['bottomCenter'],
                }}
                loading={loading}
                onChange={handleTableChange}
            />
            <ModalContactChangeStatus
                ref={refModalContactChangeStatus}
                onAfterChangeStatus={() => {
                    onGetListContacts(getValues());
                }}
                account={account}
            />
            <ModalContactView
                ref={refModalContactDetailStatus}
                onAfterUpdate={() => {
                    onGetListContacts(getValues());
                }}
            />
        </>
    );
};

export default Contact;
