import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Controller, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { useTranslation } from 'react-i18next';
import { AiOutlineExclamationCircle } from 'react-icons/ai';
import { HiOutlineEye, HiOutlineEyeSlash } from 'react-icons/hi2';
import { FaUserCircle } from 'react-icons/fa';

import { changeTheme } from '@/store/slices/commonSlice';
import { setToken } from '@/store/slices/accountSlice';
import { STORAGE_KEY } from '@/constants/storage-key';
import { MODE_THEME } from '@/constants';
import Container from '@/components/container';
import successHelper from '@/utils/success-helper';
import errorHelper from '@/utils/error-helper';
import accountApis from '@/api/accountApis';
import axiosClient from '@/api/axiosClient';
import LocalStorage from '@/utils/storage';
import yup from '@/utils/yup';
import Logo from '@/resources/images/Logo.png';
import LogoText from '@/resources/images/logo_text.png';
import Loading from '@/components/loading';

const Login = () => {
    const { t } = useTranslation();
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const { theme } = useSelector((state) => state.common);
    const onChangeTheme = () => {
        const newTheme = theme === MODE_THEME.LIGHT ? MODE_THEME.DARK : MODE_THEME.LIGHT;
        dispatch(changeTheme(newTheme));
    };

    const schema = yup.object({
        email: yup
            .string()
            .email()
            .max(255)
            .required()
            // eslint-disable-next-line no-useless-escape
            .matches(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, t('validations:email'))
            .trim(),
        password: yup.string().min(6).max(30).required().trim(),
    });

    const {
        control,
        handleSubmit,
        formState: { errors },
    } = useForm({
        resolver: yupResolver(schema),
    });

    const [loading, setLoading] = useState(false);
    const [password, setPassword] = useState(true);

    const onSubmit = (values) => {
        setLoading(true);

        accountApis
            .login({
                email: values.email,
                password: values.password,
            })
            .then(({ user, token }) => {
                axiosClient.defaults.headers.common = {
                    Authorization: `Bearer ${token}`,
                };

                LocalStorage.set(STORAGE_KEY.INFO, user);
                LocalStorage.set(STORAGE_KEY.TOKEN, token);
                dispatch(setToken(token));
                successHelper(t('create_success'));
                navigate('/');
            })
            .catch((err) => {
                errorHelper(err);
            })
            .finally(() => {
                setLoading(false);
            });
    };

    return (
        <Container title="Administration Sanfont - Login">
            <div className="w-full">
                <div className="grid grid-cols-1 md:grid-cols-1 lg:grid-cols-5 min-h-screen">
                    <div className="col-span-1 lg:col-span-3 py-8 md:py-10 lg:py-11 px-3 md:px-12 lg:px-14 dark-container">
                        <div className="flex items-center justify-between mb-6">
                            <img src={Logo} alt="logo" className="w-[150px] md:w-[180px] lg:w-[200px]" />

                            <div className="flex items-center gap-2">
                                <label className="relative inline-flex items-center mr-5 cursor-pointer">
                                    <input
                                        type="checkbox"
                                        className="sr-only peer"
                                        checked={theme === MODE_THEME.LIGHT}
                                        onChange={onChangeTheme}
                                    />
                                    <div className="w-11 h-6 bg-[#212529] rounded-full peer peer-focus:ring-4  dark:peer-focus:ring-main-color  peer-checked:after:translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-0.5 after:left-[2px] after:bg-white  after:border after:rounded-full after:h-5 after:w-5 after:transition-all peer-checked:bg-main-color"></div>
                                    <h4 className="ml-3 text-sm font-medium text-gray-900">
                                        {theme === MODE_THEME.LIGHT ? t('light') : t('dark')}
                                    </h4>
                                </label>
                            </div>
                        </div>
                        <div className="flex items-center justify-center py-12 md:py-16 lg:py-20">
                            <div className="w-[50%] max-sm:w-full">
                                <div className="flex flex-col items-center">
                                    <h1 className="flex flex-col items-center gap-2 text-3xl mb-4">
                                        <FaUserCircle className="text-main-color bg-white rounded-full" />
                                        <strong>{t('login_now')}</strong>
                                    </h1>
                                    <h4 className="font-normal mb-10">{t('enter_your_credentials_to_login')}</h4>
                                </div>

                                <form onSubmit={handleSubmit(onSubmit)}>
                                    <div className="mb-3">
                                        <h4 className="mb-2 font-medium">{t('email')}</h4>
                                        <Controller
                                            name="email"
                                            control={control}
                                            render={({ field: { onChange, onBlur } }) => (
                                                <div className="relative w-full">
                                                    <input
                                                        onChange={onChange}
                                                        onBlur={onBlur}
                                                        className={`input w-full input-bordered focus:shadow !outline-none ${
                                                            errors?.email?.message && 'border-[1px] border-error'
                                                        }`}
                                                        placeholder={t('email')}
                                                    />
                                                    <p className="absolute right-0  bottom-[15px] px-5 cursor-pointer text-error">
                                                        {errors?.email?.message && <AiOutlineExclamationCircle />}
                                                    </p>
                                                </div>
                                            )}
                                        />
                                        {errors?.email?.message && (
                                            <p className="text-error normal-case mt-2">{errors?.email?.message}</p>
                                        )}
                                    </div>

                                    <div className="mb-3">
                                        <h4 className="mb-2 font-medium">{t('password')}</h4>
                                        <Controller
                                            name="password"
                                            control={control}
                                            render={({ field: { onChange, onBlur } }) => (
                                                <div className="relative w-full">
                                                    <input
                                                        type={password ? 'password' : 'text'}
                                                        className={`input w-full input-bordered focus:shadow !outline-none ${
                                                            errors?.password?.message && 'border-[1px] border-error'
                                                        }`}
                                                        placeholder={t('password')}
                                                        onChange={onChange}
                                                        onBlur={onBlur}
                                                    />
                                                    <p
                                                        className="absolute right-0 bottom-[15px] px-5 cursor-pointer text-slate-500"
                                                        onClick={() => setPassword(!password)}
                                                    >
                                                        {password ? <HiOutlineEyeSlash /> : <HiOutlineEye />}
                                                    </p>
                                                </div>
                                            )}
                                        />
                                        {errors?.password?.message && (
                                            <p className="text-error normal-case mt-2">{errors?.password?.message}</p>
                                        )}
                                    </div>

                                    <div className="flex flex-col mt-6">
                                        <button
                                            className="flex items-center justify-center bg-main-color border-[1px] border-main-color rounded-full shadow-box-shadow text-white font-medium py-3 "
                                            type="submit"
                                        >
                                            <span>{loading && <Loading Loading />}</span>
                                            <span>{t('login')}</span>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div className="lg:col-span-2 py-11 login-bg dark-page max-lg:hidden">
                        <div className="flex flex-col items-center justify-center">
                            <div className="flex flex-col items-center w-[60%]">
                                <h1 className="text-[32px] font-semibold text-white">Hệ sinh thái Xoo Network</h1>
                                <p className="text-white text-center mt-2">
                                    Chúng tôi mang đến cho bạn hệ sinh thái công nghệ và truyền thông đa dạng và chất
                                    lượng dịch vụ hàng đầu giúp bạn kinh doanh thành công.
                                </p>
                                <a href="http://localhost:3000/">
                                    <span className="text-white underline mt-2">Xem thêm tại đây</span>
                                </a>
                            </div>
                            <div className="mt-[15%] flex items-center justify-center">
                                <img className="w-[80%] h-[80%]" src={LogoText} alt="images" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Container>
    );
};

export default Login;
