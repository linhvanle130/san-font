import React, { useState, useImperativeHandle, forwardRef, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { yupResolver } from '@hookform/resolvers/yup';
import { Controller, useForm } from 'react-hook-form';
import { Modal, Input, Select } from 'antd';

import { convertToSlug } from '@/utils/funcs';
import yup from '@/utils/yup';
import errorHelper from '@/utils/error-helper';
import successHelper from '@/utils/success-helper';
import Loading from '@/components/loading';
import productCategoryTypeApis from '@/api/productCategoryTypeApis';
import productCategoryApis from '@/api/productCategoryApis';

const schema = yup.object({
    categoryId: yup.number().required(),
    name: yup.string().trim().required().max(255),
    categorySlug: yup.string().trim().required().max(255),
});

const ModalProductCategoryUpdate = ({ onSubmit, onAfterUpdate, account }, ref) => {
    const { t } = useTranslation();

    const { Option } = Select;
    const [listCategoryType, setListCategoryType] = useState([]);
    const getListCategoryType = async () => {
        const categoryId = await productCategoryTypeApis.getAllCategoryType();
        setListCategoryType(categoryId);
    };

    useEffect(() => {
        getListCategoryType();
    }, []);

    const {
        control,
        handleSubmit,
        formState: { errors },
        setValue,
        reset,
    } = useForm({
        resolver: yupResolver(schema),
    });

    const [visible, setVisible] = useState(false);
    const [loading, setLoading] = useState(false);
    const [idDataProductCategory, setIdDataProductCategory] = useState(null);
    const [errorProductCategory, setErrorProductCategory] = useState('');

    const handleSetSlug = (value) => {
        setValue('name', value, {
            shouldValidate: true,
            shouldDirty: true,
        });
        setValue('categorySlug', convertToSlug(value), {
            shouldValidate: true,
            shouldDirty: true,
        });
    };

    const onOpen = (id) => {
        setVisible(true);
        setLoading(true);
        setIdDataProductCategory(id);
        productCategoryApis
            .getDetailCategory(id)
            .then((res) => {
                const { categoryId, name, categorySlug } = res;
                setValue('categoryId', categoryId, {
                    shouldValidate: true,
                    shouldDirty: true,
                });
                setValue('name', name?.trim(), {
                    shouldValidate: true,
                    shouldDirty: true,
                });
                setValue('categorySlug', categorySlug?.trim(), {
                    shouldValidate: true,
                    shouldDirty: true,
                });
            })
            .catch((err) => {
                errorHelper(err.message);
            })
            .finally(() => setLoading(false));
    };

    useImperativeHandle(ref, () => ({
        onOpen,
    }));

    const onClose = () => {
        setVisible(false);
        setLoading(false);
        reset();
        setIdDataProductCategory(null);
        setErrorProductCategory('');
    };

    const submitUpdateProductCategory = (values) => {
        const { categoryId, name, categorySlug } = values;
        const payload = {
            id: idDataProductCategory,
            categoryId: categoryId,
            name: name.trim(),
            categorySlug: categorySlug?.trim(),
        };

        return productCategoryApis
            .updateCategory(payload)
            .then(() => {
                successHelper(t('update_success'));
                onClose();
                onAfterUpdate();
            })
            .catch((err) => {
                errorHelper(err);
            })
            .finally(() => setLoading(false));
    };

    const customStyle = {
        border: '1px solid red',
    };

    return (
        <Modal footer={null} open={visible} onCancel={onClose} maskClosable={false} destroyOnClose>
            <form onSubmit={handleSubmit(submitUpdateProductCategory)}>
                <h3 className="font-semibold text-black mb-3">
                    {account.role === 1 ? t('category_update') : t('admin_role')}
                </h3>

                <div className="flex flex-col gap-4">
                    <div className="mt-3">
                        <h4 className="mb-2">{t('select_a_category')}</h4>
                        <Controller
                            name="categoryId"
                            control={control}
                            render={({ field }) => (
                                <Select
                                    {...field}
                                    showSearch
                                    status={errors?.categoryId?.message}
                                    control={control}
                                    allowClear={true}
                                    name="categoryId"
                                    placeholder={t('select_a_category')}
                                    optionFilterProp="children"
                                    filterOption={(input, option) =>
                                        option.children.toLowerCase().includes(input.toLowerCase())
                                    }
                                    style={errors?.categoryId?.message && customStyle}
                                >
                                    {listCategoryType?.map((e) => {
                                        return (
                                            <Option value={e.id} key={e.id}>
                                                {e.name}
                                            </Option>
                                        );
                                    })}
                                </Select>
                            )}
                        />
                        {errors?.categoryId?.message && (
                            <p className="text-error mt-1">{errors?.categoryId?.message}</p>
                        )}
                    </div>

                    <div className="mb-3">
                        <h4 className="mb-1">{t('name')}</h4>
                        <Controller
                            name="name"
                            control={control}
                            render={({ field }) => (
                                <Input
                                    {...field}
                                    onChange={(event) => handleSetSlug(event.target.value)}
                                    status={errors?.name?.message ? 'error' : null}
                                    placeholder={t('name')}
                                    style={errors?.name?.message && customStyle}
                                />
                            )}
                        />
                        {errors?.name?.message && <p className="text-error mt-1">{errors?.name?.message}</p>}
                    </div>

                    <div className="mb-3">
                        <h4 className="mb-1">{t('slug')}</h4>
                        <Controller
                            name="categorySlug"
                            control={control}
                            render={({ field }) => (
                                <Input
                                    {...field}
                                    status={errors?.categorySlug?.message || errorProductCategory ? 'error' : null}
                                    placeholder={t('slug')}
                                    disabled
                                />
                            )}
                        />
                        {errors?.categorySlug?.message && (
                            <p className="text-error mt-1">{errors?.categorySlug?.message}</p>
                        )}
                        {errorProductCategory && <p className="text-error mt-1">{errorProductCategory}</p>}
                    </div>
                </div>

                <div className="w-full flex items-center justify-center">
                    {account.role === 1 && (
                        <button type="submit" className="flex bg-main-color py-2 px-4 hover:shadow-4xl">
                            <div>{loading && <Loading Loading />}</div>
                            <span className="text-white text-sm">{t('update')}</span>
                        </button>
                    )}
                </div>
            </form>
        </Modal>
    );
};

export default forwardRef(ModalProductCategoryUpdate);
