import React, { useEffect, useRef, useState } from 'react';
import { Table, Tooltip, Input, Space, Button } from 'antd';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm, Controller } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import { AiOutlineDelete, AiOutlineEdit } from 'react-icons/ai';
import { FiChevronDown, FiChevronRight } from 'react-icons/fi';
import { IoIosSearch } from 'react-icons/io';
import moment from 'moment';

import Loading from '@/components/loading';
import errorHelper from '@/utils/error-helper';
import yup from '@/utils/yup';
import { GLOBAL_STATUS } from '@/constants';
import productCategoryApis from '@/api/productCategoryApis';
import ModalProductCategoryChangeStatus from './modal-product-category-change-status';
import ModalProductCategoryCreate from './modal-product-category-create';
import ModalProductCategoryUpdate from './modal-product-category-update';
import ModalProductCategoryDelete from './modal-product-category-delete';

const LIMIT = 1000000;

const ProductCategory = () => {
    const { t } = useTranslation();
    const [search, setSearch] = useState(true);
    const [loading, setLoading] = useState(true);
    const account = JSON.parse(localStorage.getItem('info'));

    const refModalAccountDetail = useRef();
    const refModalProductCategoryCreate = useRef();
    const refModalProductCategoryUpdate = useRef();
    const refModalProductCategoryDelete = useRef();

    const [page, setPage] = useState(1);
    const searchInput = useRef(null);
    const [listProductCategory, setListProductCategory] = useState([]);
    console.log(listProductCategory);

    const schema = yup.object({
        search: yup.string().max(255),
    });

    const onSubmit = (values) => {
        const params = {
            name: values?.name,
            size: LIMIT,
        };
        setPage(1);
        return onGetListProductCategory(params);
    };

    const {
        control,
        handleSubmit,
        getValues,
        formState: { errors },
    } = useForm({
        resolver: yupResolver(schema),
    });

    const [searchText, setSearchText] = useState('');
    const [searchedColumn, setSearchedColumn] = useState('');

    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        setSearchText(selectedKeys[0]);
        setSearchedColumn(dataIndex);
    };

    const getColumnSearchProps = (dataIndex) => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm }) => (
            <div
                style={{
                    padding: 8,
                }}
            >
                <Input
                    ref={searchInput}
                    placeholder={`${t('search')} ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{
                        marginBottom: 8,
                        display: 'block',
                    }}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<IoIosSearch />}
                        size="small"
                        style={{
                            width: 100,
                            background: '#ff5e3a',
                        }}
                    >
                        {t('search')}
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: (filtered) => (
            <IoIosSearch
                style={{
                    color: filtered ? '#1890ff' : undefined,
                }}
            />
        ),
        onFilter: (value, record) => record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
        onFilterDropdownOpenChange: (visible) => {
            if (visible) {
                setTimeout(() => searchInput.current?.select(), 100);
            }
        },
        render: (text) => text,
    });

    const onGetListProductCategory = async (values) => {
        const params = {
            ...values,
            size: LIMIT,
        };

        setLoading(true);

        return productCategoryApis
            .getListCategoryWithPaging(params)
            .then(({ rows }) => {
                setListProductCategory(rows);

                window.scroll({
                    top: 0,
                    behavior: 'smooth',
                });
            })
            .catch((err) => {
                errorHelper(err);
            })
            .finally(() => {
                setLoading(false);
            });
    };

    useEffect(() => {
        onGetListProductCategory(getValues());
    }, []);

    const columns = [
        {
            title: t('name'),
            dataIndex: 'name',
            key: 'name',
            width: 500,
            ...getColumnSearchProps('name'),
            render: (text) => <Tooltip title={text}>{text}</Tooltip>,
            sorter: (a, b) => a.name?.localeCompare(b.name),
        },
        {
            title: t('slug'),
            dataIndex: 'categorySlug',
            key: 'categorySlug',
            width: 500,
            render: (text) => <Tooltip title={text}>{text}</Tooltip>,
            sorter: (a, b) => a.categorySlug?.localeCompare(b.categorySlug),
        },
        {
            title: t('active'),
            dataIndex: 'status',
            key: 'status',
            width: 300,
            render: (item, record) => (
                <div className="w-full flex justify-center hover:opacity-80">
                    <div className="relative">
                        <input
                            type="checkbox"
                            className="sr-only peer"
                            checked={item === GLOBAL_STATUS.ACTIVE}
                            onChange={() => {}}
                        />
                        <div className="w-11 h-6 bg-gray-300 rounded-full peer peer-focus:ring-4  dark:peer-focus:ring-main-color  peer-checked:after:translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-0.5 after:left-[2px] after:bg-white  after:border after:rounded-full after:h-5 after:w-5 after:transition-all peer-checked:bg-main-color"></div>
                        <div
                            onClick={() => refModalAccountDetail.current.onOpen(record.id, record)}
                            className="absolute pt-[16px] pb-[25px] pl-[24px] pr-[35px] top-[-10px] cursor-pointer"
                        ></div>
                    </div>
                </div>
            ),
            align: 'center',
            sorter: (a, b) => a.status - b.status,
        },
        {
            title: t('created_at'),
            dataIndex: 'createdAt',
            key: 'created_at',
            width: 300,
            render: (item) => moment(item).format('YYYY-MM-DD HH:mm:ss'),
            sorter: (a, b) => a.createdAt?.localeCompare(b.createdAt),
        },
        {
            title: t('action'),
            key: 'action',
            render: (item, record) => (
                <div className="p-4 flex items-center justify-center gap-3">
                    <Tooltip title={t('update')}>
                        <button
                            onClick={() => refModalProductCategoryUpdate.current.onOpen(record.id)}
                            className="px-3 mt-2 py-[6px] bg-main-color text-white rounded-2xl cursor-pointer hover:shadow-4xl hover:opacity-80"
                        >
                            <AiOutlineEdit />
                        </button>
                    </Tooltip>
                    {account.role === 1 && (
                        <Tooltip title={t('Delete')}>
                            <button
                                className=" px-3 mt-2 py-[6px] bg-red-500 text-white rounded-2xl cursor-pointer hover:shadow-4xl hover:opacity-80"
                                onClick={() => refModalProductCategoryDelete?.current?.onOpen(record.id, record)}
                            >
                                <AiOutlineDelete />
                            </button>
                        </Tooltip>
                    )}
                </div>
            ),
            align: 'center',
            fixed: 'right',
        },
    ];

    const renderSearch = () => (
        <div className="mb-4">
            <div className="bg-white rounded-sm shadow-3xl dark-container">
                <button
                    className="flex items-center px-4 py-3 cursor-pointer transition"
                    onClick={() => setSearch(!search)}
                >
                    {search ? <FiChevronDown /> : <FiChevronRight />}
                    <h3 className="pl-2 text-base font-bold">{t('search')}</h3>
                </button>
                {search && (
                    <div className="px-4 py-3">
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <div className="w-full flex gap-5 max-lg:flex-col max-lg:gap-3">
                                <div className="w-1/3 max-sm:w-full">
                                    <Controller
                                        name="name"
                                        control={control}
                                        render={({ field }) => (
                                            <Input {...field} type="text" placeholder={t('search_dot3')} />
                                        )}
                                    />
                                    {errors?.search?.message && (
                                        <p className="text-error mt-1">{errors?.search?.message}</p>
                                    )}
                                </div>
                            </div>

                            <div className="mt-5">
                                <button
                                    type="submit"
                                    className="flex items-center justify-center bg-main-color py-2 px-4  hover:shadow-4xl max-sm:w-[100px] max-sm:mt-0"
                                >
                                    <div> {loading && <Loading Loading />}</div>
                                    <span className="text-white text-sm">{t('search')}</span>
                                </button>
                            </div>
                        </form>
                    </div>
                )}
            </div>
        </div>
    );

    const handleTableChange = (pagination) => {
        setPage(pagination.current);
    };

    return (
        <div>
            {renderSearch()}
            <div className="flex justify-between py-4 max-sm:flex-col">
                <h2 className="mb-3 text-[20px] md:text-[24px] lg:text-[30px] text-black font-medium">
                    {t('product_category')}
                </h2>
                <div>
                    <button
                        onClick={() => refModalProductCategoryCreate.current.onOpen()}
                        className="bg-main-color py-2 px-4 hover:shadow-4xl max-sm:w-[60%] max-sm:float-right"
                    >
                        <span className="text-white text-sm">{t('category_add')}</span>
                    </button>
                </div>
            </div>
            <Table
                dataSource={listProductCategory}
                rowKey="id"
                columns={columns}
                pagination={{
                    current: page,
                    position: ['bottomCenter'],
                }}
                scroll={{ x: 'max-content' }}
                onChange={handleTableChange}
            />

            <ModalProductCategoryChangeStatus
                ref={refModalAccountDetail}
                onAfterChangeStatus={() => {
                    onGetListProductCategory(getValues());
                }}
                account={account}
            />

            <ModalProductCategoryCreate
                ref={refModalProductCategoryCreate}
                onSubmit={(payload) => productCategoryApis.createProductCategory(payload)}
                onAfterCreate={() => {
                    onGetListProductCategory(getValues());
                }}
                account={account}
            />
            <ModalProductCategoryUpdate
                ref={refModalProductCategoryUpdate}
                onAfterUpdate={() => {
                    onGetListProductCategory(getValues());
                }}
                account={account}
            />
            <ModalProductCategoryDelete
                ref={refModalProductCategoryDelete}
                onSubmit={(id) => productCategoryApis.deleteCategory(id)}
                onAfterDelete={() => {
                    onGetListProductCategory(getValues());
                }}
            />
        </div>
    );
};

export default ProductCategory;
