import React, { useEffect, useRef, useState } from 'react';
import { Table, Tooltip, Input, Space, Button, Select } from 'antd';
import { useTranslation } from 'react-i18next';
import { useForm, Controller } from 'react-hook-form';
import { Link, useNavigate } from 'react-router-dom';
import { yupResolver } from '@hookform/resolvers/yup';
import { IoIosSearch } from 'react-icons/io';
import { AiOutlineCheck, AiOutlineDelete, AiOutlineEdit } from 'react-icons/ai';
import { FiChevronDown, FiChevronRight } from 'react-icons/fi';
import moment from 'moment';

import { FONT_TYPE, GLOBAL_STATUS, IMAGE_TYPE } from '@/constants';
import Loading from '@/components/loading';
import errorHelper from '@/utils/error-helper';
import yup from '@/utils/yup';
import productCategoryApis from '@/api/productCategoryApis';
import productApis from '@/api/productApis';
import ModalProductDelete from './modal-product-delete';
import ModalProductChangeStatus from './modal-product-change-status';

const LIMIT = 1000000;
const Product = () => {
    const account = JSON.parse(localStorage.getItem('info'));
    const schema = yup.object({
        search: yup.string().max(255),
    });

    const { t } = useTranslation();
    const { Option } = Select;
    const navigate = useNavigate();
    const [search, setSearch] = useState(true);

    const searchInput = useRef(null);
    const refModalAccountDetail = useRef();
    const refModalProductDelete = useRef();

    const [page, setPage] = useState(1);
    const [loading, setLoading] = useState(true);
    const [listProducts, setListProducts] = useState([]);
    console.log(listProducts);

    const [listCategory, setListCategory] = useState([]);
    const getListCategory = async () => {
        const categoryId = await productCategoryApis.getAllCategory();
        setListCategory(categoryId);
    };

    const onSubmit = (values) => {
        const params = {
            categoryId: values?.categoryId,
            name: values?.name,
            size: LIMIT,
        };
        setPage(1);
        return onGetListProducts(params);
    };

    const {
        control,
        handleSubmit,
        getValues,
        setValue,
        formState: { errors },
    } = useForm({
        resolver: yupResolver(schema),
    });

    // Search for products
    const [searchText, setSearchText] = useState('');
    const [searchedColumn, setSearchedColumn] = useState('');

    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        setSearchText(selectedKeys[0]);
        setSearchedColumn(dataIndex);
    };

    const getColumnSearchProps = (dataIndex) => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm }) => (
            <div
                style={{
                    padding: 8,
                }}
            >
                <Input
                    ref={searchInput}
                    placeholder={`${t('search')} ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{
                        marginBottom: 8,
                        display: 'block',
                    }}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<IoIosSearch />}
                        size="small"
                        style={{
                            width: 100,
                            background: '#ff5e3a',
                        }}
                    >
                        {t('search')}
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: (filtered) => (
            <IoIosSearch
                style={{
                    color: filtered ? '#1890ff' : undefined,
                }}
            />
        ),
        onFilter: (value, record) => record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
        onFilterDropdownOpenChange: (visible) => {
            if (visible) {
                setTimeout(() => searchInput.current?.select(), 100);
            }
        },
        render: (text) => text,
    });

    const handleEditProduct = (id) => {
        navigate(`/product/update/${id}`);
    };

    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: t('image'),
            key: 'productImage',
            dataIndex: 'productImage',
            width: 200,
            align: 'center',
            render: (item) => (
                <div className="flex items-center justify-center">
                    <img width={100} src={item} alt="" />
                </div>
            ),
        },
        {
            title: t('name'),
            dataIndex: 'name',
            key: 'name',
            width: 300,
            ...getColumnSearchProps('name'),
            render: (text) => <Tooltip title={text}>{text}</Tooltip>,
            sorter: (a, b) => a.name?.localeCompare(b.name),
        },
        {
            title: t('product_category'),
            dataIndex: 'productCategoryName',
            key: 'productCategoryName',
            width: 300,
            sorter: (a, b) => a.productCategoryName?.localeCompare(b.productCategoryName),
        },
        {
            title: t('author'),
            dataIndex: 'author',
            key: 'author',
            width: 250,
            ...getColumnSearchProps('author'),
            sorter: (a, b) => a.author?.localeCompare(b.author),
        },
        {
            title: t('user_poster'),
            dataIndex: 'user',
            key: 'user',
            width: 250,
        },
        {
            title: t('vietnameseization'),
            dataIndex: 'subAuthor',
            key: 'subAuthor',
            width: 250,
            ...getColumnSearchProps('subAuthor'),
            sorter: (a, b) => a.subAuthor?.localeCompare(b.subAuthor),
        },
        {
            title: t('fonts_upload'),
            dataIndex: 'productFont',
            key: 'productFont',
            width: 300,
            align: 'center',
            render: (item, record) => (
                <>
                    <a href={item} download="">
                        <span className="p-2 bg-[#3e95cd] text-[#ffffff] hover:text-[#ffffff] hover:opacity-80 rounded-md">
                            {item ? 'Tải ngay' : 'Không có font '}
                        </span>
                    </a>
                </>
            ),
        },
        {
            title: t('fonts_favorite'),
            key: 'countProductSave',
            dataIndex: 'countProductSave',
            width: 200,
            align: 'center',
            sorter: (a, b) => a.countProductSave - b.countProductSave,
        },
        {
            title: t('fonts_download'),
            key: 'countDownloadFont',
            dataIndex: 'countDownloadFont',
            width: 200,
            align: 'center',
            sorter: (a, b) => a.countDownloadFont - b.countDownloadFont,
        },
        {
            title: t('outstanding'),
            dataIndex: 'outstanding',
            key: 'outstanding',
            width: 200,
            render: (item) => (item === 0 && 'Bình thường') || (item === 1 && 'VIP') || (item === 2 && 'Font chọn lọc'),
            sorter: (a, b) => a.outstanding - b.outstanding,
            align: 'center',
        },
        {
            title: t('description'),
            dataIndex: 'description',
            key: 'description',
            render: (text) => <Tooltip title={text}>{text}</Tooltip>,
            ellipsis: {
                showTitle: false,
            },
        },
        {
            title: t('active'),
            dataIndex: 'status',
            key: 'status',
            width: 200,
            render: (item, record) => (
                <div className="w-full flex justify-center hover:opacity-80">
                    <div className="relative">
                        <input
                            type="checkbox"
                            className="sr-only peer"
                            checked={item === GLOBAL_STATUS.ACTIVE}
                            onChange={() => {}}
                        />
                        <div className="w-11 h-6 bg-gray-300 rounded-full peer peer-focus:ring-4  dark:peer-focus:ring-main-color  peer-checked:after:translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-0.5 after:left-[2px] after:bg-white  after:border after:rounded-full after:h-5 after:w-5 after:transition-all peer-checked:bg-main-color"></div>
                        <div
                            onClick={() => refModalAccountDetail.current.onOpen(record.id, record)}
                            className="absolute pt-[16px] pb-[25px] pl-[24px] pr-[35px] top-[-10px] cursor-pointer"
                        ></div>
                    </div>
                </div>
            ),
            align: 'center',
            sorter: (a, b) => a.status - b.status,
        },
        {
            title: t('created_at'),
            dataIndex: 'createdAt',
            key: 'created_at',
            width: 200,
            render: (item) => moment(item).format('YYYY-MM-DD HH:mm:ss'),
            sorter: (a, b) => a.createdAt?.localeCompare(b.createdAt),
        },
        {
            title: t('action'),
            key: 'action',
            render: (item, record) => (
                <div className="p-4 flex items-center justify-center gap-3">
                    <Tooltip title={t('update')}>
                        <button
                            onClick={() => handleEditProduct(record.id)}
                            className="px-3 mt-2 py-[6px] bg-main-color text-white rounded-2xl cursor-pointer hover:shadow-4xl hover:opacity-80"
                        >
                            <AiOutlineEdit />
                        </button>
                    </Tooltip>
                    {account.role === 1 && (
                        <Tooltip title={t('Delete')}>
                            <button
                                className=" px-3 mt-2 py-[6px] bg-red-500 text-white rounded-2xl cursor-pointer hover:shadow-4xl hover:opacity-80"
                                onClick={() => refModalProductDelete?.current?.onOpen(record.id, record)}
                            >
                                <AiOutlineDelete />
                            </button>
                        </Tooltip>
                    )}
                </div>
            ),
            fixed: 'right',
            align: 'center',
        },
    ];

    const onGetListProducts = (values) => {
        const params = {
            ...values,
            admin: true,
            size: LIMIT,
        };

        setLoading(true);

        productApis
            .getListProduct(params)
            .then(({ rows }) => {
                setListProducts(
                    rows.map((e) => {
                        return {
                            ...e,
                            productCategoryName: e.productCategory.name,
                            productImage: e.productImage.find((e) => e.isMain === IMAGE_TYPE.MAIN)?.image,
                            productFont: e.productFont.find((e) => e.isMain == FONT_TYPE.MAIN)?.font,
                            countProductSave: e.productSave.length,
                            countDownloadFont: e.downloadFont.length,
                            user: e.user.username,
                        };
                    }),
                );
                window.scroll({
                    top: 0,
                    behavior: 'smooth',
                });
            })
            .catch((err) => {
                console.log(err);
                errorHelper(err);
            })
            .finally(() => {
                setLoading(false);
            });
    };

    useEffect(() => {
        onGetListProducts();
        getListCategory();
    }, []);

    const renderSearch = () => (
        <div className="mb-4">
            <div className="bg-white rounded-sm shadow-3xl dark-container">
                <button
                    className="flex items-center px-4 py-3 cursor-pointer transition"
                    onClick={() => setSearch(!search)}
                >
                    {search ? <FiChevronDown /> : <FiChevronRight />}
                    <h3 className="pl-2 text-base font-bold">{t('search')}</h3>
                </button>
                {search && (
                    <div className="px-4 py-3">
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <div className="w-full flex gap-5 max-lg:flex-col max-lg:gap-3">
                                <div className="w-1/3 max-sm:w-full">
                                    <Controller
                                        name="name"
                                        control={control}
                                        render={({ field }) => (
                                            <Input {...field} type="text" placeholder={t('search_dot3')} />
                                        )}
                                    />
                                    {errors?.search?.message && (
                                        <p className="text-error mt-1">{errors?.search?.message}</p>
                                    )}
                                </div>
                                <div className="w-1/4 max-sm:w-full">
                                    <Controller
                                        name="categoryId"
                                        control={control}
                                        render={({ field }) => (
                                            <Select
                                                {...field}
                                                showSearch
                                                status={errors?.categoryId?.message}
                                                control={control}
                                                allowClear={true}
                                                name="categoryId"
                                                placeholder={t('select_a_category')}
                                                optionFilterProp="children"
                                                filterOption={(input, option) =>
                                                    option.children.toLowerCase().includes(input.toLowerCase())
                                                }
                                            >
                                                {listCategory?.map((e) => {
                                                    return (
                                                        <Option value={e.id} key={e.id}>
                                                            {e.name}
                                                        </Option>
                                                    );
                                                })}
                                            </Select>
                                        )}
                                    />
                                </div>
                            </div>

                            <div className="mt-5">
                                <button
                                    type="submit"
                                    className="flex items-center justify-center bg-main-color py-2 px-4  hover:shadow-4xl max-sm:w-[100px] max-sm:mt-0"
                                >
                                    <div> {loading && <Loading Loading />}</div>
                                    <span className="text-white text-sm">{t('search')}</span>
                                </button>
                            </div>
                        </form>
                    </div>
                )}
            </div>
        </div>
    );

    const handleTableChange = (pagination) => {
        setPage(pagination.current);
    };

    return (
        <>
            {renderSearch()}
            <div className="flex justify-between py-4">
                <h2 className="mb-3 text-[20px] md:text-[24px] lg:text-[30px] text-black font-medium">
                    {t('product_management')}
                </h2>
                <Link to={'/product/create'}>
                    <button
                        type="submit"
                        className="bg-main-color py-2 px-4 hover:shadow-4xl max-sm:float-right hover:opacity-80 transition-all"
                    >
                        <span className="text-white text-sm">{t('product_add')}</span>
                    </button>
                </Link>
            </div>
            <Table
                rowSelection={{
                    type: 'checkbox',
                }}
                columns={columns}
                dataSource={listProducts}
                rowKey="id"
                scroll={{ x: 'max-content' }}
                pagination={{
                    current: page,
                    position: ['bottomCenter'],
                }}
                loading={loading}
                onChange={handleTableChange}
            />

            <ModalProductChangeStatus
                ref={refModalAccountDetail}
                onAfterChangeStatus={() => {
                    onGetListProducts(getValues());
                }}
                account={account}
            />
            <ModalProductDelete
                ref={refModalProductDelete}
                onSubmit={(id) => productApis.deleteProduct(id)}
                onAfterDelete={() => {
                    onGetListProducts(getValues());
                }}
            />
        </>
    );
};

export default Product;
