import React, { useState, useImperativeHandle, forwardRef, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { yupResolver } from '@hookform/resolvers/yup';
import { Controller, useForm } from 'react-hook-form';

import { convertToSlug } from '@/utils/funcs';
import yup from '@/utils/yup';
import errorHelper from '@/utils/error-helper';
import successHelper from '@/utils/success-helper';
import Loading from '@/components/loading';
import { Modal, Input, Select } from 'antd';
import productCategoryTypeApis from '@/api/productCategoryTypeApis';
import { AiOutlineCheck } from 'react-icons/ai';

const schema = yup.object({
    categoryId: yup.number().required(),
    name: yup.string().trim().required().max(255),
    categorySlug: yup.string().trim().required().max(255),
});

const ModalProductCategoryCreate = ({ onSubmit, onAfterCreate }, ref) => {
    const { t } = useTranslation();

    const {
        control,
        handleSubmit,
        formState: { errors },
        setValue,
        reset,
    } = useForm({
        resolver: yupResolver(schema),
    });

    const [visible, setVisible] = useState(false);
    const [loading, setLoading] = useState(false);
    const [errorProductCategory, setErrorProductCategory] = useState('');

    const onOpen = () => {
        setVisible(true);
    };

    useImperativeHandle(ref, () => ({
        onOpen,
    }));

    const onClose = () => {
        setVisible(false);
        reset({
            name: '',
            categorySlug: '',
        });
        setLoading(false);
        setErrorProductCategory('');
    };

    const submitCreate = (values) => {
        const { productId, nameDownload } = values;

        const payload = {
            productId: productId,
            nameDownload: nameDownload,
        };

        setLoading(true);

        return onSubmit(payload)
            .then(() => {
                successHelper(t('create_success'));
                navigate('/');
            })
            .catch((err) => {
                console.log(err);
                errorHelper(err);
            })
            .finally(() => {
                setLoading(false);
            });
    };

    const customStyle = {
        border: '1px solid red',
    };

    return (
        <Modal footer={null} open={visible} onCancel={onClose} maskClosable={false} destroyOnClose>
            <h3 className="font-semibold text-black mb-3"></h3>

            <form onSubmit={handleSubmit(submitCreate)}>
                <div className="mb-3">
                    <h4 className="mb-2">ID product</h4>
                    <Controller
                        name="productId"
                        control={control}
                        render={({ field }) => <Input {...field} placeholder="ID product" />}
                    />
                    {errors?.productId?.message && <p className="text-error mt-1">{errors?.productId?.message}</p>}
                </div>
                <div className="mb-3">
                    <h4 className="mb-2">name down font</h4>
                    <Controller
                        name="nameDownload"
                        control={control}
                        render={({ field }) => <Input {...field} placeholder="name down font" />}
                    />
                    {errors?.nameDownload?.message && (
                        <p className="text-error mt-1">{errors?.nameDownload?.message}</p>
                    )}
                </div>
                <button type="submit" className=" bg-main-color py-2 px-4 hover:shadow-4xl rounded-[2px]">
                    <div className="flex gap-2 items-center text-white">
                        <AiOutlineCheck />
                        <span className="text-sm">{t('submit')}</span>
                    </div>
                </button>
            </form>
        </Modal>
    );
};

export default forwardRef(ModalProductCategoryCreate);
