import React, { useState, useImperativeHandle, forwardRef } from 'react';
import { useTranslation } from 'react-i18next';
import { Modal } from 'antd';
import { GLOBAL_STATUS } from '@/constants';
import successHelper from '@/utils/success-helper';
import errorHelper from '@/utils/error-helper';
import Loading from '@/components/loading';
import productApis from '@/api/productApis';

const ModalProductChangeStatus = ({ onAfterChangeStatus, account }, ref) => {
    const { t } = useTranslation();
    const [visible, setVisible] = useState(false);
    const [loading, setLoading] = useState(false);
    const [idProduct, setIdProduct] = useState(null);
    const [statusProduct, setStatusProduct] = useState(null);

    const onOpen = (id, item) => {
        setVisible(true);
        setStatusProduct(item.status);
        setIdProduct(id);
    };

    useImperativeHandle(ref, () => ({
        onOpen,
    }));

    const onClose = () => {
        setVisible(false);
        setLoading(false);
        setIdProduct(null);
        setStatusProduct(null);
    };

    const submitProductChangeStatus = () => {
        setLoading(true);
        const body = {
            id: idProduct,
            status: statusProduct === GLOBAL_STATUS.ACTIVE ? GLOBAL_STATUS.INACTIVE : GLOBAL_STATUS.ACTIVE,
        };
        return productApis
            .changeStatusProduct(body)
            .then(() => {
                successHelper(t('update_success'));
                onClose();
                onAfterChangeStatus();
            })
            .catch((err) => {
                errorHelper(err);
            })
            .finally(() => setLoading(false));
    };

    return (
        <Modal footer={null} open={visible} onCancel={onClose} maskClosable={false} destroyOnClose>
            <h3 className="font-semibold text-black">{account.role === 1 ? t('product_change') : t('admin_role')}</h3>
            <div className="flex flex-col items-center mt-3">
                {statusProduct === GLOBAL_STATUS.ACTIVE && (
                    <h4>
                        {t('category_status_new')}: {t('off')}
                    </h4>
                )}
                {statusProduct === GLOBAL_STATUS.INACTIVE && (
                    <h4>
                        {t('category_status_new')}: {t('on')}
                    </h4>
                )}
            </div>
            <div className="mt-3 flex items-center justify-center gap-3">
                {account.role === 1 && (
                    <button
                        onClick={() => submitProductChangeStatus()}
                        className="flex px-3 py-2 border-[1px] border-red-500  bg-red-500 text-white cursor-pointer rounded-sm hover:bg-red-400"
                    >
                        <span>{loading && <Loading Loading />}</span>
                        <span>{t('confirm')}</span>
                    </button>
                )}
                <button
                    onClick={() => onClose()}
                    className="flex px-3 py-2 border-[1px] bg-white border-[#ccc] cursor-pointer  rounded-sm hover:text-red-500 hover:border-red-500"
                >
                    <div>{loading && <Loading Loading />}</div>
                    <span className="text-black"> {t('close')}</span>
                </button>
            </div>
        </Modal>
    );
};

export default forwardRef(ModalProductChangeStatus);
