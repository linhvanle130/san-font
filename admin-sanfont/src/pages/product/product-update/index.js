import React, { useEffect, useRef, useState } from 'react';
import { AiOutlineArrowLeft, AiOutlineCheck, AiOutlineReload } from 'react-icons/ai';
import { MdOutlineUploadFile } from 'react-icons/md';
import { CiCamera } from 'react-icons/ci';
import { Controller, useForm, useWatch } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { yupResolver } from '@hookform/resolvers/yup';
import { Image, Spin, Input, Radio, Select } from 'antd';
import Compressor from 'compressorjs';

import { FONT_TYPE, IMAGE_TYPE } from '@/constants';
import { convertToSlug } from '@/utils/funcs';
import successHelper from '@/utils/success-helper';
import errorHelper from '@/utils/error-helper';
import yup from '@/utils/yup';
import Loading from '@/components/loading';
import productCategoryApis from '@/api/productCategoryApis';
import productApis from '@/api/productApis';
import commonApis from '@/api/commonApis';
import userApis from '@/api/userApis';
import upFontApis from '@/api/upFontApi';

const REMOVED = 'removed';
const ProductUpdate = () => {
    const { t } = useTranslation();
    const schema = yup.object({
        categoryId: yup.number().required(),
        userId: yup.number().required(),
        name: yup.string().max(255).required(),
        slug: yup.string().max(255).nullable(),
        author: yup.string().max(255).required().nullable(),
        subAuthor: yup.string().max(255).required().nullable(),
        description: yup.string().trim().nullable(),
        thumbnail: yup.string().max(255).required().nullable(),
        outstanding: yup.number().max(2).required().nullable(),
        font: yup.string().max(255).required().nullable(),
    });

    const {
        control,
        handleSubmit,
        formState: { errors },
        setValue,
    } = useForm({
        resolver: yupResolver(schema),
    });

    const { id } = useParams();
    const { Option } = Select;
    const navigate = useNavigate();
    const [loading, setLoading] = useState(false);
    const account = JSON.parse(localStorage.getItem('info'));

    const [listCategory, setListCategory] = useState([]);
    const [listUsers, setListUsers] = useState([]);

    const getListCategory = async () => {
        const category = await productCategoryApis.getAllCategory();
        setListCategory(category);
    };

    const getListUser = async () => {
        const users = await userApis.getListUsers();
        setListUsers(users);
    };

    const onChange_Radio = (e) => {
        setValue(e.target.value);
    };

    const submitUpdate = (values) => {
        const { categoryId, userId, name, slug, author, subAuthor, description, outstanding, thumbnail, font } = values;

        const payload = {
            id: +id,
            categoryId: categoryId,
            userId: userId,
            name: name,
            productSlug: slug,
            author: author,
            subAuthor: subAuthor,
            description: description?.trim(),
            outstanding: outstanding,
            mainImage: thumbnail,
            mainFont: font,
        };
        return productApis
            .updateProduct(payload)
            .then(() => {
                successHelper(t('update_success'));
                navigate('/product');
            })
            .catch((err) => {
                console.log(err);
                errorHelper(err);
            })
            .finally(() => {
                setLoading(false);
            });
    };

    useEffect(() => {
        setValue('outstanding', 0);
        getListCategory();
        getListUser();
        productApis
            .getDetailProduct(id)
            .then((res) => {
                setValue('categoryId', res.categoryId, {
                    shouldValidate: true,
                    shouldDirty: true,
                });
                setValue('userId', res.userId, {
                    shouldValidate: true,
                    shouldDirty: true,
                });
                setValue('name', res.name, {
                    shouldValidate: true,
                    shouldDirty: true,
                });
                setValue('slug', res.productSlug, {
                    shouldValidate: true,
                    shouldDirty: true,
                });
                setValue('author', res.author, {
                    shouldValidate: true,
                    shouldDirty: true,
                });
                setValue('subAuthor', res.subAuthor, {
                    shouldValidate: true,
                    shouldDirty: true,
                });
                setValue('description', res.description?.trim(), {
                    shouldValidate: true,
                    shouldDirty: true,
                });
                setValue('outstanding', res.outstanding, {
                    shouldValidate: true,
                    shouldDirty: true,
                });
                setValue('thumbnail', res.productImage.find((e) => e.isMain === IMAGE_TYPE.MAIN)?.image, {
                    shouldValidate: true,
                    shouldDirty: true,
                });
                setValue('font', res.productFont.find((e) => e.isMain === FONT_TYPE.MAIN)?.font, {
                    shouldValidate: true,
                    shouldDirty: true,
                });
            })
            .catch((err) => {
                console.log(err);
                errorHelper(err.message);
            })
            .finally(() => setLoading(false));
    }, []);

    const handleSetSlug = (value) => {
        setValue('name', value, {
            shouldValidate: true,
            shouldDirty: true,
        });
        setValue('slug', convertToSlug(value), {
            shouldValidate: true,
            shouldDirty: true,
        });
    };

    // main thumbnail
    const [isUpdateThumbnail, setIsUpdateThumbnail] = useState(false);
    const { thumbnail } = useWatch({ control });
    const refThumbnail = useRef();
    const onchangeThumbnail = (e) => {
        const files = e.target.files;
        if (files.length > 0) {
            new Compressor(files[0], {
                quality: 0.8,
                success: (compressedImg) => {
                    // compressedResult has the compressed file.
                    // Use the compressed file to upload the images to your server.
                    onUploadThumbnail(compressedImg)
                        .then((response) => {
                            setValue('thumbnail', response, {
                                shouldValidate: true,
                                shouldDirty: true,
                            });
                        })
                        .finally(() => {
                            setIsUpdateThumbnail(false);
                        });
                },
            });
        }
        e.target.value = null;
    };

    const onUploadThumbnail = async (file) => {
        setIsUpdateThumbnail(true);
        const formData = new FormData();
        formData.append('file', file);
        formData.append('fileName', file.name);
        return commonApis
            .preUploadFile(formData)
            .then(async (response) => {
                return response;
            })
            .catch((err) => {
                errorHelper(err);
            });
    };

    // main font
    const [isUpdateFont, setIsUpdateFont] = useState(false);
    const { font } = useWatch({ control });
    const refFont = useRef();
    const onchangeFont = (e) => {
        const files = e.target.files;
        if (files.length > 0) {
            new onUploadFont(files[0])
                .then((response) => {
                    setValue('font', response, {
                        shouldValidate: true,
                        shouldDirty: true,
                    });
                })
                .finally(() => {
                    setIsUpdateFont(false);
                });
        }
        e.target.value = null;
    };

    const onUploadFont = async (file) => {
        setIsUpdateFont(true);
        const formData = new FormData();
        formData.append('file', file);
        formData.append('fileName', file.name);
        return upFontApis
            .preUploadFile(formData)
            .then(async (response) => {
                return response;
            })
            .catch((err) => {
                errorHelper(err);
            });
    };

    const goBackListProduct = () => {
        navigate(`/product`);
    };

    const customStyle = {
        border: '1px solid red',
    };

    return (
        <div className="w-full max-sm:px-0 max-sm:py-2">
            <form onSubmit={handleSubmit(submitUpdate)}>
                <div className="flex justify-between max-lg:pr-3">
                    <div className="flex items-center">
                        <div onClick={() => goBackListProduct()}>
                            <AiOutlineArrowLeft className="mr-4 text-black hover:text-main-color text-lg cursor-pointer" />
                        </div>
                        <h2 className="mr-3 text-[20px] md:text-[24px] lg:text-[30px] text-black font-medium">
                            {t('product_update')}
                        </h2>
                    </div>
                    <div className="flex gap-2 max-sm:hidden">
                        {account.role === 1 && (
                            <button type="submit" className=" bg-main-color py-2 px-4 hover:shadow-4xl rounded-[2px]">
                                <div className="flex gap-2 items-center text-white">
                                    <div>{loading && <Loading Loading />}</div>
                                    <AiOutlineCheck />
                                    <span className="text-sm">{t('update')}</span>
                                </div>
                            </button>
                        )}
                        <button
                            onClick={() => {
                                goBackListProduct();
                            }}
                            className="bg-white py-2 px-4 border-[1px] border-[#ccc] rounded-[2px] hover:border-main-color hover:text-main-color"
                        >
                            <div className="flex gap-2 items-center">
                                <AiOutlineReload className="text-black" />
                                <span className="text-sm text-black">{t('back_list')}</span>
                            </div>
                        </button>
                    </div>
                </div>

                <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 mt-5 mb-[50px] md:mb-0 lg:mb-0">
                    <div className="md:col-span-1 lg:col-span-2 px-0 md:px-1 lg:px-3">
                        <div className="bg-white rounded dark-container">
                            <div className="p-3 md:p-4 lg:p-6 flex items-center border-b-[1px] border-b-[#ccc]">
                                <h3 className="font-medium text-base">{t('product_name')}</h3>
                                <div className="text-main-color">&nbsp;&nbsp;*</div>
                            </div>
                            <div className="flex flex-col gap-2 p-3 md:p-4 lg:p-6">
                                <div className="mb-3">
                                    <h4 className="mb-2">{t('product_name')}</h4>
                                    <Controller
                                        name="name"
                                        control={control}
                                        render={({ field }) => (
                                            <Input
                                                {...field}
                                                placeholder={t('product_name')}
                                                onChange={(event) => handleSetSlug(event.target.value)}
                                                status={errors?.name?.message}
                                                style={errors?.name?.message && customStyle}
                                            />
                                        )}
                                    />
                                    {errors?.name?.message && (
                                        <p className="text-error mt-1">{errors?.name?.message}</p>
                                    )}
                                </div>
                                <div className="mb-3">
                                    <h4 className="mb-1">{t('slug')}</h4>
                                    <Controller
                                        name="slug"
                                        control={control}
                                        render={({ field }) => (
                                            <Input
                                                {...field}
                                                placeholder={t('slug')}
                                                status={errors?.slug?.message}
                                                style={errors?.slug?.message && customStyle}
                                                disabled
                                            />
                                        )}
                                    />
                                    {errors?.slug?.message && (
                                        <p className="text-error mt-1">{errors?.slug?.message}</p>
                                    )}
                                </div>
                                <div className="mb-3">
                                    <h4 className="mb-1">{t('description')}</h4>
                                    <Controller
                                        name="description"
                                        control={control}
                                        render={({ field }) => (
                                            <Input.TextArea
                                                {...field}
                                                rows={6}
                                                status={errors?.description?.message ? 'error' : null}
                                                placeholder={t('description')}
                                                style={errors?.description?.message && customStyle}
                                            />
                                        )}
                                    />
                                    {errors?.description?.message && (
                                        <p className="text-error mt-1">{errors?.description?.message}</p>
                                    )}
                                </div>
                            </div>
                        </div>
                        <div className="bg-white mt-3 rounded max-sm:mr-0 dark-container">
                            <div className="p-3 md:p-4 lg:p-6 flex items-center border-b-[1px] border-b-[#ccc]">
                                <h3 className="font-medium text-base">{t('info_user_poster')}</h3>
                                <div className="text-main-color">&nbsp;&nbsp;*</div>
                            </div>
                            <div className="flex flex-col gap-2 p-3 md:p-4 lg:p-6">
                                <div className="mb-3">
                                    <h4 className="mb-2">{t('author')}</h4>
                                    <Controller
                                        name="author"
                                        control={control}
                                        render={({ field }) => (
                                            <Input
                                                {...field}
                                                placeholder={t('author')}
                                                status={errors?.author?.message}
                                                style={errors?.author?.message && customStyle}
                                            />
                                        )}
                                    />
                                    {errors?.author?.message && (
                                        <p className="text-error mt-1">{errors?.author?.message}</p>
                                    )}
                                </div>
                                <div className="mb-3">
                                    <h4 className="mb-2">{t('user_poster')}</h4>
                                    <Controller
                                        name="userId"
                                        control={control}
                                        render={({ field }) => (
                                            <Select
                                                {...field}
                                                showSearch
                                                status={errors?.userId?.message}
                                                control={control}
                                                allowClear={true}
                                                name="userId"
                                                placeholder={t('select_a_category')}
                                                optionFilterProp="children"
                                                filterOption={(input, option) =>
                                                    option.children.toLowerCase().includes(input.toLowerCase())
                                                }
                                                style={errors?.userId?.message && customStyle}
                                            >
                                                {listUsers?.rows?.map((e) => {
                                                    return (
                                                        <Option value={e.id} key={e.id}>
                                                            {e.username}
                                                        </Option>
                                                    );
                                                })}
                                            </Select>
                                        )}
                                    />
                                    {errors?.userId?.message && (
                                        <p className="text-error mt-1">{errors?.userId?.message}</p>
                                    )}
                                </div>
                                <div className="mb-3">
                                    <h4 className="mb-2">{t('vietnameseization')}</h4>
                                    <Controller
                                        name="subAuthor"
                                        control={control}
                                        render={({ field }) => (
                                            <Input
                                                {...field}
                                                placeholder={t('vietnameseization')}
                                                status={errors?.subAuthor?.message}
                                                style={errors?.subAuthor?.message && customStyle}
                                            />
                                        )}
                                    />
                                    {errors?.subAuthor?.message && (
                                        <p className="text-error mt-1">{errors?.subAuthor?.message}</p>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="md:col-span-1 lg:col-span-1 px-0 md:px-1 lg:px-3">
                        <div className="bg-white rounded mt-3 md:mt-0 lg:mt-0 dark-container">
                            <div className="p-3 md:p-4 lg:p-6 flex items-center border-b-[1px] border-b-[#ccc]">
                                <h3 className="font-medium text-base">{t('product_category')}</h3>
                                <div className="text-main-color">&nbsp;&nbsp;*</div>
                            </div>
                            <div className="p-3 md:p-4 lg:p-6">
                                <div className="mt-3">
                                    <h4 className="mb-2">{t('select_a_category')}</h4>
                                    <Controller
                                        name="categoryId"
                                        control={control}
                                        render={({ field }) => (
                                            <Select
                                                {...field}
                                                showSearch
                                                status={errors?.categoryId?.message}
                                                control={control}
                                                allowClear={true}
                                                name="categoryId"
                                                placeholder={t('select_a_category')}
                                                optionFilterProp="children"
                                                filterOption={(input, option) =>
                                                    option.children.toLowerCase().includes(input.toLowerCase())
                                                }
                                                style={errors?.categoryId?.message && customStyle}
                                            >
                                                {listCategory?.map((e) => {
                                                    return (
                                                        <Option value={e.id} key={e.id}>
                                                            {e.name}
                                                        </Option>
                                                    );
                                                })}
                                            </Select>
                                        )}
                                    />
                                    {errors?.categoryId?.message && (
                                        <p className="text-error mt-1">{errors?.categoryId?.message}</p>
                                    )}
                                </div>
                            </div>
                        </div>
                        <div className="bg-white rounded mt-3 dark-container">
                            <div className="p-3 md:p-4 lg:p-6 flex items-center border-b-[1px] border-b-[#ccc]">
                                <h3 className="font-medium text-base">{t('shows')}</h3>
                                <div className="text-main-color">&nbsp;&nbsp;*</div>
                            </div>
                            <div className="flex flex-col gap-2 p-3 md:p-4 lg:p-6">
                                <div className="mb-3">
                                    <h4 className="mb-2">{t('fonts_upload')}</h4>
                                    <p className="dart-text">{t('fonts_format')}</p>
                                    <div className="my-5">
                                        <div
                                            className={`w-full md:w-full lg:w-[60%] border-[1px] border-dashed ${
                                                errors?.font?.message ? 'border-error' : 'border-[#ccc]'
                                            }`}
                                        >
                                            <div className="font-upload">
                                                <div className="dart-text">
                                                    {font?.slice(30) || t('fonts_no_files_selected')}
                                                </div>
                                                <button
                                                    type={'button'}
                                                    disabled={isUpdateFont}
                                                    onClick={() => refFont.current.click()}
                                                    className="button-camera tooltip tooltip-left"
                                                    data-tip={t('click_upload_font')}
                                                >
                                                    {isUpdateFont ? <Spin /> : <MdOutlineUploadFile />}
                                                    <input
                                                        ref={refFont}
                                                        type="file"
                                                        accept="*/*"
                                                        onChange={onchangeFont}
                                                        style={{ display: 'none' }}
                                                    />
                                                </button>
                                            </div>
                                        </div>
                                        {errors?.font?.message && (
                                            <p className="text-error mt-1">{errors?.font?.message}</p>
                                        )}
                                    </div>
                                </div>
                                <div className="mb-3">
                                    <h4 className="mb-2">{t('featured')}</h4>
                                    <Controller
                                        name="outstanding"
                                        control={control}
                                        render={({ field }) => (
                                            <Radio.Group onChange={onChange_Radio} {...field}>
                                                <Radio value={0}>{t('normal')}</Radio>
                                                <Radio value={1}>{t('vip')}</Radio>
                                                <Radio value={2}>{t('selected_fonts')}</Radio>
                                            </Radio.Group>
                                        )}
                                    />
                                    {errors?.outstanding?.message && (
                                        <p className="text-error mt-1">{errors?.outstanding?.message}</p>
                                    )}
                                </div>
                            </div>
                        </div>
                        <div className="bg-white rounded mt-3 dark-container">
                            <div className="p-3 md:p-4 lg:p-6 flex items-center border-b-[1px] border-b-[#ccc]">
                                <h3 className="font-medium text-base">{t('thumbnail')}</h3>
                                <div className="text-main-color">&nbsp;&nbsp;*</div>
                            </div>
                            <div className="flex flex-col items-center justify-center">
                                <div className="my-5">
                                    <div
                                        className={`border-[1px] border-dashed ${
                                            errors?.font?.message ? 'border-error' : 'border-[#ccc]'
                                        }`}
                                    >
                                        <div className="thumbnail-upload">
                                            <Image
                                                className="img-thumbnail"
                                                src={
                                                    thumbnail ||
                                                    'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg=='
                                                }
                                                alt=""
                                            />
                                            <button
                                                type={'button'}
                                                disabled={isUpdateThumbnail}
                                                onClick={() => refThumbnail.current.click()}
                                                className="button-camera tooltip tooltip-left"
                                                data-tip={t('thumbnail_action_note')}
                                            >
                                                {isUpdateThumbnail ? <Spin /> : <CiCamera />}
                                                <input
                                                    ref={refThumbnail}
                                                    type="file"
                                                    accept="image/*"
                                                    onChange={onchangeThumbnail}
                                                    style={{ display: 'none' }}
                                                />
                                            </button>
                                        </div>
                                    </div>
                                    <div className="flex justify-center">
                                        {errors?.thumbnail?.message && (
                                            <p className="text-error mt-1">{errors?.thumbnail?.message}</p>
                                        )}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="flex justify-end p-3 max-sm:fixed max-sm:bottom-0 max-sm:left-0 max-sm:right-0 max-sm:justify-center max-sm:bg-white z-50 rounded ">
                    <div className="flex gap-2">
                        {account.role === 1 && (
                            <button type="submit" className=" bg-main-color py-2 px-4 hover:shadow-4xl rounded-[2px]">
                                <div className="flex gap-2 items-center text-white">
                                    <div>{loading && <Loading Loading />}</div>
                                    <AiOutlineCheck />
                                    <span className="text-sm">{t('update')}</span>
                                </div>
                            </button>
                        )}
                        <button
                            onClick={() => {
                                goBackListProduct();
                            }}
                            className="bg-white py-2 px-4 border-[1px] border-[#ccc] rounded-[2px] hover:border-main-color hover:text-main-color"
                        >
                            <div className="flex gap-2 items-center">
                                <AiOutlineReload className="text-black" />
                                <span className="text-sm text-black">{t('back_list')}</span>
                            </div>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    );
};

export default ProductUpdate;
