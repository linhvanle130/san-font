import React, { useEffect, useRef, useState } from 'react';
import { Tooltip, Table, Button, Space, Input } from 'antd';
import { useForm } from 'react-hook-form';
import { IoIosSearch } from 'react-icons/io';
import { AiOutlineDelete, AiOutlineEdit } from 'react-icons/ai';
import { useTranslation } from 'react-i18next';
import moment from 'moment';

import { IMAGE_TYPE } from '@/constants';
import errorHelper from '@/utils/error-helper';
import productSaveApis from '@/api/productSaveApis';
import userApis from '@/api/userApis';
import ModalProductSaveDelete from './modal-product-save-delete';
import ModalProductSaveUpdate from './modal-product-save-update';

const ProductSave = () => {
    const { t } = useTranslation();
    const [loading, setLoading] = useState(true);
    const [page, setPage] = useState(1);
    const account = JSON.parse(localStorage.getItem('info'));

    const { getValues } = useForm();

    const searchInput = useRef(null);
    const refModalProductSaveDelete = useRef();
    const refModalProductSaveUpdate = useRef();

    const [searchText, setSearchText] = useState('');
    const [searchedColumn, setSearchedColumn] = useState('');

    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        setSearchText(selectedKeys[0]);
        setSearchedColumn(dataIndex);
    };

    const getColumnSearchProps = (dataIndex) => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm }) => (
            <div
                style={{
                    padding: 8,
                }}
            >
                <Input
                    ref={searchInput}
                    placeholder={`${t('search')} ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{
                        marginBottom: 8,
                        display: 'block',
                    }}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<IoIosSearch />}
                        size="small"
                        style={{
                            width: 100,
                            background: '#ff5e3a',
                        }}
                    >
                        {t('search')}
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: (filtered) => (
            <IoIosSearch
                style={{
                    color: filtered ? '#1890ff' : undefined,
                }}
            />
        ),
        onFilter: (value, record) => record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
        onFilterDropdownOpenChange: (visible) => {
            if (visible) {
                setTimeout(() => searchInput.current?.select(), 100);
            }
        },
        render: (text) => text,
    });

    const [listUserProductSave, setListUserProductSave] = useState([]);
    const [listUserProductSaveType, setListUserProductSaveType] = useState([]);
    console.log(listUserProductSaveType);

    const expandedRowRender = (row) => {
        const productSave = listUserProductSave
            .filter((ft) => ft.userId === row.id)
            .map((e) => {
                return {
                    id: e.id,
                    name: e.product?.name,
                    productImage: e.product?.productImage.find((e) => e.isMain === IMAGE_TYPE.MAIN)?.image,
                    productOutstanding: e.product?.outstanding,
                    productCategoryName: e.product?.productCategory?.name,
                };
            });
        const columns = [
            {
                title: t('image'),
                key: 'productImage',
                dataIndex: 'productImage',
                width: 200,
                align: 'center',
                render: (item) => (
                    <div className="flex items-center justify-center">
                        <img width={100} src={item} alt="" />
                    </div>
                ),
            },
            {
                title: t('product_save_name'),
                dataIndex: 'name',
                key: 'name',
                width: 300,
                render: (text) => <Tooltip title={text}>{text}</Tooltip>,
                sorter: (a, b) => a.name?.localeCompare(b.name),
            },
            {
                title: t('product_category'),
                dataIndex: 'productCategoryName',
                key: 'productCategoryName',
                width: 300,
                sorter: (a, b) => a.productCategoryName?.localeCompare(b.productCategoryName),
            },
            {
                title: t('outstanding'),
                dataIndex: 'productOutstanding',
                key: 'productOutstanding',
                width: 200,
                render: (item) =>
                    (item === 0 && 'Bình thường') || (item === 1 && 'VIP') || (item === 2 && 'Font chọn lọc'),
                sorter: (a, b) => a.productOutstanding - b.productOutstanding,
                align: 'center',
            },
            {
                title: t('created_at'),
                dataIndex: 'createdAt',
                key: 'created_at',
                width: 300,
                render: (item) => moment(item).format('YYYY-MM-DD HH:mm:ss'),
                sorter: (a, b) => a.createdAt?.localeCompare(b.createdAt),
            },
            {
                title: t('action'),
                key: 'action',
                render: (item, record) => (
                    <div className="p-4 flex items-center justify-center gap-3">
                        <Tooltip title={t('update')}>
                            <button
                                onClick={() => refModalProductSaveUpdate.current.onOpen(record.id)}
                                className="px-3 mt-2 py-[6px] bg-main-color text-white rounded-2xl cursor-pointer hover:shadow-4xl hover:opacity-80"
                            >
                                <AiOutlineEdit />
                            </button>
                        </Tooltip>

                        {account.role === 1 && (
                            <Tooltip title={t('Delete')}>
                                <button
                                    className=" px-3 mt-2 py-[6px] bg-red-500 text-white rounded-2xl cursor-pointer hover:shadow-4xl hover:opacity-80"
                                    onClick={() => refModalProductSaveDelete?.current?.onOpen(record.id, record)}
                                >
                                    <AiOutlineDelete />
                                </button>
                            </Tooltip>
                        )}
                    </div>
                ),
                align: 'center',
                fixed: 'right',
                width: 200,
            },
        ];

        return (
            <Table
                rowSelection={{
                    type: 'checkbox',
                }}
                columns={columns}
                dataSource={productSave}
                pagination={false}
                rowKey="id"
                size="small"
            />
        );
    };

    const onGetListUserProductSave = (values) => {
        const params = {
            ...values,
            admin: true,
        };

        setLoading(true);

        userApis
            .getTopUserProductSave(params)
            .then(({ rows }) => {
                const productSaveDetail = [];
                setListUserProductSaveType(
                    rows.map((e) => {
                        e.productSave.map((a) => productSaveDetail.push(a));
                        return {
                            ...e,
                            countProductSave: e.productSave?.length,
                        };
                    }),
                );
                setListUserProductSave(productSaveDetail);

                window.scroll({
                    top: 0,
                    behavior: 'smooth',
                });
            })
            .catch((err) => {
                console.log(err);
                errorHelper(err);
            })
            .finally(() => {
                setLoading(false);
            });
    };

    useEffect(() => {
        onGetListUserProductSave();
    }, []);

    const columns = [
        {
            title: t('user_save'),
            dataIndex: 'username',
            key: 'username',
            width: 300,
            render: (text) => <Tooltip title={text}>{text}</Tooltip>,
            sorter: (a, b) => a.username?.localeCompare(b.username),
            ...getColumnSearchProps('username'),
        },
        {
            title: t('email'),
            dataIndex: 'email',
            key: 'email',
            width: 300,
            sorter: (a, b) => a.email?.localeCompare(b.email),
            ...getColumnSearchProps('email'),
        },
        {
            title: t('quantity'),
            dataIndex: 'countProductSave',
            key: 'countProductSave',
            width: 300,
            render: (item, record) => <Button className="bg-green-300 font-semibold py-1 px-2">{item}</Button>,
            sorter: (a, b) => a.countProductSave - b.countProductSave,
            align: 'center',
        },
    ];

    const handleTableChange = (pagination) => {
        setPage(pagination.current);
    };

    return (
        <div>
            <div className="flex py-4 max-sm:flex-col">
                <h2 className="mb-3 text-[20px] md:text-[24px] lg:text-[30px] text-black font-medium">
                    {t('product_save_list_user')}
                </h2>
            </div>
            <Table
                dataSource={listUserProductSaveType}
                rowKey="id"
                columns={columns}
                expandable={{
                    expandedRowRender,
                    expandRowByClick: true,
                }}
                pagination={{
                    current: page,
                    position: ['bottomCenter'],
                }}
                scroll={{ x: 'max-content' }}
                onChange={handleTableChange}
                loading={loading}
            />
            <ModalProductSaveDelete
                ref={refModalProductSaveDelete}
                onSubmit={(id) => productSaveApis.deleteProductSave(id)}
                onAfterDelete={() => {
                    onGetListUserProductSave(getValues());
                }}
            />
            <ModalProductSaveUpdate
                ref={refModalProductSaveUpdate}
                onAfterUpdate={() => {
                    onGetListUserProductSave(getValues());
                }}
                account={account}
            />
        </div>
    );
};

export default ProductSave;
