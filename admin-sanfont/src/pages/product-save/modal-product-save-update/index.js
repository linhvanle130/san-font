import React, { useState, useImperativeHandle, forwardRef, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { yupResolver } from '@hookform/resolvers/yup';
import { Controller, useForm } from 'react-hook-form';
import { Modal, Select } from 'antd';

import yup from '@/utils/yup';
import errorHelper from '@/utils/error-helper';
import successHelper from '@/utils/success-helper';
import Loading from '@/components/loading';
import productSaveApis from '@/api/productSaveApis';
import productApis from '@/api/productApis';
import userApis from '@/api/userApis';

const schema = yup.object({
    productId: yup.number().required(),
    userId: yup.number().required(),
});

const ModalProductSaveUpdate = ({ onAfterUpdate, account }, ref) => {
    const { t } = useTranslation();
    const { Option } = Select;

    const [listProduct, setListProduct] = useState([]);
    const [listUser, setListUser] = useState([]);

    const getListProducts = async () => {
        const productNameId = await productApis.getListProduct();
        setListProduct(productNameId);
    };

    const getListUser = async () => {
        const userNameId = await userApis.getListUsers();
        setListUser(userNameId);
    };

    useEffect(() => {
        getListProducts();
        getListUser();
    }, []);

    const {
        control,
        handleSubmit,
        formState: { errors },
        setValue,
        reset,
    } = useForm({
        resolver: yupResolver(schema),
    });

    const [visible, setVisible] = useState(false);
    const [loading, setLoading] = useState(false);
    const [idDataProductSave, setIdDataProductSave] = useState(null);

    const onOpen = (id) => {
        setVisible(true);
        setLoading(true);
        setIdDataProductSave(id);
        productSaveApis
            .getDetailProductSave(id)
            .then((res) => {
                const { productId, userId } = res;
                setValue('productId', productId, {
                    shouldValidate: true,
                    shouldDirty: true,
                });
                setValue('userId', userId, {
                    shouldValidate: true,
                    shouldDirty: true,
                });
            })
            .catch((err) => {
                errorHelper(err.message);
            })
            .finally(() => setLoading(false));
    };

    useImperativeHandle(ref, () => ({
        onOpen,
    }));

    const onClose = () => {
        setVisible(false);
        setLoading(false);
        reset();
        setIdDataProductSave(null);
    };

    const submitUpdateProductSave = (values) => {
        const { productId, userId } = values;
        const payload = {
            id: idDataProductSave,
            productId: productId,
            userId: userId,
        };

        return productSaveApis
            .updateProductSave(payload)
            .then(() => {
                successHelper(t('update_success'));
                onClose();
                onAfterUpdate();
            })
            .catch((err) => {
                errorHelper(err);
            })
            .finally(() => setLoading(false));
    };

    const customStyle = {
        border: '1px solid red',
    };

    return (
        <Modal footer={null} open={visible} onCancel={onClose} maskClosable={false} destroyOnClose>
            <form onSubmit={handleSubmit(submitUpdateProductSave)}>
                <h3 className="font-semibold text-black mb-3">
                    {account.role === 1 ? t('product_save_update') : t('admin_role')}
                </h3>

                <div className="flex flex-col gap-4">
                    <div className="mt-3">
                        <h4 className="mb-2">{t('product_save_select_category')}</h4>
                        <Controller
                            name="productId"
                            control={control}
                            render={({ field }) => (
                                <Select
                                    {...field}
                                    showSearch
                                    status={errors?.productId?.message}
                                    control={control}
                                    allowClear={true}
                                    name="productId"
                                    placeholder={t('select_a_category')}
                                    optionFilterProp="children"
                                    filterOption={(input, option) =>
                                        option.children.toLowerCase().includes(input.toLowerCase())
                                    }
                                    style={errors?.productId?.message && customStyle}
                                >
                                    {listProduct.rows?.map((e) => {
                                        return (
                                            <Option value={e.id} key={e.id}>
                                                {e.name}
                                            </Option>
                                        );
                                    })}
                                </Select>
                            )}
                        />
                        {errors?.productId?.message && <p className="text-error mt-1">{errors?.productId?.message}</p>}
                    </div>

                    <div className="mt-3">
                        <h4 className="mb-2">{t('user_save')}</h4>
                        <Controller
                            name="userId"
                            control={control}
                            render={({ field }) => (
                                <Select
                                    {...field}
                                    showSearch
                                    status={errors?.userId?.message}
                                    control={control}
                                    allowClear={true}
                                    name="userId"
                                    disabled
                                    placeholder={t('select_a_category')}
                                    optionFilterProp="children"
                                    filterOption={(input, option) =>
                                        option.children.toLowerCase().includes(input.toLowerCase())
                                    }
                                    style={errors?.userId?.message && customStyle}
                                >
                                    {listUser.rows?.map((e) => {
                                        return (
                                            <Option value={e.id} key={e.id}>
                                                {e.username}
                                            </Option>
                                        );
                                    })}
                                </Select>
                            )}
                        />
                        {errors?.userId?.message && <p className="text-error mt-1">{errors?.userId?.message}</p>}
                    </div>
                </div>

                <div className="w-full flex items-center justify-center mt-3">
                    {account.role === 1 && (
                        <button type="submit" className="flex bg-main-color py-2 px-4 hover:shadow-4xl">
                            <div>{loading && <Loading Loading />}</div>
                            <span className="text-white text-sm">{t('update')}</span>
                        </button>
                    )}
                </div>
            </form>
        </Modal>
    );
};

export default forwardRef(ModalProductSaveUpdate);
