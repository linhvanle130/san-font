import React, { useEffect, useRef, useState } from 'react';
import { Table, Tooltip } from 'antd';
import { useTranslation } from 'react-i18next';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
import { AiOutlineDelete, AiOutlineEdit } from 'react-icons/ai';
import moment from 'moment';

import yup from '@/utils/yup';
import errorHelper from '@/utils/error-helper';
import productCategoryTypeApis from '@/api/productCategoryTypeApis';
import ModalProductElementCreate from './modal-product-element-create';
import ModalProductCategoryUpdate from '../product-category/modal-product-category-update';
import productCategoryApis from '@/api/productCategoryApis';
import ModalProductCategoryDelete from '../product-category/modal-product-category-delete';

const ProductElement = () => {
    const { t } = useTranslation();
    const [loading, setLoading] = useState(true);
    const account = JSON.parse(localStorage.getItem('info'));

    const refModalProductElementCreate = useRef();
    const refModalProductCategoryUpdate = useRef();
    const refModalProductCategoryDelete = useRef();

    const [page, setPage] = useState(1);
    const [listProductCategory, setListProductCategory] = useState([]);
    const [listProductCategoryType, setListProductCategoryType] = useState([]);
    console.log(listProductCategoryType);

    const schema = yup.object({
        search: yup.string().max(255),
    });

    const { getValues } = useForm({
        resolver: yupResolver(schema),
    });

    const onGetListProductElement = (values) => {
        const params = {
            ...values,
            admin: true,
        };

        setLoading(true);

        productCategoryTypeApis
            .getCategoryWithPagingType(params)
            .then(({ rows }) => {
                const productCategoryDetail = [];
                setListProductCategoryType(
                    rows.map((e) => {
                        e.productCategory.map((a) => productCategoryDetail.push(a));
                        return {
                            ...e,
                            countCategory: e.productCategory?.length,
                        };
                    }),
                );
                setListProductCategory(productCategoryDetail);

                window.scroll({
                    top: 0,
                    behavior: 'smooth',
                });
            })
            .catch((err) => {
                console.log(err);
                errorHelper(err);
            })
            .finally(() => {
                setLoading(false);
            });
    };

    useEffect(() => {
        onGetListProductElement();
    }, []);

    const expandedRowRender = (row) => {
        const productCategory = listProductCategory
            .filter((ft) => ft.categoryId === row.id)
            .map((e) => {
                return {
                    id: e.id,
                    name: e.name,
                    categoryId: e.categoryId,
                    categorySlug: e.categorySlug,
                    createdAt: e.createdAt,
                };
            });
        const columns = [
            {
                title: t('name'),
                dataIndex: 'name',
                key: 'name',
                width: 500,
                render: (text) => <Tooltip title={text}>{text}</Tooltip>,
                sorter: (a, b) => a.name?.localeCompare(b.name),
            },
            {
                title: t('slug'),
                dataIndex: 'categorySlug',
                key: 'categorySlug',
                width: 500,
                render: (text) => <Tooltip title={text}>{text}</Tooltip>,
                sorter: (a, b) => a.categorySlug?.localeCompare(b.categorySlug),
            },
            {
                title: t('created_at'),
                dataIndex: 'createdAt',
                key: 'created_at',
                width: 300,
                render: (item) => moment(item).format('YYYY-MM-DD HH:mm:ss'),
                sorter: (a, b) => a.createdAt?.localeCompare(b.createdAt),
            },
            {
                title: t('action'),
                key: 'action',
                render: (item, record) => (
                    <div className="p-4 flex items-center justify-center gap-3">
                        <Tooltip title={t('update')}>
                            <button
                                onClick={() => refModalProductCategoryUpdate.current.onOpen(record.id)}
                                className="px-3 mt-2 py-[6px] bg-main-color text-white rounded-2xl cursor-pointer hover:shadow-4xl hover:opacity-80"
                            >
                                <AiOutlineEdit />
                            </button>
                        </Tooltip>
                        {account.role === 1 && (
                            <Tooltip title={t('Delete')}>
                                <button
                                    className=" px-3 mt-2 py-[6px] bg-red-500 text-white rounded-2xl cursor-pointer hover:shadow-4xl hover:opacity-80"
                                    onClick={() => refModalProductCategoryDelete?.current?.onOpen(record.id, record)}
                                >
                                    <AiOutlineDelete />
                                </button>
                            </Tooltip>
                        )}
                    </div>
                ),
                width: 200,
                align: 'center',
                fixed: 'right',
            },
        ];
        return <Table columns={columns} dataSource={productCategory} pagination={false} rowKey="id" />;
    };

    const columns = [
        {
            title: t('name'),
            dataIndex: 'name',
            key: 'name',
        },
    ];

    const handleTableChange = (pagination) => {
        setPage(pagination.current);
    };

    return (
        <div>
            <div className="flex justify-between py-4 max-sm:flex-col">
                <h2 className="mb-3 text-[20px] md:text-[24px] lg:text-[30px] text-black font-medium">
                    {t('product_element')}
                </h2>
                <div>
                    <button
                        onClick={() => refModalProductElementCreate.current.onOpen()}
                        className="bg-main-color py-2 px-4 hover:shadow-4xl max-sm:w-[60%] max-sm:float-right"
                    >
                        <span className="text-white text-sm">{t('product_element_add')}</span>
                    </button>
                </div>
            </div>
            <Table
                dataSource={listProductCategoryType}
                rowKey="id"
                columns={columns}
                expandable={{
                    expandedRowRender,
                    expandRowByClick: true,
                }}
                pagination={{
                    current: page,
                    position: ['bottomCenter'],
                }}
                scroll={{ x: 'max-content' }}
                onChange={handleTableChange}
            />

            <ModalProductElementCreate
                ref={refModalProductElementCreate}
                onSubmit={(payload) => productCategoryTypeApis.createProductElement(payload)}
                onAfterCreate={() => {
                    onGetListProductElement(getValues());
                }}
                account={account}
            />
            <ModalProductCategoryUpdate
                ref={refModalProductCategoryUpdate}
                onAfterUpdate={() => {
                    onGetListProductElement(getValues());
                }}
                account={account}
            />

            <ModalProductCategoryDelete
                ref={refModalProductCategoryDelete}
                onSubmit={(id) => productCategoryApis.deleteCategory(id)}
                onAfterDelete={() => {
                    onGetListProductElement(getValues());
                }}
            />
        </div>
    );
};

export default ProductElement;
