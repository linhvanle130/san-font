import React, { useState, useImperativeHandle, forwardRef, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { yupResolver } from '@hookform/resolvers/yup';
import { Controller, useForm } from 'react-hook-form';

import { convertToSlug } from '@/utils/funcs';
import yup from '@/utils/yup';
import errorHelper from '@/utils/error-helper';
import successHelper from '@/utils/success-helper';
import Loading from '@/components/loading';
import { Modal, Input } from 'antd';

const schema = yup.object({
    name: yup.string().trim().required().max(255),
    categorySlug: yup.string().trim().required().max(255),
});

const ModalProductElementCreate = ({ onSubmit, onAfterCreate, account }, ref) => {
    const { t } = useTranslation();
    const [categorySlug, setSlug] = useState('');

    const handleSetSlug = (value) => {
        setSlug(value);
        setValue('name', value, {
            shouldValidate: true,
            shouldDirty: true,
        });
        setValue('categorySlug', convertToSlug(value), {
            shouldValidate: true,
            shouldDirty: true,
        });
    };

    const {
        control,
        handleSubmit,
        formState: { errors },
        setValue,
        reset,
    } = useForm({
        resolver: yupResolver(schema),
    });

    const [visible, setVisible] = useState(false);
    const [loading, setLoading] = useState(false);
    const [errorProductCategory, setErrorProductCategory] = useState('');

    const onOpen = () => {
        setVisible(true);
    };

    useImperativeHandle(ref, () => ({
        onOpen,
    }));

    const onClose = () => {
        setVisible(false);
        reset({
            name: '',
            categorySlug: '',
        });
        setLoading(false);
        setErrorProductCategory('');
    };

    const submitProductElementCreate = (values) => {
        const { name, categorySlug } = values;

        const payload = {
            name: name.trim(),
            categorySlug: categorySlug?.trim(),
        };

        setLoading(true);

        return onSubmit(payload)
            .then(() => {
                successHelper(t('create_success'));
                onClose();
                onAfterCreate();
            })
            .catch((err) => {
                if (err?.response?.data?.code && err?.response?.data?.code === 'PRODUCT_CATEGORY_IS_EXISTED') {
                    setErrorProductCategory('errors:PRODUCT_CATEGORY_IS_EXISTED');
                } else {
                    errorHelper(err);
                }
            })
            .finally(() => setLoading(false));
    };

    const customStyle = {
        border: '1px solid red',
    };

    return (
        <Modal footer={null} open={visible} onCancel={onClose} maskClosable={false} destroyOnClose>
            <form onSubmit={handleSubmit(submitProductElementCreate)}>
                <h3 className="font-semibold text-black mb-3">
                    {account.role === 1 ? t('product_element_add') : t('admin_role')}
                </h3>

                <div className="flex flex-col gap-4">
                    <div className="mb-3">
                        <h4 className="mb-1">{t('name')}</h4>
                        <Controller
                            name="name"
                            control={control}
                            render={({ field }) => (
                                <Input
                                    {...field}
                                    onChange={(event) => handleSetSlug(event.target.value)}
                                    status={errors?.name?.message ? 'error' : null}
                                    placeholder={t('name')}
                                    style={errors?.name?.message && customStyle}
                                />
                            )}
                        />
                        {errors?.name?.message && <p className="text-error mt-1">{errors?.name?.message}</p>}
                    </div>

                    <div className="mb-3">
                        <h4 className="mb-1">{t('slug')}</h4>
                        <Controller
                            name="categorySlug"
                            control={control}
                            render={({ field }) => (
                                <Input
                                    {...field}
                                    status={errors?.categorySlug?.message || errorProductCategory ? 'error' : null}
                                    placeholder={t('slug')}
                                    disabled
                                />
                            )}
                        />
                        {errors?.categorySlug?.message && (
                            <p className="text-error mt-1">{errors?.categorySlug?.message}</p>
                        )}
                        {errorProductCategory && <p className="text-error mt-1">{errorProductCategory}</p>}
                    </div>
                </div>

                <div className="w-full flex items-center justify-center">
                    {account.role === 1 && (
                        <button type="submit" className="flex bg-main-color py-2 px-4 hover:shadow-4xl">
                            <div>{loading && <Loading Loading />}</div>
                            <span className="text-white text-sm">{t('create')}</span>
                        </button>
                    )}
                </div>
            </form>
        </Modal>
    );
};

export default forwardRef(ModalProductElementCreate);
