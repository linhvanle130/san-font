import React, { useState, useImperativeHandle, forwardRef, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { yupResolver } from '@hookform/resolvers/yup';
import { Controller, useForm } from 'react-hook-form';
import { Modal, Input } from 'antd';

import yup from '@/utils/yup';
import errorHelper from '@/utils/error-helper';
import successHelper from '@/utils/success-helper';
import Loading from '@/components/loading';
import orderPackageApis from '@/api/orderPackage';

const schema = yup.object({
    name: yup.string().trim().required().max(255),
    price: yup.number().required(),
    pricePrevious: yup.number().required(),
    note: yup.string().trim().required().max(255),
});

const ModalOrderPackageUpdate = ({ onAfterUpdate, account }, ref) => {
    const { t } = useTranslation();

    const {
        control,
        handleSubmit,
        formState: { errors },
        setValue,
        reset,
    } = useForm({
        resolver: yupResolver(schema),
    });

    const [visible, setVisible] = useState(false);
    const [loading, setLoading] = useState(false);
    const [idDataOrderPackage, setIdDataOrderPackage] = useState(null);
    const [errorOrderPackage, setErrorOrderPackage] = useState('');

    const onOpen = (id) => {
        setVisible(true);
        setLoading(true);
        setIdDataOrderPackage(id);
        orderPackageApis
            .getDetailOrderPackage(id)
            .then((res) => {
                const { name, price, pricePrevious, note } = res;
                setValue('name', name?.trim(), {
                    shouldValidate: true,
                    shouldDirty: true,
                });
                setValue('price', price, {
                    shouldValidate: true,
                    shouldDirty: true,
                });
                setValue('pricePrevious', pricePrevious, {
                    shouldValidate: true,
                    shouldDirty: true,
                });
                setValue('note', note?.trim(), {
                    shouldValidate: true,
                    shouldDirty: true,
                });
            })
            .catch((err) => {
                errorHelper(err.message);
            })
            .finally(() => setLoading(false));
    };

    useImperativeHandle(ref, () => ({
        onOpen,
    }));

    const onClose = () => {
        setVisible(false);
        setLoading(false);
        reset();
        setIdDataOrderPackage(null);
        setErrorOrderPackage('');
    };

    const submitUpdateOrderPackage = (values) => {
        const { name, price, pricePrevious, note } = values;
        const payload = {
            id: idDataOrderPackage,
            name: name.trim(),
            price: price,
            pricePrevious: pricePrevious,
            note: note.trim(),
        };

        return orderPackageApis
            .updateOrderPackage(payload)
            .then(() => {
                successHelper(t('update_success'));
                onClose();
                onAfterUpdate();
            })
            .catch((err) => {
                errorHelper(err);
            })
            .finally(() => setLoading(false));
    };

    const customStyle = {
        border: '1px solid red',
    };

    return (
        <Modal footer={null} open={visible} onCancel={onClose} maskClosable={false} destroyOnClose>
            <form onSubmit={handleSubmit(submitUpdateOrderPackage)}>
                <h3 className="font-semibold text-black mb-3">
                    {account.role === 1 ? 'Cập nhật gói thành viên' : t('admin_role')}
                </h3>

                <div className="flex flex-col gap-4">
                    <div className="mb-3">
                        <h4 className="mb-1">{t('name')}</h4>
                        <Controller
                            name="name"
                            control={control}
                            render={({ field }) => (
                                <Input
                                    {...field}
                                    status={errors?.name?.message ? 'error' : null}
                                    placeholder={t('name')}
                                    style={errors?.name?.message && customStyle}
                                />
                            )}
                        />
                        {errors?.name?.message && <p className="text-error mt-1">{errors?.name?.message}</p>}
                    </div>
                    <div className="mb-3">
                        <h4 className="mb-1">Giá trước gói thành viên</h4>
                        <Controller
                            name="pricePrevious"
                            control={control}
                            render={({ field }) => (
                                <Input
                                    {...field}
                                    status={errors?.pricePrevious?.message ? 'error' : null}
                                    placeholder="Giá trước gói thành viên"
                                    style={errors?.pricePrevious?.message && customStyle}
                                />
                            )}
                        />
                        {errors?.pricePrevious?.message && (
                            <p className="text-error mt-1">{errors?.pricePrevious?.message}</p>
                        )}
                    </div>
                    <div className="mb-3">
                        <h4 className="mb-1">Giá gói thành viên</h4>
                        <Controller
                            name="price"
                            control={control}
                            render={({ field }) => (
                                <Input
                                    {...field}
                                    status={errors?.price?.message ? 'error' : null}
                                    placeholder="Giá gói thành viên "
                                    style={errors?.price?.message && customStyle}
                                />
                            )}
                        />
                        {errors?.price?.message && <p className="text-error mt-1">{errors?.price?.message}</p>}
                    </div>
                    <div className="mb-3">
                        <h4 className="mb-1">Nội dung</h4>
                        <Controller
                            name="note"
                            control={control}
                            render={({ field }) => (
                                <Input
                                    {...field}
                                    status={errors?.note?.message ? 'error' : null}
                                    placeholder={t('note')}
                                    style={errors?.note?.message && customStyle}
                                />
                            )}
                        />
                        {errors?.note?.message && <p className="text-error mt-1">{errors?.note?.message}</p>}
                    </div>
                </div>

                <div className="w-full flex items-center justify-center">
                    {account.role === 1 && (
                        <button type="submit" className="flex bg-main-color py-2 px-4 hover:shadow-4xl">
                            <div>{loading && <Loading Loading />}</div>
                            <span className="text-white text-sm">{t('update')}</span>
                        </button>
                    )}
                </div>
            </form>
        </Modal>
    );
};

export default forwardRef(ModalOrderPackageUpdate);
