import React, { useEffect, useState, useRef } from 'react';
import { AiOutlineDelete, AiOutlineEdit } from 'react-icons/ai';
import { useTranslation } from 'react-i18next';
import { useForm } from 'react-hook-form';
import { Table, Tooltip } from 'antd';
import moment from 'moment';

import { GLOBAL_STATUS } from '@/constants';
import errorHelper from '@/utils/error-helper';
import orderPackageApis from '@/api/orderPackage';
import ModalOrderPackageStatus from './modal-order-package-status';
import ModalOrderPackageCreate from './modal-order-package-create';
import ModalOrderPackageUpdate from './modal-order-package-update';
import { numberDecimalWithCommas } from '@/utils/funcs';
import ModalOrderPackageDelete from './modal-order-package-delete';
import orderContentApis from '@/api/orderContentApis';
import ModalOrderContentCreate from '../order-content/modal-order-content-create';
import ModalOrderContentUpdate from '../order-content/modal-order-content-update';
import ModalOrderContentDelete from '../order-content/modal-order-content-delete';

const OrderPackage = () => {
    const { t } = useTranslation();

    const [page, setPage] = useState(1);
    const [loading, setLoading] = useState(true);
    const account = JSON.parse(localStorage.getItem('info'));
    const { getValues } = useForm({});

    const refModalOrderPackageStatus = useRef();
    const refModalOrderPackageCreate = useRef();
    const refModalOrderPackageUpdate = useRef();
    const refModalOrderPackageDelete = useRef();

    const refModalOrderContentCreate = useRef();
    const refModalOrderContentUpdate = useRef();
    const refModalOrderContentDelete = useRef();

    const [listOrderPackageType, setListOrderPackageType] = useState([]);
    const [listOrderPackage, setListOrderPackage] = useState([]);
    console.log(listOrderPackageType);

    const expandedRowRender = (row) => {
        const orderContent = listOrderPackage
            .filter((ft) => ft.orderPackageId === row.id)
            .map((e) => {
                return {
                    id: e.id,
                    createdAt: e.createdAt,
                    content: e.content,
                };
            });

        const columns = [
            {
                title: t('content'),
                dataIndex: 'content',
                key: 'content',
                width: 300,
                render: (item) => (
                    <Tooltip title={item}>
                        <div className="font-semibold text-green-500">{item}</div>
                    </Tooltip>
                ),
            },
            {
                title: t('created_at'),
                dataIndex: 'createdAt',
                key: 'createdAt',
                width: 300,
                render: (item) => moment(item).format('YYYY-MM-DD HH:mm:ss'),
                sorter: (a, b) => a.createdAt?.localeCompare(b.createdAt),
            },
            // {
            //     title: t('action'),
            //     key: 'action',
            //     render: (item, record) => (
            //         <div className="p-4 flex items-center justify-center gap-3  ">
            //             <Tooltip title={t('update')}>
            //                 <button
            //                     onClick={() => refModalOrderContentUpdate?.current?.onOpen(record)}
            //                     className="px-3 mt-2 py-[6px] bg-main-color text-white rounded-2xl cursor-pointer hover:shadow-4xl hover:opacity-80"
            //                 >
            //                     <AiOutlineEdit />
            //                 </button>
            //             </Tooltip>
            //             {account.role === 1 && (
            //                 <Tooltip title={t('Delete')}>
            //                     <button
            //                         className=" px-3 mt-2 py-[6px] bg-red-500 text-white rounded-2xl cursor-pointer hover:shadow-4xl hover:opacity-80"
            //                         onClick={() => refModalOrderContentDelete?.current?.onOpen(record.id, item)}
            //                     >
            //                         <AiOutlineDelete />
            //                     </button>
            //                 </Tooltip>
            //             )}
            //         </div>
            //     ),
            //     align: 'center',
            //     fixed: 'right',
            //     width: 50,
            // },
        ];

        return (
            <div>
                <Table
                    rowSelection={{
                        type: 'checkbox',
                    }}
                    columns={columns}
                    dataSource={orderContent}
                    pagination={false}
                    rowKey="id"
                    size="small"
                />
            </div>
        );
    };

    const onGetListOrderPackages = (values) => {
        const params = {
            ...values,
            page,
        };
        setLoading(true);

        orderPackageApis
            .getAllOrderPackage(params)
            .then(({ rows }) => {
                const listContent = [];
                setListOrderPackageType(
                    rows.map((e) => {
                        e.orderContent.map((a) => listContent.push(a));
                        return {
                            ...e,
                        };
                    }),
                );
                setListOrderPackage(listContent);

                window.scroll({
                    top: 0,
                    behavior: 'smooth',
                });
            })
            .catch((err) => {
                errorHelper(err);
            })
            .finally(() => {
                setLoading(false);
            });
    };

    useEffect(() => {
        onGetListOrderPackages();
    }, []);

    const columns = [
        {
            title: t('name'),
            dataIndex: 'name',
            key: 'name',
            width: 300,
            render: (text) => <Tooltip title={text}>{text}</Tooltip>,
            sorter: (a, b) => a.name?.localeCompare(b.name),
        },
        {
            title: t('name'),
            dataIndex: 'name',
            key: 'name',
            width: 300,
            render: (text) => <Tooltip title={text}>{text}</Tooltip>,
            sorter: (a, b) => a.name?.localeCompare(b.name),
        },
        {
            title: t('price'),
            dataIndex: 'price',
            key: 'price',
            width: 300,
            render: (item) => numberDecimalWithCommas(item) + ' đ',
            sorter: (a, b) => a.price - b.price,
        },
        {
            title: t('price_sales'),
            dataIndex: 'pricePrevious',
            key: 'pricePrevious',
            width: 300,
            render: (item) => numberDecimalWithCommas(item) + ' đ',
            sorter: (a, b) => a.pricePrevious - b.pricePrevious,
        },
        {
            title: t('active'),
            dataIndex: 'status',
            key: 'status',
            width: 200,
            render: (item, record) => (
                <div className="w-full flex justify-center hover:opacity-80">
                    <div className="relative">
                        <input
                            type="checkbox"
                            className="sr-only peer"
                            checked={item === GLOBAL_STATUS.ACTIVE}
                            onChange={() => {}}
                        />
                        <div className="w-11 h-6 bg-gray-300 rounded-full peer peer-focus:ring-4  dark:peer-focus:ring-main-color  peer-checked:after:translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-0.5 after:left-[2px] after:bg-white  after:border after:rounded-full after:h-5 after:w-5 after:transition-all peer-checked:bg-main-color"></div>
                        <div
                            onClick={() => refModalOrderPackageStatus.current.onOpen(record.id, record)}
                            className="absolute pt-[16px] pb-[25px] pl-[24px] pr-[35px] top-[-10px] cursor-pointer"
                        ></div>
                    </div>
                </div>
            ),
            align: 'center',
            sorter: (a, b) => a.status - b.status,
        },
        {
            title: t('created_at'),
            dataIndex: 'createdAt',
            key: 'created_at',
            width: 300,
            render: (item) => moment(item).format('YYYY-MM-DD HH:mm:ss'),
            sorter: (a, b) => a.createdAt?.localeCompare(b.createdAt),
        },
        {
            title: t('action'),
            key: 'action',
            render: (item, record) => (
                <div className="p-4 flex items-center justify-center gap-3">
                    <Tooltip title={t('update')}>
                        <button
                            onClick={() => refModalOrderPackageUpdate.current.onOpen(record.id, record)}
                            className="px-3 mt-2 py-[6px] bg-main-color text-white rounded-2xl cursor-pointer hover:shadow-4xl hover:opacity-80"
                        >
                            <AiOutlineEdit />
                        </button>
                    </Tooltip>
                    {account.role === 1 && (
                        <Tooltip title={t('Delete')}>
                            <button
                                className=" px-3 mt-2 py-[6px] bg-red-500 text-white rounded-2xl cursor-pointer hover:shadow-4xl hover:opacity-80"
                                onClick={() => refModalOrderPackageDelete?.current?.onOpen(record.id, record)}
                            >
                                <AiOutlineDelete />
                            </button>
                        </Tooltip>
                    )}
                </div>
            ),
            fixed: 'right',
            align: 'center',
        },
    ];

    const handleTableChange = (pagination) => {
        setPage(pagination.current);
    };

    return (
        <div>
            <div className="flex justify-between py-4 max-sm:flex-col">
                <h2 className="mb-3 text-[20px] md:text-[24px] lg:text-[30px] text-black font-medium">
                    {t('order_list_package')}
                </h2>
                <div className="flex gap-6">
                    <div className="flex gap-3">
                        <button
                            onClick={() => refModalOrderPackageCreate.current.onOpen()}
                            type="submit"
                            className="bg-main-color py-2 px-4 hover:shadow-4xl max-sm:float-right hover:opacity-80 transition-all"
                        >
                            <span className="text-white text-sm">{t('order_add_package')}</span>
                        </button>
                    </div>
                    <div className="flex gap-3">
                        <button
                            onClick={() => refModalOrderContentCreate.current.onOpen()}
                            type="submit"
                            className="bg-main-color py-2 px-4 hover:shadow-4xl max-sm:float-right hover:opacity-80 transition-all"
                        >
                            <span className="text-white text-sm">{t('add_content')}</span>
                        </button>
                    </div>
                </div>
            </div>
            <Table
                rowSelection={{
                    type: 'checkbox',
                }}
                dataSource={listOrderPackageType}
                rowKey="id"
                scroll={{ x: 'max-content' }}
                columns={columns}
                expandable={{
                    expandedRowRender,
                    expandRowByClick: true,
                }}
                pagination={{
                    current: page,
                    position: ['bottomCenter'],
                }}
                loading={loading}
                onChange={handleTableChange}
            />
            <ModalOrderPackageCreate
                ref={refModalOrderPackageCreate}
                onSubmit={(payload) => orderPackageApis.createOrderPackage(payload)}
                onAfterCreate={() => {
                    onGetListOrderPackages(getValues());
                }}
            />
            <ModalOrderPackageUpdate
                ref={refModalOrderPackageUpdate}
                onAfterUpdate={() => {
                    onGetListOrderPackages(getValues());
                }}
                account={account}
            />
            <ModalOrderPackageStatus
                ref={refModalOrderPackageStatus}
                onAfterChangeStatus={() => {
                    onGetListOrderPackages(getValues());
                }}
            />
            <ModalOrderPackageDelete
                ref={refModalOrderPackageDelete}
                onSubmit={(id) => orderPackageApis.deleteOrderPackage(id)}
                onAfterDelete={() => {
                    onGetListOrderPackages(getValues());
                }}
            />

            <ModalOrderContentCreate
                ref={refModalOrderContentCreate}
                onSubmit={(payload) => orderContentApis.createOrderContent(payload)}
                onAfterCreate={() => {
                    onGetListOrderPackages(getValues());
                }}
                listName={listOrderPackageType}
            />
            <ModalOrderContentUpdate
                ref={refModalOrderContentUpdate}
                onAfterUpdate={() => {
                    onGetListOrderPackages();
                }}
                account={account}
                listName={listOrderPackageType}
            />
            <ModalOrderContentDelete
                ref={refModalOrderContentDelete}
                onSubmit={(id) => orderContentApis.deleteOrderContent(id)}
                onAfterDelete={() => {
                    onGetListOrderPackages(getValues());
                }}
            />
        </div>
    );
};

export default OrderPackage;
