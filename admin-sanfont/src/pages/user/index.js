import React, { useEffect, useRef, useState } from 'react';
import { Table, Tooltip, Switch, Input, Space, Button, Select } from 'antd';
import { useTranslation } from 'react-i18next';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { AiOutlineEdit, AiOutlineInfo } from 'react-icons/ai';
import { FiChevronDown, FiChevronRight } from 'react-icons/fi';
import { IoIosSearch } from 'react-icons/io';
import { useNavigate } from 'react-router-dom';
import moment from 'moment';

import userApis from '@/api/userApis';
import { ACCOUNT_STATUS, MASTER_DATA_NAME } from '@/constants';
import errorHelper from '@/utils/error-helper';
import Loading from '@/components/loading';
import yup from '@/utils/yup';
import ModalDetailAccountUser from '@/components/modal-detail-account-user';
import masterApis from '@/api/masterAps';
import ModalUserChangeStatus from './modal-user-change-status';

const LIMIT = 1000000;

const User = () => {
    const { t } = useTranslation();
    const [search, setSearch] = useState(true);
    const navigate = useNavigate();

    const [page, setPage] = useState(1);
    const [loading, setLoading] = useState(true);
    const [listUsers, setListUsers] = useState([]);
    const [listDetailProduct, setListDetailProduct] = useState([]);
    const [masterLevelUser, setMasterLevelUser] = useState();
    const [masterRoleUser, setMasterRoleUser] = useState();
    const [masterFontUser, setMasterFontUser] = useState();

    const refModalAccountDetail = useRef();
    const refModalUserChangeStatus = useRef();
    console.log(listUsers);

    const fetchMasterData = async () => {
        const masterLevel = await masterApis.getAllMaster({
            idMaster: MASTER_DATA_NAME.LEVEL_USER,
        });
        const masterRole = await masterApis.getAllMaster({
            idMaster: MASTER_DATA_NAME.ROLE,
        });
        const masterFont = await masterApis.getAllMaster({
            idMaster: MASTER_DATA_NAME.STATUS_FONT,
        });
        setMasterRoleUser(masterRole);
        setMasterLevelUser(masterLevel);
        setMasterFontUser(masterFont);
    };

    useEffect(() => {
        fetchMasterData();
    }, []);

    const schema = yup.object({
        search: yup.string().max(255).nullable(),
    });

    const {
        control,
        handleSubmit,
        getValues,
        formState: { errors },
    } = useForm({
        defaultValues: {
            search: new URLSearchParams(search).get('email'),
        },
        resolver: yupResolver(schema),
    });

    const onSubmit = (values) => {
        const params = {
            ...values,
            search: values?.email,
            size: LIMIT,
        };

        setPage(1);
        return onGetListUsers(params);
    };

    const [searchText, setSearchText] = useState('');
    const [searchedColumn, setSearchedColumn] = useState('');
    const searchInput = useRef(null);

    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        setSearchText(selectedKeys[0]);
        setSearchedColumn(dataIndex);
    };

    const handleEditProduct = (id) => {
        navigate(`/product/update/${id}`);
    };

    const getColumnSearchProps = (dataIndex) => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm }) => (
            <div
                style={{
                    padding: 8,
                }}
            >
                <Input
                    ref={searchInput}
                    placeholder={`${t('search')} ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{
                        marginBottom: 8,
                        display: 'block',
                    }}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<IoIosSearch />}
                        size="small"
                        style={{
                            width: 100,
                            background: '#ff5e3a',
                        }}
                    >
                        {t('search')}
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: (filtered) => (
            <IoIosSearch
                style={{
                    color: filtered ? '#1890ff' : undefined,
                }}
            />
        ),
        onFilter: (value, record) => record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
        onFilterDropdownOpenChange: (visible) => {
            if (visible) {
                setTimeout(() => searchInput.current?.select(), 100);
            }
        },
        render: (text) => text,
    });

    const onGetListUsers = (values) => {
        const params = {
            ...values,
            size: LIMIT,
        };

        setLoading(true);

        userApis
            .getListUsers(params)
            .then(({ rows }) => {
                const userDetails = [];
                setListUsers(
                    rows.map((e) => {
                        e.product.map((a) => userDetails.push(a));
                        return {
                            ...e,
                            phone: e.phoneCode ? `+${e.phoneCode}${e.phoneNumber}` : '',
                            order: e.order.length,
                        };
                    }),
                );
                setListDetailProduct(userDetails);

                window.scroll({
                    top: 0,
                    behavior: 'smooth',
                });
            })
            .catch((err) => {
                errorHelper(err);
            })
            .finally(() => {
                setLoading(false);
            });
    };

    useEffect(() => {
        onGetListUsers(getValues());
    }, [getValues]);

    const expandedRowRender = (row) => {
        const userDetail = listDetailProduct
            .filter((ft) => ft.userId === row.id)
            .map((e) => {
                return {
                    id: e.id,
                    name: e.name,
                    author: e.author,
                    subAuthor: e.subAuthor,
                    outstanding: e.outstanding,
                    createdAt: e.createdAt,
                };
            });
        const columns = [
            {
                title: t('name'),
                key: 'name',
                dataIndex: 'name',
                width: 250,
                ...getColumnSearchProps('name'),
                render: (text) => <Tooltip title={text}>{text}</Tooltip>,
            },
            {
                title: t('author'),
                dataIndex: 'author',
                key: 'author',
                ...getColumnSearchProps('author'),
                width: 250,
            },
            {
                title: t('vietnameseization'),
                dataIndex: 'subAuthor',
                key: 'subAuthor',
                width: 200,
            },
            {
                title: t('featured'),
                dataIndex: 'outstanding',
                key: 'outstanding',
                width: 200,
                render: (item) =>
                    (item === 0 && 'Bình thường') || (item === 1 && 'VIP') || (item === 2 && 'Font chọn lọc'),
                align: 'center',
            },
            {
                title: t('created_at'),
                dataIndex: 'createdAt',
                key: 'created_at',
                width: 200,
                render: (item) => moment(item).format('YYYY-MM-DD HH:mm:ss'),
                sorter: (a, b) => a.createdAt?.localeCompare(b.createdAt),
            },
            {
                title: t('action'),
                key: 'action',
                render: (record) => (
                    <div className="p-4 flex items-center justify-center gap-3">
                        <Tooltip title={t('update')}>
                            <button
                                onClick={() => handleEditProduct(record.id)}
                                className="px-3 mt-2 py-[6px] bg-main-color text-white rounded-2xl cursor-pointer hover:shadow-4xl hover:opacity-80"
                            >
                                <AiOutlineEdit />
                            </button>
                        </Tooltip>
                    </div>
                ),
                align: 'center',
                width: 100,
            },
        ];
        return (
            <Table
                rowSelection={{
                    type: 'checkbox',
                }}
                columns={columns}
                dataSource={userDetail}
                pagination={false}
                rowKey="id"
                size="small"
            />
        );
    };

    const columns = [
        {
            title: t('email'),
            dataIndex: 'email',
            key: 'email',
            width: 200,
            sorter: (a, b) => a.email?.localeCompare(b.email),
            ...getColumnSearchProps('email'),
        },
        {
            title: t('username'),
            dataIndex: 'username',
            key: 'username',
            width: 200,
            sorter: (a, b) => a.username?.localeCompare(b.username),
            ...getColumnSearchProps('username'),
        },
        {
            title: t('user_code'),
            dataIndex: 'userCode',
            key: 'userCode',
            width: 200,
            align: 'center',
            sorter: (a, b) => a.userCode?.localeCompare(b.userCode),
            ...getColumnSearchProps('userCode'),
        },
        {
            title: t('phone'),
            key: 'phone',
            dataIndex: 'phone',
            width: 200,
            align: 'center',
            sorter: (a, b) => a.phone.localeCompare(b.phone),
            ...getColumnSearchProps('phone'),
        },
        {
            title: t('font_status'),
            dataIndex: 'orderStatus',
            key: 'orderStatus',
            width: 300,
            render: (item, record) => (
                <Select
                    showSearch
                    status={errors?.orderStatus?.message}
                    name="orderStatus"
                    onChange={(e) =>
                        refModalUserChangeStatus.current.onOpen(record.id, e, MASTER_DATA_NAME.STATUS_FONT)
                    }
                    defaultValue={masterFontUser?.find((e) => e.id === item)?.name}
                    optionFilterProp="children"
                    filterOption={(input, option) => option.children.includes(input)}
                >
                    {masterFontUser?.map((e) => {
                        return (
                            <Select.Option value={e.id} key={e.id}>
                                {e.name}
                            </Select.Option>
                        );
                    })}
                </Select>
            ),
            align: 'center',
            sorter: (a, b) => a.orderStatus - b.orderStatus,
        },
        {
            title: t('level'),
            dataIndex: 'level',
            key: 'level',
            width: 200,
            render: (item, record) => (
                <Select
                    showSearch
                    status={errors?.role?.message}
                    name="level"
                    onChange={(e) =>
                        refModalUserChangeStatus.current.onOpen(record.id, e, MASTER_DATA_NAME.STATUS_ORDER)
                    }
                    defaultValue={masterLevelUser?.find((e) => e.id === item)?.name}
                    optionFilterProp="children"
                    filterOption={(input, option) => option.children.includes(input)}
                >
                    {masterLevelUser?.map((e) => {
                        return (
                            <Select.Option value={e.id} key={e.id}>
                                {e.name}
                            </Select.Option>
                        );
                    })}
                </Select>
            ),
            align: 'center',
            sorter: (a, b) => a.level - b.level,
        },
        {
            title: t('role'),
            dataIndex: 'role',
            key: 'role',
            width: 200,
            render: (item, record) => (
                <Select
                    showSearch
                    status={errors?.role?.message}
                    name="role"
                    onChange={(e) => refModalUserChangeStatus.current.onOpen(record.id, e, MASTER_DATA_NAME.ROLE)}
                    defaultValue={masterRoleUser?.find((e) => e.id === item)?.name}
                    optionFilterProp="children"
                    filterOption={(input, option) => option.children.includes(input)}
                >
                    {masterRoleUser?.map((e) => {
                        return (
                            <Select.Option value={e.id} key={e.id}>
                                {e.name}
                            </Select.Option>
                        );
                    })}
                </Select>
            ),
            align: 'center',
            sorter: (a, b) => a.role - b.role,
        },
        {
            title: t('active'),
            dataIndex: 'status',
            key: 'status',
            width: 200,
            render: (item) => (
                <div className="flex justify-center">
                    <Switch checked={item === ACCOUNT_STATUS.ACTIVATE} disabled />
                </div>
            ),
            align: 'center',
            sorter: (a, b) => a.status - b.status,
        },
        {
            title: t('order_amount'),
            key: 'order',
            dataIndex: 'order',
            width: 200,
            align: 'center',
            sorter: (a, b) => a.order - b.order,
        },
        {
            title: t('created_at'),
            dataIndex: 'createdAt',
            key: 'createdAt',
            render: (item) => moment(item).format('YYYY-MM-DD HH:mm:ss'),
            width: 200,
            sorter: (a, b) => a.createdAt?.localeCompare(b.createdAt),
        },
        {
            title: t('action'),
            key: 'action',
            render: (record) => (
                <div className="p-4 flex items-center justify-center gap-3">
                    <Tooltip title="chi tiết tài khoản">
                        <button
                            onClick={() =>
                                refModalAccountDetail.current.onOpen({
                                    id: record?.id,
                                    record,
                                })
                            }
                            className="px-3 mt-2 py-[6px] bg-main-color text-white rounded-2xl cursor-pointer hover:shadow-4xl hover:opacity-80"
                        >
                            <AiOutlineInfo />
                        </button>
                    </Tooltip>
                </div>
            ),
            fixed: 'right',
            align: 'center',
            width: 100,
        },
    ];

    const renderSearch = () => (
        <div className="mb-4">
            <div className="bg-white rounded-sm shadow-3xl dark-container">
                <button
                    className="flex items-center px-4 py-3 cursor-pointer transition"
                    onClick={() => setSearch(!search)}
                >
                    {search ? <FiChevronDown /> : <FiChevronRight />}
                    <h3 className="pl-2 text-base font-bold">{t('search')}</h3>
                </button>
                {search && (
                    <div className="px-4 py-3">
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <div className="w-full flex gap-5 max-lg:flex-col max-lg:gap-3">
                                <div className="w-1/3 max-sm:w-full">
                                    <Controller
                                        name="email"
                                        control={control}
                                        render={({ field }) => (
                                            <Input {...field} type="text" placeholder={t('search_dot3')} />
                                        )}
                                    />
                                    {errors?.search?.message && (
                                        <p className="text-error mt-1">{errors?.search?.message}</p>
                                    )}
                                </div>
                            </div>

                            <div className="mt-5">
                                <button
                                    type="submit"
                                    className="flex items-center justify-center bg-main-color py-2 px-4 hover:shadow-4xl max-sm:w-[100px] max-sm:mt-0"
                                >
                                    <div> {loading && <Loading Loading />}</div>
                                    <span className="text-white text-sm">{t('search')}</span>
                                </button>
                            </div>
                        </form>
                    </div>
                )}
            </div>
        </div>
    );
    const handleTableChange = (pagination) => {
        setPage(pagination.current);
    };

    return (
        <div>
            {renderSearch()}
            <h2 className="mb-3 text-[20px] md:text-[24px] lg:text-[30px] text-black font-medium">{t('list_users')}</h2>
            <Table
                rowSelection={{
                    type: 'checkbox',
                }}
                dataSource={listUsers}
                rowKey="id"
                scroll={{ x: 'max-content' }}
                columns={columns}
                pagination={{
                    current: page,
                    position: ['bottomCenter'],
                }}
                expandable={{
                    expandedRowRender,
                    expandRowByClick: true,
                }}
                loading={loading}
                onChange={handleTableChange}
            />
            <ModalDetailAccountUser ref={refModalAccountDetail} />
            <ModalUserChangeStatus
                ref={refModalUserChangeStatus}
                onAfterChangeStatus={() => {
                    onGetListUsers(getValues());
                }}
            />
        </div>
    );
};

export default User;
