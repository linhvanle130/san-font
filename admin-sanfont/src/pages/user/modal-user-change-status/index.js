import React, { forwardRef, useImperativeHandle, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Modal } from 'antd';
import { MASTER_DATA_NAME } from '@/constants';
import successHelper from '@/utils/success-helper';
import errorHelper from '@/utils/error-helper';
import Loading from '@/components/loading';
import userApis from '@/api/userApis';

const ModalUserChangeStatus = ({ onSubmit, onAfterChangeStatus }, ref) => {
    const { t } = useTranslation();
    const [visible, setVisible] = useState(false);
    const [loading, setLoading] = useState(false);
    const [idUser, setIdUser] = useState(null);
    const [type, setType] = useState(null);
    const [statusUser, setStatusUser] = useState(null);
    const account = JSON.parse(localStorage.getItem('info'));

    const onOpen = (id, level, type) => {
        setVisible(true);
        setStatusUser(level);
        setIdUser(id);
        setType(type);
    };

    useImperativeHandle(ref, () => ({
        onOpen,
    }));

    const onClose = () => {
        setVisible(false);
        setLoading(false);
        setIdUser(null);
        setStatusUser(null);
    };

    const submitUserChangeRole = async () => {
        setLoading(true);
        let body = {
            id: idUser,
        };
        type === MASTER_DATA_NAME.STATUS_ORDER && (body.level = statusUser);
        type === MASTER_DATA_NAME.ROLE && (body.role = statusUser);
        type === MASTER_DATA_NAME.STATUS_FONT && (body.orderStatus = statusUser);
        return userApis
            .setLevelUser(body)
            .then(() => {
                successHelper(t('update_success'));
                onClose();
                onAfterChangeStatus();
            })
            .catch((err) => {
                errorHelper(err);
            })
            .finally(() => setLoading(false));
    };

    return (
        <>
            <Modal footer={null} open={visible} onCancel={onClose} maskClosable={false} destroyOnClose>
                <h3 className="font-semibold text-black">
                    {account.role === 1 ? t('user_status_change') : t('admin_role')}
                </h3>
                <div className="mt-3 flex items-center justify-center gap-3">
                    {account.role === 1 && (
                        <button
                            onClick={() => submitUserChangeRole()}
                            className="flex px-3 py-2 border-[1px] border-red-500  bg-red-500 text-white cursor-pointer rounded-sm hover:bg-red-400"
                        >
                            <span>{loading && <Loading Loading />}</span>
                            <span>{t('confirm')}</span>
                        </button>
                    )}
                    <button
                        onClick={() => onClose()}
                        className="flex px-3 py-2 border-[1px] bg-white border-[#ccc] cursor-pointer  rounded-sm hover:text-red-500 hover:border-red-500"
                    >
                        <div>{loading && <Loading Loading />}</div>
                        <span className="text-black"> {t('close')}</span>
                    </button>
                </div>
            </Modal>
        </>
    );
};

export default forwardRef(ModalUserChangeStatus);
