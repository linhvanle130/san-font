import React from 'react';
import { Bar } from 'react-chartjs-2';
import { useTranslation } from 'react-i18next';

const ChartBarUser = ({ data }) => {
    const { t } = useTranslation();
    const labels = [];
    const dataChart = [];
    data.map((e) => {
        labels.push(e.type);
        dataChart.push(e.value);
    });
    return (
        <Bar
            data={{
                labels: labels,
                datasets: [
                    {
                        label: t('quantity'),
                        backgroundColor: ['#3e95cd', '#8e5ea2', '#3cba9f', '#e8c3b9', '#c45850'],
                        data: dataChart,
                    },
                ],
            }}
            options={{
                indexAxis: 'y',
                plugins: {
                    legend: {
                        display: false,
                    },
                },
            }}
        />
    );
};

export default ChartBarUser;
