import React, { useCallback, useEffect, useState } from 'react';
import { RiShoppingCart2Line } from 'react-icons/ri';
import { AiOutlineScan, AiOutlineTeam } from 'react-icons/ai';
import VirtualList from 'rc-virtual-list';
import moment from 'moment';

import Chart from 'chart.js/auto';
import { CategoryScale } from 'chart.js';
import { MASTER_DATA_NAME, STATUS_ORDER } from '@/constants';
import { numberDecimalWithCommas } from '@/utils/funcs';
import Vector from '../../resources/images/vector.png';
import masterApis from '@/api/masterAps';
import orderApis from '@/api/orderApi';
import userApis from '@/api/userApis';
import ChartLineSumThisMonth from './chartLineSumThisMonth';
import ChartLineSumYear from './chartLineSumYear';
import ChartBarUser from './chartBarUser';
import { useTranslation } from 'react-i18next';

Chart.register(CategoryScale);
const Home = () => {
    const { t } = useTranslation();
    const [user, setUser] = useState([]);

    const appendUserProduct = async () => {
        const topUserProducts = await userApis.getTopUserProduct();
        const first10Data = topUserProducts.slice(0, 10);

        setUser(first10Data);
    };

    useEffect(() => {
        appendUserProduct();
    }, []);

    const onScroll = (e) => {
        if (e.currentTarget.scrollHeight - e.currentTarget.scrollTop === ContainerHeight) {
            appendUserProduct();
        }
    };

    const ContainerHeight = 400;
    const [masterLevelUser, setMasterLevelUser] = useState();
    const [listLevelUser, setListLevelUser] = useState([]);

    const [listOrderThisMonth, setListOrderThisMonth] = useState([]);
    const [listOrderLastMonth, setListOrderLastMonth] = useState([]);
    const [listOrderThisYear, setListOrderThisYear] = useState([]);
    const [totalThisMonth, setTotalThisMonth] = useState();
    const [totalLastMonth, setTotalLastMonth] = useState();
    const [totalThisYear, setTotalThisYear] = useState();

    const [inactiveHome, setInactiveHome] = useState(false);

    const fetchMasterData = async () => {
        const masterLevel = await masterApis.getAllMaster({
            idMaster: MASTER_DATA_NAME.LEVEL_USER,
        });
        setMasterLevelUser(masterLevel);
    };

    const fetchListUser = useCallback(async () => {
        const getListLevel = [];
        const setListLevel = [];
        try {
            const fetchUser = await userApis.getListUsers({ size: 10000 });

            masterLevelUser?.map((master) => {
                fetchUser?.rows.map((user) => {
                    if (master.id === user.level) {
                        getListLevel.push({
                            id: master.id,
                        });
                    }
                });
            });
            masterLevelUser?.map((master) => {
                const countLevel = getListLevel.filter((level) => level.id === master.id);
                setListLevel.push({
                    type: master.name,
                    value: countLevel.length,
                });
            });
            setListLevelUser(setListLevel);
        } catch (e) {
            setInactiveHome(true);
        }
    }, [masterLevelUser]);

    async function fetchOrderListSuccess() {
        var date = new Date();
        var firstDayOfThisMonth = new Date(date.getFullYear(), date.getMonth(), 1);
        var lastDayOfThisMonth = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        var firstDayOfLastMonth = new Date(date.getFullYear(), date.getMonth() - 1, 1);
        var lastDayOfLastMonth = new Date(date.getFullYear(), date.getMonth(), 0);
        var firstDayOfThisYear = new Date(date.getFullYear(), 0, 1);
        var lastDayOfThisYear = new Date(date.getFullYear(), 11, 31);
        try {
            const orderThisMonth = await orderApis.getListOrder({
                status: STATUS_ORDER.SUCCESS,
                startDate: moment(firstDayOfThisMonth).format('YYYY-MM-DD HH:mm:ss'),
                endDate: moment(lastDayOfThisMonth).format('YYYY-MM-DD HH:mm:ss'),
            });
            const orderThisLastMonth = await orderApis.getListOrder({
                status: STATUS_ORDER.SUCCESS,
                startDate: moment(firstDayOfLastMonth).format('YYYY-MM-DD HH:mm:ss'),
                endDate: moment(lastDayOfLastMonth).format('YYYY-MM-DD HH:mm:ss'),
            });
            const orderThisYear = await orderApis.getListOrder({
                status: STATUS_ORDER.SUCCESS,
                startDate: moment(firstDayOfThisYear).format('YYYY-MM-DD HH:mm:ss'),
                endDate: moment(lastDayOfThisYear).format('YYYY-MM-DD HH:mm:ss'),
            });
            setListOrderThisMonth(orderThisMonth);
            setListOrderLastMonth(orderThisLastMonth);
            setListOrderThisYear(orderThisYear);

            setTotalLastMonth(orderThisLastMonth?.reduce((sum, order) => (sum = +sum + +order.total), 0));
            setTotalThisMonth(orderThisMonth?.reduce((sum, order) => (sum = +sum + +order.total), 0));
            setTotalThisYear(orderThisYear?.reduce((sum, order) => (sum = +sum + +order.total), 0));
        } catch (e) {
            setInactiveHome(true);
        }
    }

    useEffect(() => {
        fetchMasterData();
    }, []);

    useEffect(() => {
        fetchListUser();
        fetchOrderListSuccess();
    }, [fetchListUser, masterLevelUser]);

    return (
        <>
            {!inactiveHome && (
                <div className="px-2 pb-5">
                    <div className="flex items-center justify-between w-full max-lg:flex-wrap max-sm:flex-col">
                        <div className="w-1/3 max-lg:w-1/2 max-sm:w-full">
                            <div className="pr-3 max-sm:p-0">
                                <div className="flex items-center p-6 mb-4 bg-white rounded-2xl shadow-3xl dark-container">
                                    <div className="pr-4 red text-3xl ">
                                        <RiShoppingCart2Line className="text-gray-800 dark-icon" />
                                    </div>
                                    <div className="">
                                        <span className="font-medium text-black dart-text">
                                            {totalLastMonth !== undefined &&
                                                `${numberDecimalWithCommas(totalLastMonth)}đ`}
                                        </span>
                                        <p className="text-gray-500 dart-text">{t('total_revenue_last_month')}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="w-1/3 max-lg:w-1/2 max-sm:w-full">
                            <div className="px-3 max-sm:p-0">
                                <div className="flex items-center p-6 mb-4 bg-white rounded-2xl shadow-3xl dark-container">
                                    <div className="pr-4 red text-3xl ">
                                        <AiOutlineScan className="text-gray-800 dark-icon" />
                                    </div>
                                    <div className="">
                                        <span className="font-medium text-black dart-text">
                                            {totalThisMonth !== undefined &&
                                                `${numberDecimalWithCommas(totalThisMonth)}đ`}
                                        </span>
                                        <p className="text-gray-500 dart-text">{t('total_revenue_this_month')}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="w-1/3 max-lg:w-1/2 max-sm:w-full">
                            <div className="pl-3 max-lg:pl-0 max-lg:pr-3 max-sm:p-0">
                                <div className="flex items-center p-6 mb-4 bg-white rounded-2xl shadow-3xl dark-container">
                                    <div className="pr-4 red text-3xl ">
                                        <AiOutlineTeam className="text-gray-800 dark-icon" />
                                    </div>
                                    <div className="">
                                        <span className="font-medium text-black dart-text">
                                            {totalThisYear !== undefined &&
                                                `${numberDecimalWithCommas(totalThisYear)}đ`}
                                        </span>
                                        <p className="text-gray-500 dart-text">{t('total_revenue_for_the_year')}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="p-1 md:p-3 lg:p-6 w-full  bg-white rounded-2xl mb-4 shadow-3xl dark-container">
                        <div className="text-center my-4">
                            <h3 className="text-base font-medium">{t('month_statistics_chart_revenue')}</h3>
                        </div>
                        <div className="w-full">
                            <ChartLineSumThisMonth dataThisMonth={listOrderThisMonth} />
                        </div>
                    </div>

                    <div className="bg-white rounded-2xl mb-4 shadow-3xl dark-container">
                        <div className="py-4 px-6">
                            <h3 className="text-base font-medium">{t('fonts_users_most_top')}</h3>
                        </div>
                        <VirtualList
                            data={user}
                            height={400}
                            itemHeight={30}
                            itemKey="email"
                            className=" border-t-[0.5px] border-t-[#ccc] p-6"
                            onScroll={onScroll}
                        >
                            {(item) => (
                                <ul key={item.email}>
                                    <li className="flex items-center justify-between py-3 border-b-[0.5px] border-b-[#ccc] max-sm:flex-col max-sm:items-start">
                                        <div className="flex items-center">
                                            <div className="w-10 h-10 mr-4  rounded-full">
                                                <img
                                                    src={item?.userInformation?.avatar || Vector}
                                                    alt="avatar"
                                                    className=" w-full h-full object-cover rounded-full max-sm:w-10 max-sm:h-10"
                                                />
                                            </div>
                                            <div>
                                                <h4 className="text-base font-medium pb-1">{item?.username}</h4>
                                                <p className="text-[14px] font-normal dart-text">{item?.email}</p>
                                            </div>
                                        </div>
                                        <div className="max-sm:ml-[54px] max-sm:py-1">
                                            <span className="text-[16px] font-light dart-text">
                                                {t('quantity')} : {item?.product?.length} font
                                            </span>
                                        </div>
                                    </li>
                                </ul>
                            )}
                        </VirtualList>
                    </div>

                    <div className="w-full flex mb-4 max-sm:flex-col gap-4 ">
                        <div className="w-2/4 max-sm:w-full max-sm:p-0  max-sm:mb-4">
                            <div className="p-1 md:p-3 lg:p-6 bg-white rounded-2xl shadow-3xl dark-container">
                                <div className="text-center max-sm:p-3">
                                    <h3 className="py-6 text-base font-medium">{t('user_level_list')}</h3>
                                </div>
                                <div>{listLevelUser.length > 0 && <ChartBarUser data={listLevelUser} />}</div>
                            </div>
                        </div>
                        <div className="w-2/4 max-sm:w-full max-sm:p-0">
                            <div className="p-1 md:p-3 lg:p-6 bg-white rounded-2xl shadow-3xl dark-container">
                                <div className="text-center max-sm:p-3">
                                    <h3 className="py-6 text-base font-medium">
                                        {t('total_statistics_chart_revenue')}
                                    </h3>
                                </div>
                                <div>
                                    <ChartLineSumYear dataThisYear={listOrderThisYear} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </>
    );
};

export default Home;
