import React from 'react';
import { useNavigate } from 'react-router-dom';
import Found404 from '../../resources/images/404.png';
import { useTranslation } from 'react-i18next';

const NotFound = () => {
    const { t } = useTranslation();
    const navigate = useNavigate();
    const homePage = () => {
        navigate('/');
    };

    return (
        <div className="min-h-screen px-4 py-24 flex items-center justify-center ">
            <div className="flex flex-col items-center justify-center gap-6">
                <div>
                    <img src={Found404} alt="" />
                </div>
                <div className="text-center">
                    <h1 className="my-2 text-[#fff] font-bold text-2xl">{t('404')}</h1>
                    <p className="my-2 text-[#fff]">{t('404_subtitle')}</p>
                </div>
                <button onClick={homePage} type="submit" className="bg-red-500 py-2 px-4 hover:shadow-4xl">
                    <span className="text-white text-sm">{t('go_homepage')}</span>
                </button>
            </div>
        </div>
    );
};

export default NotFound;
