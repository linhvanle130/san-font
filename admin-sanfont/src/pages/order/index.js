import React, { useEffect, useRef, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { useTranslation } from 'react-i18next';
import { Table, Tooltip, Select, Input, Button, Space } from 'antd';
import { FiChevronDown, FiChevronRight } from 'react-icons/fi';
import { AiOutlineDelete, AiOutlineEdit, AiOutlineInfo } from 'react-icons/ai';
import { IoIosSearch } from 'react-icons/io';
import moment from 'moment';

import orderApis from '@/api/orderApi';
import errorHelper from '@/utils/error-helper';
import Loading from '@/components/loading';
import yup from '@/utils/yup';
import { MASTER_DATA_NAME, STATUS_ORDER } from '@/constants';
import ModalOrderDelete from './modal-order-delete';
import ModalOrderChangeStatus from './modal-order-change-status';
import masterApis from '@/api/masterAps';

const Order = () => {
    const { t } = useTranslation();
    const navigate = useNavigate();
    const [search, setSearch] = useState(true);
    const [loading, setLoading] = useState(true);
    const [page, setPage] = useState(1);
    const account = JSON.parse(localStorage.getItem('info'));

    const searchInput = useRef(null);
    const refModalOrderDelete = useRef();
    const refModalOrderStatus = useRef();

    const [listOrder, setListOrder] = useState([]);
    const [masterOrderStatus, setMasterOrderStatus] = useState();
    console.log(listOrder);

    const fetchMasterData = async () => {
        const masterOrder = await masterApis.getAllMaster({
            idMaster: MASTER_DATA_NAME.STATUS_ORDER,
        });
        setMasterOrderStatus(masterOrder);
    };

    const schema = yup.object({
        search: yup.string().max(255),
    });

    const onSubmit = (values) => {
        const params = {
            fullName: values?.fullName,
        };
        setPage(1);
        return onGetListOrders(params);
    };

    const {
        control,
        handleSubmit,
        getValues,
        formState: { errors },
    } = useForm({
        resolver: yupResolver(schema),
    });

    const [searchText, setSearchText] = useState('');
    const [searchedColumn, setSearchedColumn] = useState('');

    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        setSearchText(selectedKeys[0]);
        setSearchedColumn(dataIndex);
    };

    const getColumnSearchProps = (dataIndex) => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm }) => (
            <div
                style={{
                    padding: 8,
                }}
            >
                <Input
                    ref={searchInput}
                    placeholder={`Tìm kiếm ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{
                        marginBottom: 8,
                        display: 'block',
                    }}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<IoIosSearch />}
                        size="small"
                        style={{
                            width: 100,
                            background: '#ff5e3a',
                        }}
                    >
                        Tìm kiếm
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: (filtered) => (
            <IoIosSearch
                style={{
                    color: filtered ? '#1890ff' : undefined,
                }}
            />
        ),
        onFilter: (value, record) => record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
        onFilterDropdownOpenChange: (visible) => {
            if (visible) {
                setTimeout(() => searchInput.current?.select(), 100);
            }
        },
        render: (text) => text,
    });

    const onGetListOrders = (values) => {
        const params = {
            ...values,
            page,
        };
        setLoading(true);

        orderApis
            .getAllOrder(params)
            .then(({ rows }) => {
                setListOrder(
                    rows.map((e) => {
                        return {
                            ...e,
                            price: e.orderPackage?.price,
                            pricePrevious: e.orderPackage?.pricePrevious,
                            note: e.orderPackage?.note,
                        };
                    }),
                );

                window.scroll({
                    top: 0,
                    behavior: 'smooth',
                });
            })
            .catch((err) => {
                errorHelper(err);
            })
            .finally(() => {
                setLoading(false);
            });
    };

    useEffect(() => {
        onGetListOrders();
        fetchMasterData();
    }, []);

    const handleViewOrder = (id) => {
        navigate(`/order-detail/${id}`);
    };
    const handleEditOrder = (id) => {
        navigate(`/order-update/${id}`);
    };

    const columns = [
        {
            title: t('name'),
            dataIndex: 'fullName',
            key: 'fullName',
            width: 200,
            ...getColumnSearchProps('fullName'),
            render: (text) => <Tooltip title={text}>{text}</Tooltip>,
            sorter: (a, b) => a.fullName?.localeCompare(b.fullName),
        },
        {
            title: t('email'),
            dataIndex: 'email',
            key: 'email',
            width: 200,
        },
        {
            title: t('order_code'),
            dataIndex: 'orderCode',
            key: 'orderCode',
            width: 200,
            ...getColumnSearchProps('orderCode'),
            sorter: (a, b) => a.orderCode?.localeCompare(b.orderCode),
        },
        {
            title: t('note'),
            dataIndex: 'note',
            key: 'note',
            width: 250,
            render: (text) => <Tooltip title={text}>{text}</Tooltip>,
        },
        {
            title: t('price'),
            dataIndex: 'price',
            key: 'price',
            width: 200,
            render: (item) => (item ? `${item.toFixed(1).replace(/\d(?=(\d{3})+\.)/g, '$&,')} đ ` : 0),
            sorter: (a, b) => a.price - b.price,
            align: 'center',
        },
        {
            title: t('price_sales'),
            dataIndex: 'pricePrevious',
            key: 'pricePrevious',
            width: 200,
            render: (item) => (item ? `${item.toFixed(1).replace(/\d(?=(\d{3})+\.)/g, '$&,')} đ ` : 0),
            sorter: (a, b) => a.pricePrevious - b.pricePrevious,
            align: 'center',
        },
        {
            title: t('active'),
            dataIndex: 'orderStatus',
            key: 'orderStatus',
            sorter: (a, b) => a.orderStatus - b.orderStatus,
            width: 300,
            render: (item, record) => (
                <Button
                    className="bg-green-300 cursor-pointer w-full"
                    onClick={(e) => refModalOrderStatus.current.onOpen(record.id)}
                    disabled={item === STATUS_ORDER.SUCCESS}
                >
                    {masterOrderStatus?.find((e) => e.id === item)?.name}
                </Button>
            ),
            align: 'center',
        },
        {
            title: t('address'),
            dataIndex: 'address',
            key: 'address',
            width: 300,
        },
        {
            title: t('created_at'),
            dataIndex: 'createdAt',
            key: 'created_at',
            width: 200,
            render: (item) => moment(item).format('YYYY-MM-DD HH:mm:ss'),
            sorter: (a, b) => a.createdAt?.localeCompare(b.createdAt),
            align: 'center',
        },
        {
            title: t('action'),
            key: 'action',
            render: (item, record) => (
                <div className="p-4 flex items-center justify-center gap-3">
                    <Tooltip title="View">
                        <button
                            onClick={() => handleViewOrder(item.orderCode)}
                            className="px-3 mt-2 py-[6px] bg-green-400 text-white rounded-2xl cursor-pointer hover:shadow-4xl hover:opacity-80"
                        >
                            <AiOutlineInfo />
                        </button>
                    </Tooltip>
                    {account.role === 1 && (
                        <>
                            <Tooltip title={t('update')}>
                                <button
                                    onClick={() => handleEditOrder(item.id)}
                                    className="px-3 mt-2 py-[6px] bg-main-color text-white rounded-2xl cursor-pointer hover:shadow-4xl hover:opacity-80"
                                >
                                    <AiOutlineEdit />
                                </button>
                            </Tooltip>
                            <Tooltip title={t('delete')}>
                                <button
                                    className=" px-3 mt-2 py-[6px] bg-red-500 text-white rounded-2xl cursor-pointer hover:shadow-4xl hover:opacity-80"
                                    onClick={() => refModalOrderDelete?.current?.onOpen(record.id, record)}
                                >
                                    <AiOutlineDelete />
                                </button>
                            </Tooltip>
                        </>
                    )}
                </div>
            ),
            fixed: 'right',
            align: 'center',
        },
    ];

    const handleTableChange = (pagination) => {
        setPage(pagination.current);
    };

    const renderSearch = () => (
        <div className="mb-4">
            <div className="bg-white rounded-sm shadow-3xl dark-container">
                <button
                    className="flex items-center px-4 py-3 cursor-pointer transition"
                    onClick={() => setSearch(!search)}
                >
                    {search ? <FiChevronDown /> : <FiChevronRight />}
                    <h3 className="pl-2 text-base font-bold">{t('search')}</h3>
                </button>
                {search && (
                    <div className="px-4 py-3">
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <div className="w-full flex gap-5 max-lg:flex-col max-lg:gap-3">
                                <div className="w-1/3 max-sm:w-full">
                                    <Controller
                                        name="fullName"
                                        control={control}
                                        render={({ field }) => (
                                            <Input {...field} type="text" placeholder={t('search_dot3')} />
                                        )}
                                    />
                                    {errors?.search?.message && (
                                        <p className="text-error mt-1">{errors?.search?.message}</p>
                                    )}
                                </div>
                            </div>

                            <div className="mt-5">
                                <button
                                    type="submit"
                                    className="flex items-center justify-center bg-main-color py-2 px-4  hover:shadow-4xl max-sm:w-[100px] max-sm:mt-0"
                                >
                                    <div> {loading && <Loading Loading />}</div>
                                    <span className="text-white text-sm">{t('search')}</span>
                                </button>
                            </div>
                        </form>
                    </div>
                )}
            </div>
        </div>
    );

    return (
        <div>
            {renderSearch()}
            <div className="flex justify-between py-4 max-sm:flex-col">
                <h2 className="mb-3 text-[20px] md:text-[24px] lg:text-[30px] text-black font-medium">
                    Quản lý đơn hàng
                </h2>
                <div className="flex gap-3">
                    <Link to={'/order-new'}>
                        <button
                            type="submit"
                            className="bg-main-color py-2 px-4 hover:shadow-4xl max-sm:float-right hover:opacity-80 transition-all"
                        >
                            <span className="text-white text-sm">Thêm đơn hàng</span>
                        </button>
                    </Link>
                </div>
            </div>
            <Table
                rowSelection={{
                    type: 'checkbox',
                }}
                dataSource={listOrder}
                rowKey="id"
                scroll={{ x: 'max-content' }}
                columns={columns}
                pagination={{
                    current: page,
                    position: ['bottomCenter'],
                }}
                loading={loading}
                onChange={handleTableChange}
            />
            <ModalOrderDelete
                ref={refModalOrderDelete}
                onSubmit={(id) => orderApis.deleteOrder(id)}
                onAfterDelete={() => {
                    onGetListOrders(getValues());
                }}
            />
            <ModalOrderChangeStatus
                ref={refModalOrderStatus}
                onAfterUpdate={() => {
                    onGetListOrders(getValues());
                }}
                account={account}
            />
        </div>
    );
};

export default Order;
