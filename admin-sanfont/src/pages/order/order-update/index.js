import React, { useEffect, useState } from 'react';
import { AiOutlineArrowLeft, AiOutlineCheck, AiOutlineReload } from 'react-icons/ai';
import { useForm, Controller } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router-dom';
import { yupResolver } from '@hookform/resolvers/yup';
import { useTranslation } from 'react-i18next';
import { Input, Select } from 'antd';

import { MASTER_DATA_NAME, PAYMENT_METHOD_MAP } from '@/constants';
import errorHelper from '@/utils/error-helper';
import successHelper from '@/utils/success-helper';
import yup from '@/utils/yup';
import Loading from '@/components/loading';
import userApis from '@/api/userApis';
import orderApis from '@/api/orderApi';
import orderPackageApis from '@/api/orderPackage';
import masterApis from '@/api/masterAps';

const OrderUpdate = () => {
    const { t } = useTranslation();
    const { Option } = Select;
    const schema = yup.object({
        userId: yup.number().required(),
        orderPackageId: yup.number().required(),
        paymentMethod: yup.string().required().nullable(),
        fullName: yup.string().max(255).required(),
        email: yup.string().max(255).required(),
        note: yup.string().trim().required(),
        telephone: yup.string().trim().required(),
        address: yup.string().max(255).required(),
        total: yup.number().required(),
        orderStatus: yup.number().required(),
    });

    const [loading, setLoading] = useState(false);
    const navigate = useNavigate();
    const { id } = useParams();

    const [listUsers, setListUsers] = useState([]);
    const getListUser = async () => {
        const users = await userApis.getListUsers();
        setListUsers(users);
    };

    const [listOrderPackage, setListOrderPackage] = useState([]);
    const [masterOrderStatus, setMasterOrderStatus] = useState();

    const getListOrderPackage = async () => {
        const orderPackages = await orderPackageApis.getAllOrderPackage();
        setListOrderPackage(orderPackages);
    };

    const fetchMasterData = async () => {
        const masterOrder = await masterApis.getAllMaster({
            idMaster: MASTER_DATA_NAME.STATUS_ORDER,
        });
        setMasterOrderStatus(masterOrder);
    };

    const {
        control,
        handleSubmit,
        formState: { errors },
        setValue,
    } = useForm({
        resolver: yupResolver(schema),
    });

    const submitUpdate = (values) => {
        const { orderPackageId, paymentMethod, note, telephone, address, orderStatus } = values;

        const payload = {
            id: +id,
            orderPackageId: orderPackageId,
            paymentMethod: paymentMethod,
            note: note,
            telephone: telephone,
            address: address,
            orderStatus: orderStatus,
        };
        return orderApis
            .updateOrder(payload)
            .then(() => {
                successHelper(t('update_success'));
                navigate('/order');
            })
            .catch((err) => {
                console.log(err);
                errorHelper(err);
            })
            .finally(() => {
                setLoading(false);
            });
    };

    useEffect(() => {
        getListUser();
        getListOrderPackage();
        fetchMasterData();
        orderApis
            .getDetailOrder(id)
            .then((res) => {
                setValue('userId', res.userId, {
                    shouldValidate: true,
                    shouldDirty: true,
                });
                setValue('orderPackageId', res.orderPackageId, {
                    shouldValidate: true,
                    shouldDirty: true,
                });
                setValue('paymentMethod', res.orderPayment?.paymentMethod, {
                    shouldValidate: true,
                    shouldDirty: true,
                });
                setValue('email', res.email, {
                    shouldValidate: true,
                    shouldDirty: true,
                });
                setValue('fullName', res.fullName, {
                    shouldValidate: true,
                    shouldDirty: true,
                });
                setValue('note', res.note, {
                    shouldValidate: true,
                    shouldDirty: true,
                });
                setValue('telephone', res.telephone, {
                    shouldValidate: true,
                    shouldDirty: true,
                });
                setValue('address', res.address, {
                    shouldValidate: true,
                    shouldDirty: true,
                });
                setValue('total', res.total, {
                    shouldValidate: true,
                    shouldDirty: true,
                });
                setValue('orderStatus', res.orderStatus, {
                    shouldValidate: true,
                    shouldDirty: true,
                });
            })
            .catch((err) => {
                console.log(err);
                errorHelper(err.message);
            })
            .finally(() => setLoading(false));
    }, []);

    const goBackListOrder = () => {
        navigate(`/order`);
    };

    const customStyle = {
        border: '1px solid red',
    };

    return (
        <div className="w-full max-sm:px-0 max-sm:py-2">
            <form onSubmit={handleSubmit(submitUpdate)}>
                <div className="flex justify-between max-lg:pr-3">
                    <div className="flex items-center">
                        <div onClick={() => goBackListOrder()}>
                            <AiOutlineArrowLeft className="mr-4 text-black hover:text-main-color text-lg cursor-pointer" />
                        </div>
                        <h2 className="text-[20px] md:text-[24px] lg:text-[30px] text-black font-medium">
                            Cập nhật đơn hàng
                        </h2>
                    </div>
                    <div className="flex gap-2 max-sm:hidden">
                        <button type="submit" className=" bg-main-color py-2 px-4 hover:shadow-4xl rounded-[2px]">
                            <div className="flex gap-2 items-center text-white">
                                <div>{loading && <Loading Loading />}</div>
                                <AiOutlineCheck />
                                <span className="text-sm">{t('update')}</span>
                            </div>
                        </button>
                        <button
                            onClick={() => {
                                goBackListOrder();
                            }}
                            type="reset"
                            className="bg-white py-2 px-4 border-[1px] border-[#ccc] rounded-[2px] hover:border-main-color hover:text-main-color"
                        >
                            <div className="flex gap-2 items-center">
                                <AiOutlineReload className="text-black" />
                                <span className="text-sm text-black">{t('back_list')}</span>
                            </div>
                        </button>
                    </div>
                </div>

                <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 mt-5 mb-[50px] md:mb-0 lg:mb-0">
                    <div className="md:col-span-1 lg:col-span-2 px-0 md:px-1 lg:px-3">
                        <div className="bg-white rounded dark-container">
                            <div className="p-6 flex items-center border-b-[1px] border-b-[#ccc]">
                                <h3 className="font-medium text-base">Thông tin gói thành viên</h3>
                                <div className="text-main-color">&nbsp;&nbsp;*</div>
                            </div>
                            <div className="flex flex-col gap-2 px-6 pt-6 pb-3">
                                <div className="mb-3">
                                    <h4 className="mb-2">Mã người dùng</h4>
                                    <Controller
                                        name="userId"
                                        control={control}
                                        render={({ field }) => (
                                            <Select
                                                {...field}
                                                showSearch
                                                disabled
                                                status={errors?.userId?.message}
                                                control={control}
                                                allowClear={true}
                                                name="userId"
                                                placeholder="Mã người dùng"
                                                optionFilterProp="children"
                                                filterOption={(input, option) =>
                                                    option.children.toLowerCase().includes(input.toLowerCase())
                                                }
                                                style={errors?.userId?.message && customStyle}
                                            >
                                                {listUsers?.rows?.map((e) => {
                                                    return (
                                                        <Option value={e.id} key={e.id}>
                                                            <div className="flex items-center gap-2">
                                                                <span>{e.id}</span>
                                                                <span>{e.username}</span>
                                                            </div>
                                                        </Option>
                                                    );
                                                })}
                                            </Select>
                                        )}
                                    />
                                    {errors?.userId?.message && (
                                        <p className="text-error mt-1">{errors?.userId?.message}</p>
                                    )}
                                </div>
                                <div className="mb-3">
                                    <h4 className="mb-2">Gói thành viên</h4>
                                    <Controller
                                        name="orderPackageId"
                                        control={control}
                                        render={({ field }) => (
                                            <Select
                                                {...field}
                                                showSearch
                                                status={errors?.orderPackageId?.message}
                                                control={control}
                                                allowClear={true}
                                                name="orderPackageId"
                                                placeholder="Gói thành viên"
                                                optionFilterProp="children"
                                                filterOption={(input, option) =>
                                                    option.children.toLowerCase().includes(input.toLowerCase())
                                                }
                                                style={errors?.orderPackageId?.message && customStyle}
                                            >
                                                {listOrderPackage?.rows?.map((e) => {
                                                    return (
                                                        <Option value={e.id} key={e.id}>
                                                            {e.name}
                                                        </Option>
                                                    );
                                                })}
                                            </Select>
                                        )}
                                    />
                                    {errors?.orderPackageId?.message && (
                                        <p className="text-error mt-1">{errors?.orderPackageId?.message}</p>
                                    )}
                                </div>
                                <div className="mb-3">
                                    <h4 className="mb-2">Phương thức thanh toán</h4>
                                    <Controller
                                        name="paymentMethod"
                                        control={control}
                                        render={({ field }) => (
                                            <Select
                                                {...field}
                                                showSearch
                                                status={errors?.paymentMethod?.message}
                                                control={control}
                                                allowClear={true}
                                                name="paymentMethod"
                                                placeholder="Phương thức thanh toán"
                                                optionFilterProp="children"
                                                filterOption={(input, option) =>
                                                    option.children.toLowerCase().includes(input.toLowerCase())
                                                }
                                                style={errors?.paymentMethod?.message && customStyle}
                                            >
                                                {PAYMENT_METHOD_MAP?.map((e) => {
                                                    return (
                                                        <Option value={e.value} key={e.value}>
                                                            {e.label}
                                                        </Option>
                                                    );
                                                })}
                                            </Select>
                                        )}
                                    />
                                    {errors?.paymentMethod?.message && (
                                        <p className="text-error mt-1">{errors?.paymentMethod?.message}</p>
                                    )}
                                </div>
                                <div className="mb-3">
                                    <h4 className="mb-2">Tình trạng đơn hàng</h4>
                                    <Controller
                                        name="orderStatus"
                                        control={control}
                                        render={({ field }) => (
                                            <Select
                                                {...field}
                                                showSearch
                                                status={errors?.orderStatus?.message}
                                                control={control}
                                                allowClear={true}
                                                name="orderStatus"
                                                placeholder="Tình trạng đơn hàng"
                                                optionFilterProp="children"
                                                filterOption={(input, option) =>
                                                    option.children.toLowerCase().includes(input.toLowerCase())
                                                }
                                                style={errors?.orderStatus?.message && customStyle}
                                            >
                                                {masterOrderStatus?.map((e) => {
                                                    return (
                                                        <Option value={e.id} key={e.id}>
                                                            {e.name}
                                                        </Option>
                                                    );
                                                })}
                                            </Select>
                                        )}
                                    />
                                    {errors?.orderStatus?.message && (
                                        <p className="text-error mt-1">{errors?.orderStatus?.message}</p>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="md:col-span-1 lg:col-span-2 px-0 md:px-1 lg:px-3">
                        <div className="bg-white rounded dark-container">
                            <div className="p-6 flex items-center border-b-[1px] border-b-[#ccc]">
                                <h3 className="font-medium text-base">Thông tin đơn hàng</h3>
                                <div className="text-main-color">&nbsp;&nbsp;*</div>
                            </div>
                            <div className="flex flex-col gap-2 px-6 py-3">
                                <div className="mb-3">
                                    <h4 className="mb-2">Tên người dùng</h4>
                                    <Controller
                                        name="fullName"
                                        control={control}
                                        render={({ field }) => (
                                            <Input
                                                {...field}
                                                disabled
                                                placeholder={t('name')}
                                                status={errors?.fullName?.message}
                                                style={errors?.fullName?.message && customStyle}
                                            />
                                        )}
                                    />
                                    {errors?.fullName?.message && (
                                        <p className="text-error mt-1">{errors?.fullName?.message}</p>
                                    )}
                                </div>
                                <div className="mb-3">
                                    <h4 className="mb-2">Email</h4>
                                    <Controller
                                        name="email"
                                        control={control}
                                        render={({ field }) => (
                                            <Input
                                                {...field}
                                                disabled
                                                placeholder={t('email')}
                                                status={errors?.email?.message}
                                                style={errors?.email?.message && customStyle}
                                            />
                                        )}
                                    />
                                    {errors?.email?.message && (
                                        <p className="text-error mt-1">{errors?.email?.message}</p>
                                    )}
                                </div>
                                <div className="mb-3">
                                    <h4 className="mb-2">{t('price')}</h4>
                                    <Controller
                                        name="total"
                                        control={control}
                                        render={({ field }) => (
                                            <Input
                                                {...field}
                                                disabled
                                                placeholder={t('price')}
                                                status={errors?.total?.message}
                                                style={errors?.total?.message && customStyle}
                                            />
                                        )}
                                    />
                                    {errors?.total?.message && (
                                        <p className="text-error mt-1">{errors?.total?.message}</p>
                                    )}
                                </div>
                                <div className="mb-3">
                                    <h4 className="mb-2">{t('phone')}</h4>
                                    <Controller
                                        name="telephone"
                                        control={control}
                                        render={({ field }) => (
                                            <Input
                                                {...field}
                                                placeholder={t('phone')}
                                                status={errors?.telephone?.message}
                                                style={errors?.telephone?.message && customStyle}
                                            />
                                        )}
                                    />
                                    {errors?.telephone?.message && (
                                        <p className="text-error mt-1">{errors?.telephone?.message}</p>
                                    )}
                                </div>
                                <div className="mb-3">
                                    <h4 className="mb-2">{t('address')}</h4>
                                    <Controller
                                        name="address"
                                        control={control}
                                        render={({ field }) => (
                                            <Input
                                                {...field}
                                                placeholder={t('address')}
                                                status={errors?.address?.message}
                                                style={errors?.address?.message && customStyle}
                                            />
                                        )}
                                    />
                                    {errors?.address?.message && (
                                        <p className="text-error mt-1">{errors?.address?.message}</p>
                                    )}
                                </div>
                                <div className="mb-3">
                                    <h4 className="mb-2">{t('note')}</h4>
                                    <Controller
                                        name="note"
                                        control={control}
                                        render={({ field }) => (
                                            <Input.TextArea
                                                {...field}
                                                rows={2}
                                                placeholder={t('note')}
                                                status={errors?.note?.message}
                                                style={errors?.note?.message && customStyle}
                                            />
                                        )}
                                    />
                                    {errors?.note?.message && (
                                        <p className="text-error mt-1">{errors?.note?.message}</p>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="flex justify-end p-3 max-sm:fixed max-sm:bottom-0 max-sm:left-0 max-sm:right-0 max-sm:justify-center max-sm:bg-white z-50 rounded ">
                    <div className="flex gap-2">
                        <button type="submit" className=" bg-main-color py-2 px-4 hover:shadow-4xl rounded-[2px]">
                            <div className="flex gap-2 items-center text-white">
                                <div>{loading && <Loading Loading />}</div>
                                <AiOutlineCheck />
                                <span className="text-sm">{t('update')}</span>
                            </div>
                        </button>
                        <button
                            onClick={() => {
                                goBackListOrder();
                            }}
                            type="reset"
                            className="bg-white py-2 px-4 border-[1px] border-[#ccc] rounded-[2px] hover:border-main-color hover:text-main-color"
                        >
                            <div className="flex gap-2 items-center">
                                <AiOutlineReload className="text-black" />
                                <span className="text-sm text-black">{t('back_list')}</span>
                            </div>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    );
};

export default OrderUpdate;
