import React, { useCallback, useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { AiOutlineArrowLeft } from 'react-icons/ai';
import { MdOutlineEmail } from 'react-icons/md';
import { FaRegUser } from 'react-icons/fa';
import moment from 'moment';

import { numberDecimalWithCommas } from '@/utils/funcs';
import { MASTER_DATA_NAME, PAYMENT_METHOD_MAP } from '@/constants';
import orderApis from '@/api/orderApi';
import Bank from '@/resources/images/bank.svg';
import Copy from '@/resources/images/copy.svg';
import masterApis from '@/api/masterAps';

const OrderView = () => {
    const { t } = useTranslation();
    const navigate = useNavigate();
    const { orderCode } = useParams();

    const [order, setOrder] = useState([]);
    const [masterOrderStatus, setMasterOrderStatus] = useState();

    const fetchMasterData = async () => {
        const masterOrder = await masterApis.getAllMaster({
            idMaster: MASTER_DATA_NAME.STATUS_ORDER,
        });
        setMasterOrderStatus(masterOrder);
    };

    useEffect(() => {
        fetchMasterData();
    }, []);

    const getOrderByCode = useCallback(async () => {
        const orderDetail = await orderApis.getSearchOrder({ orderCode });
        if (orderDetail.data === null) {
            navigate(`/not-found`);
        }
        setOrder(orderDetail);
    }, [navigate, orderCode]);

    const goBackListOrder = () => {
        navigate(`/order`);
    };

    useEffect(() => {
        getOrderByCode();
    }, [getOrderByCode]);

    return (
        <div className="p-7">
            <div className="flex items-center justify-center">
                <div className="px-3 w-[1250px]">
                    <div className="py-4 px-6">
                        <div className="flex items-center  my-1">
                            <div onClick={() => goBackListOrder()} className="cursor-pointer">
                                <AiOutlineArrowLeft className="mr-4 text-black hover:text-main-color text-lg" />
                            </div>
                            <h2 className="mr-3 text-lg text-black font-semibold">{t('order') + ': '}</h2>
                            <h2 className="mr-3 font-light dark-icon">#{order?.orderCode}</h2>
                        </div>
                        <div className="mt-3 bg-white rounded dark-container">
                            <div className="p-6">
                                <h3 className="mb-4 font-semibold uppercase">
                                    {masterOrderStatus?.find((e) => e.id === order?.orderStatus)?.name}
                                </h3>
                                <div className="px-8">
                                    <ul className="mb-7 flex">
                                        <li className="pr-8 mr-8 border-r-[1px] border-r-[#cfc8d8] border-dashed">
                                            <h3 className="uppercase">Mã đơn hàng:</h3>
                                            <strong className="font-bold">{order?.orderCode}</strong>
                                        </li>
                                        <li className="pr-8 mr-8 border-r-[1px] border-r-[#cfc8d8] border-dashed">
                                            <h3 className="uppercase">Ngày:</h3>
                                            <strong className="font-bold">
                                                {order?.orderDate &&
                                                    moment(order?.orderDate).format('DD-MM-YYYY HH:mm:ss')}
                                            </strong>
                                        </li>
                                        <li className="pr-8 mr-8 border-r-[1px] border-r-[#cfc8d8] border-dashed">
                                            <h3 className="uppercase">Email:</h3>
                                            <strong className="font-bold">{order?.email}</strong>
                                        </li>
                                        <li className="pr-8 mr-8 border-r-[1px] border-r-[#cfc8d8] border-dashed">
                                            <h3 className="uppercase">Tổng cộng:</h3>
                                            <strong className="font-bold">
                                                {numberDecimalWithCommas(Number(order?.orderPackage?.price))} ₫
                                            </strong>
                                        </li>
                                        <li className="pr-8 mr-8">
                                            <h3 className="uppercase">Phương thức thanh toán:</h3>
                                            <strong className="font-bold">
                                                {
                                                    PAYMENT_METHOD_MAP.find(
                                                        (e) => e.value === order?.orderPayment?.paymentMethod,
                                                    )?.label
                                                }
                                            </strong>
                                        </li>
                                    </ul>
                                    <div className="flex justify-center ">
                                        <div className="border-[1px] border-[#e2e2e2] border-solid rounded-xl">
                                            <div className="flex flex-col p-4">
                                                <h3 className="font-bold text-center">THÔNG TIN CHUYỂN KHOẢN</h3>
                                                <span className="text-center dart-text">
                                                    Hỗ trợ Ví điện tử MoMo/ZaloPay <br />
                                                    Hoặc ứng dụng ngân hàng để chuyển khoản nhanh 24/7
                                                </span>
                                                <div className="">
                                                    <h3 className="font-bold text-center uppercase">
                                                        {order.fullName}
                                                    </h3>
                                                    <table>
                                                        <tbody>
                                                            <tr>
                                                                <th className="px-4 py-3 dart-text">Ngân hàng: </th>
                                                                <th className="px-4 py-3 dart-text">
                                                                    <img src={Bank} alt="image-bank" />
                                                                </th>
                                                            </tr>
                                                            <tr>
                                                                <th className="px-4 py-3 dart-text">Tài Khoản: </th>
                                                                <th className="px-4 py-3 flex items-center dart-text">
                                                                    <span>0041000232130 </span>
                                                                    <button className="ml-2">
                                                                        <img
                                                                            src={Copy}
                                                                            alt="image-copy"
                                                                            className="w-[30px]"
                                                                        />
                                                                    </button>
                                                                </th>
                                                            </tr>
                                                            <tr>
                                                                <th className="px-4 py-3 dart-text">Số Tiền: </th>
                                                                <th className="px-4 py-3 flex items-center dart-text">
                                                                    <span>
                                                                        {numberDecimalWithCommas(
                                                                            Number(order?.orderPackage?.price),
                                                                        )}
                                                                        ₫
                                                                    </span>
                                                                    <button className="ml-2">
                                                                        <img
                                                                            src={Copy}
                                                                            alt="image-copy"
                                                                            className="w-[30px]"
                                                                        />
                                                                    </button>
                                                                </th>
                                                            </tr>
                                                            <tr>
                                                                <th className="px-4 py-3 dart-text">Nội Dung: </th>
                                                                <th className="px-4 py-3 flex items-center dart-text">
                                                                    <span className="border-[1px] border-main-color rounded px-1 text-main-color">
                                                                        {order?.orderCode}
                                                                    </span>
                                                                    <button className="ml-2">
                                                                        <img
                                                                            src={Copy}
                                                                            alt="image-copy"
                                                                            className="w-[30px]"
                                                                        />
                                                                    </button>
                                                                </th>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="px-8 mt-8">
                                    <h3 className="text-lg font-medium text-black mb-2">Chi tiết đơn hàng</h3>
                                    <div className="border-[1px] border-solid border-[#e2e2e2] rounded-md">
                                        <table className="w-full text-left">
                                            <thead>
                                                <tr>
                                                    <th className="font-bold py-2 px-3 leading-6 dart-text">
                                                        Sản phẩm
                                                    </th>
                                                    <th className="font-bold py-2 px-3 leading-6 dart-text">Tổng</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th className="py-2 px-3 font-medium leading-6 border-t-[1px] border-t-[#e2e2e2]">
                                                        <span className="text-[#0d6efd] ">
                                                            {order?.orderPackage?.name}
                                                        </span>
                                                    </th>
                                                    <th className="py-2 px-3 font-light leading-6 border-t-[1px] border-t-[#e2e2e2] dart-text dart-text">
                                                        {numberDecimalWithCommas(Number(order?.orderPackage?.price))} ₫
                                                    </th>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th className="py-2 px-3 font-medium leading-6 border-t-[1px] border-t-[#e2e2e2] dart-text">
                                                        Tổng số phụ:
                                                    </th>
                                                    <th className="py-2 px-3 font-medium leading-6 border-t-[1px] border-t-[#e2e2e2] dart-text">
                                                        {numberDecimalWithCommas(Number(order?.orderPackage?.price))} ₫
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <th className="py-2 px-3 font-medium leading-6 border-t-[1px] border-t-[#e2e2e2] dart-text">
                                                        Phương thức thanh toán:
                                                    </th>
                                                    <th className="py-2 px-3 font-medium leading-6 border-t-[1px] border-t-[#e2e2e2] dart-text">
                                                        {
                                                            PAYMENT_METHOD_MAP.find(
                                                                (e) => e.value === order?.orderPayment?.paymentMethod,
                                                            )?.label
                                                        }
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <th className="py-2 px-3 font-medium leading-6 border-t-[1px] border-t-[#e2e2e2] dart-text">
                                                        Tổng cộng:
                                                    </th>
                                                    <th className="py-2 px-3 font-medium leading-6 border-t-[1px] border-t-[#e2e2e2] dart-text">
                                                        {numberDecimalWithCommas(Number(order?.orderPackage?.price))} ₫
                                                    </th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <div className="px-8 mt-8">
                                    <h3 className="text-lg font-medium text-black mb-2">Địa chỉ thanh toán</h3>
                                    <div className="border-[1px] border-solid border-[#e2e2e2] rounded-md">
                                        <div className="w-full py-2 px-3">
                                            <div className="flex items-center gap-2 dart-text">
                                                <FaRegUser />
                                                <span>{order?.fullName}</span>
                                            </div>
                                            <div className="flex items-center gap-2 dart-text">
                                                <MdOutlineEmail />
                                                <span>{order?.email}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default OrderView;
