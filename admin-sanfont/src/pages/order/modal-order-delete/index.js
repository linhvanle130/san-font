import React, { forwardRef, useImperativeHandle, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Modal } from 'antd';
import Loading from '@/components/loading';
import errorHelper from '@/utils/error-helper';
import successHelper from '@/utils/success-helper';

const ModalOrderDelete = ({ onSubmit, onAfterDelete }, ref) => {
    const { t } = useTranslation();

    const [visible, setVisible] = useState(false);
    const [loading, setLoading] = useState(false);
    const [dataOrder, setDataOrder] = useState({
        id: null,
    });

    const onOpen = (id) => {
        setVisible(true);
        setDataOrder({ id });
    };

    useImperativeHandle(ref, () => ({
        onOpen,
    }));

    const onClose = () => {
        setVisible(false);
        setLoading(false);
        setDataOrder(null);
    };

    const submitOrderDelete = () => {
        setLoading(true);

        return onSubmit(dataOrder.id)
            .then(() => {
                successHelper(t('delete_success'));
                onClose();
                onAfterDelete();
            })
            .catch((err) => errorHelper(err))
            .finally(() => setLoading(false));
    };

    return (
        <Modal footer={null} open={visible} onCancel={onClose} maskClosable={false} destroyOnClose>
            <h3 className="text-center font-semibold text-black">Bạn có chắc chắn muốn xóa đơn hàng này không?</h3>
            <div className="mt-3 flex items-center justify-center gap-3">
                <button
                    onClick={() => submitOrderDelete()}
                    className="flex px-3 py-2 border-[1px] border-red-500  bg-red-500 text-white cursor-pointer rounded-sm hover:bg-red-400"
                >
                    <span>{loading && <Loading Loading />}</span>
                    <span>{t('approve')}</span>
                </button>
                <button
                    onClick={() => onClose()}
                    className="flex px-3 py-2 border-[1px] bg-white border-[#ccc] cursor-pointer  rounded-sm hover:text-red-500 hover:border-red-500"
                >
                    <div>{loading && <Loading Loading />}</div>
                    <span className="text-black">{t('close')}</span>
                </button>
            </div>
        </Modal>
    );
};

export default forwardRef(ModalOrderDelete);
