import React, { useState, useImperativeHandle, forwardRef } from 'react';
import { useTranslation } from 'react-i18next';
import { yupResolver } from '@hookform/resolvers/yup';
import { Controller, useForm } from 'react-hook-form';
import { Modal, Select } from 'antd';

import yup from '@/utils/yup';
import errorHelper from '@/utils/error-helper';
import successHelper from '@/utils/success-helper';
import Loading from '@/components/loading';
import orderApis from '@/api/orderApi';
import { MASTER_DATA_NAME } from '@/constants';
import masterApis from '@/api/masterAps';
import { useEffect } from 'react';

const schema = yup.object({
    orderStatus: yup.number().required(),
});

const ModalOrderChangeStatus = ({ onAfterUpdate, account }, ref) => {
    const { t } = useTranslation();
    const { Option } = Select;

    const [masterOrderStatus, setMasterOrderStatus] = useState();
    const fetchMasterData = async () => {
        const masterOrder = await masterApis.getAllMaster({
            idMaster: MASTER_DATA_NAME.STATUS_ORDER,
        });
        setMasterOrderStatus(masterOrder);
    };

    useEffect(() => {
        fetchMasterData();
    }, []);

    const {
        control,
        handleSubmit,
        formState: { errors },
        setValue,
    } = useForm({
        resolver: yupResolver(schema),
    });

    const [visible, setVisible] = useState(false);
    const [loading, setLoading] = useState(false);
    const [idDataOrderStatus, setIdDataOrderStatus] = useState(null);

    const onOpen = (id) => {
        setVisible(true);
        setLoading(true);
        setIdDataOrderStatus(id);
        orderApis
            .getDetailOrder(id)
            .then((res) => {
                const { orderStatus } = res;
                setValue('orderStatus', orderStatus, {
                    shouldValidate: true,
                    shouldDirty: true,
                });
            })
            .catch((err) => {
                errorHelper(err.message);
            })
            .finally(() => setLoading(false));
    };

    useImperativeHandle(ref, () => ({
        onOpen,
    }));

    const onClose = () => {
        setVisible(false);
        setLoading(false);
        setIdDataOrderStatus(null);
    };

    const submitOrderStatus = (values) => {
        const { orderStatus } = values;
        const payload = {
            id: idDataOrderStatus,
            orderStatus: orderStatus,
        };

        return orderApis
            .changeStatusOrder(payload)
            .then(() => {
                successHelper(t('update_success'));
                onClose();
                onAfterUpdate();
            })
            .catch((err) => {
                errorHelper(err);
            })
            .finally(() => setLoading(false));
    };

    const customStyle = {
        border: '1px solid red',
    };

    return (
        <Modal footer={null} open={visible} onCancel={onClose} maskClosable={false} destroyOnClose>
            <form onSubmit={handleSubmit(submitOrderStatus)}>
                <h3 className="font-semibold text-black mb-3">
                    {account.role === 1 ? 'Cập nhật tình trạng đơn hàng' : t('admin_role')}
                </h3>
                {account.role === 1 && (
                    <>
                        <div className="flex flex-col gap-4">
                            <div className="mb-3">
                                <h4 className="mb-2">Tình trạng đơn hàng</h4>
                                <Controller
                                    name="orderStatus"
                                    control={control}
                                    render={({ field }) => (
                                        <Select
                                            {...field}
                                            showSearch
                                            status={errors?.orderStatus?.message}
                                            control={control}
                                            allowClear={true}
                                            name="orderStatus"
                                            placeholder="Tình trạng đơn hàng"
                                            optionFilterProp="children"
                                            filterOption={(input, option) =>
                                                option.children.toLowerCase().includes(input.toLowerCase())
                                            }
                                            style={errors?.orderStatus?.message && customStyle}
                                        >
                                            {masterOrderStatus?.map((e) => {
                                                return (
                                                    <Option value={e.id} key={e.id}>
                                                        {e.name}
                                                    </Option>
                                                );
                                            })}
                                        </Select>
                                    )}
                                />
                                {errors?.orderStatus?.message && (
                                    <p className="text-error mt-1">{errors?.orderStatus?.message}</p>
                                )}
                            </div>
                        </div>

                        <div className="w-full flex items-center justify-center">
                            <button type="submit" className="flex bg-main-color py-2 px-4 hover:shadow-4xl">
                                <div>{loading && <Loading Loading />}</div>
                                <span className="text-white text-sm">{t('update')}</span>
                            </button>
                        </div>
                    </>
                )}
            </form>
        </Modal>
    );
};

export default forwardRef(ModalOrderChangeStatus);
