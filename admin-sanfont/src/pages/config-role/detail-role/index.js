import React, { useEffect, useRef, useState } from 'react';
import { Select, Table, Tooltip } from 'antd';
import { useTranslation } from 'react-i18next';
import { useForm } from 'react-hook-form';
import { AiOutlineDelete } from 'react-icons/ai';
import yup from '@/utils/yup';

import { MODULE_MAP } from '@/constants';
import { yupResolver } from '@hookform/resolvers/yup';
import successHelper from '@/utils/success-helper';
import aclApis from '@/api/aclApis';
import ModalConfigRoleDelete from '../modal-config-role-delete';

const DetailRole = ({ role }) => {
    const { Option } = Select;
    const { t } = useTranslation();
    const [detailGroup, setDetailGroup] = useState([]);
    const [chooseModule, setChooseModule] = useState();
    const [checkActions, setCheckActions] = useState([]);
    const [reloadTable, setReloadTable] = useState(false);
    const refModalDeleteRoleModule = useRef();

    const schema = yup.object({
        groupId: yup.number().required(),
        actions: yup.array().required(),
    });
    const {
        handleSubmit,
        formState: { errors },
        setValue,
    } = useForm({
        resolver: yupResolver(schema),
    });

    const handleSetValue = () => {
        setValue('groupId', role?.id);
    };

    const onSubmit = async (value) => {
        const body = {
            groupId: value.groupId,
            actionsId: value.actions,
        };
        try {
            await aclApis.createRoleModule(body);
            successHelper('Đã thêm quyền thành công');
        } catch (e) {
            console.log(e);
            errorHelper(e);
        } finally {
            setReloadTable((prv) => !prv);
            setChooseModule();
            setCheckActions([]);
            setValue('actions');
        }
    };

    const onChangeModule = async (moduleId) => {
        setChooseModule(moduleId);
        const listActions = await aclApis.getAclActionWithModuleId(moduleId);
        setCheckActions(
            listActions.map((action) => ({
                label: action.name,
                value: action.id,
            })),
        );
        setValue('actions', [listActions[0].id]);
    };

    const CreateGroupRoleForm = () => (
        <div className="bg-white dark-container">
            <div className="p-4 border-b-[1px] border-b-[#ccc]">
                <h4 className="font-medium text-base ">{t('role_create')}</h4>
            </div>
            <div className="px-4 py-3">
                <form onSubmit={handleSubmit(onSubmit)}>
                    <h4 className="mb-1">{t('role_function')}</h4>
                    <div className="w-full flex justify-between gap-5 max-sm:flex-col max-sm:gap-2">
                        <div className="w-1/3 max-sm:w-full">
                            <Select
                                showSearch
                                status={errors?.categoryId?.message}
                                name="moduleId"
                                placeholder={t('role_function')}
                                optionFilterProp="children"
                                onChange={onChangeModule}
                                value={chooseModule}
                                filterOption={(input, option) =>
                                    option.children.toLowerCase().includes(input.toLowerCase())
                                }
                            >
                                {MODULE_MAP?.map((e) => {
                                    return (
                                        <Option value={e.id} key={e.id}>
                                            {e.name}
                                        </Option>
                                    );
                                })}
                            </Select>
                            {errors?.name?.message && <p className="text-error mt-1">{errors?.name?.message}</p>}
                        </div>
                        <div className="flex items-center ">
                            <button
                                type="submit"
                                className={`mr-16 py-2 px-4 bg-main-color ${!chooseModule && 'bg-slate-300'}`}
                                disabled={!chooseModule}
                                onClick={handleSetValue}
                            >
                                <span className="text-white text-sm">{t('role_add')}</span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    );

    const fetchAcl = async () => {
        const aclDetailGroup = await aclApis.getAclGroup({ groupId: role.id });
        const mapAclDetailGroup = [];
        MODULE_MAP.map((mm) => {
            aclDetailGroup.map((dg) => {
                if (
                    dg.actions.moduleId === mm.id &&
                    !mapAclDetailGroup.find((e) => e.moduleId === dg.actions.moduleId)
                ) {
                    mapAclDetailGroup.push({
                        id: mm.id,
                        moduleId: mm.id,
                        moduleName: mm.name,
                        description: dg.actions?.modules?.description,
                        groupId: role.id,
                        actions: aclDetailGroup
                            .map((aclDetail) => {
                                if (aclDetail.actions.moduleId === mm.id) {
                                    return aclDetail.actions;
                                }
                            })
                            .filter((e) => e),
                    });
                }
            });
        });

        setDetailGroup(mapAclDetailGroup);
    };
    useEffect(() => {
        fetchAcl();
    }, [reloadTable]);

    const columns = [
        {
            title: t('role_function'),
            dataIndex: 'moduleName',
            key: 'moduleName',
            width: 400,
            align: 'center',
            sorter: (a, b) => a.moduleName?.localeCompare(b.moduleName),
        },
        {
            title: t('role_function_description'),
            dataIndex: 'description',
            key: 'description',
            width: 400,
            align: 'center',
            sorter: (a, b) => a.description?.localeCompare(b.description),
        },
        {
            title: t('action'),
            key: 'action',
            render: (item, record) => (
                <div className="p-4 flex items-center justify-center gap-3">
                    <Tooltip title={t('Delete')}>
                        <button
                            className=" px-3 mt-2 py-[6px] bg-red-500 text-white rounded-2xl cursor-pointer hover:shadow-4xl hover:opacity-80"
                            onClick={() => refModalDeleteRoleModule.current.onOpen(record)}
                        >
                            <AiOutlineDelete />
                        </button>
                    </Tooltip>
                </div>
            ),
            align: 'center',
            fixed: 'right',
            width: 100,
        },
    ];

    return (
        <div>
            <CreateGroupRoleForm />
            <div className=" bg-white mt-5 shadow-3xl mb-5">
                <Table
                    columns={columns}
                    dataSource={detailGroup}
                    rowKey="id"
                    bordered
                    pagination={false}
                    scroll={{ x: 'max-content' }}
                />
                {refModalDeleteRoleModule && (
                    <ModalConfigRoleDelete
                        ref={refModalDeleteRoleModule}
                        onSubmit={(body) => aclApis.deleteRoleModule(body)}
                        onAfterDelete={() => {
                            setReloadTable((prv) => !prv);
                        }}
                    />
                )}
            </div>
        </div>
    );
};

export default DetailRole;
