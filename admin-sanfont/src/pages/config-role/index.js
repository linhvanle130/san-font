import React, { useEffect, useState } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import { MASTER_DATA_NAME } from '@/constants';
import masterApis from '@/api/masterAps';
import DetailRole from './detail-role';
import { useTranslation } from 'react-i18next';

const ConfigRole = () => {
    const { t } = useTranslation();
    const [masterRoleUser, setMasterRoleUser] = useState();
    const fetchMasterData = async () => {
        const masterRole = await masterApis.getAllMaster({
            idMaster: MASTER_DATA_NAME.ROLE,
        });
        setMasterRoleUser(masterRole);
    };

    useEffect(() => {
        fetchMasterData();
    }, []);

    return (
        <>
            <div className="flex max-sm:flex-col">
                <h2 className="mb-3 text-[20px] md:text-[24px] lg:text-[30px] text-black font-medium">
                    {t('config_role')}
                </h2>
            </div>
            <div className="pt-3 ">
                <div className="bg-[#16213e] rounded">
                    <div className="p-6">
                        <Tabs defaultIndex={0}>
                            <TabList className="flex mb-4">
                                {masterRoleUser &&
                                    masterRoleUser.map((role) => (
                                        <Tab
                                            key={role.id}
                                            className="py-3 ml-8 text-white font-normal text-base cursor-pointer hover:text-main-color border-b-[1px] border-b-transparent hover:border-b-main-color"
                                        >
                                            {role.name}
                                        </Tab>
                                    ))}
                            </TabList>
                            {masterRoleUser &&
                                masterRoleUser.map((role) => (
                                    <TabPanel tab={role.name} key={role.id}>
                                        <DetailRole role={role} />
                                    </TabPanel>
                                ))}
                        </Tabs>
                    </div>
                </div>
            </div>
        </>
    );
};

export default ConfigRole;
