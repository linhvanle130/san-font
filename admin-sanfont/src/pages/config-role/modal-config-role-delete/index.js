import React, { useImperativeHandle, useState, forwardRef } from 'react';
import { useTranslation } from 'react-i18next';
import { Modal } from 'antd';
import successHelper from '@/utils/success-helper';
import errorHelper from '@/utils/error-helper';
import Loading from '@/components/loading';

const ModalConfigRoleDelete = ({ onSubmit, onAfterDelete }, ref) => {
    const { t } = useTranslation();
    const [visible, setVisible] = useState(false);
    const [loading, setLoading] = useState(false);
    const [actionsId, setActionsId] = useState([]);
    const [groupId, setGroupId] = useState();

    const onOpen = (roleGroup) => {
        setVisible(true);
        setGroupId(roleGroup.groupId);
        setActionsId(roleGroup.actions.map((actions) => actions.id));
    };

    useImperativeHandle(ref, () => ({
        onOpen,
    }));

    const onClose = () => {
        setVisible(false);
        setLoading(false);
        setGroupId();
        setActionsId([]);
    };

    const submitDeleteRoleModule = () => {
        const body = {
            actionsId,
            groupId,
        };
        setLoading(true);
        return onSubmit(body)
            .then(() => {
                successHelper(t('delete_success'));
                onClose();
                onAfterDelete();
            })
            .catch((err) => errorHelper(err))
            .finally(() => setLoading(false));
    };

    return (
        <Modal footer={null} open={visible} onCancel={onClose} maskClosable={false} destroyOnClose>
            <h3 className="font-semibold text-black">Bạn có muốn xoá quyền truy cập module này không ?</h3>
            <div className="mt-3 flex items-center justify-center gap-3">
                <button
                    onClick={() => submitDeleteRoleModule()}
                    className="flex px-3 py-2 border-[1px] border-red-500  bg-red-500 text-white cursor-pointer rounded-sm hover:bg-red-400"
                >
                    <span>{loading && <Loading Loading />}</span>
                    <span>{t('approve')}</span>
                </button>
                <button
                    onClick={() => onClose()}
                    className="flex px-3 py-2 border-[1px] border-[#ccc] cursor-pointer  rounded-sm hover:text-red-500 hover:border-red-500"
                >
                    <div>{loading && <Loading Loading />}</div>
                    <span> {t('close')}</span>
                </button>
            </div>
        </Modal>
    );
};

export default forwardRef(ModalConfigRoleDelete);
