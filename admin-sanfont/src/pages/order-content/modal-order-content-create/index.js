import React, { useState, useImperativeHandle, forwardRef } from 'react';
import { useTranslation } from 'react-i18next';
import { yupResolver } from '@hookform/resolvers/yup';
import { Controller, useForm } from 'react-hook-form';
import { Modal, Input, Select } from 'antd';

import yup from '@/utils/yup';
import errorHelper from '@/utils/error-helper';
import successHelper from '@/utils/success-helper';
import Loading from '@/components/loading';

const schema = yup.object({
    orderPackageId: yup.number().required(),
    content: yup.string().trim().required().max(255),
});

const ModalOrderContentCreate = ({ onSubmit, onAfterCreate, listName }, ref) => {
    const { t } = useTranslation();
    const { Option } = Select;

    const {
        control,
        handleSubmit,
        formState: { errors },
        reset,
    } = useForm({
        resolver: yupResolver(schema),
    });

    const [visible, setVisible] = useState(false);
    const [loading, setLoading] = useState(false);
    const [errorOrderContent, setErrorOrderContent] = useState('');

    const onOpen = () => {
        setVisible(true);
    };

    useImperativeHandle(ref, () => ({
        onOpen,
    }));

    const onClose = () => {
        setVisible(false);
        reset({
            orderPackageId: '',
            content: '',
        });
        setLoading(false);
        setErrorOrderContent('');
    };

    const submitOrderContentCreate = (values) => {
        const { orderPackageId, content } = values;

        const payload = {
            orderPackageId: orderPackageId,
            content: content.trim(),
        };

        setLoading(true);

        return onSubmit(payload)
            .then(() => {
                successHelper(t('create_success'));
                onClose();
                onAfterCreate();
            })
            .catch((err) => {
                if (err?.response?.data?.code && err?.response?.data?.code === 'PRODUCT_CATEGORY_IS_EXISTED') {
                    setErrorOrderContent('errors:PRODUCT_CATEGORY_IS_EXISTED');
                } else {
                    errorHelper(err);
                }
            })
            .finally(() => setLoading(false));
    };

    const customStyle = {
        border: '1px solid red',
    };

    return (
        <Modal footer={null} open={visible} onCancel={onClose} maskClosable={false} destroyOnClose>
            <form onSubmit={handleSubmit(submitOrderContentCreate)}>
                <h3 className="font-semibold text-black mb-3">{t('add_content')}</h3>

                <div className="flex flex-col gap-4">
                    <div className="mb-3">
                        <h4 className="mb-1">{t('code')}</h4>
                        <Controller
                            name="orderPackageId"
                            control={control}
                            render={({ field }) => (
                                <>
                                    <Select
                                        {...field}
                                        control={control}
                                        name="orderPackageId"
                                        placeholder={t('select_a_config_data')}
                                        showSearch
                                        allowClear
                                        optionFilterProp="children"
                                        filterOption={(input, option) =>
                                            option.children.toLowerCase().includes(input.toLowerCase())
                                        }
                                        style={errors?.orderPackageId?.message && customStyle}
                                    >
                                        {listName.map((e) => {
                                            return (
                                                <Option value={e.id} key={e.id}>
                                                    {e.name}
                                                </Option>
                                            );
                                        })}
                                    </Select>
                                </>
                            )}
                        />
                        {errors?.orderPackageId?.message && (
                            <p className="text-error mt-1">{errors?.orderPackageId?.message}</p>
                        )}
                    </div>
                    <div className="mb-3">
                        <h4 className="mb-1">{t('content')}</h4>
                        <Controller
                            name="content"
                            control={control}
                            render={({ field }) => (
                                <Input.TextArea
                                    {...field}
                                    rows={6}
                                    status={errors?.content?.message ? 'error' : null}
                                    placeholder={t('content')}
                                    style={errors?.content?.message && customStyle}
                                />
                            )}
                        />
                        {errors?.content?.message && <p className="text-error mt-1">{errors?.content?.message}</p>}
                    </div>
                </div>

                <div className="w-full flex items-center justify-center">
                    <button type="submit" className="flex bg-main-color py-2 px-4 hover:shadow-4xl">
                        <div>{loading && <Loading Loading />}</div>
                        <span className="text-white text-sm">{t('create')}</span>
                    </button>
                </div>
            </form>
        </Modal>
    );
};

export default forwardRef(ModalOrderContentCreate);
