import React, { useState, useImperativeHandle, forwardRef } from 'react';
import { useTranslation } from 'react-i18next';
import { yupResolver } from '@hookform/resolvers/yup';
import { Controller, useForm } from 'react-hook-form';
import { Modal, Input, Select } from 'antd';

import yup from '@/utils/yup';
import errorHelper from '@/utils/error-helper';
import successHelper from '@/utils/success-helper';
import Loading from '@/components/loading';
import orderContentApis from '@/api/orderContentApis';

const schema = yup.object({
    orderPackageId: yup.number().required(),
    content: yup.string().trim().required().max(255),
});

const ModalOrderContentUpdate = ({ onAfterUpdate, account, listName }, ref) => {
    const { t } = useTranslation();
    const { Option } = Select;

    const {
        control,
        handleSubmit,
        formState: { errors },
        setValue,
        reset,
    } = useForm({
        resolver: yupResolver(schema),
    });

    const [visible, setVisible] = useState(false);
    const [loading, setLoading] = useState(false);
    const [orderContent, setOrderContent] = useState();
    const [errorOrderContent, setErrorOrderContent] = useState('');

    const onOpen = (record) => {
        setVisible(true);
        setLoading(true);
        setOrderContent(record);
        orderContentApis
            .getAllOrderContent({ id: record.id, idContent: record.idContent })
            .then((res) => {
                const { idContent, content } = res[0];
                setValue('orderPackageId', idContent, {
                    shouldValidate: true,
                    shouldDirty: true,
                });
                setValue('content', content, {
                    shouldValidate: true,
                    shouldDirty: true,
                });
            })
            .catch((err) => {
                errorHelper(err.message);
            })
            .finally(() => setLoading(false));
    };

    useImperativeHandle(ref, () => ({
        onOpen,
    }));

    const onClose = () => {
        setVisible(false);
        setLoading(false);
        reset();
        setOrderContent(null);
        setErrorOrderContent('');
    };

    const submitUpdateContentPackage = (values) => {
        const { orderPackageId, content } = values;
        const payload = {
            id: orderContent.id,
            idContent: +orderPackageId,
            orderPackageId: orderPackageId,
            content: content,
        };

        return orderContentApis
            .updateOrderContent(payload)
            .then(() => {
                successHelper(t('update_success'));
                onClose();
                onAfterUpdate();
            })
            .catch((err) => {
                console.log(err);
                errorHelper(err);
            })
            .finally(() => setLoading(false));
    };

    const customStyle = {
        border: '1px solid red',
    };

    return (
        <Modal footer={null} open={visible} onCancel={onClose} maskClosable={false} destroyOnClose>
            <form onSubmit={handleSubmit(submitUpdateContentPackage)}>
                <h3 className="font-semibold text-black mb-3">
                    {account.role === 1 ? t('content_update') : t('admin_role')}
                </h3>

                <div className="flex flex-col gap-4">
                    <div className="mb-3 hidden">
                        <h4 className="mb-1">{t('code')}</h4>
                        <Controller
                            name="orderPackageId"
                            control={control}
                            render={({ field }) => (
                                <>
                                    <Select
                                        {...field}
                                        control={control}
                                        name="orderPackageId"
                                        placeholder={t('select_a_config_data')}
                                        showSearch
                                        allowClear
                                        optionFilterProp="children"
                                    >
                                        {listName.map((e) => {
                                            return (
                                                <Option value={e.id} key={e.id}>
                                                    {e.name}
                                                </Option>
                                            );
                                        })}
                                    </Select>
                                </>
                            )}
                        />
                        {errors?.orderPackageId?.message && (
                            <p className="text-error mt-1">{errors?.orderPackageId?.message}</p>
                        )}
                    </div>
                    <div className="mb-3">
                        <h4 className="mb-1">{t('content')}</h4>
                        <Controller
                            name="content"
                            control={control}
                            render={({ field }) => (
                                <Input.TextArea
                                    {...field}
                                    rows={3}
                                    status={errors?.content?.message ? 'error' : null}
                                    placeholder={t('content')}
                                    style={errors?.content?.message && customStyle}
                                />
                            )}
                        />
                        {errors?.content?.message && <p className="text-error mt-1">{errors?.content?.message}</p>}
                    </div>
                </div>

                <div className="w-full flex items-center justify-center">
                    {account.role === 1 && (
                        <button type="submit" className="flex bg-main-color py-2 px-4 hover:shadow-4xl">
                            <div>{loading && <Loading Loading />}</div>
                            <span className="text-white text-sm">{t('update')}</span>
                        </button>
                    )}
                </div>
            </form>
        </Modal>
    );
};

export default forwardRef(ModalOrderContentUpdate);
