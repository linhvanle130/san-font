import React, { useState, useImperativeHandle, forwardRef } from 'react';
import { useTranslation } from 'react-i18next';
import successHelper from '@/utils/success-helper';
import errorHelper from '@/utils/error-helper';
import Loading from '@/components/loading';
import { Modal } from 'antd';

const ModalOrderContentDelete = ({ onSubmit, onAfterDelete }, ref) => {
    const { t } = useTranslation();
    const [visible, setVisible] = useState(false);
    const [loading, setLoading] = useState(false);
    const [orderContent, setOrderContent] = useState({
        id: null,
    });

    const onOpen = (id) => {
        setVisible(true);
        setOrderContent({ id });
    };

    useImperativeHandle(ref, () => ({
        onOpen,
    }));

    const onClose = () => {
        setVisible(false);
        setLoading(false);
        setOrderContent(null);
    };

    const submitOrderContentDelete = () => {
        setLoading(true);
        return onSubmit(orderContent.id)
            .then(() => {
                successHelper(t('delete_success'));
                onClose();
                onAfterDelete();
            })
            .catch((err) => errorHelper(err))
            .finally(() => setLoading(false));
    };

    return (
        <Modal footer={null} open={visible} onCancel={onClose} maskClosable={false} destroyOnClose>
            <h3 className="font-semibold text-black">{t('content_delete_confirm')}</h3>

            <div className="mt-3 flex items-center justify-center gap-3">
                <button
                    type="submit"
                    onClick={() => submitOrderContentDelete()}
                    className="flex px-3 py-2 border-[1px] border-slate-400 bg-slate-400 text-white rounded-sm"
                >
                    <span>{loading && <Loading Loading />}</span>
                    <span>{t('approve')}</span>
                </button>
                <button
                    onClick={() => onClose()}
                    className="flex px-3 py-2 border-[1px] border-[#ccc] cursor-pointer  rounded-sm hover:text-red-500 hover:border-red-500"
                >
                    <div>{loading && <Loading Loading />}</div>
                    <span> {t('close')}</span>
                </button>
            </div>
        </Modal>
    );
};

export default forwardRef(ModalOrderContentDelete);
