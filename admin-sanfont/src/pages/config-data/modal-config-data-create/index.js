import React, { forwardRef, useImperativeHandle, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import { yupResolver } from '@hookform/resolvers/yup';
import { MASTER_DATA } from '@/constants';
import successHelper from '@/utils/success-helper';
import errorHelper from '@/utils/error-helper';
import yup from '@/utils/yup';
import Loading from '@/components/loading';
import { Modal, Select, Input } from 'antd';

const schema = yup.object({
    name: yup.string().trim().required().max(255),
    nameMaster: yup.string().trim().required().max(255).nullable(),
    note: yup.string().trim().required().nullable(),
});

const ModalConfigDataCreate = ({ onSubmit, onAfterCreate }, ref) => {
    const { t } = useTranslation();
    const { Option } = Select;

    const {
        control,
        handleSubmit,
        formState: { errors },
        reset,
    } = useForm({
        resolver: yupResolver(schema),
    });

    const [visible, setVisible] = useState(false);
    const [loading, setLoading] = useState(false);
    const [errorConfigData, setErrorConfigData] = useState('');
    const onOpen = () => {
        setVisible(true);
    };

    useImperativeHandle(ref, () => ({
        onOpen,
    }));

    const onClose = () => {
        setVisible(false);
        reset({
            name: '',
            nameMaster: null,
            note: '',
        });
        setLoading(false);
        setErrorConfigData('');
    };

    const submitConfigDataCreate = (values) => {
        const { name, nameMaster, note } = values;

        const payload = {
            name: name,
            nameMaster: MASTER_DATA.find((e) => e.value === +nameMaster)?.nameMaster,
            note: note,
        };

        setLoading(true);

        return onSubmit(payload)
            .then(() => {
                successHelper(t('create_success'));
                onClose();
                onAfterCreate();
            })
            .catch((err) => {
                errorHelper(err);
            })
            .finally(() => setLoading(false));
    };

    const customStyle = {
        border: '1px solid red',
    };

    return (
        <Modal footer={null} open={visible} onCancel={onClose} maskClosable={false} destroyOnClose>
            <form onSubmit={handleSubmit(submitConfigDataCreate)}>
                <h3 className="font-semibold text-black mb-3">{t('config_data_add')}</h3>

                <div className="mb-3">
                    <h4 className="mb-1">{t('code')}</h4>
                    <Controller
                        name="nameMaster"
                        control={control}
                        render={({ field }) => (
                            <>
                                <Select
                                    {...field}
                                    control={control}
                                    name="nameMaster"
                                    placeholder={t('select_a_config_data')}
                                    showSearch
                                    allowClear
                                    optionFilterProp="children"
                                    filterOption={(input, option) =>
                                        option.children.toLowerCase().includes(input.toLowerCase())
                                    }
                                    style={errors?.nameMaster?.message && customStyle}
                                >
                                    {MASTER_DATA.map((e) => {
                                        return (
                                            <Option value={e.value} key={e.value}>
                                                {e.nameMaster}
                                            </Option>
                                        );
                                    })}
                                </Select>
                            </>
                        )}
                    />
                    {errors?.nameMaster?.message && <p className="text-error mt-1">{errors?.nameMaster?.message}</p>}
                </div>
                <div className="mb-3">
                    <h4 className="mb-1">{t('name')}</h4>
                    <Controller
                        name="name"
                        control={control}
                        render={({ field }) => (
                            <Input
                                {...field}
                                status={errors?.name?.message ? 'error' : null}
                                placeholder={t('name')}
                                style={errors?.name?.message && customStyle}
                            />
                        )}
                    />
                    {errors?.name?.message && <p className="text-error mt-1">{errors?.name?.message}</p>}
                </div>
                <div className="mb-3">
                    <h4 className="mb-1">{t('note')}</h4>
                    <Controller
                        name="note"
                        control={control}
                        render={({ field }) => (
                            <Input.TextArea
                                {...field}
                                rows={6}
                                status={errors?.note?.message ? 'error' : null}
                                placeholder={t('note')}
                                style={errors?.note?.message && customStyle}
                            />
                        )}
                    />
                    {errors?.note?.message && <p className="text-error mt-1">{errors?.note?.message}</p>}
                </div>

                <div className="flex items-center justify-center">
                    <button type="submit" className="flex bg-main-color py-2 px-4 hover:shadow-4xl">
                        <div>{loading && <Loading Loading />}</div>
                        <span className="text-white text-sm">{t('create')}</span>
                    </button>
                </div>
            </form>
        </Modal>
    );
};

export default forwardRef(ModalConfigDataCreate);
