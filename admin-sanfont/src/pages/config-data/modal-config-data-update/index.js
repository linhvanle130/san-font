import React, { forwardRef, useImperativeHandle, useState } from 'react';
import { yupResolver } from '@hookform/resolvers/yup';
import { Controller, useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import { MASTER_DATA } from '@/constants';
import successHelper from '@/utils/success-helper';
import errorHelper from '@/utils/error-helper';
import yup from '@/utils/yup';
import Loading from '@/components/loading';
import { Modal, Select, Input } from 'antd';
import masterApis from '@/api/masterAps';

const schema = yup.object({
    name: yup.string().trim().required().max(255),
    nameMaster: yup.string().trim().required().max(255),
    note: yup.string().trim().required().nullable(),
});

const ModalConfigDataUpdate = ({ onAfterUpdate }, ref) => {
    const { t } = useTranslation();
    const { Option } = Select;

    const {
        control,
        handleSubmit,
        formState: { errors },
        setValue,
        reset,
    } = useForm({
        resolver: yupResolver(schema),
    });

    const [visible, setVisible] = useState(false);
    const [loading, setLoading] = useState(false);
    const [dataConfigData, setDataConfigData] = useState();
    const [errorConfigData, setErrorConfigData] = useState('');

    const onOpen = (record) => {
        setVisible(true);
        setLoading(true);
        setDataConfigData(record);
        masterApis
            .getAllMaster({ id: record.id, idMaster: record.idMaster })
            .then((res) => {
                const { name, idMaster, note } = res[0];
                setValue('name', name, {
                    shouldValidate: true,
                    shouldDirty: true,
                });
                setValue('nameMaster', idMaster, {
                    shouldValidate: true,
                    shouldDirty: true,
                });
                setValue('note', note, {
                    shouldValidate: true,
                    shouldDirty: true,
                });
            })
            .catch((err) => {
                errorHelper(err.message);
            })
            .finally(() => setLoading(false));
    };

    useImperativeHandle(ref, () => ({
        onOpen,
    }));

    const onClose = () => {
        setVisible(false);
        setLoading(false);
        reset();
        setDataConfigData(null);
        setErrorConfigData('');
    };

    const submitUpdateConfigData = (values) => {
        const { name, nameMaster, note } = values;
        const payload = {
            id: dataConfigData.id,
            idMaster: +nameMaster,
            name: name,
            nameMaster: MASTER_DATA.find((e) => e.value === +nameMaster)?.nameMaster,
            note: note,
        };

        return masterApis
            .updateMaster(payload)
            .then(() => {
                successHelper(t('update_success'));
                onClose();
                onAfterUpdate();
            })
            .catch((err) => {
                console.log(err);
                errorHelper(err);
            })
            .finally(() => setLoading(false));
    };

    const customStyle = {
        border: '1px solid red',
    };

    return (
        <Modal footer={null} open={visible} onCancel={onClose} maskClosable={false} destroyOnClose>
            <h4 className="font-semibold text-black mb-3">{t('config_data_update')}</h4>
            <form onSubmit={handleSubmit(submitUpdateConfigData)}>
                <div className="mb-3">
                    <h4 className="mb-1">{t('code')}</h4>
                    <Controller
                        name="nameMaster"
                        control={control}
                        render={({ field }) => (
                            <Select
                                {...field}
                                disabled
                                control={control}
                                name="nameMaster"
                                placeholder={t('select_a_config_data')}
                                style={errors?.name?.message && customStyle}
                            >
                                {MASTER_DATA.map((e) => {
                                    return (
                                        <Option value={e.value} key={e.value}>
                                            {e.nameMaster}
                                        </Option>
                                    );
                                })}
                            </Select>
                        )}
                    />
                    {errors?.nameMaster?.message && <p className="text-error mt-1">{errors?.nameMaster?.message}</p>}
                </div>

                <div className="mb-3">
                    <h4 className="mb-1">{t('name')}</h4>
                    <Controller
                        name="name"
                        control={control}
                        render={({ field }) => (
                            <Input
                                {...field}
                                status={errors?.name?.message ? 'error' : null}
                                placeholder={t('name')}
                                style={errors?.name?.message && customStyle}
                            />
                        )}
                    />
                    {errors?.name?.message && <p className="text-error mt-1">{errors?.name?.message}</p>}
                </div>
                <div className="mb-3">
                    <h4 className="mb-1">{t('note')}</h4>
                    <Controller
                        name="note"
                        control={control}
                        render={({ field }) => (
                            <Input.TextArea
                                {...field}
                                rows={6}
                                status={errors?.note?.message ? 'error' : null}
                                placeholder={t('note')}
                                style={errors?.note?.message && customStyle}
                            />
                        )}
                    />
                    {errors?.note?.message && <p className="text-error mt-1">{errors?.note?.message}</p>}
                </div>

                <div className="mt-3 flex flex-col w-full items-center">
                    <button
                        type="submit"
                        className="flex items-center justify-center bg-main-color py-2 px-4 hover:shadow-4xl w-1/4"
                    >
                        <span>{loading && <Loading Loading />}</span>
                        <span className="text-white text-sm">{t('update')}</span>
                    </button>
                </div>
            </form>
        </Modal>
    );
};

export default forwardRef(ModalConfigDataUpdate);
