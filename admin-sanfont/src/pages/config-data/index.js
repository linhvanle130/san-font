import React, { useCallback, useEffect, useRef, useState } from 'react';
import { Table, Tooltip } from 'antd';
import { useTranslation } from 'react-i18next';
import masterApis from '@/api/masterAps';
import { MASTER_DATA } from '@/constants';
import errorHelper from '@/utils/error-helper';
import { useForm } from 'react-hook-form';
import { AiOutlineDelete, AiOutlineEdit } from 'react-icons/ai';
import ModalConfigDataUpdate from './modal-config-data-update';
import ModalConfigDataCreate from './modal-config-data-create';
import ModalConfigDataDelete from './modal-config-data-delete';

const LIMIT = 100;
const ConfigData = () => {
    const { t } = useTranslation();

    const refModalConfigDataCreate = useRef();
    const refModalConfigDataUpdate = useRef();
    const refModalConfigDataDelete = useRef();

    const { getValues } = useForm();

    const [listConfigData, setListConfigData] = useState([]);
    console.log(listConfigData);
    const [countListConfigData, setCountListConfigData] = useState([]);
    const [loading, setLoading] = useState(true);
    const [page, setPage] = useState(1);

    const expandedRowRender = (row) => {
        const columns = [
            {
                title: t('config_code'),
                dataIndex: 'nameMaster',
                key: 'nameMaster',
                width: 100,
            },
            {
                title: t('name'),
                key: 'name',
                dataIndex: 'name',
                render: (text) => <Tooltip title={t('update') + ': ' + text}>{text}</Tooltip>,
                width: 200,
            },
            {
                title: t('note'),
                key: 'note',
                dataIndex: 'note',
                render: (text) => <Tooltip title={text}>{text}</Tooltip>,
                width: 200,
            },
            {
                title: t('action'),
                key: 'action',
                render: (item, record) => (
                    <div className="p-4 flex items-center justify-center gap-3">
                        <Tooltip title={t('update')}>
                            <button
                                onClick={() => refModalConfigDataUpdate?.current?.onOpen(record)}
                                className="px-3 mt-2 py-[6px] bg-main-color text-white rounded-2xl cursor-pointer hover:shadow-4xl hover:opacity-80"
                            >
                                <AiOutlineEdit />
                            </button>
                        </Tooltip>
                        <Tooltip title={t('Delete')}>
                            <button
                                className=" px-3 mt-2 py-[6px] bg-red-500 text-white rounded-2xl cursor-pointer hover:shadow-4xl hover:opacity-80"
                                onClick={() => refModalConfigDataDelete?.current?.onOpen(record.id, item)}
                            >
                                <AiOutlineDelete />
                            </button>
                        </Tooltip>
                    </div>
                ),
                align: 'center',
                fixed: 'right',
                width: 100,
            },
        ];

        return <Table columns={columns} dataSource={row.childrenMaster} pagination={false} rowKey="id" />;
    };

    const columns = [
        {
            title: t('config_code'),
            dataIndex: 'nameMaster',
            key: 'nameMaster',
        },
    ];

    const onGetListConfigData = useCallback(async () => {
        const params = {
            page,
            size: LIMIT,
        };

        setLoading(true);

        return masterApis
            .getListPagingMaster(params)
            .then(({ rows, count }) => {
                if (page > 1 && rows.length === 0) {
                    setPage(1);
                } else {
                    setCountListConfigData(count);
                    setListConfigData(
                        MASTER_DATA.map((md) => {
                            if (rows.filter((row) => row.idMaster === md.value).length > 0) {
                                return {
                                    idMaster: md.value,
                                    nameMaster: md.nameMaster,
                                    childrenMaster: rows.filter((row) => row.idMaster === md.value),
                                };
                            }
                        }).filter((e) => e),
                    );
                }

                window.scroll({
                    top: 0,
                    behavior: 'smooth',
                });
            })
            .catch((err) => {
                errorHelper(err);
            })
            .finally(() => {
                setLoading(false);
            });
    }, [page]);

    useEffect(() => {
        onGetListConfigData(getValues());
    }, [onGetListConfigData]);

    return (
        <div>
            <div className="flex justify-between py-4 max-sm:flex-col">
                <h2 className="mb-3 text-[20px] md:text-[24px] lg:text-[30px] text-black font-medium">
                    {t('config_data')}
                </h2>
                <button
                    type="submit"
                    onClick={() => refModalConfigDataCreate.current.onOpen()}
                    className=" bg-main-color py-2 px-4 hover:shadow-4xl"
                >
                    <span className="text-white text-sm">{t('config_data_add')}</span>
                </button>
            </div>
            <Table
                columns={columns}
                dataSource={listConfigData}
                rowKey="idMaster"
                expandable={{
                    expandedRowRender,
                    expandRowByClick: true,
                }}
                scroll={{ x: 'max-content' }}
                pagination={false}
                loading={loading}
            />
            <ModalConfigDataCreate
                ref={refModalConfigDataCreate}
                onSubmit={(payload) => masterApis.createMaster(payload)}
                onAfterCreate={() => {
                    onGetListConfigData(getValues());
                }}
            />
            <ModalConfigDataUpdate
                ref={refModalConfigDataUpdate}
                onAfterUpdate={() => {
                    onGetListConfigData();
                }}
            />
            <ModalConfigDataDelete
                ref={refModalConfigDataDelete}
                onSubmit={(id) => masterApis.deleteMaster(id)}
                onAfterDelete={() => {
                    onGetListConfigData(getValues());
                }}
            />
        </div>
    );
};

export default ConfigData;
