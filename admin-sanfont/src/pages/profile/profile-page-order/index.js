import React, { useEffect, useRef, useState } from 'react';
import { AiOutlineDelete, AiOutlineEdit } from 'react-icons/ai';
import { Tooltip, Table, Button } from 'antd';
import { useTranslation } from 'react-i18next';
import moment from 'moment';
import { useForm } from 'react-hook-form';
import ModalOrderDelete from '@/pages/order/modal-order-delete';
import orderApis from '@/api/orderApi';
import { numberDecimalWithCommas } from '@/utils/funcs';
import { MASTER_DATA_NAME, STATUS_ORDER } from '@/constants';
import masterApis from '@/api/masterAps';
import { useNavigate } from 'react-router-dom';

const ProfilePageOrder = ({ profile }) => {
    const { t } = useTranslation();

    const refModalOrderDelete = useRef();
    const { getValues } = useForm();
    const navigate = useNavigate();

    const [loading, setLoading] = useState(true);
    const [page, setPage] = useState(1);

    const [masterOrderStatus, setMasterOrderStatus] = useState();

    const fetchMasterData = async () => {
        const masterOrder = await masterApis.getAllMaster({
            idMaster: MASTER_DATA_NAME.STATUS_ORDER,
        });
        setMasterOrderStatus(masterOrder);
    };

    const orderData = {
        getList: () => profile?.order,
    };
    const [listOrder, setListOrder] = useState([]);
    const data = listOrder.map((e) => {
        return {
            ...e,
            orderName: e.orderPackage?.name,
            orderPrice: e.orderPackage?.price,
        };
    });

    console.log(data);

    const onGetListOrder = async () => {
        const fetchOrder = await orderData.getList();
        setListOrder(fetchOrder);
        setLoading(false);
    };

    useEffect(() => {
        onGetListOrder();
        fetchMasterData();
    }, []);

    const handleEditOrder = (id) => {
        navigate(`/order-update/${id}`);
    };

    const columns = [
        {
            title: t('order_name'),
            dataIndex: 'orderName',
            key: 'orderName',
            width: 100,
            sorter: (a, b) => a.orderName?.localeCompare(b.orderName),
        },
        {
            title: t('price'),
            dataIndex: 'orderPrice',
            key: 'orderPrice',
            width: 200,
            render: (total) => numberDecimalWithCommas(total) + ' đ',
            sorter: (a, b) => a.orderPrice - b.orderPrice,
            align: 'center',
        },
        {
            title: t('active'),
            dataIndex: 'orderStatus',
            key: 'orderStatus',
            sorter: (a, b) => a.orderStatus - b.orderStatus,
            width: 200,
            render: (item, record) => (
                <div className={`${(item === 1 && ' text-[#d48806]') || (item === 2 && 'text-green-500')}`}>
                    {masterOrderStatus?.find((e) => e.id === item)?.name}
                </div>
            ),
            align: 'center',
        },
        {
            title: t('created_at'),
            dataIndex: 'createdAt',
            key: 'created_at',
            width: 200,
            render: (item) => moment(item).format('YYYY-MM-DD HH:mm:ss'),
            sorter: (a, b) => a.createdAt?.localeCompare(b.createdAt),
        },
        {
            title: t('action'),
            key: 'action',
            render: (item, record) => (
                <div className="p-4 flex items-center justify-center gap-3">
                    <Tooltip title={t('update')}>
                        <button
                            onClick={() => handleEditOrder(item.id)}
                            className="px-3 mt-2 py-[6px] bg-main-color text-white rounded-2xl cursor-pointer hover:shadow-4xl hover:opacity-80"
                        >
                            <AiOutlineEdit />
                        </button>
                    </Tooltip>
                    <Tooltip title={t('Delete')}>
                        <button
                            className=" px-3 mt-2 py-[6px] bg-red-500 text-white rounded-2xl cursor-pointer hover:shadow-4xl hover:opacity-80"
                            onClick={() => refModalOrderDelete?.current?.onOpen(record.id, record)}
                        >
                            <AiOutlineDelete />
                        </button>
                    </Tooltip>
                </div>
            ),
            fixed: 'right',
            width: 50,
            align: 'center',
        },
    ];

    const handleTableChange = (pagination) => {
        setPage(pagination.current);
    };

    const totalPrice = profile?.order?.map((item) => item?.orderPackage?.price)?.reduce((acc, curr) => acc + curr, 0);

    console.log(totalPrice);
    return (
        <>
            <Table
                rowSelection={{
                    type: 'checkbox',
                }}
                columns={columns}
                dataSource={data}
                rowKey="id"
                scroll={{ x: 'max-content' }}
                pagination={{
                    current: page,
                    position: ['bottomCenter'],
                    pageSize: 5,
                }}
                loading={loading}
                onChange={handleTableChange}
            />
            <ModalOrderDelete
                ref={refModalOrderDelete}
                onSubmit={(id) => orderApis.deleteOrder(id)}
                onAfterDelete={() => {
                    onGetListOrder(getValues());
                    window.location.reload();
                }}
            />
            <div className="py-6 float-right">
                <h2 className="mb-4 font-medium">{t('order_total')}</h2>
                <table>
                    <tbody className="border-[1px] border-[#ccc] rounded-md">
                        <tr className="border-b-[1px] border-b-[#ccc]">
                            <th className="py-2 px-4 font-medium">{t('provisional_total')}</th>
                            <td className="py-2 px-4">{numberDecimalWithCommas(totalPrice)} đ</td>
                        </tr>
                        <tr>
                            <th className="py-2 px-4 font-medium">{t('total')}</th>
                            <td className="py-2 px-4 font-medium">{numberDecimalWithCommas(totalPrice)} đ</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </>
    );
};

export default ProfilePageOrder;
