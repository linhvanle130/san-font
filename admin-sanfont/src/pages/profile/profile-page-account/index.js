import React, { useState } from 'react';
import { Input, Space } from 'antd';
import { AiOutlineCheck } from 'react-icons/ai';
import { useTranslation } from 'react-i18next';
import { HiOutlineEye, HiOutlineEyeSlash } from 'react-icons/hi2';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm, Controller } from 'react-hook-form';

import errorHelper from '@/utils/error-helper';
import accountApis from '@/api/accountApis';
import successHelper from '@/utils/success-helper';
import yup from '@/utils/yup';
import Loading from '@/components/loading';

export const ProfilePageAccount = ({ profile }) => {
    const { t } = useTranslation();

    const schema = yup.object({
        email: yup.string().required().max(255),
        oldPassword: yup.string().min(6).max(30).required().trim(),
        password: yup.string().min(6).max(30).required().trim(),
        newPassword: yup.string().min(6).max(30).required().trim(),
    });

    const [loading, setLoading] = useState(false);

    const {
        control,
        handleSubmit,
        formState: { errors },
    } = useForm({
        resolver: yupResolver(schema),
    });

    const submitCreate = (values) => {
        const { email, oldPassword, password, newPassword } = values;

        const payload = {
            email: email,
            oldPassword: oldPassword,
            password: password,
            newPassword: newPassword,
        };
        return accountApis
            .changePassword(payload)
            .then(() => {
                successHelper(t('updated_password_success'));
            })
            .catch((err) => {
                console.log(err);
                errorHelper(err);
            })
            .finally(() => {
                setLoading(false);
            });
    };

    const email = profile?.email;

    const [isOldPassword, setIsOldPassword] = useState(false);
    const [isPassword, setIsPassword] = useState(false);
    const [isNewPassword, setIsNewPassword] = useState(false);

    const toggleOldPassword = () => {
        setIsOldPassword(!isOldPassword);
    };

    const togglePassword = () => {
        setIsPassword(!isPassword);
    };

    const toggleNewPassword = () => {
        setIsNewPassword(!isNewPassword);
    };

    return (
        <div>
            <form onSubmit={handleSubmit(submitCreate)}>
                <div className="flex flex-col gap-3">
                    <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-6 w-full">
                        <div className="flex flex-col gap-2 w-full">
                            <div className="flex items-center">
                                <h3>{t('username')}</h3>
                                <div className="text-main-color">&nbsp;&nbsp;*</div>
                            </div>
                            <Input placeholder={t('username')} value={profile?.username} />
                        </div>
                        <div className="flex flex-col gap-2 w-full">
                            <div className="flex items-center">
                                <h3>{t('email')}</h3>
                                <div className="text-main-color">&nbsp;&nbsp;*</div>
                            </div>
                            <div>
                                <Controller
                                    name="email"
                                    control={control}
                                    render={({ field }) => (
                                        <Input
                                            {...field}
                                            placeholder={t('email')}
                                            status={errors?.oldPassword?.message}
                                            defaultValue={email}
                                        />
                                    )}
                                />
                                {errors?.email?.message && <p className="text-error mt-1">{errors?.email?.message}</p>}
                            </div>
                        </div>
                    </div>
                    <div className="mt-3">
                        <h3 className="font-medium mb-6">{t('change_password')}</h3>
                        <div className="flex flex-col gap-6">
                            <div className="mb-3">
                                <h3 className="mb-2">{t('current_password')}</h3>
                                <Controller
                                    name="oldPassword"
                                    control={control}
                                    render={({ field }) => (
                                        <Input
                                            {...field}
                                            placeholder={t('current_password')}
                                            status={errors?.oldPassword?.message}
                                            type={isOldPassword ? 'text' : 'password'}
                                            suffix={
                                                <Space>
                                                    {isOldPassword ? (
                                                        <HiOutlineEye onClick={toggleOldPassword} />
                                                    ) : (
                                                        <HiOutlineEyeSlash onClick={toggleOldPassword} />
                                                    )}
                                                </Space>
                                            }
                                        />
                                    )}
                                />
                                {errors?.oldPassword?.message && (
                                    <p className="text-error mt-1">{errors?.oldPassword?.message}</p>
                                )}
                            </div>

                            <div className="mb-3">
                                <h3 className="mb-2">{t('new_password')}</h3>
                                <Controller
                                    name="password"
                                    control={control}
                                    render={({ field }) => (
                                        <Input
                                            {...field}
                                            placeholder={t('new_password')}
                                            status={errors?.password?.message}
                                            type={isPassword ? 'text' : 'password'}
                                            suffix={
                                                <Space>
                                                    {isPassword ? (
                                                        <HiOutlineEye onClick={togglePassword} />
                                                    ) : (
                                                        <HiOutlineEyeSlash onClick={togglePassword} />
                                                    )}
                                                </Space>
                                            }
                                        />
                                    )}
                                />
                                {errors?.password?.message && (
                                    <p className="text-error mt-1">{errors?.password?.message}</p>
                                )}
                            </div>

                            <div className="mb-3">
                                <h3 className="mb-2">{t('confirm_new_password')}</h3>
                                <Controller
                                    name="newPassword"
                                    control={control}
                                    render={({ field }) => (
                                        <Input
                                            {...field}
                                            placeholder={t('confirm_new_password')}
                                            status={errors?.newPassword?.message}
                                            type={isNewPassword ? 'text' : 'password'}
                                            suffix={
                                                <Space>
                                                    {isNewPassword ? (
                                                        <HiOutlineEye onClick={toggleNewPassword} />
                                                    ) : (
                                                        <HiOutlineEyeSlash onClick={toggleNewPassword} />
                                                    )}
                                                </Space>
                                            }
                                        />
                                    )}
                                />
                                {errors?.newPassword?.message && (
                                    <p className="text-error mt-1">{errors?.newPassword?.message}</p>
                                )}
                            </div>
                            <div>
                                <button
                                    type="submit"
                                    className=" bg-main-color py-2 px-4 hover:shadow-4xl rounded-[2px]"
                                >
                                    <div className="flex gap-2 items-center text-white">
                                        <div>{loading && <Loading Loading />}</div>
                                        <AiOutlineCheck />
                                        <span className="text-sm">{t('save_changes')}</span>
                                    </div>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    );
};
