import React, { useEffect, useState } from 'react';
import Vector from '../../resources/images/vector.png';
import accountApis from '@/api/accountApis';
import masterApis from '@/api/masterAps';
import { MASTER_DATA_NAME } from '@/constants';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import ProfilePageSave from './profile-page-save';
import ProfilePageProduct from './profile-page-product';
import ProfilePageOrder from './profile-page-order';
import ProfilePageDownLoad from './profile-page-download/index';
import ProfilePageAddress from './profile-page-address';
import { ProfilePageAccount } from './profile-page-account';
import { useTranslation } from 'react-i18next';

const Profile = () => {
    const { t } = useTranslation();
    const [profile, setProfile] = useState();
    const getProfileAccount = async () => {
        const profile = await accountApis.getProfile();
        setProfile(profile);
    };

    const [masterLevelUser, setMasterLevelUser] = useState();
    const [masterRoleUser, setMasterRoleUser] = useState();
    const [masterFontUser, setMasterFontUser] = useState();

    const fetchMasterData = async () => {
        const masterLevel = await masterApis.getAllMaster({
            idMaster: MASTER_DATA_NAME.LEVEL_USER,
        });
        const masterRole = await masterApis.getAllMaster({
            idMaster: MASTER_DATA_NAME.ROLE,
        });
        const masterFont = await masterApis.getAllMaster({
            idMaster: MASTER_DATA_NAME.STATUS_FONT,
        });
        setMasterRoleUser(masterRole);
        setMasterLevelUser(masterLevel);
        setMasterFontUser(masterFont);
    };

    useEffect(() => {
        getProfileAccount();
        fetchMasterData();
    }, []);

    return (
        <div className="bg-[#ffffff] dark-container">
            <div className="">
                <div className="py-6 px-16 border-b-[1px] border-b-[#ccc]">
                    <h3 className="text-[32px] text-center">{t('info_profile')}</h3>
                </div>
            </div>
            <div className="px-3">
                <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4">
                    <div className="md:col-span-2 lg:col-span-1">
                        <div className="flex justify-center py-8 px-6 lg:border-r-[1px] lg:border-r-[#ccc]">
                            <div className="flex flex-col">
                                <img
                                    src={profile?.userInformation?.avatar || Vector}
                                    alt="avatar"
                                    className="w-[250px] h-[250px] rounded-full object-cover"
                                />
                                <h3 className="py-3 font-medium text-center uppercase">{profile?.username}</h3>
                                <ul>
                                    <li className="py-2">
                                        <div className="flex gap-2">
                                            <h4 className="font-medium">{t('email')} :</h4>
                                            <span className="dart-text">{profile?.email}</span>
                                        </div>
                                    </li>
                                    <li className="py-2">
                                        <div className="flex gap-2">
                                            <h4 className="font-medium">{t('role')} :</h4>
                                            <span className="text-main-color">
                                                {masterRoleUser?.find((e) => e.id === profile?.role)?.name}
                                            </span>
                                        </div>
                                    </li>
                                    <li className="py-2">
                                        <div className="flex gap-2">
                                            <h4 className="font-medium">{t('level')} :</h4>
                                            <span className="dart-text">
                                                {masterLevelUser?.find((e) => e.id === profile?.level)?.name ||
                                                    'Chưa phân cấp bật'}
                                            </span>
                                        </div>
                                    </li>
                                    <li className="py-2">
                                        <div className="flex gap-2">
                                            <h4 className="font-medium">{t('fonts_favorite')} :</h4>
                                            <span className="dart-text">{profile?.productSave?.length || 0}</span>
                                        </div>
                                    </li>
                                    <li className="py-2">
                                        <div className="flex gap-2">
                                            <h4 className="font-medium">{t('fonts_upload')} :</h4>
                                            <span className="dart-text">{profile?.product?.length || 0}</span>
                                        </div>
                                    </li>
                                    <li className="py-2">
                                        <div className="flex gap-2">
                                            <h4 className="font-medium">{t('order_amount')} :</h4>
                                            <span className="dart-text">{profile?.order?.length || 0}</span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="md:col-span-2 lg:col-span-3">
                        <div className="py-8 px-1 md:px-3 lg:px-6">
                            <Tabs defaultIndex={0}>
                                <TabList className="mb-4 grid grid-cols-1 md:grid-cols-6 lg:grid-cols-8">
                                    <Tab className="py-3 ml-3 md:ml-4 lg:ml-8 font-semibold text-base cursor-pointer hover:text-main-color border-b-[1px] border-b-transparent hover:border-b-main-color dart-text">
                                        {t('account')}
                                    </Tab>
                                    <Tab className="py-3 ml-3 md:ml-4 lg:ml-8 font-semibold text-base cursor-pointer hover:text-main-color border-b-[1px] border-b-transparent hover:border-b-main-color dart-text">
                                        {t('fonts_favorite')}
                                    </Tab>
                                    <Tab className="py-3 ml-3 md:ml-4 lg:ml-8 font-semibold text-base cursor-pointer hover:text-main-color border-b-[1px] border-b-transparent hover:border-b-main-color dart-text">
                                        {t('fonts_upload')}
                                    </Tab>
                                    <Tab className="py-3 ml-3 md:ml-4 lg:ml-8 font-semibold text-base cursor-pointer hover:text-main-color border-b-[1px] border-b-transparent hover:border-b-main-color dart-text">
                                        {t('order')}
                                    </Tab>
                                    <Tab className="py-3 ml-3 md:ml-4 lg:ml-8 font-semibold text-base cursor-pointer hover:text-main-color border-b-[1px] border-b-transparent hover:border-b-main-color dart-text">
                                        {t('fonts_download')}
                                    </Tab>
                                    <Tab className="py-3 ml-3 md:ml-4 lg:ml-8 font-semibold text-base cursor-pointer hover:text-main-color border-b-[1px] border-b-transparent hover:border-b-main-color dart-text">
                                        {t('address')}
                                    </Tab>
                                    <div></div>
                                    <div></div>
                                </TabList>
                                <div className="px-1 md:px-4 lg:px-8">
                                    <TabPanel>
                                        <ProfilePageAccount profile={profile} />
                                    </TabPanel>
                                    <TabPanel>
                                        <ProfilePageSave profile={profile} />
                                    </TabPanel>
                                    <TabPanel>
                                        <ProfilePageProduct profile={profile} />
                                    </TabPanel>

                                    <TabPanel>
                                        <ProfilePageOrder profile={profile} />
                                    </TabPanel>
                                    <TabPanel>
                                        <ProfilePageDownLoad profile={profile} />
                                    </TabPanel>
                                    <TabPanel>
                                        <ProfilePageAddress profile={profile} />
                                    </TabPanel>
                                </div>
                            </Tabs>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Profile;
