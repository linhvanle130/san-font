import { IMAGE_TYPE } from '@/constants';
import { Table, Tooltip } from 'antd';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

const ProfilePageDownLoad = ({ profile }) => {
    const { t } = useTranslation();

    const [loading, setLoading] = useState(true);
    const [page, setPage] = useState(1);

    const productDownloadFont = {
        getList: () => profile?.downloadFont,
    };
    const [listDownloadFont, setListDownloadFont] = useState([]);
    const data = listDownloadFont.map((e) => {
        return {
            ...e,
            productName: e.product?.name,
            productImage: e.product?.productImage?.find((e) => e.isMain === IMAGE_TYPE.MAIN)?.image,
            productFont: e.product?.productFont?.find((e) => e.isMain === IMAGE_TYPE.MAIN)?.font,
        };
    });

    const onGetListDownloadFont = async () => {
        const fetchDownloadFont = await productDownloadFont.getList();
        setListDownloadFont(fetchDownloadFont);
        setLoading(false);
    };

    useEffect(() => {
        onGetListDownloadFont();
    }, []);

    const columns = [
        {
            title: t('image'),
            key: 'productImage',
            dataIndex: 'productImage',
            width: 100,
            align: 'center',
            render: (item) => (
                <div className="flex items-center justify-center">
                    <img width={100} src={item} alt="" />
                </div>
            ),
        },
        {
            title: t('product_name'),
            dataIndex: 'productName',
            key: 'productName',
            width: 200,
            render: (text) => <Tooltip title={text}>{text}</Tooltip>,
            sorter: (a, b) => a.productName?.localeCompare(b.productName),
        },
        {
            title: t('font'),
            dataIndex: 'productFont',
            key: 'productFont',
            width: 200,
            align: 'center',
            render: (item, record) => item?.slice(12),
        },
        {
            title: t('created_at'),
            dataIndex: 'createdAt',
            key: 'created_at',
            width: 200,
            render: (item) => moment(item).format('YYYY-MM-DD HH:mm:ss'),
            sorter: (a, b) => a.createdAt?.localeCompare(b.createdAt),
        },
    ];

    const handleTableChange = (pagination) => {
        setPage(pagination.current);
    };
    return (
        <div>
            <Table
                rowSelection={{
                    type: 'checkbox',
                }}
                columns={columns}
                dataSource={data}
                rowKey="id"
                scroll={{ x: 'max-content' }}
                pagination={{
                    current: page,
                    position: ['bottomCenter'],
                    pageSize: 5,
                }}
                loading={loading}
                onChange={handleTableChange}
            />
            <div className="py-6 float-right">
                <h2 className="mb-4 font-medium">{t('font_total')}</h2>
                <table>
                    <tbody className="border-[1px] border-[#ccc] rounded-md">
                        <tr>
                            <th className="py-2 px-4 font-medium">{t('total')}</th>
                            <td className="py-2 px-4 font-medium">{profile?.downloadFont?.length}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    );
};

export default ProfilePageDownLoad;
