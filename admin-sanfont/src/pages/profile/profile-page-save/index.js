import React, { useEffect, useRef, useState } from 'react';
import { Table, Tooltip } from 'antd';
import { useTranslation } from 'react-i18next';
import moment from 'moment';
import { AiOutlineDelete } from 'react-icons/ai';
import { IMAGE_TYPE } from '@/constants';
import ModalProductSaveDelete from '@/pages/product-save/modal-product-save-delete';
import { useForm } from 'react-hook-form';
import productSaveApis from '@/api/productSaveApis';

const ProfilePageSave = ({ profile }) => {
    const { t } = useTranslation();
    const { getValues } = useForm();

    const refModalProductSaveDelete = useRef();
    const [loading, setLoading] = useState(true);
    const [page, setPage] = useState(1);

    const productSaveData = {
        getList: () => profile?.productSave,
    };
    const [listProductSave, setListProductSave] = useState([]);
    const data = listProductSave.map((e) => {
        return {
            ...e,
            productName: e.product?.name,
            productImage: e.product?.productImage?.find((e) => e.isMain === IMAGE_TYPE.MAIN)?.image,
        };
    });

    const onGetListProductSave = async () => {
        const fetchProductSave = await productSaveData.getList();
        setListProductSave(fetchProductSave);
        setLoading(false);
    };

    useEffect(() => {
        onGetListProductSave();
    }, []);

    const columns = [
        {
            title: t('image'),
            key: 'productImage',
            dataIndex: 'productImage',
            width: 100,
            align: 'center',
            render: (item) => (
                <div className="flex items-center justify-center">
                    <img width={100} src={item} alt="" />
                </div>
            ),
        },
        {
            title: t('product_name'),
            dataIndex: 'productName',
            key: 'productName',
            width: 300,
            render: (text) => <Tooltip title={text}>{text}</Tooltip>,
            sorter: (a, b) => a.productName?.localeCompare(b.productName),
        },
        {
            title: t('created_at'),
            dataIndex: 'createdAt',
            key: 'created_at',
            width: 200,
            render: (item) => moment(item).format('YYYY-MM-DD HH:mm:ss'),
            sorter: (a, b) => a.createdAt?.localeCompare(b.createdAt),
        },
        {
            title: t('action'),
            key: 'action',
            render: (item, record) => (
                <div className="p-4 flex items-center justify-center gap-3">
                    <Tooltip title={t('Delete')}>
                        <button
                            className=" px-3 mt-2 py-[6px] bg-red-500 text-white rounded-2xl cursor-pointer hover:shadow-4xl hover:opacity-80"
                            onClick={() => refModalProductSaveDelete?.current?.onOpen(record.id, record)}
                        >
                            <AiOutlineDelete />
                        </button>
                    </Tooltip>
                </div>
            ),
            fixed: 'right',
            width: 50,
            align: 'center',
        },
    ];

    const handleTableChange = (pagination) => {
        setPage(pagination.current);
    };
    return (
        <div>
            <Table
                rowSelection={{
                    type: 'checkbox',
                }}
                columns={columns}
                dataSource={data}
                rowKey="id"
                scroll={{ x: 'max-content' }}
                pagination={{
                    current: page,
                    position: ['bottomCenter'],
                    pageSize: 5,
                }}
                loading={loading}
                onChange={handleTableChange}
            />
            <ModalProductSaveDelete
                ref={refModalProductSaveDelete}
                onSubmit={(id) => productSaveApis.deleteProductSave(id)}
                onAfterDelete={() => {
                    onGetListProductSave(getValues());
                    window.location.reload();
                }}
            />
        </div>
    );
};

export default ProfilePageSave;
