import React, { useEffect, useRef, useState } from 'react';
import { Controller, useForm, useWatch } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import { yupResolver } from '@hookform/resolvers/yup';
import { useNavigate, useParams } from 'react-router-dom';
import { Input, Spin, Image } from 'antd';
import Compressor from 'compressorjs';
import { AiOutlineCheck } from 'react-icons/ai';
import { CiCamera } from 'react-icons/ci';

import yup from '@/utils/yup';
import successHelper from '@/utils/success-helper';
import errorHelper from '@/utils/error-helper';
import Loading from '@/components/loading';
import accountApis from '@/api/accountApis';
import commonApis from '@/api/commonApis';

const ProfileAdmin = () => {
    const { t } = useTranslation();
    const { id } = useParams();
    const refAvatar = useRef();

    const [loading, setLoading] = useState(false);
    const [isUpdateAvatar, setIsUpdateAvatar] = useState(false);

    const schema = yup.object({
        name: yup.string().max(100).required(),
        email: yup
            .string()
            .email()
            .max(255)
            .required()
            // eslint-disable-next-line no-useless-escape
            .matches(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, t('validations:email'))
            .trim(),
        avatar: yup.string().required().trim(),
    });

    const {
        control,
        handleSubmit,
        formState: { errors },
        setValue,
    } = useForm({
        resolver: yupResolver(schema),
    });

    const { avatar } = useWatch({ control });

    const submitUpdate = (values) => {
        const { name, email, avatar } = values;

        const payload = {
            id: +id,
            name: name,
            email: email,
            avatar: avatar,
        };
        return accountApis
            .updateAccount(payload)
            .then(() => {
                successHelper(t('update_success'));
            })
            .catch((err) => {
                console.log(err);
                errorHelper(err);
            })
            .finally(() => {
                setLoading(false);
            });
    };

    useEffect(() => {
        accountApis
            .getDetailAccount(id)
            .then((res) => {
                setValue('name', res.name, {
                    shouldValidate: true,
                    shouldDirty: true,
                });
                setValue('email', res.email, {
                    shouldValidate: true,
                    shouldDirty: true,
                });
                setValue('avatar', res.avatar, {
                    shouldValidate: true,
                    shouldDirty: true,
                });
            })
            .catch((err) => {
                console.log(err);
                errorHelper(err.message);
            })
            .finally(() => setLoading(false));
    }, []);

    const customStyle = {
        border: '1px solid red',
    };

    const onchangeAvatar = (e) => {
        const files = e.target.files;
        if (files.length > 0) {
            new Compressor(files[0], {
                quality: 0.8,
                success: (compressedImg) => {
                    // compressedResult has the compressed file.
                    // Use the compressed file to upload the images to your server.
                    onUploadAvatar(compressedImg)
                        .then((response) => {
                            setValue('avatar', response, {
                                shouldValidate: true,
                                shouldDirty: true,
                            });
                        })
                        .finally(() => {
                            setIsUpdateAvatar(false);
                        });
                },
            });
        }
        e.target.value = null;
    };

    const onUploadAvatar = async (file) => {
        setIsUpdateAvatar(true);
        const formData = new FormData();
        formData.append('file', file);
        formData.append('fileName', file.name);
        return commonApis
            .preUploadFile(formData)
            .then(async (response) => {
                return response;
            })
            .catch((err) => {
                errorHelper(err);
            });
    };

    return (
        <div className="">
            <h3 className="mb-3 text-2xl font-semibold text-center">{t('info_Admin')}</h3>
            <form onSubmit={handleSubmit(submitUpdate)}>
                <div className="flex items-center justify-center">
                    <div className="w-full md:w-[80%] lg:w-[60%] bg-[#ffffff] dark-container rounded-md">
                        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 h-full">
                            <div className="h-full">
                                <div className="p-6">
                                    <div className="mb-3">
                                        <h4 className="mb-2">Tên</h4>
                                        <Controller
                                            name="name"
                                            control={control}
                                            render={({ field }) => (
                                                <Input
                                                    {...field}
                                                    placeholder="Tên"
                                                    status={errors?.name?.message}
                                                    style={errors?.name?.message && customStyle}
                                                />
                                            )}
                                        />
                                        {errors?.name?.message && (
                                            <p className="text-error mt-1">{errors?.name?.message}</p>
                                        )}
                                    </div>
                                    <div className="mb-3">
                                        <h4 className="mb-2">Email</h4>
                                        <Controller
                                            name="email"
                                            control={control}
                                            render={({ field }) => (
                                                <Input
                                                    {...field}
                                                    placeholder={t('email')}
                                                    status={errors?.email?.message}
                                                    style={errors?.email?.message && customStyle}
                                                />
                                            )}
                                        />
                                        {errors?.email?.message && (
                                            <p className="text-error mt-1">{errors?.email?.message}</p>
                                        )}
                                    </div>
                                </div>
                            </div>
                            <div className="flex items-center justify-center">
                                <div className="p-5">
                                    <div
                                        className={
                                            errors?.avatar?.message
                                                ? 'border-[1px] border-error border-dashed'
                                                : 'border-[1px] border-[#ccc] border-dashed'
                                        }
                                    >
                                        <div className="thumbnail-upload">
                                            <Image
                                                className="img-thumbnail"
                                                src={
                                                    avatar ||
                                                    'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg=='
                                                }
                                                alt=""
                                            />
                                            <button
                                                type={'button'}
                                                disabled={isUpdateAvatar}
                                                onClick={() => refAvatar.current.click()}
                                                className="button-camera tooltip tooltip-left "
                                                data-tip={t('thumbnail_action_note')}
                                            >
                                                {isUpdateAvatar ? <Spin /> : <CiCamera />}
                                                <input
                                                    ref={refAvatar}
                                                    type="file"
                                                    accept="image/*"
                                                    onChange={onchangeAvatar}
                                                    style={{ display: 'none' }}
                                                />
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                {errors?.avatar?.message && (
                                    <p className="text-error mt-1">{errors?.avatar?.message}</p>
                                )}
                            </div>
                        </div>
                        <div className="flex justify-center my-5">
                            <button
                                type="submit"
                                className=" bg-main-color py-2 px-4 hover:shadow-4xl rounded-[2px] hover:opacity-80"
                            >
                                <div className="flex gap-2 items-center text-white">
                                    <div>{loading && <Loading LoadingData />}</div>
                                    <AiOutlineCheck />
                                    <span className="text-sm">{t('update')}</span>
                                </div>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    );
};

export default ProfileAdmin;
