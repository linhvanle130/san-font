import React, { useEffect, useState } from 'react';
import { AiOutlineCheck } from 'react-icons/ai';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { Input } from 'antd';

import successHelper from '@/utils/success-helper';
import errorHelper from '@/utils/error-helper';
import yup from '@/utils/yup';
import accountApis from '@/api/accountApis';
import userApis from '@/api/userApis';
import { setProfileAuth } from '@/store/slices/accountSlice';
import Vector from '../../../resources/images/vector.png';

const ProfilePageAddress = ({ profile }) => {
    const information = profile?.userInformation;
    console.log(information);
    const dispatch = useDispatch();

    const { t } = useTranslation();
    const schema = yup.object({
        address: yup.string().max(255).trim().required().nullable(),
        cityCode: yup.number().integer().required().nullable(),
        districtCode: yup.number().integer().required().nullable(),
        wardCode: yup.number().integer().required().nullable(),
    });

    const { register, handleSubmit, reset } = useForm({
        resolver: yupResolver(schema),
        defaultValues: {
            address: profile?.userInformation?.address,
            cityCode: profile?.userInformation?.cityCode,
            districtCode: profile?.userInformation?.districtCode,
            wardCode: profile?.userInformation?.wardCode,
        },
        mode: 'onChange',
    });

    const [loadingUpdateProfile, setLoadingUpdateProfile] = useState(false);

    const submitUpdate = (values) => {
        const { address, cityCode, districtCode, wardCode } = values;

        setLoadingUpdateProfile(true);
        return userApis
            .updateProfileUser({
                id: profile?.id,
                address,
                cityCode: +cityCode,
                districtCode: +districtCode,
                wardCode: +wardCode,
            })
            .then(() => {
                return accountApis.getProfile();
            })
            .then((res) => {
                successHelper(t('update_user_address'));
                dispatch(setProfileAuth(res));
                reset({
                    address,
                    cityCode: +cityCode,
                    districtCode: +districtCode,
                    wardCode: +wardCode,
                });
            })
            .catch((err) => {
                errorHelper(err);
            })
            .finally(() => {
                setLoadingUpdateProfile(false);
            });
    };

    useEffect(() => {
        accountApis
            .getProfile()
            .then((res) => {
                dispatch(setProfileAuth(res));
            })
            .catch((err) => {
                errorHelper(err);
            })
            .finally(() => {});
    }, []);

    return (
        <div className="">
            <h3 className="font-semibold my-6 text-center">{t('update_user_address')}</h3>
            <form onSubmit={handleSubmit(submitUpdate)}>
                <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2">
                    <div className="lg:col-span-1">
                        <div className="flex flex-col gap-6">
                            <div className="flex flex-col gap-3">
                                <div className="flex flex-col gap-2 w-full">
                                    <div className="flex items-center">
                                        <h3>{t('address')}</h3>
                                    </div>
                                    <Input
                                        placeholder={t('address')}
                                        {...register('address')}
                                        autoComplete="off"
                                        defaultValue={profile?.userInformation?.address}
                                    />
                                </div>
                            </div>
                            <div className="flex flex-col gap-3">
                                <div className="flex flex-col gap-2 w-full">
                                    <div className="flex items-center">
                                        <h3>{t('city')}</h3>
                                    </div>
                                    <Input
                                        placeholder={t('city')}
                                        {...register('cityCode')}
                                        autoComplete="off"
                                        defaultValue={profile?.userInformation?.cityCode}
                                    />
                                </div>
                            </div>
                            <div className="flex flex-col gap-3">
                                <div className="flex flex-col gap-2 w-full">
                                    <div className="flex items-center">
                                        <h3>{t('district')}</h3>
                                    </div>
                                    <Input
                                        placeholder={t('district')}
                                        {...register('districtCode')}
                                        autoComplete="off"
                                        defaultValue={profile?.userInformation?.districtCode}
                                    />
                                </div>
                            </div>
                            <div className="flex flex-col gap-3">
                                <div className="flex flex-col gap-2 w-full">
                                    <div className="flex items-center">
                                        <h3>{t('ward')}</h3>
                                    </div>
                                    <Input
                                        placeholder={t('ward')}
                                        {...register('wardCode')}
                                        autoComplete="off"
                                        defaultValue={profile?.userInformation?.wardCode}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="lg:col-span-1 mt-6 md:mt-1 lg:mt-0">
                        <div className="flex justify-center">
                            <div className="px-5">
                                <img
                                    src={profile?.userInformation?.avatar || Vector}
                                    alt="avatar"
                                    className="w-[250px] h-[250px] rounded-full object-cover"
                                />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="justify-center mt-6 hidden">
                    <button type="submit" className=" bg-main-color py-2 px-4 hover:shadow-4xl rounded-[2px]">
                        <div className="flex gap-2 items-center text-white">
                            <AiOutlineCheck />
                            <span className="text-sm">{t('save_changes')}</span>
                        </div>
                    </button>
                </div>
            </form>
        </div>
    );
};

export default ProfilePageAddress;
