import React, { useEffect, useRef, useState } from 'react';
import { AiOutlineDelete } from 'react-icons/ai';
import { Tooltip, Table } from 'antd';
import { useTranslation } from 'react-i18next';
import moment from 'moment';
import { IMAGE_TYPE } from '@/constants';
import ModalProductDelete from '@/pages/product/modal-product-delete';
import productApis from '@/api/productApis';
import { useForm } from 'react-hook-form';

const ProfilePageProduct = ({ profile }) => {
    const { t } = useTranslation();

    const refModalProductDelete = useRef();
    const { getValues } = useForm();

    const [loading, setLoading] = useState(true);
    const [page, setPage] = useState(1);

    const productFontData = {
        getList: () => profile?.product,
    };
    const [listProductFont, setListProductFont] = useState([]);
    const data = listProductFont.map((e) => {
        return {
            ...e,
            productImage: e.productImage?.find((e) => e.isMain === IMAGE_TYPE.MAIN)?.image,
        };
    });

    const onGetListProductFont = async () => {
        const fetchProductFont = await productFontData.getList();
        setListProductFont(fetchProductFont);
        setLoading(false);
    };

    useEffect(() => {
        onGetListProductFont();
    }, []);

    const columns = [
        {
            title: t('image'),
            key: 'productImage',
            dataIndex: 'productImage',
            width: 100,
            align: 'center',
            render: (item) => (
                <div className="flex items-center justify-center">
                    <img width={100} src={item} alt="" />
                </div>
            ),
        },
        {
            title: t('product_name'),
            dataIndex: 'name',
            key: 'name',
            width: 300,
            render: (text) => <Tooltip title={text}>{text}</Tooltip>,
            sorter: (a, b) => a.name?.localeCompare(b.name),
        },
        {
            title: t('created_at'),
            dataIndex: 'createdAt',
            key: 'created_at',
            width: 200,
            render: (item) => moment(item).format('YYYY-MM-DD HH:mm:ss'),
            sorter: (a, b) => a.createdAt?.localeCompare(b.createdAt),
        },
        {
            title: t('action'),
            key: 'action',
            render: (item, record) => (
                <div className="p-4 flex items-center justify-center gap-3">
                    <Tooltip title={t('Delete')}>
                        <button
                            className=" px-3 mt-2 py-[6px] bg-red-500 text-white rounded-2xl cursor-pointer hover:shadow-4xl hover:opacity-80"
                            onClick={() => refModalProductDelete?.current?.onOpen(record.id, record)}
                        >
                            <AiOutlineDelete />
                        </button>
                    </Tooltip>
                </div>
            ),
            fixed: 'right',
            width: 50,
            align: 'center',
        },
    ];

    const handleTableChange = (pagination) => {
        setPage(pagination.current);
    };

    return (
        <>
            <div>
                <Table
                    rowSelection={{
                        type: 'checkbox',
                    }}
                    columns={columns}
                    dataSource={data}
                    rowKey="id"
                    scroll={{ x: 'max-content' }}
                    pagination={{
                        current: page,
                        position: ['bottomCenter'],
                        pageSize: 5,
                    }}
                    loading={loading}
                    onChange={handleTableChange}
                />
                <ModalProductDelete
                    ref={refModalProductDelete}
                    onSubmit={(id) => productApis.deleteProduct(id)}
                    onAfterDelete={() => {
                        onGetListProductFont(getValues());
                        window.location.reload();
                    }}
                />
            </div>
        </>
    );
};

export default ProfilePageProduct;
