import React, { useEffect, useRef, useState } from 'react';
import { Tooltip, Table, Button, Space, Input } from 'antd';
import { AiOutlineDelete, AiOutlineEdit } from 'react-icons/ai';
import { IoIosSearch } from 'react-icons/io';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import moment from 'moment';

import { IMAGE_TYPE } from '@/constants';
import errorHelper from '@/utils/error-helper';
import commentApis from '@/api/commentApis';
import ModalCommentDelete from './modal-comment-delete';
import ModalCommentUpdate from './modal-comment-update';
import productApis from '@/api/productApis';

const Comment = () => {
    const { t } = useTranslation();
    const [loading, setLoading] = useState(true);
    const [page, setPage] = useState(1);
    const account = JSON.parse(localStorage.getItem('info'));

    const searchInput = useRef(null);
    const refModalCommentUpdate = useRef();
    const refModalCommentDelete = useRef();

    const { getValues } = useForm();

    const [searchText, setSearchText] = useState('');
    const [searchedColumn, setSearchedColumn] = useState('');

    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        setSearchText(selectedKeys[0]);
        setSearchedColumn(dataIndex);
    };

    const getColumnSearchProps = (dataIndex) => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm }) => (
            <div
                style={{
                    padding: 8,
                }}
            >
                <Input
                    ref={searchInput}
                    placeholder={`Tìm kiếm ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{
                        marginBottom: 8,
                        display: 'block',
                    }}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<IoIosSearch />}
                        size="small"
                        style={{
                            width: 100,
                            background: '#ff5e3a',
                        }}
                    >
                        Tìm kiếm
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: (filtered) => (
            <IoIosSearch
                style={{
                    color: filtered ? '#1890ff' : undefined,
                }}
            />
        ),
        onFilter: (value, record) => record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
        onFilterDropdownOpenChange: (visible) => {
            if (visible) {
                setTimeout(() => searchInput.current?.select(), 100);
            }
        },
        render: (text) => text,
    });

    const [listUserProductComment, setListUserProductComment] = useState([]);
    const [listUserProductCommentType, setListUserProductCommentType] = useState([]);
    console.log(listUserProductCommentType);

    const expandedRowRender = (row) => {
        const comment = listUserProductComment
            .filter((ft) => ft.productId === row.id)
            .map((e) => {
                return {
                    id: e.id,
                    createdAt: e.createdAt,
                    note: e.note,
                    userComment: e.user?.username,
                };
            });

        const columns = [
            {
                title: t('feedback_note'),
                dataIndex: 'note',
                key: 'note',
                width: 250,
                ...getColumnSearchProps('note'),
                render: (item) => (
                    <Tooltip title={item}>
                        <div className="font-semibold text-green-500">{item}</div>
                    </Tooltip>
                ),
            },
            {
                title: t('feedback_user_name'),
                dataIndex: 'userComment',
                key: 'userComment',
                width: 250,
                ...getColumnSearchProps('userComment'),
            },
            {
                title: t('created_at'),
                dataIndex: 'createdAt',
                key: 'createdAt',
                width: 200,
                render: (item) => moment(item).format('YYYY-MM-DD HH:mm:ss'),
                sorter: (a, b) => a.createdAt?.localeCompare(b.createdAt),
            },
            {
                title: t('action'),
                key: 'action',
                render: (item, record) => (
                    <div className="p-4 flex items-center justify-center gap-3">
                        <Tooltip title={t('update')}>
                            <button
                                onClick={() => refModalCommentUpdate.current.onOpen(record.id)}
                                className="px-3 mt-2 py-[6px] bg-main-color text-white rounded-2xl cursor-pointer hover:shadow-4xl hover:opacity-80"
                            >
                                <AiOutlineEdit />
                            </button>
                        </Tooltip>
                        {account.role === 1 && (
                            <Tooltip title={t('Delete')}>
                                <button
                                    className=" px-3 mt-2 py-[6px] bg-red-500 text-white rounded-2xl cursor-pointer hover:shadow-4xl hover:opacity-80"
                                    onClick={() => refModalCommentDelete?.current?.onOpen(record.id, record)}
                                >
                                    <AiOutlineDelete />
                                </button>
                            </Tooltip>
                        )}
                    </div>
                ),
                align: 'center',
                fixed: 'right',
                width: 100,
            },
        ];

        return (
            <Table
                rowSelection={{
                    type: 'checkbox',
                }}
                columns={columns}
                dataSource={comment}
                pagination={false}
                rowKey="id"
                size="small"
            />
        );
    };

    const onGetListUserProductComment = (values) => {
        const params = {
            ...values,
            admin: true,
        };

        setLoading(true);

        productApis
            .getTopUserProductComment(params)
            .then(({ rows }) => {
                const productCommentDetail = [];
                setListUserProductCommentType(
                    rows.map((e) => {
                        e.comment.map((a) => productCommentDetail.push(a));
                        return {
                            ...e,
                            productImage: e.productImage.find((e) => e.isMain === IMAGE_TYPE.MAIN)?.image,
                            productCategoryName: e.productCategory?.name,
                            countProductComment: e.comment?.length,
                        };
                    }),
                );
                setListUserProductComment(productCommentDetail);

                window.scroll({
                    top: 0,
                    behavior: 'smooth',
                });
            })
            .catch((err) => {
                console.log(err);
                errorHelper(err);
            })
            .finally(() => {
                setLoading(false);
            });
    };

    useEffect(() => {
        onGetListUserProductComment();
    }, []);

    const columns = [
        {
            title: t('image'),
            key: 'productImage',
            dataIndex: 'productImage',
            width: 200,
            align: 'center',
            render: (item) => (
                <div className="flex items-center justify-center">
                    <img width={100} src={item} alt="" />
                </div>
            ),
        },
        {
            title: t('feedback_product_name'),
            dataIndex: 'name',
            key: 'name',
            width: 300,
            ...getColumnSearchProps('name'),
            render: (text) => <Tooltip title={text}>{text}</Tooltip>,
            sorter: (a, b) => a.name?.localeCompare(b.name),
        },
        {
            title: t('product_category'),
            dataIndex: 'productCategoryName',
            key: 'productCategoryName',
            width: 300,
            sorter: (a, b) => a.productCategoryName?.localeCompare(b.productCategoryName),
        },
        {
            title: t('outstanding'),
            dataIndex: 'outstanding',
            key: 'outstanding',
            width: 200,
            render: (item) => (item === 0 && 'Bình thường') || (item === 1 && 'VIP') || (item === 2 && 'Font chọn lọc'),
            sorter: (a, b) => a.outstanding - b.outstanding,
            align: 'center',
        },
        {
            title: t('feedback_quantity'),
            dataIndex: 'countProductComment',
            key: 'countProductComment',
            width: 300,
            render: (item, record) => <Button className="bg-green-300 font-semibold py-1 px-2">{item}</Button>,
            sorter: (a, b) => a.countProductComment - b.countProductComment,
            align: 'center',
        },
    ];

    const handleTableChange = (pagination) => {
        setPage(pagination.current);
    };

    return (
        <div>
            <div className="flex py-4 max-sm:flex-col">
                <h2 className="mb-3 text-[20px] md:text-[24px] lg:text-[30px] text-black font-medium">
                    {t('user_feedback_list_product')}
                </h2>
            </div>
            <Table
                dataSource={listUserProductCommentType}
                rowKey="id"
                columns={columns}
                expandable={{
                    expandedRowRender,
                    expandRowByClick: true,
                }}
                pagination={{
                    current: page,
                    position: ['bottomCenter'],
                }}
                scroll={{ x: 'max-content' }}
                onChange={handleTableChange}
                loading={loading}
            />
            <ModalCommentUpdate
                ref={refModalCommentUpdate}
                onAfterUpdate={() => {
                    onGetListUserProductComment(getValues());
                }}
                account={account}
            />
            <ModalCommentDelete
                ref={refModalCommentDelete}
                onSubmit={(id) => commentApis.deleteComment(id)}
                onAfterDelete={() => {
                    onGetListUserProductComment(getValues());
                }}
            />
        </div>
    );
};

export default Comment;
