import axiosClient from './axiosClient';

const contactApis = {
    getListContacts: (params) => axiosClient.get('/api/contract/', { params }),
    updateStatusContact: (payload) => axiosClient.put('/api/contract/update-contract', payload),
    getDetailContact: (id) => axiosClient.get(`/api/contract/detail-contract/${id}`),
};

export default contactApis;
