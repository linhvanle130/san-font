import axiosClient from './axiosClient';

const productSaveApis = {
    getListProductSaveType: (params) => axiosClient.get('/api/product/get-save-product-type', { params }),
    getDetailProductSave: (id) => axiosClient.get(`/api/product/detail-save-product/${id}`),
    deleteProductSave: (id) => axiosClient.delete(`/api/product/delete-save-product/${id}`),
    updateProductSave: (payload) => axiosClient.put('/api/product/update-save-product', payload),
};

export default productSaveApis;
