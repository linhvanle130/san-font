import axiosClient from './axiosClient';

const upFontApis = {
    preUploadFile: (formData) => axiosClient.post(`/api/font/create-font-url`, formData),
    uploadFile: ({ urlUpload, file }) => {
        return fetch(urlUpload, {
            method: 'PUT',
            body: file,
        });
    },
};

export default upFontApis;
