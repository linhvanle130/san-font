import axiosClient from './axiosClient';

const blogCategoryApis = {
    getDetailBlogCategory: (id) => axiosClient.get(`/api/blog/detail-blog-category/${id}`),
    createBlogCategory: (payload) => axiosClient.post('/api/blog/new-blog-category', payload),
    updateBlogCategory: (payload) => axiosClient.put('/api/blog/update-blog-category', payload),
    deleteBlogCategory: (id) => axiosClient.delete(`/api/blog/delete-blog-category/${id}`),
};

export default blogCategoryApis;
