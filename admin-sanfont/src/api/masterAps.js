import axiosClient from './axiosClient';

const masterApis = {
    getListPagingMaster: (params) => axiosClient.get('/api/master/', { params }),
    createMaster: (payload) => axiosClient.post('/api/master/new-master', payload),
    getAllMaster: (params) => axiosClient.get('/api/master/get-master', { params }),
    updateMaster: (payload) => axiosClient.put('/api/master/update-master', payload),
    deleteMaster: (payload) => axiosClient.delete('/api/master/delete-master', payload),
};

export default masterApis;
