import axiosClient from './axiosClient';

const orderContentApis = {
    getAllOrderContent: (params) => axiosClient.get('/api/order-content/get-content', { params }),
    createOrderContent: (payload) => axiosClient.post('/api/order-content/new-content', payload),
    updateOrderContent: (payload) => axiosClient.put('/api/order-content/update-content', payload),
    deleteOrderContent: (payload) => axiosClient.delete('/api/order-content/delete-content', payload),
};

export default orderContentApis;
