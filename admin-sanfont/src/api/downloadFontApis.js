import axiosClient from './axiosClient';

const downloadFontApis = {
    getListPagingDownloadFont: (params) => axiosClient.get('/api/download/', { params }),
    createDownloadFont: (payload) => axiosClient.post('/api/download/new-download', payload),
};

export default downloadFontApis;
