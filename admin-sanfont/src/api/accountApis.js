import axiosClient from './axiosClient';

const accountApis = {
    login: (payload) => axiosClient.post('/api/admin/auth/sign-in', payload),
    updateAccount: (payload) => axiosClient.put('/api/admin/auth/update-info', payload),
    getDetailAccount: (id) => axiosClient.get(`/api/admin/auth/detail-info/${id}`),
    changePassword: (payload) => axiosClient.post('/api/user/auth/change-password', payload),
    getProfile: () => axiosClient.get('/api/user/auth/information'),
};

export default accountApis;
