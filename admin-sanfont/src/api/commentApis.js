import axiosClient from './axiosClient';

const commentApis = {
    getListComment: (params) => axiosClient.get('/api/comment', { params }),
    getDetailComment: (id) => axiosClient.get(`/api/comment/detail-comment/${id}`),
    updateComment: (payload) => axiosClient.put('/api/comment/update-comment', payload),
    deleteComment: (id) => axiosClient.delete(`/api/comment/delete-comment/${id}`),
};

export default commentApis;
