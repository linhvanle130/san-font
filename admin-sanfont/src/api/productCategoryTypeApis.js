import axiosClient from './axiosClient';

const productCategoryTypeApis = {
    getAllCategoryType: () => axiosClient.get('/api/product/get-list-category-type'),
    getCategoryWithPagingType: (params) =>
        axiosClient.get('/api/product/get-list-category-with-paging-type', { params }),
    createProductElement: (payload) => axiosClient.post('/api/product/new-category-type', payload),
};

export default productCategoryTypeApis;
