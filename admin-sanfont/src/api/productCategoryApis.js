import axiosClient from './axiosClient';

const productCategoryApis = {
    getAllCategory: () => axiosClient.get('/api/product/get-list-category'),
    getListCategoryWithPaging: (params) => axiosClient.get('/api/product/get-list-category-with-paging', { params }),
    createProductCategory: (payload) => axiosClient.post('/api/product/new-category', payload),
    updateCategory: (payload) => axiosClient.put('/api/product/update-category', payload),
    updateStatusCategory: (payload) => axiosClient.put('/api/product/change-status-product-category', payload),
    getDetailCategory: (id) => axiosClient.get(`/api/product/detail-category/${id}`),
    deleteCategory: (id) => axiosClient.delete(`/api/product/delete-category/${id}`),
};

export default productCategoryApis;
