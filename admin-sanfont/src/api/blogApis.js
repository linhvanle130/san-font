import axiosClient from './axiosClient';

const blogApis = {
    getListBlog: (params) => axiosClient.get('/api/blog', { params }),
    getDetailBlog: (id) => axiosClient.get(`/api/blog/detail-blog/${id}`),
    changeStatusBlog: (payload) => axiosClient.put('/api/blog/change-status-blog', payload),
    updateBlog: (payload) => axiosClient.put('/api/blog/update-blog', payload),
    createBlog: (payload) => axiosClient.post('/api/blog/create-blog', payload),
    deleteBlog: (id) => axiosClient.delete(`/api/blog/delete-blog/${id}`),
};

export default blogApis;
