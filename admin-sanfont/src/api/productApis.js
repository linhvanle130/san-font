import axiosClient from './axiosClient';

const productApis = {
    getListProduct: (params) => axiosClient.get('/api/product', { params }),
    getDetailProduct: (id) => axiosClient.get(`/api/product/detail-product/${id}`),
    createProduct: (payload) => axiosClient.post('/api/product/new-product', payload),
    updateProduct: (payload) => axiosClient.put('/api/product/update-product', payload),
    changeStatusProduct: (payload) => axiosClient.put('/api/product/change-status-product', payload),
    deleteProduct: (id) => axiosClient.delete(`/api/product/delete-product/${id}`),
    getTopUserProductComment: (params) => axiosClient.get('/api/product/top-product-comment', { params }),
};

export default productApis;
