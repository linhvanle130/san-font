import axiosClient from './axiosClient';

const orderPackageApis = {
    getAllOrderPackage: (params) => axiosClient.get('/api/order-package/', { params }),
    changeStatusOrderPackage: (payload) => axiosClient.put('/api/order-package/status-order-package', payload),
    createOrderPackage: (payload) => axiosClient.post('/api/order-package/new-order-package', payload),
    updateOrderPackage: (payload) => axiosClient.put('/api/order-package/update-order-package', payload),
    getDetailOrderPackage: (id) => axiosClient.get(`/api/order-package/detail-order-package/${id}`),
    deleteOrderPackage: (id) => axiosClient.delete(`/api/order-package/delete-order-package/${id}`),
};

export default orderPackageApis;
