import axiosClient from './axiosClient';

const aclApis = {
    getListModule: (params) => axiosClient.get('/api/acl/get-list-module', { params }),
    getAclGroup: (params) => axiosClient.get('/api/acl/', { params }),
    createRoleModule: (body) => axiosClient.post('/api/acl/create-role-module/', body),
    getAclActionWithModuleId: (moduleId) => axiosClient.get(`/api/acl/get-acl-with-module/${moduleId}`),
    deleteRoleModule: (body) => axiosClient.put('/api/acl/delete-role-module/', body),
};

export default aclApis;
