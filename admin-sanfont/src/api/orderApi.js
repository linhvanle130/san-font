import axiosClient from './axiosClient';

const orderApis = {
    getAllOrder: (params) => axiosClient.get('/api/order/list-order', { params }),
    getListOrder: (params) => axiosClient.get('/api/order/list-order-with-conditions', { params }),
    getDetailOrder: (id) => axiosClient.get(`/api/order/detail-order/${id}`),
    getSearchOrder: (params) => axiosClient.get('/api/order/search-order', { params }),
    updateOrder: (payload) => axiosClient.put('/api/order/update-order', payload),
    createOrder: (payload) => axiosClient.post('/api/order/new-order', payload),
    deleteOrder: (id) => axiosClient.delete(`/api/order/delete-order/${id}`),
    changeStatusOrder: (payload) => axiosClient.put('/api/order/status-order', payload),
};

export default orderApis;
