import axiosClient from './axiosClient';

const userApis = {
    getListUsers: (params) => axiosClient.get('/api/user/user-info/', { params }),
    getAccountInfoUser: ({ id }) => axiosClient.get(`/api/user/user-info/detail/${id}`),
    getTopReferrer: () => axiosClient.get('/api/user/user-info/top-user-referrer'),
    getTopUserProduct: () => axiosClient.get('/api/user/user-info/top-user-product'),
    getTopUserProductSave: (params) => axiosClient.get('/api/user/user-info/top-user-product-save', { params }),
    setLevelUser: (body) => axiosClient.put('/api/user/user-info/set-level-user/level', body),
    updateOrderStatusUser: (body) => axiosClient.put('/api/user/user-info/update-order-user/order-status', body),
    updateProfileUser: ({ id, ...payload }) => axiosClient.put(`/api/user/user-info/${id}`, payload),
};

export default userApis;
