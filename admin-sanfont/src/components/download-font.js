import React, { useState } from 'react';
import downloadFontApis from './../api/downloadFontApis';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router';
import { yupResolver } from '@hookform/resolvers/yup';
import successHelper from '@/utils/success-helper';
import errorHelper from '@/utils/error-helper';
import { Controller, useForm } from 'react-hook-form';
import yup from '@/utils/yup';
import { Input } from 'antd';
import { AiOutlineCheck } from 'react-icons/ai';

const DownloadFont = () => {
    const { t } = useTranslation();
    const [loading, setLoading] = useState(false);
    const navigate = useNavigate();

    const schema = yup.object({
        productId: yup.number().required(),
        nameDownload: yup.string().max(255).required(),
    });

    const {
        control,
        reset,
        handleSubmit,
        formState: { errors },
        setValue,
    } = useForm({
        resolver: yupResolver(schema),
    });

    const submitCreate = (values) => {
        const { productId, nameDownload } = values;

        const payload = {
            productId: productId,
            nameDownload: nameDownload,
        };
        return downloadFontApis
            .createDownloadFont(payload)
            .then(() => {
                successHelper(t('create_success'));
                navigate('/');
            })
            .catch((err) => {
                console.log(err);
                errorHelper(err);
            })
            .finally(() => {
                setLoading(false);
            });
    };

    return (
        <div>
            <Modal footer={null} open={visible} onCancel={onClose} maskClosable={false} destroyOnClose>
                <form onSubmit={handleSubmit(submitCreate)}>
                    <div className="mb-3">
                        <h4 className="mb-2">ID product</h4>
                        <Controller
                            name="productId"
                            control={control}
                            render={({ field }) => <Input {...field} placeholder="ID product" />}
                        />
                        {errors?.productId?.message && <p className="text-error mt-1">{errors?.productId?.message}</p>}
                    </div>
                    <div className="mb-3">
                        <h4 className="mb-2">name down font</h4>
                        <Controller
                            name="nameDownload"
                            control={control}
                            render={({ field }) => <Input {...field} placeholder="name down font" />}
                        />
                        {errors?.nameDownload?.message && (
                            <p className="text-error mt-1">{errors?.nameDownload?.message}</p>
                        )}
                    </div>
                    <button type="submit" className=" bg-main-color py-2 px-4 hover:shadow-4xl rounded-[2px]">
                        <div className="flex gap-2 items-center text-white">
                            <AiOutlineCheck />
                            <span className="text-sm">{t('submit')}</span>
                        </div>
                    </button>
                </form>
            </Modal>
        </div>
    );
};

export default DownloadFont;
