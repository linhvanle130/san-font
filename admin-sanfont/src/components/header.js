import React, { useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { AiOutlineAlignLeft, AiOutlineAlignRight } from 'react-icons/ai';
import { CiLogin } from 'react-icons/ci';
import { FaGlobe } from 'react-icons/fa';
import { FiUser } from 'react-icons/fi';
import { useTranslation } from 'react-i18next';
import classNames from 'classnames';

import LocalStorage from '@/utils/storage';
import { MODE_THEME } from '@/constants';
import { STORAGE_KEY } from '@/constants/storage-key';
import { logout } from '@/store/slices/accountSlice';
import { changeTheme } from '@/store/slices/commonSlice';
import accountApis from '@/api/accountApis';
import Vector from '../resources/images/vector.png';

const Header = ({ pushToggle, isSideBarOpen, isShown }) => {
    const dispatch = useDispatch();
    const { t } = useTranslation();
    const { i18n } = useTranslation();
    const navigate = useNavigate();

    const handleChangeLng = (lng) => {
        i18n.changeLanguage(lng);
        localStorage.setItem('lng', lng);
    };

    const { theme } = useSelector((state) => state.common);
    const onChangeTheme = () => {
        const newTheme = theme === MODE_THEME.LIGHT ? MODE_THEME.DARK : MODE_THEME.LIGHT;
        dispatch(changeTheme(newTheme));
    };

    const onClickMenu = () => {
        onLogout();
    };

    const onLogout = () => {
        LocalStorage.remove(STORAGE_KEY.TOKEN);
        LocalStorage.remove(STORAGE_KEY.INFO);
        dispatch(logout());
    };

    const handleToggle = () => {
        pushToggle(!isSideBarOpen);
    };

    const handleEditProfile = () => {
        navigate(`/profile`);
    };

    const [profile, setProfile] = useState();
    const getProfileAccount = async () => {
        const profile = await accountApis.getProfile();
        setProfile(profile);
    };
    useEffect(() => {
        getProfileAccount();
    }, []);

    return (
        <div
            className={classNames('page-topbar bg-[#f8f9fa] dark-container', {
                sidebar_shift: !isSideBarOpen && !isShown,
            })}
        >
            <div className="flex items-center justify-between w-full">
                <div className="text-[24px] cursor-pointer" onClick={handleToggle}>
                    {isSideBarOpen ? (
                        <AiOutlineAlignLeft className="text-black dark-icon-header text-dark max-lg:text-white" />
                    ) : (
                        <AiOutlineAlignRight className="text-black dark-icon-header text-dark max-lg:text-white" />
                    )}
                </div>
                <div className="flex items-center justify-items-start">
                    <div className="flex items-center gap-2">
                        <label className="relative inline-flex items-center mr-5 cursor-pointer">
                            <input
                                type="checkbox"
                                className="sr-only peer"
                                checked={theme === MODE_THEME.LIGHT}
                                onChange={onChangeTheme}
                            />
                            <div className="w-11 h-6 bg-[#212529] rounded-full peer peer-focus:ring-4  dark:peer-focus:ring-main-color  peer-checked:after:translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-0.5 after:left-[2px] after:bg-white  after:border after:rounded-full after:h-5 after:w-5 after:transition-all peer-checked:bg-main-color"></div>
                            <h4 className="ml-3 text-sm font-medium text-gray-900 max-lg:text-white">
                                {theme === MODE_THEME.LIGHT ? t('light') : t('dark')}
                            </h4>
                        </label>
                    </div>
                    <div className="dropdown dropdown-end">
                        <div className="w-[28px] h-[28px] rounded-full cursor-pointer hover:opacity-80">
                            <img
                                tabIndex={0}
                                src={profile?.userInformation?.avatar || Vector}
                                alt=""
                                className="w-full h-full rounded-full object-cover"
                            />
                        </div>
                        <ul
                            tabIndex={0}
                            className="dropdown-content menu p-2 shadow rounded-md w-48 z-10 bg-white mt-5 "
                        >
                            <li>
                                <div
                                    onClick={() => handleEditProfile()}
                                    className="text-gray-700 hover:text-red-500 bg-transparent h-11"
                                >
                                    <FiUser />
                                    <span className="uppercase">{profile?.username}</span>
                                </div>
                            </li>
                            <li onClick={() => onClickMenu()}>
                                <Link to="/login" className="text-gray-700 hover:text-red-500 bg-transparent h-11">
                                    <CiLogin /> <span>{t('logout')}</span>
                                </Link>
                            </li>
                        </ul>
                    </div>
                    <div className="dropdown dropdown-end pl-3 max-sm:hidden">
                        <FaGlobe tabIndex={0} className="text-2xl m-1 mr-3 cursor-pointe dark-icon-header" />
                        <ul
                            tabIndex={0}
                            className="dropdown-content menu p-2 shadow rounded-md w-48 z-10 bg-white mt-5 "
                        >
                            <li>
                                <Link
                                    className="text-gray-700 hover:text-red-500 bg-transparent h-11"
                                    onClick={() => handleChangeLng('en')}
                                >
                                    {t('en')}
                                </Link>
                            </li>
                            <li>
                                <Link
                                    to="/"
                                    className="text-gray-700 hover:text-red-500 bg-transparent h-11"
                                    onClick={() => handleChangeLng('vi')}
                                >
                                    {t('vi')}
                                </Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Header;
