import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { AiOutlineChrome, AiOutlineSetting } from 'react-icons/ai';
import { BiShoppingBag } from 'react-icons/bi';
import { FiChevronDown, FiChevronUp, FiUsers } from 'react-icons/fi';
import { TbBrandBlogger } from 'react-icons/tb';
import { MdOutlinePayment } from 'react-icons/md';
import classNames from 'classnames';

import aclApis from '@/api/aclApis';
import { MODULE } from '@/constants';
import Vector from '../resources/images/vector.png';
import Logo from '../resources/images/Logo.png';
import '@/locales/i18n';

const Sidebar = ({ isSideBarOpen, isShown }) => {
    const { t } = useTranslation();
    const [menus, setMenus] = useState([]);
    const [openMenu, setOpenMenu] = useState('');
    const [activeMenu, setActiveMenu] = useState('');
    const account = JSON.parse(localStorage.getItem('info'));

    const fetchAclOfUser = async () => {
        const listAclGroup = await aclApis.getAclGroup({ groupId: account.role });
        const listMenu = [];
        listAclGroup.map((acl) =>
            menu.map((mn) => {
                if (
                    acl.actions.moduleId === mn.module &&
                    mn.module &&
                    !listMenu.find((e) => e.module === acl.actions.moduleId)
                ) {
                    listMenu.push(mn);
                }
            }),
        );
        setMenus(listMenu);
    };
    useEffect(() => {
        fetchAclOfUser();
    }, []);

    const menu = [
        {
            name: t('dashboard'),
            icon: <AiOutlineChrome />,
            module: MODULE.DASHBOARD,
            link: '/',
            menu: [],
        },
        {
            name: t('product_management'),
            icon: <BiShoppingBag />,
            module: MODULE.PRODUCT,
            menu: [
                {
                    link: '/product/create',
                    title: t('product_create'),
                },
                {
                    link: '/product',
                    title: t('product_all'),
                },
                {
                    link: '/product-category',
                    title: t('product_category'),
                },
                {
                    link: '/product-element',
                    title: t('product_element'),
                },
                {
                    link: '/product-save',
                    title: t('product_save'),
                },
            ],
        },
        {
            name: t('user'),
            icon: <FiUsers />,
            module: MODULE.USER,
            menu: [
                {
                    link: '/user',
                    title: t('list_users'),
                },
                {
                    link: '/comment',
                    title: t('user_feedback'),
                },
                {
                    link: '/contact',
                    title: t('contact'),
                },
            ],
        },
        {
            name: t('sale_management'),
            icon: <MdOutlinePayment />,
            module: MODULE.ORDER,
            menu: [
                {
                    link: '/order-new',
                    title: t('order_new'),
                },
                {
                    link: '/order',
                    title: t('order_list'),
                },
                {
                    link: '/order-package',
                    title: t('order_package'),
                },
            ],
        },
        {
            name: t('blog_management'),
            icon: <TbBrandBlogger />,
            module: MODULE.BLOG,
            menu: [
                {
                    link: '/blog/create',
                    title: t('blog_create'),
                },
                {
                    link: '/blog',
                    title: t('blog_all'),
                },
                {
                    link: 'blog-category/create',
                    title: t('blog_category_create'),
                },
            ],
        },
        {
            name: t('config'),
            icon: <AiOutlineSetting />,
            module: MODULE.CONFIG,
            menu: [
                {
                    link: '/config-data',
                    title: t('config_data'),
                },
                {
                    link: '/config-role',
                    title: t('config_role'),
                },
            ],
        },
    ];

    useEffect(() => {
        menus.forEach((e) => {
            if (e.menu?.length) {
                const activeChild = e.menu.find((i) => i.link === window.location.pathname);
                if (activeChild) {
                    setOpenMenu(e.name);
                    setActiveMenu(activeChild.link);
                }
            }
        });
    }, [window.location.pathname]);
    return (
        <div
            className={classNames('page-sidebar bg-[#16213e] dark-container absolute-side', {
                'collapseit close-sidebar': !isSideBarOpen && !isShown,
            })}
        >
            <div className="page-sidebar__wrapper" id="main-menu-wrapper">
                <div className="logo-area">
                    <div className="logo">
                        <div className="logo__icon">
                            <img src={Vector} alt="logo" className="h-full w-full" />
                        </div>

                        <div className="logo__text">
                            <img src={Logo} alt="logo" className="h-full w-full" />
                        </div>
                    </div>
                </div>
                <ul className="wraplist">
                    {menus.map((item, index) => (
                        <li className="wraplist__item" key={index}>
                            {item.name && (
                                <ul
                                    className="navbar-nav"
                                    onClick={() => setOpenMenu(item.name === openMenu ? '' : item.name)}
                                >
                                    <li className="nav-item flex justify-between">
                                        {item.menu.length == 0 || item.link == '/' ? (
                                            <Link to={item.link} key={item.title} className="flex items-center">
                                                <i className="nav-item__img">{item.icon}</i>
                                                <span className="nav-item__title">{item.name}</span>
                                            </Link>
                                        ) : (
                                            <div className="flex items-center">
                                                <i className="nav-item__img">{item.icon}</i>
                                                <span className="nav-item__title">{item.name}</span>
                                            </div>
                                        )}

                                        {item.menu.length > 0 ? (
                                            openMenu === item.name ? (
                                                <i>
                                                    <FiChevronUp />
                                                </i>
                                            ) : (
                                                <i>
                                                    <FiChevronDown />
                                                </i>
                                            )
                                        ) : (
                                            ''
                                        )}
                                    </li>

                                    <ul
                                        className={classNames('nav-item__sub', {
                                            'is-open-menu': openMenu === item.name,
                                        })}
                                        style={{
                                            height:
                                                openMenu === item.name || !item.name
                                                    ? `${item.menu.length * 45}px`
                                                    : '0',
                                        }}
                                    >
                                        {item.menu.map((e, i) => {
                                            return (
                                                e && (
                                                    <li
                                                        key={i}
                                                        className={classNames({
                                                            open: activeMenu === e.link,
                                                        })}
                                                        onClick={() => setActiveMenu(e.link)}
                                                    >
                                                        <Link
                                                            to={e.link || '/'}
                                                            key={e.title}
                                                            onClick={() => setActiveMenu(e.link)}
                                                        >
                                                            <span className="title">{e.title}</span>
                                                        </Link>
                                                    </li>
                                                )
                                            );
                                        })}
                                    </ul>
                                </ul>
                            )}
                        </li>
                    ))}
                </ul>
            </div>
        </div>
    );
};

export default Sidebar;
