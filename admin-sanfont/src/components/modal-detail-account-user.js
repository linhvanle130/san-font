import React, { forwardRef, useEffect, useImperativeHandle, useState } from 'react';
import { Modal, Spin } from 'antd';
import { useTranslation } from 'react-i18next';
import { numberDecimalWithCommas } from '@/utils/funcs';
import { MASTER_DATA_NAME } from '@/constants';
import errorHelper from '@/utils/error-helper';
import userApis from '@/api/userApis';
import masterApis from '@/api/masterAps';

const ProfileTab = ({ info }) => {
    const { t } = useTranslation();
    const [masterLevelUser, setMasterLevelUser] = useState();
    const [masterRoleUser, setMasterRoleUser] = useState();

    const fetchMasterData = async () => {
        const masterLevel = await masterApis.getAllMaster({
            idMaster: MASTER_DATA_NAME.LEVEL_USER,
        });
        const masterRole = await masterApis.getAllMaster({
            idMaster: MASTER_DATA_NAME.ROLE,
        });
        setMasterLevelUser(masterLevel);
        setMasterRoleUser(masterRole);
    };

    useEffect(() => {
        fetchMasterData();
    }, [info]);

    const totalPrice = info?.order
        ?.map((item) => item.total)
        ?.reduce((acc, curr) => acc + curr, 0)
        ?.reduce((acc, curr) => acc + curr, 0);

    return (
        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 p-1">
            <div className="col-span-1 flex items-center justify-center my-3">
                <div className="w-[200px] h-[200px] object-cover">
                    <img
                        src={info?.userInformation?.avatar || '/ddf-logo.png'}
                        alt="images"
                        className="w-full h-full rounded-full"
                    />
                </div>
            </div>
            <div className="col-span-2">
                <div className="mx-0 md:mx-2 lg:mx-8">
                    <div className="flex justify-between items-center py-2">
                        <h4 className="font-semibold text-black w-2/4">Id người dùng</h4>
                        <h4 className="w-2/4">{info?.userCode}</h4>
                    </div>
                    <div className="flex justify-between items-center py-2">
                        <h4 className="font-semibold text-black w-2/4">{t('full_name')}</h4>
                        <h4 className="w-2/4">{info?.username}</h4>
                    </div>
                    <div className="flex justify-between items-center py-2">
                        <h4 className="font-semibold text-black w-2/4">{t('email')}</h4>
                        <h4 className="w-2/4">{info?.email}</h4>
                    </div>
                    <div className="flex justify-between items-center py-2">
                        <h4 className="font-semibold text-black w-2/4">{t('level')}</h4>
                        <h4 className="w-2/4">{masterLevelUser?.find((e) => e.id === info?.level)?.name}</h4>
                    </div>
                    <div className="flex justify-between items-center py-2">
                        <h4 className="font-semibold text-black w-2/4">{t('role')}</h4>
                        <h4 className="w-2/4">{masterRoleUser?.find((e) => e.id === info?.role)?.name}</h4>
                    </div>
                    <div className="flex justify-between items-center py-2">
                        <h4 className="font-semibold text-black w-2/4">Tổng sô tiền đăng ký gói thành viên</h4>
                        <h4 className="w-2/4"> {numberDecimalWithCommas(totalPrice)} đ</h4>
                    </div>
                    <div className="flex justify-between items-center py-2">
                        <h4 className="font-semibold text-black w-2/4">{t('phone')}</h4>
                        <h4 className="w-2/4">
                            {info?.phoneCode && info?.phoneNumber && `+${info?.phoneCode}${info?.phoneNumber}`}
                        </h4>
                    </div>
                    <div className="flex justify-between items-center py-2">
                        <h4 className="font-semibold text-black w-2/4">{t('address')}</h4>
                        <h4 className="w-2/4">{info?.userInformation?.address}</h4>
                    </div>
                    <div className="flex justify-between items-center py-2">
                        <h4 className="font-semibold text-black w-2/4">Tổng số up font</h4>
                        <h4 className="w-2/4 flex flex-col">{info?.product?.length}</h4>
                    </div>
                    <div className="flex justify-between items-center py-2">
                        <h4 className="font-semibold text-black w-2/4">Tổng số font đã lưu</h4>
                        <h4 className="w-2/4 flex flex-col">{info?.productSave?.length}</h4>
                    </div>
                </div>
            </div>
        </div>
    );
};

const ModalDetailAccountUser = ({}, ref) => {
    const { t } = useTranslation();

    const [visible, setVisible] = useState(false);
    const [loading, setLoading] = useState(false);
    const [accountInfo, setAccountInfo] = useState(null);

    const onGetAccountInfo = ({ id }) => {
        setLoading(true);
        userApis
            .getAccountInfoUser({ id })
            .then((res) => {
                setAccountInfo(res);
            })
            .catch((err) => {
                errorHelper(err);
            })
            .finally(() => {
                setLoading(false);
            });
    };

    const onOpen = (data) => {
        const { id } = data;
        setVisible(true);
        onGetAccountInfo({ id });
    };

    useImperativeHandle(ref, () => ({
        onOpen,
    }));

    const onClose = () => {
        setVisible(false);
        setLoading(false);
        setAccountInfo(null);
    };

    return (
        <Modal footer={null} open={visible} onCancel={onClose} maskClosable={false} destroyOnClose width={655}>
            <h4 className="font-semibold text-black">{t('account_detail')}</h4>
            <Spin spinning={loading}>{accountInfo && <ProfileTab info={accountInfo} />}</Spin>
        </Modal>
    );
};

export default forwardRef(ModalDetailAccountUser);
