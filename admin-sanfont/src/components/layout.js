import React, { useState } from 'react';
import Header from './header';
import { Outlet } from 'react-router-dom';
import Sidebar from './sidebar';

const Layout = () => {
    const [isShown, setIsShown] = useState(false);
    const [isSideBarOpen, setIsSideBarOpen] = useState(true);
    const pushToggle = (toggle) => {
        setIsSideBarOpen(toggle);
    };

    return (
        <div className="bg-[#f8f9fa] min-h-screen">
            <div className="">
                <div onMouseEnter={() => setIsShown(true)} onMouseLeave={() => setIsShown(false)}>
                    <Sidebar pushToggle={pushToggle} isSideBarOpen={isSideBarOpen} isShown={isShown} />
                </div>
                <Header pushToggle={pushToggle} isSideBarOpen={isSideBarOpen} isShown={isShown} />
                <div id="main-content" className={!isSideBarOpen && !isShown ? 'sidebar_shift' : ''}>
                    <div className="main-wrapper p-3 md:p-6 lg:p-8">
                        <Outlet />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Layout;
