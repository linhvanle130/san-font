import classNames from 'classnames';
import React from 'react';

const Container = ({ title = 'Administration ', children, className }) => {
    return (
        <div className={classNames(className, 'flex flex-1')}>
            {/* <Helmet> */}
            <title>{title}</title>
            {/* </Helmet> */}
            {children}
        </div>
    );
};

export default Container;
