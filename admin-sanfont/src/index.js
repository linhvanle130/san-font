import '@/locales/i18n';
import React from 'react';
import '@/resources/styles/index.scss';
import 'react-toastify/dist/ReactToastify.css';
import { createRoot } from 'react-dom/client';
import { Provider } from 'react-redux';
import MainApp from './app/main';
import store from './store';
import { ConfigProvider } from 'antd';
import viVN from 'antd/lib/locale/vi_VN';

const container = document.getElementById('application');
const root = createRoot(container);

root.render(
    <ConfigProvider locale={viVN}>
        <Provider store={store}>
            <MainApp />
        </Provider>
    </ConfigProvider>,
);
