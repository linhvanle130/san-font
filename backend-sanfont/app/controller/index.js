import { initController } from "./init.controller";
import { initWebUserController } from "./user/user.controller";
import { initWebAuthController } from "./user/auth.controller";
import { initWebAuthAdminController } from "./admin/auth.controller";
import { initOtpController } from "./user/otp.controller";
import { initOtpAdminController } from "./admin/otp.controller";
import { initWebProductController } from "./product.controller";
import { initWebCommonController } from "./common.controller";
import { initWebBlogController } from "./blog.controller";
import { initAclController } from "./acl.controller";
import { initWebContractController } from "./contract.controller";
import { initWebOrderPackageController } from "./order/orderPackage.controller";
import { initWebOrderController } from "./order/order.controller";
import { initWebUpFontController } from "./upfont.controller";
import { initWebCommentController } from "./comment.controller";
import { initWebMasterController } from "./master.controller";
import { initWebDownloadFontController } from "./download.controller";
import { initWebOrderContentController } from "./order/orderContent.controller";

export function initApiController(app) {
  initController(app);
  initAclController(app);
  initWebAuthController(app);
  initWebAuthAdminController(app);
  initOtpController(app);
  initOtpAdminController(app);
  initWebProductController(app);
  initWebCommonController(app);
  initWebUserController(app);
  initWebBlogController(app);
  initWebContractController(app);
  initWebOrderController(app);
  initWebOrderPackageController(app);
  initWebOrderContentController(app);
  initWebUpFontController(app);
  initWebCommentController(app);
  initWebMasterController(app);
  initWebDownloadFontController(app)
}

