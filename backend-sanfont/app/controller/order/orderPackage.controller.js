import express from "express";
import { successResponse } from "../../util/response.util";
import { pagingParse } from "../../middleware/paging.middleware";
import { createOrderPackage, deleteOrderPackage, getDetailOrderPackage, getListOrderPackage, statusOrderPackage, updateOrderPackage } from "../../service/order/orderPackage.service";
import { newOrderPackageValidator, statusOrderPackageValidator, updateOrderPackageValidator } from "../../validator/order.validator";
import { isAuthenticated } from "../../middleware/permission";
import { MODULE } from "../../constants/common.constant";

const order = express.Router();

order.post("/new-order-package", isAuthenticated([MODULE.ORDER]), [newOrderPackageValidator], async (req, res, next) => {
  return createOrderPackage(req.body)
    .then((t) => successResponse(res, t))
    .catch(next);
});

order.get(
  '/',
  [pagingParse({ column: 'id', dir: 'desc' })],
  (req, res, next) => {
    return getListOrderPackage(req.query, req.paging)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

order.get("/detail-order-package/:id", (req, res, next) => {
  return getDetailOrderPackage(req.params.id)
    .then((t) => successResponse(res, t))
    .catch(next);
});

order.put(
  '/update-order-package',
  [updateOrderPackageValidator],
  (req, res, next) => {
    return updateOrderPackage(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

order.delete(
  '/delete-order-package/:id',
  isAuthenticated([MODULE.ORDER]),
  (req, res, next) => {
    return deleteOrderPackage(req.params.id)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

order.put(
  '/status-order-package',
  [statusOrderPackageValidator],
  (req, res, next) => {
    return statusOrderPackage(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

export function initWebOrderPackageController(app) {
  app.use("/api/order-package", order);
}
