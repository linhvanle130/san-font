import express from "express";
import { successResponse } from "../../util/response.util";
import { createOrder, deleteOrder, getDetailOrder, getListOrder, getListOrderWithCondition, getOrderByMailAndCode, getOrderUser, statusOrder, updateOrder } from "../../service/order/order.service";
import { newOrderValidator, statusOrderValidator, updateOrderValidator } from "../../validator/order.validator";
import { pagingParse } from '../../middleware/paging.middleware';

const order = express.Router();

order.post("/new-order", [newOrderValidator], async (req, res, next) => {
  return createOrder(req.body)
    .then((t) => successResponse(res, t))
    .catch(next);
});

order.get(
  '/list-order',
  [pagingParse({ column: 'id', dir: 'desc' })],
  (req, res, next) => {
    return getListOrder(req.query, req.paging)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

order.get(
  "/list-order-with-conditions",
  (req, res, next) => {
    return getListOrderWithCondition(req.query)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

order.get(
  '/my-order',
  (req, res, next) => {
    return getOrderUser(req.query)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

order.get(
  '/search-order',
  async (req, res, next) => {
    return getOrderByMailAndCode(req.query)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

order.get(
  '/detail-order/:id',
  (req, res, next) => {
    return getDetailOrder(req.params.id)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

order.put(
  "/update-order",
  [updateOrderValidator],
  (req, res, next) => {
    return updateOrder(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

order.delete(
  '/delete-order/:id',
  (req, res, next) => {
    return deleteOrder(req.params.id)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

order.put(
  '/status-order',
  [statusOrderValidator],
  (req, res, next) => {
    return statusOrder(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

export function initWebOrderController(app) {
  app.use("/api/order", order);
}
