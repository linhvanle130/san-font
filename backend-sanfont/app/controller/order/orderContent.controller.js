import express from 'express';
import { successResponse } from '../../util/response.util';
import { createOrderContent, deleteOrderContent, getListOrderContent, getOrderContent, updateOrderContent } from '../../service/order/orderContent.service';
import { createOrderContentValidator, deleteOrderContentValidator, updateOrderContentValidator } from '../../validator/order.validator';
import { pagingParse } from '../../middleware/paging.middleware';

const content = express.Router();

content.post(
  '/new-content',
  [createOrderContentValidator],
  (req, res, next) => {
    return createOrderContent(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

content.get(
  '/',
  [pagingParse({ column: 'id', dir: 'desc' })],
  (req, res, next) => {
    return getListOrderContent(req.query, req.paging)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

content.get(
  "/get-content",
  (req, res, next) => {
    return getOrderContent(req.query)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

content.put(
  '/update-content',
  [updateOrderContentValidator],
  (req, res, next) => {
    return updateOrderContent(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

content.delete(
  '/delete-content',
  [deleteOrderContentValidator],
  (req, res, next) => {
    return deleteOrderContent(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

export function initWebOrderContentController(app) {
  app.use("/api/order-content", content);
}
