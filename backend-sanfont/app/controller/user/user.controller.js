import express from 'express';
import { pagingParse } from '../../middleware/paging.middleware';
import { isAuthenticated } from '../../middleware/permission';
import {
  deleteUserAddress,
  getListReferalUser,
  getListUser,
  getMyReferrer,
  getTopUserProduct,
  getTopUserProductSave,
  getTopUserReferrer,
  getUserByUserCode,
  getUserDetail,
  registerUserAddress,
  setDefaultAddress,
  updateLevelUser,
  updateUserAddress,
  updateUserProfile
} from "../../service/user/user.service";
import { successResponse } from '../../util/response.util';
import {
  updateUserProfileValidator,
  registUserAddressValidator,
  setLevelUserValidator
} from '../../validator/user.validator';

const user = express.Router();

user.put(
  '/:id',
  [isAuthenticated(), updateUserProfileValidator],
  (req, res, next) => {
    return updateUserProfile(req.user, req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

user.put(
  '/set-level-user/level',
  [setLevelUserValidator],
  (req, res, next) => {
    return updateLevelUser(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

user.post(
  '/register-user-address',
  [isAuthenticated(), registUserAddressValidator],
  async (req, res, next) => {
    return registerUserAddress(req.user, req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

user.put(
  '/update-user-address/:id',
  [isAuthenticated(), registUserAddressValidator],
  async (req, res, next) => {
    return updateUserAddress(req.params.id, req.user, req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

user.put(
  '/set-default-address/:id',
  [isAuthenticated()],
  async (req, res, next) => {
    return setDefaultAddress(req.params.id, req.user)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

user.delete(
  '/delete-address/:id',
  [isAuthenticated()],
  async (req, res, next) => {
    return deleteUserAddress(req.params.id)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

user.get(
  '/',
  [pagingParse({ column: 'id', dir: 'desc' })],
  (req, res, next) => {
    return getListUser(req.query, req.paging)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

user.get(
  '/top-user-referrer',
  (req, res, next) => {
    return getTopUserReferrer()
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

user.get(
  '/user-with-user-code',
  (req, res, next) => {
    return getUserByUserCode(req.query)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

user.get('/detail/:userId',
 (req, res, next) => {
  return getUserDetail(req.params.userId)
    .then((t) => successResponse(res, t))
    .catch(next);
});

user.get('/my-referrer/:id', (req, res, next) => {
  return getMyReferrer(req.params.id)
    .then((t) => successResponse(res, t))
    .catch(next);
});

user.get('/count-referrer-with-level', (req, res, next) => {
  return getListReferalUser(req.query)
    .then((t) => successResponse(res, t))
    .catch(next);
});

user.get(
  '/top-user-product',
  (req, res, next) => {
    return getTopUserProduct()
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

user.get(
  "/top-user-product-save",
  [pagingParse({ column: "id", dir: "desc" })],
  (req, res, next) => {
    return getTopUserProductSave(req.query, req.paging)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

export function initWebUserController(app) {
  app.use('/api/user/user-info', user);
}
