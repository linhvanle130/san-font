import express from 'express';
import { successResponse } from '../util/response.util';
import { createDownloadFontValidator, updateDownloadFontValidator } from '../validator/download.validator';
import { createDownloadFont, getDownloadFont, getListDownloadFont, updateDownloadFont } from '../service/download.service';
import { pagingParse } from '../middleware/paging.middleware';

const download = express.Router();

download.post(
  '/new-download',
  [createDownloadFontValidator],
  (req, res, next) => {
    return createDownloadFont(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

download.get(
  '/',
  [pagingParse({ column: 'id', dir: 'desc' })],
  (req, res, next) => {
    return getListDownloadFont(req.query, req.paging)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

download.get(
  "/get-download",
  (req, res, next) => {
    return getDownloadFont(req.query)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

download.put(
  '/update-download',
  [updateDownloadFontValidator],
  (req, res, next) => {
    return updateDownloadFont(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

export function initWebDownloadFontController(app) {
  app.use("/api/download", download);
}
