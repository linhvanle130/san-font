import express from 'express';
import { successResponse } from '../util/response.util';
import { createContractValidator, updateContractValidator } from '../validator/contract.validator';
import { createContract, getDetailContract, getListContract, updateContract } from '../service/contract.service';
import { pagingParse } from '../middleware/paging.middleware';

const contract = express.Router();

contract.get(
  '/',
  [pagingParse({ column: 'id', dir: 'desc' })],
  (req, res, next) => {
    return getListContract(req.query, req.paging)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

contract.post(
  '/new-contract',
  [createContractValidator],
  (req, res, next) => {
    return createContract(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

contract.put(
  '/update-contract',
  [updateContractValidator],
  (req, res, next) => {
    return updateContract(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

contract.get(
  '/detail-contract/:id',
  (req, res, next) => {
    return getDetailContract(req.params.id)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

export function initWebContractController(app) {
  app.use("/api/contract", contract);
}
