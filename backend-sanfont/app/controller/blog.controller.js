import express from 'express';
import { pagingParse } from '../middleware/paging.middleware';
import { successResponse } from '../util/response.util';
import { changeStatusBlogValidator, createBlogCategoryValidator, createBlogValidator, updateBlogCategoryValidator, updateBlogValidator } from '../validator/blog.validator';
import { changeStatusBlog, createBlog, createBlogCategory, deleteBlog, deleteBlogCategory, getBlogCategoryWithPaging, getDetailBlog, getDetailBlogCategory, getListBlog, getListBlogCategory, updateBlog, updateBlogCategory } from '../service/blog.service';
import { isAuthenticated } from '../middleware/permission';
import { MODULE } from '../constants/common.constant';

const blog = express.Router();

/**
 * ================ CONTORLLER BLOG ====================
 */
blog.get(
  '/',
  [pagingParse({ column: 'id', dir: 'desc' })],
  (req, res, next) => {
    return getListBlog(req.query, req.paging)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

blog.get("/detail-blog/:id", (req, res, next) => {
  return getDetailBlog(req.params.id)
    .then((t) => successResponse(res, t))
    .catch(next);
});

blog.post(
  '/create-blog',
  isAuthenticated([MODULE.BLOG]),
  [createBlogValidator],
  (req, res, next) => {
    return createBlog(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

blog.put(
  '/update-blog',
  isAuthenticated([MODULE.BLOG]),
  [updateBlogValidator],
  (req, res, next) => {
    return updateBlog(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

blog.put(
  '/change-status-blog',
  [changeStatusBlogValidator],
  (req, res, next) => {
    return changeStatusBlog(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);


blog.delete(
  '/delete-blog/:id',
  isAuthenticated([MODULE.BLOG]),
  (req, res, next) => {
    return deleteBlog(req.params.id)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

/**
 * ================ CONTORLLER CATEGORY ====================
 */
blog.get(
  "/get-list-blog-category",
  (req, res, next) => {
    return getListBlogCategory()
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

blog.get(
  "/get-list-blog-category-with-paging",
  [pagingParse({ column: "id", dir: "desc" })],
  (req, res, next) => {
    return getBlogCategoryWithPaging(req.query, req.paging)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

blog.post(
  "/new-blog-category",
  isAuthenticated([MODULE.BLOG]),
  [createBlogCategoryValidator],
  (req, res, next) => {
    return createBlogCategory(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

blog.put(
  '/update-blog-category',
  isAuthenticated([MODULE.BLOG]),
  [updateBlogCategoryValidator],
  (req, res, next) => {
    return updateBlogCategory(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

blog.get(
  '/detail-blog-category/:id',
  async (req, res, next) => {
    return getDetailBlogCategory(req.params.id)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

blog.delete(
  '/delete-blog-category/:id',
  isAuthenticated([MODULE.BLOG]),
  (req, res, next) => {
    return deleteBlogCategory(req.params.id)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

export function initWebBlogController(app) {
  app.use("/api/blog", blog);
}
