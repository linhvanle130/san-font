import express from 'express';
import { successResponse } from '../util/response.util';
import { createCommentValidator, updateCommentValidator } from '../validator/comment.validator';
import { createComment, deleteComment, getDetailComment, getListComment, updateComment} from '../service/comment.service';
import { pagingParse } from '../middleware/paging.middleware';

const comment = express.Router();

comment.post(
  "/create-comment",
  [createCommentValidator],
  async (req, res, next) => {
    return createComment(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

comment.get(
  '/',
  [pagingParse({ column: 'id', dir: 'desc' })],
  (req, res, next) => {
    return getListComment(req.query, req.paging)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

comment.get(
  '/detail-comment/:id',
  async (req, res, next) => {
    return getDetailComment(req.params.id)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

comment.put(
  '/update-comment',
  [updateCommentValidator],
  (req, res, next) => {
    return updateComment(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

comment.delete(
  '/delete-comment/:id',
  (req, res, next) => {
    return deleteComment(req.params.id)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

export function initWebCommentController(app) {
  app.use("/api/comment", comment);
}
