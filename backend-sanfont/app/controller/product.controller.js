import express from "express";
import { pagingParse } from "../middleware/paging.middleware";
import {
  changeOutstandingProduct,
  changeStatusProduct,
  changeStatusProductCategory,
  createCategory,
  createCategoryType,
  createProduct,
  createSaveProduct,
  deleteCategory,
  deleteCategoryType,
  deleteProduct,
  deleteSaveProduct,
  getCapacityProduct,
  getCategoryWithPaging,
  getCategoryWithPagingType,
  getDetailCategory,
  getDetailCategoryType,
  getDetailProduct,
  getDetailSaveProduct,
  getListCategory,
  getListCategoryCode,
  getListCategoryType,
  getListProduct,
  getListSaveProduct,
  getListSaveProductType,
  getProductBySlug,
  getProductCategoryBySlug,
  getTopProductComment,
  updateCategory,
  updateCategoryType,
  updateProduct,
  updateSaveProduct
} from "../service/product.service";
import { successResponse } from "../util/response.util";
import {
  changeOutstandingProductValidator,
  changeStatusProductValidator,
  createCategoryTypeValidator,
  createCategoryValidator,
  createProductValidator,
  createSaveProductValidator,
  updateCategoryTypeValidator,
  updateCategoryValidator,
  updateProductValidator,
  updateSaveProductValidator
} from "../validator/product.validator";
import { isAuthenticated } from "../middleware/permission";
import { MODULE } from "../constants/common.constant";

const product = express.Router();

/**
 * ================ CONTORLLER PRODUCT ====================
 */
product.get(
  "/",
  [pagingParse({ column: "id", dir: "desc" })],
  (req, res, next) => {
    return getListProduct(req.query, req.paging)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

product.get("/detail-product/:id", (req, res, next) => {
  return getDetailProduct(req.params.id)
    .then((t) => successResponse(res, t))
    .catch(next);
});

product.post(
  "/new-product",
  isAuthenticated([MODULE.PRODUCT]),
  [createProductValidator],
  (req, res, next) => {
    return createProduct(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

product.put(
  "/update-product",
  isAuthenticated([MODULE.PRODUCT]),
  [updateProductValidator],
  (req, res, next) => {
    return updateProduct(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

product.put(
  "/change-status-product",
  [changeStatusProductValidator],
  (req, res, next) => {
    return changeStatusProduct(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

product.put(
  "/change-outstanding-product",
  [changeOutstandingProductValidator],
  (req, res, next) => {
    return changeOutstandingProduct(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

product.get("/get-by-slug", (req, res, next) => {
  return getProductCategoryBySlug(req.query)
    .then((t) => successResponse(res, t))
    .catch(next);
});

product.get(
  '/get-product-by-slug',
  (req, res, next) => {
    return getProductBySlug(req.query)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

product.get("/get-capacity-product", (req, res, next) => {
  return getCapacityProduct(req.query)
    .then((t) => successResponse(res, t))
    .catch(next);
});

product.delete(
  "/delete-product/:id",
  isAuthenticated([MODULE.PRODUCT]),
  (req, res, next) => {
    return deleteProduct(req.params.id)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

/**
 * ================ CONTORLLER CATEGORY ====================
 */
product.get("/get-list-category", (req, res, next) => {
  return getListCategory()
    .then((t) => successResponse(res, t))
    .catch(next);
});

product.get(
  "/get-list-category-with-paging",
  [pagingParse({ column: "id", dir: "desc" })],
  (req, res, next) => {
    return getCategoryWithPaging(req.query, req.paging)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

product.post(
  "/new-category",
  isAuthenticated([MODULE.PRODUCT]),
  [createCategoryValidator],
  (req, res, next) => {
    return createCategory(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

product.put(
  "/update-category",
  isAuthenticated([MODULE.PRODUCT]),
  [updateCategoryValidator],
  (req, res, next) => {
    return updateCategory(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

product.delete(
  "/delete-category/:id",
  isAuthenticated([MODULE.PRODUCT]),
  (req, res, next) => {
    return deleteCategory(req.params.id)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

product.get("/detail-category/:id", async (req, res, next) => {
  return getDetailCategory(req.params.id)
    .then((t) => successResponse(res, t))
    .catch(next);
});

product.put(
  "/change-status-product-category",
  [changeStatusProductValidator],
  (req, res, next) => {
    return changeStatusProductCategory(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

product.get("/get-list-category-code", (req, res, next) => {
  return getListCategoryCode(req.query)
    .then((t) => successResponse(res, t))
    .catch(next);
});

/**
 * ================ CATEGORY ====================
 */

product.get(
  "/get-list-category-type",
  [pagingParse({ column: "id", dir: "desc" })],
  (req, res, next) => {
    return getListCategoryType()
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

product.get(
  "/get-list-category-with-paging-type",
  [pagingParse({ column: "id", dir: "desc" })],
  (req, res, next) => {
    return getCategoryWithPagingType(req.query, req.paging)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

product.post(
  "/new-category-type",
  isAuthenticated([MODULE.PRODUCT]),
  [createCategoryTypeValidator],
  (req, res, next) => {
    return createCategoryType(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

product.put(
  "/update-category-type",
  isAuthenticated([MODULE.PRODUCT]),
  [updateCategoryTypeValidator],
  (req, res, next) => {
    return updateCategoryType(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

product.get("/detail-category-type/:id", async (req, res, next) => {
  return getDetailCategoryType(req.params.id)
    .then((t) => successResponse(res, t))
    .catch(next);
});

product.delete(
  "/delete-category-type/:id",
  isAuthenticated([MODULE.PRODUCT]),
  (req, res, next) => {
    return deleteCategoryType(req.params.id)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

/**
 * ================ PRODUCT SAVE ====================
 */
product.post(
  "/create-save-product",
  isAuthenticated([MODULE.PRODUCT]),
  [createSaveProductValidator],
  async (req, res, next) => {
    return createSaveProduct(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

product.get("/get-save-product", (req, res, next) => {
  return getListSaveProduct()
    .then((t) => successResponse(res, t))
    .catch(next);
});

product.get(
  "/get-save-product-type",
  [pagingParse({ column: "id", dir: "desc" })],
  (req, res, next) => {
    return getListSaveProductType(req.query, req.paging)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

product.get("/detail-save-product/:id", async (req, res, next) => {
  return getDetailSaveProduct(req.params.id)
    .then((t) => successResponse(res, t))
    .catch(next);
});

product.delete(
  "/delete-save-product/:id",
  isAuthenticated(),
  (req, res, next) => {
    return deleteSaveProduct(req.params.id)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

product.put(
  "/update-save-product",
  isAuthenticated([MODULE.PRODUCT]),
  [updateSaveProductValidator],
  (req, res, next) => {
    return updateSaveProduct(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

product.get(
  "/top-product-comment",
  [pagingParse({ column: "id", dir: "desc" })],
  (req, res, next) => {
    return getTopProductComment(req.query, req.paging)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

export function initWebProductController(app) {
  app.use("/api/product", product);
}
