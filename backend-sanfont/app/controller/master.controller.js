import express from 'express';
import { successResponse } from '../util/response.util';
import { createMasterValidator, deleteMasterValidator, updateMasterValidator } from '../validator/master.validator';
import { createMaster, deleteMaster, getListMaster, getMaster, updateMaster } from '../service/master.service';
import { pagingParse } from '../middleware/paging.middleware';

const master = express.Router();

master.post(
  '/new-master',
  [createMasterValidator],
  (req, res, next) => {
    return createMaster(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

master.get(
  '/',
  [pagingParse({ column: 'id', dir: 'desc' })],
  (req, res, next) => {
    return getListMaster(req.query, req.paging)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

master.get(
  "/get-master",
  (req, res, next) => {
    return getMaster(req.query)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

master.put(
  '/update-master',
  [updateMasterValidator],
  (req, res, next) => {
    return updateMaster(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

master.delete(
  '/delete-master',
  [deleteMasterValidator],
  (req, res, next) => {
    return deleteMaster(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

export function initWebMasterController(app) {
  app.use("/api/master", master);
}
