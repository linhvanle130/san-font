import express from 'express';
import { createModuleValidator, createRoleModuleValidator, deleteRoleModuleValidator, updateModuleValidator } from '../validator/acl.validator';
import { successResponse } from '../util/response.util';
import { createModule, createRoleModule, deleteModule, deleteRoleModule, getAclActionWithModuleId, getListAcl, getListGroup, getListModule, updateModule } from '../service/acl.service';
import { pagingParse } from '../middleware/paging.middleware';

const acl = express.Router();

acl.get("/", (req, res, next) => {
  return getListAcl(req.query)
    .then((t) => successResponse(res, t))
    .catch(next);
});

acl.get("/get-acl-with-module/:moduleId", (req, res, next) => {
  return getAclActionWithModuleId(req.params.moduleId)
    .then((t) => successResponse(res, t))
    .catch(next);
});

acl.post(
  "/create-role-module",
  [createRoleModuleValidator],
  async (req, res, next) => {
    return createRoleModule(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

acl.put(
  "/delete-role-module",
  [deleteRoleModuleValidator],
  async (req, res, next) => {
    return deleteRoleModule(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

/**
 * ================ ACL Module ====================
 */
acl.post(
  "/create-module",
  [createModuleValidator],
  async (req, res, next) => {
    return createModule(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

acl.put(
  '/update-module',
  [updateModuleValidator],
  (req, res, next) => {
    return updateModule(req.body)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

acl.delete(
  '/delete-module/:id',
  (req, res, next) => {
    return deleteModule(req.params.id)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

acl.get(
  "/get-list-module",
  [pagingParse({ column: 'id', dir: 'desc' })],
  (req, res, next) => {
    return getListModule(req.query, req.paging)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

acl.get(
  "/get-list-group",
  [pagingParse({ column: 'id', dir: 'desc' })],
  (req, res, next) => {
    return getListGroup(req.query, req.paging)
      .then((t) => successResponse(res, t))
      .catch(next);
  }
);

export function initAclController(app) {
  app.use('/api/acl', acl );
}
