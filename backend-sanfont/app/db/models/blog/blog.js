import {DataTypes, Model} from 'sequelize';
import { GLOBAL_STATUS } from '../../../constants/common.constant';

export default class Blog extends Model {
  static init(sequelize, opts) {
    return super.init(
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        title: {
          type: DataTypes.STRING
        },
        description: {
          type: DataTypes.STRING
        },
        thumbnail: {
          type: DataTypes.TEXT
        },
        status: {
          type: DataTypes.TINYINT(1),
          defaultValue: GLOBAL_STATUS.ACTIVE
        },
        createdById: {
          type: DataTypes.INTEGER
        },
        updatedById: {
          type: DataTypes.INTEGER
        },
        createdAt: {
          type: DataTypes.DATE
        },
        updatedAt: {
          type: DataTypes.DATE
        }
      },
      {
        tableName: "blog",
        modelName: "blog",
        timestamps: true,
        sequelize,
        ...opts
      }
    );
  }

  static associate(models) {
    this.hasMany(models.BlogCategory, { foreignKey: 'blogId', as: 'blogCategory' });
    this.belongsTo(models.User, { foreignKey: 'createdById', as: 'userCreate' });
    this.belongsTo(models.User, { foreignKey: 'updatedById', as: 'userUpdate' });
  }
}
