import { DataTypes, Model } from "sequelize";
import bcrypt from "bcrypt";

const saltRounds = 10;
export const USER_CODE = "SS";
export const USER_STATUS = Object.freeze({
  ACTIVE: 1,
  BLOCKED: 0
});

export const USER_ORDER_STATUS = Object.freeze({
  NORMAL: 1,
  VIP: 2
});

export default class User extends Model {
  static init(sequelize, opts) {
    return super.init(
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        userCode: {
          type: DataTypes.STRING(20),
          allowNull: false,
          unique: true
        },
        email: {
          type: DataTypes.STRING,
          allowNull: false
        },
        username: {
          type: DataTypes.STRING
        },
        password: {
          type: DataTypes.STRING,
          allowNull: false
        },
        phoneCode: {
          type: DataTypes.INTEGER(6).UNSIGNED
        },
        phoneNumber: {
          type: DataTypes.STRING
        },
        level: {
          type: DataTypes.TINYINT(1)
        },
        role: {
          type: DataTypes.INTEGER
        },
        orderStatus: {
          type: DataTypes.TINYINT(1),
          defaultValue: USER_ORDER_STATUS.NORMAL
        },
        status: {
          type: DataTypes.TINYINT(1),
          defaultValue: USER_STATUS.ACTIVE
        },
        createdAt: {
          type: DataTypes.DATE
        },
        updatedAt: {
          type: DataTypes.DATE
        }
      },
      {
        tableName: "user",
        modelName: "user",
        timestamps: true,
        sequelize,
        ...opts
      }
    );
  }

  static hashPassword(password) {
    const salt = bcrypt.genSaltSync(saltRounds);
    return bcrypt.hashSync(password, salt);
  }

  static comparePassword(password, hash) {
    return bcrypt.compareSync(password, hash);
  }

  static associate(models) {
    const newLocal = this;
    newLocal.hasOne(models.UserInformation, {
      foreignKey: "userId",
      as: "userInformation"
    });
    this.hasOne(models.UserAddress, {
      foreignKey: "userId",
      as: "userAddress"
    });
    this.hasOne(models.UserReferral, {
      foreignKey: "registerId",
      as: "userReferral"
    });
    this.hasMany(models.Product, {
      foreignKey: "userId",
      as: "product"
    });
    this.hasMany(models.UserReferral, {
      foreignKey: "referrerId",
      as: "userReferrer"
    });
    this.hasMany(models.ProductSave, {
      foreignKey: 'userId',
      as: 'productSave'
    });
    this.hasMany(models.Order, {
      foreignKey: 'userId',
      as: 'order'
    });
    this.hasMany(models.Comment, {
      foreignKey: 'userId',
      as: 'comment'
    });
    this.hasMany(models.DownloadFont, {
      foreignKey: 'userId',
      as: 'downloadFont'
    });
  }

  toJSON() {
    const values = Object.assign({}, this.get());
    delete values.password;
    return values;
  }
}
