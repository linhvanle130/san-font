import Sequelize from "sequelize";
import databaseConfig from "../../config/database";
import User from "./user/user";
import OTP from "./otp";
import UserInformation from "./user/user-information";
import UserAddress from "./user/user-address";
import ProductInventory from "./product/product-inventory";
import ProductImage from "./product/product-image";
import ProductCategory from "./product/product-category";
import Admin from "./admin/admin";
import UserReferral from "./user/user-referral";
import ProductDetail from "./product/product-detail";
import Product from "./product/product";
import Blog from "./blog/blog";
import BlogCategory from "./blog/blog-category";
import Category from "./product/category";
import ACLModule from "./acl/acl-module";
import ACLAction from "./acl/acl-action";
import ACLGroup from "./acl/acl-group";
import ACLGroupAction from "./acl/acl-group-action";
import Contract from "./contract";
import Order from "./order/order";
import OrderPackage from "./order/order-package";
import OrderPayment from "./order/orderPayment";
import ProductFont from './product/product-font';
import ProductSave from "./product/product-save";
import Comment from "./comment";
import MasterData from "./masterData.js/masterData";
import DownloadFont from "./downloadFont";
import OrderContent from "./order/order-content";

const env = process.env.NODE_ENV || "development";

const config = {
  ...databaseConfig[env]
};

const sequelize = new Sequelize(
  config.database,
  config.username,
  config.password,
  config
);
const models = {
  // Admin
  Admin: Admin.init(sequelize),

  // ACL
  ACLAction: ACLAction.init(sequelize),
  ACLGroup: ACLGroup.init(sequelize),
  ACLGroupAction: ACLGroupAction.init(sequelize),
  ACLModule: ACLModule.init(sequelize),

  // Master
  MasterData: MasterData.init(sequelize),

  // User
  User: User.init(sequelize),
  UserInformation: UserInformation.init(sequelize),
  UserAddress: UserAddress.init(sequelize),
  UserReferral: UserReferral.init(sequelize),
  OTP: OTP.init(sequelize),

  // Product
  Product: Product.init(sequelize),
  ProductDetail: ProductDetail.init(sequelize),
  ProductInventory: ProductInventory.init(sequelize),
  ProductImage: ProductImage.init(sequelize),
  ProductFont: ProductFont.init(sequelize),
  ProductSave: ProductSave.init(sequelize),
  ProductCategory: ProductCategory.init(sequelize),
  Category: Category.init(sequelize),

  // Order
  OrderPackage: OrderPackage.init(sequelize),
  Order: Order.init(sequelize),
  OrderPayment: OrderPayment.init(sequelize),
  OrderContent: OrderContent.init(sequelize),

  // Contract
  Contract: Contract.init(sequelize),

  // Blogs
  Blog: Blog.init(sequelize),
  BlogCategory: BlogCategory.init(sequelize),

  // Comment
  Comment: Comment.init(sequelize),

  DownloadFont: DownloadFont.init(sequelize)
};

Object.values(models)
  .filter((model) => typeof model.associate === "function")
  .forEach((model) => model.associate(models));

models.sequelize = sequelize;
models.Sequelize = Sequelize;

export default models;
