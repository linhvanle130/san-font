import { DataTypes, Model } from "sequelize";
import { GLOBAL_STATUS } from "../../../constants/common.constant";

export default class Category extends Model {
  static init(sequelize, opts) {
    return super.init(
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        name: {
          type: DataTypes.STRING
        },
        categorySlug: {
          type: DataTypes.STRING,
          unique: true
        },
        status: {
          type: DataTypes.TINYINT(1),
          defaultValue: GLOBAL_STATUS.ACTIVE
        },
        createdById: {
          type: DataTypes.INTEGER
        },
        updatedById: {
          type: DataTypes.INTEGER
        },
        createdAt: {
          type: DataTypes.DATE
        },
        updatedAt: {
          type: DataTypes.DATE
        }
      },
      {
        tableName: "category",
        modelName: "category",
        timestamps: true,
        sequelize,
        ...opts
      }
    );
  }

  static associate(models) {
    this.hasMany(models.ProductCategory, { foreignKey: 'categoryId', as: 'productCategory' });
    this.belongsTo(models.User, {
      foreignKey: "createdById",
      as: "userCreate"
    });
    this.belongsTo(models.User, {
      foreignKey: "updatedById",
      as: "userUpdate"
    });
  }
}
