import {DataTypes, Model} from 'sequelize';
import { GLOBAL_STATUS } from '../../../constants/common.constant';

export default class Product extends Model {
  static init(sequelize, opts) {
    return super.init({
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        categoryId: {
          type: DataTypes.INTEGER,
          allowNull: false
        },
        userId: {
          type: DataTypes.INTEGER
        },
        productSlug: {
          type: DataTypes.STRING,
          unique: true
        },
        name: {
          type: DataTypes.STRING
        },
        author: {
          type: DataTypes.STRING
        },
        poster: {
          type: DataTypes.STRING
        },
        subAuthor: {
          type: DataTypes.STRING
        },
        description: {
          type: DataTypes.TEXT
        },
        outstanding: {
          type: DataTypes.TINYINT(1)
        },
        status: {
          type: DataTypes.TINYINT(1),
          defaultValue: GLOBAL_STATUS.ACTIVE
        },
        createdById: {
          type: DataTypes.INTEGER
        },
        updatedById: {
          type: DataTypes.INTEGER
        },
        createdAt: {
          type: DataTypes.DATE
        },
        updatedAt: {
          type: DataTypes.DATE
        }
      },
      {
        tableName: 'product',
        modelName: 'product',
        timestamps: true,
        sequelize, ...opts
      }
    );
  }

  static associate(models) {
    this.belongsTo(models.ProductCategory, { foreignKey: 'categoryId', as: 'productCategory' });
    this.hasMany(models.ProductImage, { foreignKey: 'productId', as: 'productImage' });
    this.hasMany(models.ProductFont, { foreignKey: 'productId', as: 'productFont' });
    this.hasMany(models.ProductSave, { foreignKey: 'productId', as: 'productSave' });
    this.hasMany(models.DownloadFont, { foreignKey: 'productId', as: 'downloadFont' });
    this.hasMany(models.Comment, { foreignKey: 'productId', as: 'comment' });
    this.hasMany(models.ProductDetail, { foreignKey: 'productId', as: 'productDetail' });
    this.hasMany(models.ProductInventory, { foreignKey: 'productId', as: 'productInventory' });
    this.belongsTo(models.User, { foreignKey: 'userId', as: 'user' })
    this.belongsTo(models.User, { foreignKey: 'createdById', as: 'userCreate' });
    this.belongsTo(models.User, { foreignKey: 'updatedById', as: 'userUpdate' });
  }
}
