import { DataTypes, Model } from "sequelize";

export const ORDER_STATUS = Object.freeze({
  WAITTING_CONFIRM: 1,
  CONFIRMED: 2,
  SHIPPING: 3,
  DELIVERED: 4,
  REJECT: 5
});

export const ORDER_CODE = "SANFONT";

export default class Order extends Model {
  static init(sequelize, opts) {
    return super.init(
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        paymentId: {
          type: DataTypes.INTEGER
        },
        userId: {
          type: DataTypes.INTEGER
        },
        orderPackageId: {
          type: DataTypes.INTEGER,
          allowNull: false
        },
        fullName: {
          type: DataTypes.STRING
        },
        email: {
          type: DataTypes.STRING
        },
        note: {
          type: DataTypes.STRING
        },
        identification: {
          type: DataTypes.STRING
        },
        total: {
          type: DataTypes.DECIMAL
        },
        orderCode: {
          type: DataTypes.STRING
        },
        orderDate: {
          type: DataTypes.DATE
        },
        telephone: {
          type: DataTypes.STRING
        },
        address: {
          type: DataTypes.STRING
        },
        orderStatus: {
          type: DataTypes.TINYINT(1)
        },
        createdById: {
          type: DataTypes.INTEGER
        },
        updatedById: {
          type: DataTypes.INTEGER
        },
        createdAt: {
          type: DataTypes.DATE
        },
        updatedAt: {
          type: DataTypes.DATE
        }
      },
      {
        tableName: "order",
        modelName: "order",
        timestamps: true,
        sequelize,
        ...opts
      }
    );
  }

  static associate(models) {
    this.belongsTo(models.OrderPayment, { foreignKey: 'paymentId',  as: 'orderPayment' });
    this.belongsTo(models.OrderPackage, { foreignKey: 'orderPackageId', as: 'orderPackage' });
    this.belongsTo(models.User, { foreignKey: 'userId', as: 'user' });
    this.belongsTo(models.User, {
      foreignKey: "createdById",
      as: "userCreate"
    });
    this.belongsTo(models.User, {
      foreignKey: "updatedById",
      as: "userUpdate"
    });
  }
}
