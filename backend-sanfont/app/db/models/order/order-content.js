import {DataTypes, Model} from 'sequelize';

export default class OrderContent extends Model {
  static init(sequelize, opts) {
    return super.init(
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true
        },
        idContent: {
          type: DataTypes.INTEGER,
          allowNull: false
        },
        orderPackageId: {
          type: DataTypes.INTEGER,
          allowNull: false
        },
        content: {
          type: DataTypes.STRING,
          allowNull: false
        },
        createdAt: {
          type: DataTypes.DATE
        },
        updatedAt: {
          type: DataTypes.DATE
        }
      },
      {
        tableName: 'order_content',
        modelName: 'orderContent',
        timestamps: true,
        sequelize, ...opts
      }
    );
  }

  static associate(models) {
    this.belongsTo(models.OrderPackage, { foreignKey: 'orderPackageId', as: 'orderPackage' });
  }
}
