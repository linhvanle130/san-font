import {DataTypes, Model} from 'sequelize';
import { GLOBAL_STATUS } from '../../../constants/common.constant';

export default class OrderPackage extends Model {
  static init(sequelize, opts) {
    return super.init({
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        name: {
          type: DataTypes.STRING
        },
        pricePrevious : {
          type: DataTypes.DECIMAL
        },
        price: {
          type: DataTypes.DECIMAL
        },
        note: {
          type: DataTypes.TEXT
        },
        status: {
          type: DataTypes.TINYINT(1),
          defaultValue: GLOBAL_STATUS.ACTIVE
        },
        createdById: {
          type: DataTypes.INTEGER
        },
        updatedById: {
          type: DataTypes.INTEGER
        },
        createdAt: {
          type: DataTypes.DATE
        },
        updatedAt: {
          type: DataTypes.DATE
        }
      },
      {
        tableName: 'order_package',
        modelName: 'orderPackage',
        timestamps: true,
        sequelize, ...opts
      }
    );
  }

  static associate(models) {
    this.hasMany(models.OrderContent, {
      foreignKey: 'orderPackageId',
      as: 'orderContent'
    });
    this.belongsTo(models.User, { foreignKey: 'createdById', as: 'userCreate' });
    this.belongsTo(models.User, { foreignKey: 'updatedById', as: 'userUpdate' });
  }
}
