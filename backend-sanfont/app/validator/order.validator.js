import joi from "joi";
import { validator, validatorType } from "./index";

export const newOrderValidator = validator(
  joi.object().keys({
    userId: joi.number().allow("", null).required(),
    orderPackageId: joi.number().allow("", null).required(),
    email: joi.string().trim().max(256).required(),
    fullName: joi.string().trim().max(256).required(),
    note: joi.string().trim().max(256).allow("", null),
    telephone: joi
      .string()
      .trim()
      .allow("", null)
      .min(10)
      .max(10),
    address: joi.string().trim().max(256).allow("", null),
    paymentMethod: joi.string().max(1),
    orderStatus:  joi.number(),
    total: joi.number()
  }),
  validatorType.BODY
);

export const updateOrderValidator = validator(joi.object().keys({
  id: joi.number().required(),
  orderPackageId: joi.number().allow("", null),
  paymentMethod: joi.string().max(1),
  note: joi.string().trim().max(256).allow("", null),
  telephone: joi
    .string()
    .trim()
    .allow("", null)
    .min(10)
    .max(10),
  address: joi.string().trim().max(256).allow("", null),
  orderStatus: joi.number()
}), validatorType.BODY);

export const statusOrderValidator = validator(joi.object().keys({
  id: joi.number().required(),
  orderStatus: joi.number().required()
}), validatorType.BODY);

export const newOrderPackageValidator = validator(
  joi.object().keys({
    name: joi.string().trim().max(255).required(),
    pricePrevious: joi.number().required(),
    price: joi.number().required(),
    note: joi.string().allow(null, '')
  }),
  validatorType.BODY
);

export const updateOrderPackageValidator = validator(
  joi.object().keys({
    id: joi.number().required(),
    name: joi.string().trim().max(255),
    pricePrevious: joi.number(),
    price: joi.number(),
    note: joi.string().allow(null, '')
  }),
  validatorType.BODY
);

export const statusOrderPackageValidator = validator(joi.object().keys({
  id: joi.number().required(),
  status: joi.number().required()
}), validatorType.BODY);

export const createOrderContentValidator = validator(joi.object().keys({
  orderPackageId: joi.number().required(),
  content: joi.string().required().max(256)
}), validatorType.BODY);

export const updateOrderContentValidator = validator(joi.object().keys({
  id: joi.number().required(),
  idContent: joi.number().required(),
  orderPackageId: joi.number().required(),
  content: joi.string().max(256)
}), validatorType.BODY);

export const deleteOrderContentValidator = validator(joi.object().keys({
  id: joi.number().required(),
  idContent: joi.number().required()
}), validatorType.BODY);
