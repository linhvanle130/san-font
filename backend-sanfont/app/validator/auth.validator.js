import joi from 'joi';
import { REGEX } from '../util/regex.util';
import { validator, validatorType } from './index';

export const loginValidator = validator(joi.object().keys({
  email: joi.string().email().required(),
  password: joi.string().max(256).required()
}), validatorType.BODY);

export const registerUserValidator = validator(joi.object().keys({
  username: joi.string().trim().allow(null).allow(''),
  email: joi.string().trim().email().regex(REGEX.email, 'email').max(256).required(),
  password: joi.string().trim().max(256).required(),
  referralCode: joi.string().allow(null).allow('')
}), validatorType.BODY);

export const resetPasswordValidator = validator(joi.object().keys({
  email: joi.string().email().regex(REGEX.email, 'email').required(),
  otpCode: joi.string().trim().required(),
  password: joi.string().required(),
  rePassword: joi.string().required()
}), validatorType.BODY);

export const updateInfoValidator = validator(joi.object().keys({
  id: joi.number().required(),
  name: joi.string().trim().allow(null).allow(''),
  email: joi.string().trim().email().regex(REGEX.email, 'email').max(256),
  avatar: joi.string()
}), validatorType.BODY);

export const changePasswordValidator = validator(joi.object().keys({
  email: joi.string().email().regex(REGEX.email, 'email').required(),
  oldPassword: joi.string().required(),
  password: joi.string().required(),
  newPassword: joi.string().required()
}), validatorType.BODY);
