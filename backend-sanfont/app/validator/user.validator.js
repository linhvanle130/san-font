import joi from 'joi';
import { validator, validatorType } from './index';

export const updateTypeValidator = validator(joi.object().keys({
  type: joi.number()
}), validatorType.BODY);

export const updateUserProfileValidator = validator(joi.object().keys({
  phoneCode: joi.number().allow('', null),
  phoneNumber: joi.string().trim().allow('', null),
  address: joi.string().trim().max(255).allow('', null),
  cityCode: joi.number().allow('', null),
  districtCode: joi.number().allow('', null),
  wardCode: joi.number().allow('', null),
  avatar: joi.string().trim().allow('', null)
}), validatorType.BODY);

export const registUserAddressValidator = validator(joi.object().keys({
  address: joi.string().trim().max(255).required(),
  cityCode: joi.number().required(),
  districtCode: joi.number().required(),
  wardCode: joi.number().required(),
  telephone: joi.string().trim().allow('', null),
  orderId: joi.number()
}), validatorType.BODY);

export const setLevelUserValidator = validator(joi.object().keys({
  id: joi.number().required(),
  level: joi.number().allow('', null),
  role: joi.number().allow('', null),
  orderStatus: joi.number().allow('', null)
}), validatorType.BODY);
