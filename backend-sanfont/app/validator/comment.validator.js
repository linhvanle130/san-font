import joi from 'joi';
import { validator, validatorType } from './index';

export const createCommentValidator = validator(joi.object().keys({
  productId: joi.number().required(),
  userId: joi.number().required(),
  note: joi.string().trim().max(256).allow("", null).required()
}), validatorType.BODY);

export const updateCommentValidator = validator(joi.object().keys({
  id: joi.number().required(),
  productId: joi.number(),
  userId: joi.number(),
  note: joi.string().trim().max(256).allow("", null)
}), validatorType.BODY);
