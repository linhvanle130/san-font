import joi from 'joi';
import { validator, validatorType } from './index';

export const createBlogCategoryValidator = validator(joi.object().keys({
  blogId: joi.number().required().allow(null, ''),
  title: joi.string().required().allow(null, ''),
  thumbnail: joi.string().required().allow(null, '')
}), validatorType.BODY);

export const updateBlogCategoryValidator = validator(joi.object().keys({
  id: joi.number().required(),
  blogId: joi.number(),
  title: joi.string(),
  thumbnail: joi.string()
}), validatorType.BODY);

export const createBlogValidator = validator(joi.object().keys({
  title: joi.string().required(),
  description: joi.string().required(),
  thumbnail: joi.string().required()
}), validatorType.BODY);

export const updateBlogValidator = validator(joi.object().keys({
  id: joi.number().required(),
  title: joi.string(),
  description: joi.string(),
  thumbnail: joi.string(),
  status: joi.number()
}), validatorType.BODY);

export const changeStatusBlogValidator = validator(joi.object().keys({
  id: joi.number().required(),
  status: joi.number().required()
}), validatorType.BODY);
