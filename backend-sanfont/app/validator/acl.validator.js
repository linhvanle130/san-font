import joi from 'joi';
import { validator, validatorType } from './index';

export const deleteRoleModuleValidator = validator(
  joi.object().keys({
    actionsId: joi.array().required(),
    groupId: joi.number().required()
  }),
  validatorType.BODY
);

export const createRoleModuleValidator = validator(
  joi.object().keys({
    actionsId: joi.array().required(),
    groupId: joi.number().required()
  }),
  validatorType.BODY
);

export const createModuleValidator = validator(joi.object().keys({
  name: joi.string().required(),
  description: joi.string().required()
}), validatorType.BODY);

export const updateModuleValidator = validator(joi.object().keys({
  id: joi.number().required(),
  name: joi.string(),
  description: joi.string()
}), validatorType.BODY);


