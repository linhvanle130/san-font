import joi from 'joi';
import { validator, validatorType } from './index';

export const createDownloadFontValidator = validator(joi.object().keys({
  productId: joi.number().required(),
  userId: joi.number().required(),
  nameDownload: joi.string().required().max(256)
}), validatorType.BODY);

export const updateDownloadFontValidator = validator(joi.object().keys({
  id: joi.number().required(),
  idDownload: joi.number().required(),
  productId: joi.number(),
  userId: joi.number(),
  nameDownload: joi.string().max(256)
}), validatorType.BODY);

