import joi from 'joi';
import { validator, validatorType } from './index';

export const createCategoryTypeValidator = validator(joi.object().keys({
  name: joi.string().max(256).required(),
  categorySlug: joi.string().max(256).allow(null, '')
}), validatorType.BODY);

export const updateCategoryTypeValidator = validator(joi.object().keys({
  id: joi.number().required(),
  name: joi.string().max(256),
  categorySlug: joi.string().max(256).allow(null, ''),
  status: joi.number()
}), validatorType.BODY);

export const createCategoryValidator = validator(joi.object().keys({
  categoryId: joi.number().required(),
  name: joi.string().max(256).required(),
  categorySlug: joi.string().max(256).allow(null, '')
}), validatorType.BODY);

export const updateCategoryValidator = validator(joi.object().keys({
  id: joi.number().required(),
  name: joi.string().max(256),
  categoryId: joi.number(),
  categorySlug: joi.string().max(256).allow(null, ''),
  status: joi.number()
}), validatorType.BODY);

export const createProductValidator = validator(joi.object().keys({
  categoryId: joi.number().required(),
  userId: joi.number(),
  productSlug: joi.string().max(256).allow(null, ''),
  name: joi.string().max(256).required(),
  author: joi.string().max(256).allow(null, ''),
  poster: joi.string().max(256).allow(null, ''),
  subAuthor: joi.string().max(256).allow(null, ''),
  description: joi.string().allow(null, ''),
  outstanding: joi.number().required(),
  mainImage: joi.string().required(),
  mainFont: joi.string().required()
}), validatorType.BODY);

export const updateProductValidator = validator(joi.object().keys({
  id: joi.number().required(),
  categoryId: joi.number(),
  userId: joi.number(),
  productSlug: joi.string().max(256).allow(null, ''),
  name: joi.string().max(256).allow(null, ''),
  author: joi.string().max(256).allow(null, ''),
  poster: joi.string().max(256).allow(null, ''),
  subAuthor: joi.string().max(256).allow(null, ''),
  description: joi.string().allow(null, ''),
  outstanding: joi.number(),
  mainImage: joi.string().allow(null, ''),
  mainFont: joi.string().allow(null, '')
}), validatorType.BODY);

export const changeStatusProductValidator = validator(joi.object().keys({
  id: joi.number().required(),
  status: joi.number().required()
}), validatorType.BODY);

export const changeOutstandingProductValidator = validator(joi.object().keys({
  id: joi.number().required(),
  outstanding: joi.number().required()
}), validatorType.BODY);

export const createSaveProductValidator = validator(joi.object().keys({
  productId: joi.number().required(),
  userId: joi.number().required()
}), validatorType.BODY);

export const updateSaveProductValidator = validator(joi.object().keys({
  id: joi.number().required(),
  productId: joi.number().required(),
  userId: joi.number(),
  status: joi.number()
}), validatorType.BODY);
