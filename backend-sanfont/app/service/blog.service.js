import { FIELD_ERROR, badRequest } from '../config/error';
import { GLOBAL_STATUS } from '../constants/common.constant';
import db from '../db/models';

const { Op } = db.Sequelize;

/**
 * Get list Blog
 * @param {*} query
 * @param {*} paging
 */
export async function getListBlog(query, { offset, limit, order }) {
  const { status, search, isAdmin } = query;
  const conditions = {};

  status && (conditions.status = status);
  isAdmin && (conditions.status = GLOBAL_STATUS.ACTIVE);
  if (search) {
    conditions[Op.or] = [
      { title: { [Op.like]: `%${search}%` } }
    ];
  }

  return db.Blog.findAndCountAll({
    where: conditions,
    include: [
      {
        model: db.BlogCategory,
        as: 'blogCategory',
        separate: true
      }
    ],
    offset,
    limit,
    order
  });
}

export async function getDetailBlog(id) {
  return db.Blog.findOne({
    where: { id },
    include: [
      {
        model: db.BlogCategory,
        as: 'blogCategory',
        separate: true
      }
    ]
  });
}

/**
 * Create new Blog
 * @param {*} body
 */
 export async function createBlog(body) {

  try {
    await db.Blog.create(body);
    return true;
  } catch (e) {
    console.log("ERROR_CREATE_BLOG: ", e);
    throw e;
  }
}

/**
 * Update Blog
 * @param {*} body
 */
export async function updateBlog(body) {
  try {
    await db.Blog.update(body, {
      where: { id: body.id }
    });
    return true;
  } catch (e) {
    console.log("ERROR_UPDATE_NEWS: ", e);
    throw e;
  }
}

/**
 * Change status blog
 * @param {*} body form
 * @returns
 */
export async function changeStatusBlog(body) {
  try {
    await db.Blog.update(
      { status: body.status },
      { where: { id: body.id } }
    );
    return true;
  } catch (e) {
    console.log("ERROR_UPDATE_STATUS: ", e);
    throw e;
  }
}

/**
 * Delete Blog
 * @param {*} body
 */
export async function deleteBlog(id) {
  try {
    await db.Blog.destroy({ where: { id } });
    await db.BlogCategory.destroy({ where: { blogId: id } });
    return true;
  } catch (e) {
    console.log("ERROR_DELETE_PRODUCT: ", e);
    throw e;
  }
}


// ================ CONTORLER CATEGORY ====================
/**
 * Get list category for combobox
 */
export async function getListBlogCategory(body) {
  const listBlogCategory = await db.BlogCategory.findAll({
    where: body
  });

  return listBlogCategory;
}

/**
 * Get list category for paging
 * @param {*} query
 * @param {*} paging
 */
export async function getBlogCategoryWithPaging(query, { offset, limit, order }) {
  const { title, status } = query;
  const conditions = {};

  title && (conditions.title = { [Op.like]: `%${title.trim()}%` });
  status && (conditions.status = status);

  const listBlogCategory = await db.BlogCategory.findAndCountAll({
    where: conditions,
    include: {
      model: db.Blog,
      as: 'blog'
    },
    offset,
    limit,
    order
  });

  return listBlogCategory;
}

/**
 * Create new category
 * @param {*} body
 */
export async function createBlogCategory(body) {
  try {
     await db.BlogCategory.create(body);
     return true;
   } catch (e) {
     console.log("ERROR_CREATE_CATEGORY: ", e);
     throw e;
   }
}

/**
 * Update new category
 * @param {*} body
 */
export async function updateBlogCategory(body) {
  try {
    await db.BlogCategory.update(body, {
      where: { id: body.id }
    });
    return true;
  } catch (e) {
    console.log("ERROR_UPDATE_NEWS: ", e);
    throw e;
  }
}

/**
 * Get detail category
 * @param {*} id
 */
export async function getDetailBlogCategory(id) {
  const blogCategory = await db.BlogCategory.findOne({
    where: { id }
  });

  if (!blogCategory) {
    throw badRequest('get_product', FIELD_ERROR.PRODUCT_NOT_FOUND, 'Product not found');
  }

  return blogCategory;
}

/**
 * Delete category
 * @param {*} body
 */
export async function deleteBlogCategory(id) {
  try {
    await db.BlogCategory.destroy({ where: { id } });
    return true;
  } catch (e) {
    console.log("ERROR_DELETE_CATEGORY: ", e);
    throw e;
  }
}
