import { badRequest } from '../config/error';
import { GLOBAL_STATUS } from '../constants/common.constant';
import db from '../db/models';

const { Op } = db.Sequelize;

/**
 * Create new Master
 * @param {*} body
 */
export async function createDownloadFont(body) {
  try {
    let idDownload;
    let id;

    // Check exists master
    const existDownload = await db.DownloadFont.findOne({
      where: { nameDownload: body.nameDownload }
    });

    if (existDownload) {
      // Set master id equals with current master
      idDownload = existDownload.idDownload;

      // Get lastest id of master
      const lasterIdOfDownload = await db.DownloadFont.findOne({
        where: { idDownload : existDownload.idDownload },
        order: [['id', 'DESC']],
        limit: 1
      });

      // Increasement id of master
      id = lasterIdOfDownload.id + 1;
    } else {
      // Get lastest id master
      const lasterIdDownload = await db.DownloadFont.findOne({
        order: [['idDownload', 'DESC']],
        limit: 1
      });

      // Check exist id master
      if (lasterIdDownload) {
        idDownload = lasterIdDownload.idDownload + 1;
      } else {
        // Case insert first record master data
        idDownload = 1;
      }
      id = 1;
    }

    const downloadFont = {
      ...body,
      id,
      idDownload
    }

    await db.DownloadFont.create(downloadFont);
    return true;
   } catch (e) {
     console.log("ERROR_CREATE_DOWNLOAD: ", e);
     throw e;
   }
}

/**
 * Get list mastrer
 * @param {*} query
 * @param {*} paging
 */
export async function getListDownloadFont(query, { offset, limit, order }) {
  const { status, idMaster, nameMaster } = query;
  const conditions = {};

  status && ( conditions.status = status );
  idMaster && ( conditions.idMaster = idMaster );
  nameMaster && (conditions.nameMaster = { [Op.like]: `%${nameMaster.trim()}%` });

  const downloadFontList = await db.DownloadFont.findAndCountAll({
    where: conditions,
    offset,
    limit,
    order
  });

  return downloadFontList;
}


export async function getDownloadFont(query) {
  const { idDownload, nameDownload, id } = query;
  const conditions = {
    status: GLOBAL_STATUS.ACTIVE
  };

  idDownload && (conditions.idDownload = idDownload);
  nameDownload && (conditions.nameDownload = nameDownload);
  id && (conditions.id = id);

  const downloadFont = await db.DownloadFont.findAll({
    where: conditions
  });

  if (!downloadFont) {
    throw badRequest('Download font not found');
  }

  return downloadFont;
}


/**
 * Update master
 * @param {*} body
 */
export async function updateDownloadFont(body) {
  try {
    const download = await db.DownloadFont.findOne({
      where: { idDownload: body.idDownload, nameDownload: body.nameDownload }
    })

    await db.DownloadFont.update(body, { where: { id: body.id, idDownload: body.idDownload } });
    if (!download) {
      await db.DownloadFont.update(
        { nameDownload: body.nameDownload },
        { where: { idDownload: body.idDownload } }
      );
    }

    return true;
  } catch (e) {
    console.log("ERROR_UPDATE_DOWNLOAD_FONT: ", e);
    throw e;
  }
}



