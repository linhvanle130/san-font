import { FIELD_ERROR, badRequest } from '../../config/error';
import db from '../../db/models';
import { ORDER_CODE, ORDER_STATUS } from '../../db/models/order/order';
import { getNextOrderCode } from '../../util/helper.util';
import { mailAwsService } from '../common/ses.service';
import { PAYMENT_METHOD_MAP } from '../../constants/common.constant';

const { Op } = db.Sequelize;
const GUEST = 'GUEST'
const USER = 'USER'


export async function sendMailComplete(body) {
  try {
    const subject = body.subject;
    const toEmails = body.toEmails;
    const params = body;
    const templateName = "order-success";
    await mailAwsService.sendMail({ templateName, params, subject, toEmails });
    return true;
  } catch (e) {
    throw e;
  }
}


/**
 * Create new order
 * @param {*} order form
 * @returns
 */
export async function createOrder(createOrderForm) {

  try {
    let identification = GUEST;

     // Create order detail
    const orderCode = await getNextOrderCode(db.Order);

    // Create payment detail
    const payment = await db.OrderPayment.create(
      {
        paymentMethod: createOrderForm.paymentMethod
      },
    );

    const account = await db.User.findOne({
      where: {
        id: createOrderForm.userId
      },
      attributes: { exclude: ['password'] },
      include: [
        {
          model: db.UserInformation,
          as: 'userInformation'
        },
        {
          model: db.UserAddress,
          as: 'userAddress'
        }
      ]
    });

    if (account) {
      identification = USER
    }

    const order = await db.Order.create(
      {
        paymentId: payment.id,
        userId: createOrderForm.userId,
        orderPackageId: createOrderForm.orderPackageId,
        fullName: createOrderForm.fullName,
        email: createOrderForm.email,
        note: createOrderForm.note,
        telephone: createOrderForm.telephone,
        address: createOrderForm.address,
        total: createOrderForm.total,
        orderCode: `${ORDER_CODE}${orderCode}`,
        identification,
        orderDate: new Date(),
        orderStatus: ORDER_STATUS.WAITTING_CONFIRM
      }
    );

    // Check create order detail
    if (!order) {
      throw badRequest(
        'create_order',
        FIELD_ERROR.CREATE_ORDER_FAILED,
        'Order not success'
      );
    }

    // Commit transaction
    sendMailComplete({
      toEmails: createOrderForm.email,
      orderCode: `${ORDER_CODE}${orderCode}`,
      subject: `[SANFONT] Đơn hàng của bạn : ${ORDER_CODE}${orderCode}`,
      fullName: order.fullName,
      note: order.note,
      telephone: order.telephone,
      address: order.address,
      total: order.total,
      datetime: new Date().toLocaleString(),
      payment: PAYMENT_METHOD_MAP.find(
        (e) => e.value === +payment.paymentMethod
      ).label
    })

    return {
      order: order
    };
  } catch (e) {
    console.log("ERROR_CREATE_ORDER: ", e);
    throw e;
  }
}


/**
 * Get list order
 * @param {*} user
 * @param {*} query
 */
export async function getListOrder(query, { offset, order }) {
  const { fullName, orderStatus } = query;
  const conditions = {};

  fullName && (conditions.fullName = { [Op.like]: `%${fullName.trim()}%` });
  orderStatus && (conditions.orderStatus = orderStatus);

  return db.Order.findAndCountAll({
    where: conditions,
    include: [
      {
        model: db.OrderPackage,
        as: 'orderPackage'
      },
      {
        model: db.OrderPayment,
        as: 'orderPayment'
      }
    ],
    offset,
    order
  });
}

/**
 * Get list order
 * @param {*} user
 * @param {*} query
 */
export async function getListOrderWithCondition(query) {
  const { status, startDate, endDate } = query
  const conditions = {}

  status && (conditions.orderStatus = +status);
  startDate && endDate &&
    (conditions.orderDate = {
      [Op.between]: [startDate, endDate]
    });

  return db.Order.findAll({
    where: conditions
  });
}

/**
 * Get list order of user
 * @param {*} user form
 * @param {*} orderCode form
 * @returns
 */

export async function getOrderUser(params) {
  return db.Order.findOne({
    where: {
      orderCode: params.orderCode
    },
    include: [
      {
        model: db.OrderPackage,
        as: 'orderPackage'
      },
      {
        model: db.OrderPayment,
        as: 'orderPayment'
      }
    ]
  });
}


/**
 * Get order by mail and code
 * @param {*} params form
 * @returns
 */
export async function getOrderByMailAndCode(params) {
  const { email, orderCode } = params

  if ( !email && !orderCode ) {
    throw badRequest(
      'order_code_and_email_is_required',
      FIELD_ERROR.ORDER_CODE_AND_EMAIL_IS_REQUIRED,
      `Need input order code and email`
    )
  }

  const conditions = {};
  email && (conditions.email = email)
  orderCode && (conditions.orderCode = orderCode)
  try {
    const listOrder = await db.Order.findOne({
      where: conditions,
      include: [
        {
          model: db.OrderPackage,
          as: 'orderPackage'
        },
        {
          model: db.OrderPayment,
          as: 'orderPayment'
        }
      ]
    });

    return listOrder;
  } catch(e) {
    console.log('ERROR_GET_ORDER_INFO: ', e);
    throw e;
  }
}

export async function getDetailOrder(id) {
  const orders = await db.Order.findOne({
    where: { id },
    include: [
      {
        model: db.OrderPackage,
        as: 'orderPackage'
      },
      {
        model: db.OrderPayment,
        as: 'orderPayment'
      }
    ]
  });

  if (!orders) {
    throw badRequest('get_product', FIELD_ERROR.ORDER_NOT_FOUND, 'Order not found');
  }

  return orders;
}



/**
 * Update order
 * @param {*} user
 * @param {*} formUpdate
 * @returns
 */
export async function updateOrder(body) {
  try {
    await db.Order.update(body, {
      where: { id: body.id }
    });
    return true;
  } catch (e) {
    console.log("ERROR_UPDATE_ORDER: ", e);
    throw e;
  }
}

/**
 * Delete order
 * @param {*} body
 */
export async function deleteOrder(id) {
  try {
    await db.Order.destroy({ where: { id } });
    return true;
  } catch (e) {
    console.log("ERROR_DELETE_ORDER: ", e);
    throw e;
  }
}

export async function statusOrder(body) {
  try {
    await db.Order.update(
      { orderStatus: body.orderStatus },
      { where: { id: body.id } }
    );
    return true;
  } catch (e) {
    console.log("ERROR_UPDATE_STATUS: ", e);
    throw e;
  }
}

