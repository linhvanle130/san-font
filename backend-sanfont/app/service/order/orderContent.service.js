import { Op } from 'sequelize';
import db from '../../db/models';
import { badRequest } from '../../config/error';

export async function createOrderContent(body) {
  try {
    let idContent;
    let id;

    // Check exists master
    const existDownload = await db.OrderContent.findOne({
      where: { orderPackageId: body.orderPackageId }
    });

    if (existDownload) {
      // Set master id equals with current master
      idContent = existDownload.idContent;

      // Get lastest id of master
      const lasterIdOfDownload = await db.OrderContent.findOne({
        where: { idContent : existDownload.idContent },
        order: [['id', 'DESC']],
        limit: 1
      });

      // Increasement id of master
      id = lasterIdOfDownload.id + 1;
    } else {
      // Get lastest id master
      const lasterIdDownload = await db.OrderContent.findOne({
        order: [['idContent', 'DESC']],
        limit: 1
      });

      // Check exist id master
      if (lasterIdDownload) {
        idContent = lasterIdDownload.idContent + 1;
      } else {
        // Case insert first record master data
        idContent = 1;
      }
      id = 1;
    }

    const downloadFont = {
      ...body,
      id,
      idContent
    }

    await db.OrderContent.create(downloadFont);
    return true;
   } catch (e) {
     console.log("ERROR_CREATE_ORDER_CONTENT: ", e);
     throw e;
   }
}

export async function getListOrderContent(query) {
  const { orderPackageId, content } = query;
  const conditions = {};

  orderPackageId && ( conditions.orderPackageId = orderPackageId );
  content && (conditions.content = { [Op.like]: `%${content.trim()}%` });

  const downloadFontList = await db.OrderContent.findAndCountAll({
    where: conditions
  });

  return downloadFontList;
}

export async function getOrderContent(query) {
  const { idContent, orderPackageId, id } = query;
  const conditions = {};

  idContent && (conditions.idContent = idContent);
  orderPackageId && (conditions.orderPackageId = orderPackageId);
  id && (conditions.id = id);

  const content = await db.OrderContent.findAll({
    where: conditions
  });

  if (!content) {
    throw badRequest('get_order_content');
  }

  return content;
}

export async function updateOrderContent(body) {
  try {
    const content = await db.OrderContent.findOne({
      where: { idContent: body.idContent, orderPackageId: body.orderPackageId }
    })

    await db.OrderContent.update(body, { where: { id: body.id, idContent: body.idContent } });
    if (!content) {
      await db.OrderContent.update(
        { orderPackageId: body.orderPackageId },
        { where: { idContent: body.idContent } }
      );
    }

    return true;
  } catch (e) {
    console.log("ERROR_UPDATE_ORDER_CONTENT: ", e);
    throw e;
  }
}

export async function deleteOrderContent(body) {
  try {
    await db.OrderContent.destroy({
      where: { id: body.id, idContent: body.idContent }
    });
    return true;
  } catch (e) {
    console.log("ERROR_DELETE_ORDER_CONTENT: ", e);
    throw e;
  }
}
