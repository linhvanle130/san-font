import { GLOBAL_STATUS } from '../../constants/common.constant';
import db from '../../db/models';


const { Op } = db.Sequelize;

/**
 * Create new order package
 * @param {*} order package form
 * @returns
 */

export async function createOrderPackage(body) {
  try {
    await db.OrderPackage.create(body);
    return true;
  } catch (e) {
    console.log("ERROR_CREATE_ORDER: ", e);
    throw e;
  }
}

/**
 * Get list order package
 * @param {*} user
 * @param {*} query
 */
export async function getListOrderPackage(query, { offset, limit, order }) {
  const {name, note, isAdmin} = query;
  const conditions = {};

  isAdmin && (conditions.status = GLOBAL_STATUS.ACTIVE);
  name && (conditions.name = { [Op.like]: `%${name.trim()}%` });
  note && (conditions.note = { [Op.like]: `%${note.trim()}%` });

  return db.OrderPackage.findAndCountAll({
    where: conditions,
    include: [
      {
        model: db.OrderContent,
        as: 'orderContent'
      }
    ],
    offset,
    limit,
    order
  });
}

export async function getDetailOrderPackage(id) {
  return db.OrderPackage.findOne({
    where: { id },
    include: [
      {
        model: db.OrderContent,
        as: 'orderContent'
      }
    ]
  });
}

/**
 * Update order package
 * @param {*} body
 */
export async function updateOrderPackage(body) {
  try {
    await db.OrderPackage.update(body, {
      where: { id: body.id }
    });
    return true;
  } catch (e) {
    console.log("ERROR_UPDATE_ORDER: ", e);
    throw e;
  }
}

/**
 * Delete order package
 * @param {*} body
 */
export async function deleteOrderPackage(id) {
  try {
    await db.OrderPackage.destroy({ where: { id } });
    return true;
  } catch (e) {
    console.log("ERROR_DELETE_PRODUCT: ", e);
    throw e;
  }
}


/**
 * Change status order package
 * @param {*} body form
 * @returns
 */
export async function statusOrderPackage(body) {
  try {
    await db.OrderPackage.update(
      { status: body.status },
      { where: { id: body.id } }
    );
    return true;
  } catch (e) {
    console.log("ERROR_UPDATE_STATUS: ", e);
    throw e;
  }
}
