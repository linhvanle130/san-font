import db from '../db/models';

export async function createComment(body) {
  try {
    await db.Comment.create(body);
    return true;
  } catch (e) {
    console.log("ERROR_CREATE_COMMENT: ", e);
    throw e;
  }
}

export async function getListComment() {
  const conditions = {};
  const listComment = await db.Comment.findAndCountAll({
    where: conditions,
    include: [
      {
        model: db.User,
        as: 'user'
      },
      {
        model: db.Product,
        as: "product",
        include: [
          {
            model: db.ProductImage,
            as: 'productImage',
            separate: true
          },
          {
            model: db.ProductFont,
            as: 'productFont',
            separate: true
          },
          {
            model: db.ProductCategory,
            as: 'productCategory',
            include: {
              model: db.Category,
              as: 'category'
            }
          }
        ]
      }
    ]
  });

  return listComment;
}


export async function getDetailComment(id) {
  const detailComment = await db.Comment.findOne({
    where: { id },
    include: [
      {
        model: db.User,
        as: 'user'
      },
      {
        model: db.Product,
        as: "product",
        include: [
          {
            model: db.ProductImage,
            as: 'productImage',
            separate: true
          },
          {
            model: db.ProductFont,
            as: 'productFont',
            separate: true
          },
          {
            model: db.ProductCategory,
            as: 'productCategory',
            include: {
              model: db.Category,
              as: 'category'
            }
          }
        ]
      }
    ]
  });
  return detailComment;
}

export async function updateComment(body) {
  try {
    await db.Comment.update(body, {
      where: { id: body.id }
    });
    return true;
  } catch (e) {
    console.log("ERROR_UPDATE_COMMENT: ", e);
    throw e;
  }
}

export async function deleteComment(id) {
  try {
    await db.Comment.destroy({ where: { id } });
    return true;
  } catch (e) {
    console.log("ERROR_DELETE_COMMENT: ", e);
    throw e;
  }
}
