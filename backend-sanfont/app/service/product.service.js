import db from "../db/models";
import { badRequest, FIELD_ERROR } from "../config/error";
import { GLOBAL_STATUS, GLOBAL_SWITCH } from "../constants/common.constant";

const { Op } = db.Sequelize;

/**
 * Get list product
 * @param {*} query
 * @param {*} paging
 */
export async function getListProduct(query, { offset, order }) {
  const {
    name,
    author,
    poster,
    subAuthor,
    status,
    getMainImage,
    getMainFont,
    categoryId
  } = query;
  const conditions = {};

  status && (conditions.status = status);
  categoryId && (conditions.categoryId = categoryId);
  name && (conditions.name = { [Op.like]: `%${name.trim()}%` });
  author && (conditions.author = { [Op.like]: `%${author.trim()}%` });
  poster && (conditions.poster = { [Op.like]: `%${poster.trim()}%` });
  subAuthor && (conditions.subAuthor = { [Op.like]: `%${subAuthor.trim()}%` });

  const products = await db.Product.findAndCountAll({
    where: conditions,
    include: [
      {
        model: db.ProductCategory,
        as: "productCategory",
        where: { status: GLOBAL_STATUS.ACTIVE },
        include: {
          model: db.Category,
          as: "category"
        }
      },
      {
        model: db.ProductImage,
        as: "productImage",
        separate: true,
        where: {
          isMain: getMainImage
            ? GLOBAL_SWITCH.ON
            : [GLOBAL_SWITCH.ON, GLOBAL_SWITCH.OFF]
        }
      },
      {
        model: db.ProductFont,
        as: "productFont",
        separate: true,
        where: {
          isMain: getMainFont
            ? GLOBAL_SWITCH.ON
            : [GLOBAL_SWITCH.ON, GLOBAL_SWITCH.OFF]
        }
      },
      { model: db.User, as: "user" },
      {
        model: db.ProductInventory,
        as: "productInventory",
        separate: true
      },
      {
        model: db.ProductSave,
        as: "productSave"
      },
      {
        model: db.Comment,
        as: "comment"
      },
      {
        model: db.DownloadFont,
        as: "downloadFont"
      }
    ],
    offset,
    order
  });

  return products;
}

/**
 * Get detail product
 * @param {*} id
 */
export async function getDetailProduct(id) {
  const products = await db.Product.findOne({
    where: { id },
    include: [
      {
        model: db.ProductCategory,
        as: "productCategory",
        where: { status: GLOBAL_STATUS.ACTIVE }
      },
      {
        model: db.ProductImage,
        as: "productImage"
      },
      { model: db.User, as: "user" },
      {
        model: db.ProductFont,
        as: "productFont"
      },
      {
        model: db.ProductInventory,
        as: "productInventory"
      },
      {
        model: db.ProductSave,
        as: "productSave"
      },
      {
        model: db.Comment,
        as: "comment"
      },
      {
        model: db.DownloadFont,
        as: "downloadFont"
      }
    ]
  });

  if (!products) {
    throw badRequest(
      "get_product",
      FIELD_ERROR.PRODUCT_NOT_FOUND,
      "Product not found"
    );
  }

  return products;
}

/**
 * Get product detail
 * @param {*} id
 */
export async function getCapacityProduct(body) {
  const products = await db.ProductDetail.findOne({
    where: body
  });

  return products;
}

/**
 * Get product by slug
 * @param {*} query
 */
export async function getProductCategoryBySlug(query) {
  const { categorySlug, getMainImage, getMainFont } = query;

  const category = await db.ProductCategory.findOne({
    where: { categorySlug },
    attributes: ["id"]
  });

  const products = await db.Product.findAndCountAll({
    where: {
      categoryId: category.id
    },
    include: [
      {
        model: db.ProductCategory,
        as: "productCategory",
        where: { status: GLOBAL_STATUS.ACTIVE }
      },
      {
        model: db.ProductInventory,
        as: "productInventory"
      },
      {
        model: db.ProductImage,
        as: "productImage",
        separate: true,
        where: {
          isMain: getMainImage
            ? GLOBAL_SWITCH.ON
            : [GLOBAL_SWITCH.ON, GLOBAL_SWITCH.OFF]
        }
      },
      {
        model: db.ProductFont,
        as: "productFont",
        separate: true,
        where: {
          isMain: getMainFont
            ? GLOBAL_SWITCH.ON
            : [GLOBAL_SWITCH.ON, GLOBAL_SWITCH.OFF]
        }
      },
      { model: db.User, as: 'user' },
      {
        model: db.ProductSave,
        as: "productSave"
      },
      {
        model: db.Comment,
        as: "comment"
      },
      {
        model: db.DownloadFont,
        as: "downloadFont"
      }
    ]
  });

  if (!products) {
    throw badRequest(
      "get_product",
      FIELD_ERROR.PRODUCT_NOT_FOUND,
      "Product not found"
    );
  }

  return products;
}


/**
 * Get product by slug
 * @param {*} query
 */
export async function getProductBySlug(query) {
  const { productSlug, getMainImage, getMainFont } = query;
  const conditions = {
    status: GLOBAL_STATUS.ACTIVE
  };

  productSlug && (conditions.productSlug = productSlug);

  const products = await db.Product.findOne({
    where: conditions,
    include: [
      {
        model: db.ProductCategory,
        as: "productCategory",
        where: { status: GLOBAL_STATUS.ACTIVE }
      },
      {
        model: db.ProductInventory,
        as: "productInventory"
      },
      {
        model: db.ProductImage,
        as: "productImage",
        separate: true,
        where: {
          isMain: getMainImage
            ? GLOBAL_SWITCH.ON
            : [GLOBAL_SWITCH.ON, GLOBAL_SWITCH.OFF]
        }
      },
      {
        model: db.ProductFont,
        as: "productFont",
        separate: true,
        where: {
          isMain: getMainFont
            ? GLOBAL_SWITCH.ON
            : [GLOBAL_SWITCH.ON, GLOBAL_SWITCH.OFF]
        }
      },
      { model: db.User, as: 'user' },
      {
        model: db.ProductSave,
        as: "productSave"
      },
      {
        model: db.Comment,
        as: "comment"
      },
      {
        model: db.DownloadFont,
        as: "downloadFont"
      }
    ]
  });

  if (!products) {
    throw badRequest('get_product', FIELD_ERROR.PRODUCT_NOT_FOUND, 'Product not found');
  }

  return products;
}


/**
 * Register product
 * @param {*} product form
 * @returns
 */
export async function createProduct(createProductForm) {
  const t = await db.sequelize.transaction();
  // let subProductId = 0;

  try {
    if (createProductForm.productSlug) {
      const slugExist = await db.Product.findOne({
        where: {
          productSlug: createProductForm.productSlug
        }
      });

      // Check create order detail
      if (slugExist) {
        throw badRequest(
          "check_slug",
          FIELD_ERROR.SLUG_IS_EXISTS,
          "Slug is exists"
        );
      }
    }

    // Create order detail
    const product = await db.Product.create(createProductForm, {
      transaction: t
    });

    // Check create order detail
    if (!product) {
      throw badRequest(
        "create_product",
        FIELD_ERROR.CREATE_PRODUCT_FAILED,
        "Create product not success"
      );
    }

    // Create main image
    await db.ProductImage.create(
      {
        productId: product.id,
        image: createProductForm.mainImage,
        isMain: GLOBAL_SWITCH.ON,
        status: GLOBAL_STATUS.ACTIVE
      },
      {
        transaction: t
      }
    );

    // Create main image
    await db.ProductFont.create(
      {
        productId: product.id,
        font: createProductForm.mainFont,
        isMain: GLOBAL_SWITCH.ON,
        status: GLOBAL_STATUS.ACTIVE
      },
      {
        transaction: t
      }
    );

    // Commit transaction
    await t.commit();

    return true;
  } catch (e) {
    console.log("ERROR_CREATE_PRODUCT: ", e);
    if (t) await t.rollback();
    throw e;
  }
}

/**
 * Change status product
 * @param {*} body form
 * @returns
 */
export async function changeStatusProduct(body) {
  try {
    await db.Product.update(
      { status: body.status },
      { where: { id: body.id } }
    );
    return true;
  } catch (e) {
    console.log("ERROR_UPDATE_STATUS: ", e);
    throw e;
  }
}

/**
 * Change status product
 * @param {*} body form
 * @returns
 */
export async function changeOutstandingProduct(body) {
  try {
    await db.Product.update(
      { outstanding: body.outstanding },
      { where: { id: body.id } }
    );
    return true;
  } catch (e) {
    console.log("ERROR_UPDATE_STATUS: ", e);
    throw e;
  }
}

/**
 * Change status product
 * @param {*} body form
 * @returns
 */
export async function changeStatusProductCategory(body) {
  try {
    await db.ProductCategory.update(
      { status: body.status },
      { where: { id: body.id } }
    );
    return true;
  } catch (e) {
    console.log("ERROR_UPDATE_STATUS: ", e);
    throw e;
  }
}

/**
 * Update product
 * @param {*} updateProductForm form
 * @returns
 */
export async function updateProduct(updateProductForm) {
  const t = await db.sequelize.transaction();
  // let subProductId = 0;

  try {
    if (updateProductForm.productSlug) {
      const slugExist = await db.Product.findOne({
        where: {
          productSlug: updateProductForm.productSlug,
          id: { [Op.not]: updateProductForm.id }
        }
      });
      // Check update order detail
      if (slugExist) {
        throw badRequest(
          "check_slug",
          FIELD_ERROR.SLUG_IS_EXISTS,
          "Slug is exists"
        );
      }
    }

    // Update order detail
    await db.Product.update(updateProductForm, {
      where: { id: updateProductForm.id },
      transaction: t
    });

    // Delete old product inventory & detail
    await db.ProductInventory.destroy({
      where: {
        productId: updateProductForm.id
      },
      transaction: t
    });

    await db.ProductDetail.destroy({
      where: {
        productId: updateProductForm.id
      },
      transaction: t
    });

    // Delete old main image
    await db.ProductImage.destroy({
      where: {
        productId: updateProductForm.id
      }
    });
    // Create main image
    await db.ProductImage.create(
      {
        productId: updateProductForm.id,
        image: updateProductForm.mainImage,
        isMain: GLOBAL_SWITCH.ON,
        status: GLOBAL_STATUS.ACTIVE
      },
      {
        transaction: t
      }
    );

    // Delete old main font
    await db.ProductFont.destroy({
      where: {
        productId: updateProductForm.id
      }
    });
    // Create main font
    await db.ProductFont.create(
      {
        productId: updateProductForm.id,
        font: updateProductForm.mainFont,
        isMain: GLOBAL_SWITCH.ON,
        status: GLOBAL_STATUS.ACTIVE
      },
      {
        transaction: t
      }
    );


    // Commit transaction
    await t.commit();

    return true;
  } catch (e) {
    console.log("ERROR_UPDATE_PRODUCT: ", e);
    if (t) await t.rollback();
    throw e;
  }
}

/**
 * Delete product
 * @param {*} body
 */
export async function deleteProduct(id) {
  try {
    await db.Product.destroy({ where: { id } });
    await db.ProductImage.destroy({ where: { productId: id } });
    await db.ProductInventory.destroy({ where: { productId: id } });
    await db.ProductDetail.destroy({ where: { productId: id } });
    return true;
  } catch (e) {
    console.log("ERROR_DELETE_PRODUCT: ", e);
    throw e;
  }
}

// ================ CONTORLER CATEGORY ====================
/**
 * Get list category for combobox
 */
export async function getListCategory() {
  const listProductCategory = await db.ProductCategory.findAll({
    where: { status: GLOBAL_STATUS.ACTIVE }
  });

  return listProductCategory;
}

/**
 * Get list category for paging
 * @param {*} query
 * @param {*} paging
 */
export async function getCategoryWithPaging(query, { offset, limit, order }) {
  const { name, status } = query;
  const conditions = {};

  name && (conditions.name = { [Op.like]: `%${name.trim()}%` });
  status && (conditions.status = status);

  const listProductCategory = await db.ProductCategory.findAndCountAll({
    where: conditions,
    offset,
    limit,
    order
  });

  return listProductCategory;
}

/**
 * Get detail category
 * @param {*} id
 */
export async function getDetailCategory(id) {
  const productCategory = await db.ProductCategory.findOne({
    where: { id }
  });

  if (!productCategory) {
    throw badRequest(
      "get_product",
      FIELD_ERROR.PRODUCT_NOT_FOUND,
      "Product not found"
    );
  }

  return productCategory;
}

/**
 * Create new category
 * @param {*} body
 */
export async function createCategory(body) {
  try {
    const category = {
      ...body,
      status: GLOBAL_STATUS.ACTIVE
    };

    if (category.categorySlug) {
      const slugExist = await db.ProductCategory.findOne({
        where: {
          categorySlug: body.categorySlug
        }
      });

      // Check create order detail
      if (slugExist) {
        throw badRequest(
          "check_slug",
          FIELD_ERROR.SLUG_IS_EXISTS,
          "Slug is exists"
        );
      }
    }

    await db.ProductCategory.create(category);
    return true;
  } catch (e) {
    console.log("ERROR_CREATE_CATEGORY: ", e);
    throw e;
  }
}

/**
 * Update category
 * @param {*} body
 */
export async function updateCategory(body) {
  try {
    const category = {
      ...body
    };

    if (category.categorySlug) {
      const slugExist = await db.ProductCategory.findOne({
        where: {
          categorySlug: category.categorySlug,
          id: { [Op.not]: category.id }
        }
      });

      // Check create order detail
      if (slugExist) {
        throw badRequest(
          "check_slug",
          FIELD_ERROR.SLUG_IS_EXISTS,
          "Slug is exists"
        );
      }
    }

    await db.ProductCategory.update(category, { where: { id: body.id } });
    return true;
  } catch (e) {
    console.log("ERROR_UPDATE_CATEGORY: ", e);
    throw e;
  }
}

/**
 * Update category
 * @param {*} body
 */
export async function deleteCategory(id) {
  try {
    await db.ProductCategory.destroy({ where: { id } });
    return true;
  } catch (e) {
    console.log("ERROR_DELETE_CATEGORY: ", e);
    throw e;
  }
}

export async function getListCategoryCode(params) {
  return db.ProductCategory.findAndCountAll({
    where: {
      categoryId: params.categoryId
    }
  });
}

// ================ CONTORLER CATEGORY ====================

export async function getListCategoryType() {
  const listProductCategoryType = await db.Category.findAll({
    where: { status: GLOBAL_STATUS.ACTIVE }
  });

  return listProductCategoryType;
}

export async function getCategoryWithPagingType(
  query,
  { offset, limit, order }
) {
  const { name, status, isAdmin } = query;
  const conditions = {};

  name && (conditions.name = { [Op.like]: `%${name.trim()}%` });
  status && (conditions.status = status);
  isAdmin && (conditions.status = GLOBAL_STATUS.ACTIVE);

  const listProductCategory = await db.Category.findAndCountAll({
    where: conditions,
    include: [
      {
        model: db.ProductCategory,
        as: "productCategory",
        where: { status: GLOBAL_STATUS.ACTIVE }
      }
    ],
    offset,
    limit,
    order
  });

  return listProductCategory;
}

export async function createCategoryType(body) {
  try {
    await db.Category.create(body);
    return true;
  } catch (e) {
    console.log("ERROR_CREATE_NEWS: ", e);
    throw e;
  }
}

export async function updateCategoryType(body) {
  try {
    await db.Category.update(body, {
      where: { id: body.id }
    });
    return true;
  } catch (e) {
    console.log("ERROR_UPDATE_NEWS: ", e);
    throw e;
  }
}

export async function getDetailCategoryType(id) {
  return db.Category.findOne({
    where: { id },
    include: [
      {
        model: db.ProductCategory,
        as: "productCategory",
        separate: true
      }
    ]
  });
}

export async function deleteCategoryType(id) {
  try {
    await db.Category.destroy({ where: { id } });
    await db.ProductCategory.destroy({ where: { categoryId: id } });
    return true;
  } catch (e) {
    console.log("ERROR_DELETE_PRODUCT: ", e);
    throw e;
  }
}

// ================ SAVE PRODUCT ====================
export async function createSaveProduct(body) {
  try {
    await db.ProductSave.create(body);
    return true;
  } catch (e) {
    console.log("ERROR_CREATE_SAVE_PRODUCT: ", e);
    throw e;
  }
}

/**
 * Get list category for combobox
 */
export async function getListSaveProduct(body) {
  const listSaveProduct = await db.ProductSave.findAll({
    where: body,
    include: [
      {
        model: db.User,
        as: "user"
      },
      {
        model: db.Product,
        as: "product",
        include: [
          {
            model: db.ProductImage,
            as: "productImage",
            separate: true
          },
          {
            model: db.ProductFont,
            as: "productFont",
            separate: true
          },
          {
            model: db.ProductCategory,
            as: "productCategory",
            include: {
              model: db.Category,
              as: "category"
            }
          },
          {
            model: db.DownloadFont,
            as: "downloadFont"
          }
        ]
      }
    ]
  });

  return listSaveProduct;
}

export async function getListSaveProductType() {
  const conditions = {};
  const listProductCategory = await db.ProductSave.findAndCountAll({
    where: conditions,
    include: [
      {
        model: db.User,
        as: "user"
      },
      {
        model: db.Product,
        as: "product",
        include: [
          {
            model: db.ProductImage,
            as: "productImage",
            separate: true
          },
          {
            model: db.ProductFont,
            as: "productFont",
            separate: true
          },
          {
            model: db.ProductCategory,
            as: "productCategory",
            include: {
              model: db.Category,
              as: "category"
            }
          },
          {
            model: db.DownloadFont,
            as: "downloadFont"
          }
        ]
      }
    ]
  });

  return listProductCategory;
}

export async function getDetailSaveProduct(id) {
  const productSave = await db.ProductSave.findOne({
    where: { id }
  });

  if (!productSave) {
    throw badRequest(
      "get_product",
      FIELD_ERROR.PRODUCT_NOT_FOUND,
      "Product not found"
    );
  }

  return productSave;
}

export async function deleteSaveProduct(id) {
  try {
    await db.ProductSave.destroy({ where: { id } });
    return true;
  } catch (e) {
    console.log("ERROR_DELETE_PRODUCT: ", e);
    throw e;
  }
}

export async function updateSaveProduct(body) {
  try {
    await db.ProductSave.update(body, {
      where: { id: body.id }
    });
    return true;
  } catch (e) {
    console.log("ERROR_UPDATE_PRODUCT: ", e);
    throw e;
  }
}


export async function getTopProductComment(
  query,
  { offset, limit, order }
) {
  const { status, isAdmin } = query;
  const conditions = {};

  status && (conditions.status = status);
  isAdmin && (conditions.status = GLOBAL_STATUS.ACTIVE);

  const listUserProductComment = await db.Product.findAndCountAll({
    where: conditions,
    include: [
      {
        model: db.ProductImage,
        as: "productImage",
        separate: true
      },
      {
        model: db.ProductCategory,
        as: "productCategory",
        include: {
          model: db.Category,
          as: "category"
        }
      },
      {
        model: db.Comment,
        as: "comment",
        where: { status: GLOBAL_STATUS.ACTIVE },
        include: [
          {
            model: db.User,
            as: "user"
          }
        ]
      }
    ],
    offset,
    limit,
    order
  });

  return listUserProductComment;
}

