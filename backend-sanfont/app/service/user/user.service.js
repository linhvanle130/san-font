import db from "../../db/models";
import { getAccountInfo } from "./auth.service";
import { checkUserExisted } from "./otp.service";
import { ADDRESS_DEFAULT } from "../../db/models/user/user-address";
import { badRequest, FIELD_ERROR } from "../../config/error";
import { USER_STATUS } from "../../db/models/user/user";
import { GLOBAL_STATUS } from "../../constants/common.constant";

const { Op } = db.Sequelize;

/**
 * Update user profile
 * @param {*} user
 * @param {*} formUpdate
 * @returns
 */
export async function updateUserProfile(user, formUpdate) {
  const profile = await getAccountInfo(user, false);
  const transaction = await db.sequelize.transaction();
  try {
    if (!formUpdate.avatar) {
      await checkUserExisted(
        formUpdate.email,
        formUpdate.phoneCode,
        formUpdate.phoneNumber,
        formUpdate.username,
        { id: { [Op.not]: user.id } }
      );
    }

    const dataUserUpdate = {
      phoneCode: formUpdate.phoneCode,
      phoneNumber: formUpdate.phoneNumber,
      username: formUpdate.username,
      address: formUpdate.address
    };

    const dataUserInfoUpdate = {
      address: formUpdate.address,
      cityCode: formUpdate.cityCode,
      districtCode: formUpdate.districtCode,
      wardCode: formUpdate.wardCode,
      avatar: formUpdate.avatar
    };

    await Promise.all([
      db.User.update(dataUserUpdate, {
        where: { id: profile.id },
        transaction
      }),
      db.UserInformation.update(dataUserInfoUpdate, {
        where: { userId: profile.id },
        transaction
      })
    ]);

    await transaction.commit();
    return true;
  } catch (e) {
    console.log("ERROR_UPDATE_USER_PROFILE: ", e);
    if (transaction) await transaction.rollback();
    throw e;
  }
}

/**
 * Update user profile
 * @param {*} user
 * @param {*} formUpdate
 * @returns
 */
export async function updateLevelUser(formUpdate) {
  try {
    await db.User.update(formUpdate, {
      where: { id: formUpdate.id }
    });
    return true;
  } catch (e) {
    console.log("ERROR_UPDATE_USER_PROFILE: ", e);
    throw e;
  }
}

export async function registerUserAddress(user, registerForm) {
  let isDefault = ADDRESS_DEFAULT.NOT_DEFAULT;

  const addressExisted = await db.UserAddress.findOne({
    where: { userId: user.id }
  });

  if (!addressExisted) {
    isDefault = ADDRESS_DEFAULT.DEFAULT;
  }
  console.log(addressExisted);

  try {
    await db.UserAddress.create({
      userId: user.id,
      address: registerForm.address,
      cityCode: registerForm.cityCode,
      districtCode: registerForm.districtCode,
      wardCode: registerForm.wardCode,
      telephone: registerForm.telephone,
      default: isDefault
    });
    return true;
  } catch (e) {
    console.log("ERROR_UPDATE_USER_PROFILE: ", e);
    throw e;
  }
}

export async function updateUserAddress(id, user, updateForm) {
  const userAddress = await db.UserAddress.findOne({
    where: { id: user.id }
  });
  if (!userAddress) {
    // throw badRequest('get_address_info', FIELD_ERROR.ADDRESS_NOT_FOUND, 'Address not found');
  }

  try {
    await db.UserAddress.update(updateForm, { where: { id } });
    return true;
  } catch (e) {
    console.log("ERROR_UPDATE_USER_PROFILE: ", e);
    throw e;
  }
}

export async function setDefaultAddress(id, user) {
  const transaction = await db.sequelize.transaction();
  const userAddress = db.UserAddress.findOne({
    where: { id }
  });
  if (!userAddress) {
    throw badRequest(
      "get_address_info",
      FIELD_ERROR.ADDRESS_NOT_FOUND,
      "Address not found"
    );
  }
  try {
    await Promise.all([
      db.UserAddress.update(
        { default: ADDRESS_DEFAULT.NOT_DEFAULT },
        {
          where: { userId: user.id },
          transaction
        }
      ),
      db.UserAddress.update(
        { default: ADDRESS_DEFAULT.DEFAULT },
        {
          where: { id },
          transaction
        }
      )
    ]);

    await transaction.commit();
    return true;
  } catch (e) {
    console.log("ERROR_UPDATE_USER_PROFILE: ", e);
    if (transaction) await transaction.rollback();
    throw e;
  }
}

export async function deleteUserAddress(id) {
  try {
    const userAddress = await db.UserAddress.findByPk(id);
    if (!userAddress) {
      throw badRequest(
        "bonusWallet",
        FIELD_ERROR.BONUS_WALLET_NOT_FOUND,
        "Bonus wallet not found"
      );
    }

    await userAddress.destroy();

    return true;
  } catch (e) {
    console.log("ERROR_UPDATE_USER_PROFILE: ", e);
    throw e;
  }
}

export async function getListUser(query, { offset }) {
  const { search, level, role } = query;

  const conditions = {
    status: USER_STATUS.ACTIVE
  };

  if (search) {
    conditions[Op.or] = [
      { email: { [Op.like]: `%${search}%` } },
      { username: { [Op.like]: `%${search}%` } },
      { userCode: { [Op.like]: `%${search}%` } },
      { phoneNumber: { [Op.like]: `%${search}%` } }
    ];
  }

  level && (conditions.level = level);
  role && (conditions.role = role);

  const result = await db.User.findAndCountAll({
    where: conditions,
    include: [
      {
        model: db.Product,
        as: "product",
        include: [
          {
            model: db.ProductImage,
            as: 'productImage',
            separate: true
          },
          {
            model: db.ProductFont,
            as: 'productFont',
            separate: true
          },
          {
            model: db.ProductCategory,
            as: 'productCategory',
            include: {
              model: db.Category,
              as: 'category'
            }
          }
        ]
      },
      {
        model: db.ProductSave,
        as: "productSave",
        include: [
          {
            model: db.Product,
            as: "product",
            include: [
              {
                model: db.ProductImage,
                as: 'productImage',
                separate: true
              },
              {
                model: db.ProductFont,
                as: 'productFont',
                separate: true
              },
              {
                model: db.ProductCategory,
                as: 'productCategory',
                include: {
                  model: db.Category,
                  as: 'category'
                }
              },
              {
                model: db.DownloadFont,
                as: "downloadFont"
              }
            ]
          }
        ]
      },
      {
        model: db.Order,
        as: "order",
        include: [
          {
            model: db.OrderPackage,
            as: 'orderPackage'
          },
          {
            model: db.OrderPayment,
            as: 'orderPayment'
          }
        ]
      },
      {
        model: db.UserInformation,
        as: "userInformation"
      },
      {
        model: db.UserAddress,
        as: 'userAddress'
      },
      {
        model: db.Comment,
        as: "comment",
        include: [
          {
            model: db.Product,
            as: "product",
            include: [
              {
                model: db.ProductImage,
                as: 'productImage',
                separate: true
              },
              {
                model: db.ProductFont,
                as: 'productFont',
                separate: true
              },
              {
                model: db.ProductCategory,
                as: 'productCategory',
                include: {
                  model: db.Category,
                  as: 'category'
                }
              }
            ]
          }
        ]
      },
      {
        model: db.DownloadFont,
        as: "downloadFont"
      }
    ],
    offset
  });

  return result;
}

export async function getTopUserReferrer() {
  const result = await db.User.findAll({
    include: [
      {
        model: db.UserInformation,
        as: "userInformation"
      },
      {
        model: db.UserReferral,
        as: "userReferrer",
        required: true
      }
    ]
  });
  const sorted_userReferrer = result.sort((one, other) => {
    return other.userReferrer.length - one.userReferrer.length;
  });
  return sorted_userReferrer;
}

/**
 * Get user information
 * @param {*} userId
 */
export async function getUserDetail(userId) {
  const user = await db.User.findByPk(userId, {
    include: [
      {
        model: db.Product,
        as: "product",
        include: [
          {
            model: db.ProductImage,
            as: 'productImage',
            separate: true
          },
          {
            model: db.ProductFont,
            as: 'productFont',
            separate: true
          },
          {
            model: db.ProductCategory,
            as: 'productCategory',
            include: {
              model: db.Category,
              as: 'category'
            }
          }
        ]
      },
      {
        model: db.ProductSave,
        as: "productSave",
        include: [
          {
            model: db.Product,
            as: "product",
            include: [
              {
                model: db.ProductImage,
                as: 'productImage',
                separate: true
              },
              {
                model: db.ProductFont,
                as: 'productFont',
                separate: true
              },
              {
                model: db.ProductCategory,
                as: 'productCategory',
                include: {
                  model: db.Category,
                  as: 'category'
                }
              },
              {
                model: db.DownloadFont,
                as: "downloadFont"
              }
            ]
          }
        ]
      },
      {
        model: db.Order,
        as: "order",
        include: [
          {
            model: db.OrderPackage,
            as: 'orderPackage'
          },
          {
            model: db.OrderPayment,
            as: 'orderPayment'
          }
        ]
      },
      {
        model: db.UserInformation,
        as: "userInformation"
      },
      {
        model: db.UserAddress,
        as: 'userAddress'
      },
      {
        model: db.Comment,
        as: "comment",
        include: [
          {
            model: db.Product,
            as: "product",
            include: [
              {
                model: db.ProductImage,
                as: 'productImage',
                separate: true
              },
              {
                model: db.ProductFont,
                as: 'productFont',
                separate: true
              },
              {
                model: db.ProductCategory,
                as: 'productCategory',
                include: {
                  model: db.Category,
                  as: 'category'
                }
              }
            ]
          }
        ]
      },
      {
        model: db.DownloadFont,
        as: "downloadFont"
      }
    ]
  });

  return user;
}

/**
 * Get user information
 * @param {*} userId
 */
export async function getMyReferrer(id) {
  const myReferrer = await db.UserReferral.findAll({
    where: {
      referrerId: id
    },
    include: [
      {
        model: db.User,
        as: "register",
        include: [
          {
            model: db.UserInformation,
            as: "userInformation"
          }
        ]
      }
    ]
  });

  return myReferrer;
}

export async function getListReferalUser(params) {
  const listUserReferal = await db.UserReferral.count({
    where: {
      referrerId: params.userId
    },
    include: [
      {
        model: db.User,
        as: "register",
        where: {
          level: params.level
        }
      }
    ]
  });
  return listUserReferal;
}

export async function getUserByUserCode(params) {
  return db.User.findOne({
    where: {
      userCode: params.userCode
    }
  });
}

export async function getTopUserProduct() {
  const result = await db.User.findAll({
    include: [
      {
        model: db.UserInformation,
        as: "userInformation"
      },
      {
        model: db.Product,
        as: "product"
      }
    ]
  });
  const sorted_product = result.sort((one, other) => {
    return other.product.length - one.product.length;
  });
  return sorted_product;
}

export async function getTopUserProductSave(
  query,
  { offset, limit, order }
) {
  const { name, status, isAdmin } = query;
  const conditions = {};

  name && (conditions.name = { [Op.like]: `%${name.trim()}%` });
  status && (conditions.status = status);
  isAdmin && (conditions.status = GLOBAL_STATUS.ACTIVE);

  const listUserProductSave = await db.User.findAndCountAll({
    where: conditions,
    include: [
      {
        model: db.ProductSave,
        as: "productSave",
        where: { status: GLOBAL_STATUS.ACTIVE },
        include: [
          {
            model: db.Product,
            as: "product",
            include: [
              {
                model: db.ProductImage,
                as: 'productImage',
                separate: true
              },
              {
                model: db.ProductFont,
                as: 'productFont',
                separate: true
              },
              {
                model: db.ProductCategory,
                as: 'productCategory',
                include: {
                  model: db.Category,
                  as: 'category'
                }
              },
              {
                model: db.DownloadFont,
                as: "downloadFont"
              }
            ]
          }
        ]
      }
    ],
    offset,
    limit,
    order
  });

  return listUserProductSave;
}

