-- MySQL dump 10.13  Distrib 8.0.30, for Win64 (x86_64)
--
-- Host: localhost    Database: web_sanfont
-- ------------------------------------------------------
-- Server version	8.0.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acl_action`
--

DROP TABLE IF EXISTS `acl_action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `acl_action` (
  `id` int NOT NULL AUTO_INCREMENT,
  `moduleId` int DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `createdById` bigint DEFAULT '0',
  `updatedById` bigint DEFAULT '0',
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_action`
--

LOCK TABLES `acl_action` WRITE;
/*!40000 ALTER TABLE `acl_action` DISABLE KEYS */;
INSERT INTO `acl_action` VALUES (1,1,'READ','Xem quản lý thống kê',0,0,'2022-06-21 14:38:09','2022-06-21 14:38:09'),(2,2,'READ','Xem quản lý sản phẩm',0,0,'2022-06-21 14:38:09','2022-06-21 14:38:09'),(3,3,'READ','Xem quản lý user',0,0,'2022-06-21 14:38:09','2022-06-21 14:38:09'),(4,4,'READ','Xem quản lý order',0,0,'2023-12-14 14:18:39','2023-12-14 14:18:39'),(5,5,'READ','Xem quản lý blog',0,0,'2024-03-13 11:01:40','2024-03-13 11:01:40'),(6,6,'READ','Xem quản lý config',0,0,'2024-03-13 11:25:04','2024-03-13 11:25:04');
/*!40000 ALTER TABLE `acl_action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_group`
--

DROP TABLE IF EXISTS `acl_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `acl_group` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `createdById` bigint DEFAULT '0',
  `updatedById` bigint DEFAULT '0',
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_group`
--

LOCK TABLES `acl_group` WRITE;
/*!40000 ALTER TABLE `acl_group` DISABLE KEYS */;
INSERT INTO `acl_group` VALUES (1,'ADMIN','Quản lý tổng quan hệ thống bán hàng',0,0,'2022-06-21 14:31:53','2022-06-21 14:31:53'),(2,'SALES','Người bán hàng',0,0,'2022-06-21 14:31:53','2022-06-21 14:31:53'),(3,'USER','Khách hàng',0,0,'2022-06-21 14:31:53','2022-06-21 14:31:53');
/*!40000 ALTER TABLE `acl_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_group_action`
--

DROP TABLE IF EXISTS `acl_group_action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `acl_group_action` (
  `groupId` int NOT NULL,
  `actionId` int NOT NULL,
  `type` int DEFAULT NULL,
  PRIMARY KEY (`groupId`,`actionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_group_action`
--

LOCK TABLES `acl_group_action` WRITE;
/*!40000 ALTER TABLE `acl_group_action` DISABLE KEYS */;
INSERT INTO `acl_group_action` VALUES (1,1,NULL),(1,2,NULL),(1,3,NULL),(1,4,NULL),(1,5,NULL),(1,6,NULL),(2,1,NULL),(2,2,NULL),(2,3,NULL),(2,4,NULL),(2,5,NULL),(3,1,NULL),(3,2,NULL),(3,3,NULL),(3,5,NULL);
/*!40000 ALTER TABLE `acl_group_action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_module`
--

DROP TABLE IF EXISTS `acl_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `acl_module` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `createdById` bigint DEFAULT '0',
  `updatedById` bigint DEFAULT '0',
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_module`
--

LOCK TABLES `acl_module` WRITE;
/*!40000 ALTER TABLE `acl_module` DISABLE KEYS */;
INSERT INTO `acl_module` VALUES (1,'Dashboard','Quản lý thống kê',0,0,'2022-06-21 14:35:31','2022-06-21 14:35:31'),(2,'Product','Quản lý sản phẩm',0,0,'2023-11-25 17:11:09','2023-11-25 17:11:09'),(3,'User','Quản lý người dùng',0,0,'2023-11-25 17:16:38','2023-11-25 17:16:38'),(4,'Order','Quản lý đơn hàng',0,0,'2023-11-25 17:17:47','2023-11-25 17:17:47'),(5,'Blog','Quản lý blogs',0,0,'2023-11-25 17:18:18','2023-11-25 17:18:18'),(6,'Config','Cấu hình hệ thống',0,0,'2023-11-25 17:18:29','2023-11-25 17:18:29');
/*!40000 ALTER TABLE `acl_module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `avatar` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `status` tinyint(1) DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `deletedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'van linh','admin@gmail.com','$2b$10$Y6qXyU8eH0OyuCrulLLIme6vQhs7ya75AfjBjyR3r55PQmiwGoWii','http://localhost:3001/images/file-1711199847450',1,'2022-06-07 17:09:50','2024-03-23 13:17:32',NULL);
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog`
--

DROP TABLE IF EXISTS `blog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `blog` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `thumbnail` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci,
  `status` int DEFAULT NULL,
  `createdById` int DEFAULT NULL,
  `updatedById` int DEFAULT NULL,
  `createdAt` date DEFAULT NULL,
  `updatedAt` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog`
--

LOCK TABLES `blog` WRITE;
/*!40000 ALTER TABLE `blog` DISABLE KEYS */;
INSERT INTO `blog` VALUES (1,'Top font Việt hóa UTM được yêu thích nhất ( phần 3 )','Font việt hóa UTM Arruba KT Font việt hóa UTM Showcard Font việt hóa UTM Androgyne Font việt hoá UTM Impact Font việt hóa UTM…','https://fonttiengviet.com/wp-content/uploads/2023/03/Font-viet-hoa-UTM-Androgyne.jpg',1,NULL,NULL,'2023-11-22','2023-12-05'),(3,'Top font Việt hóa UTM được yêu thích nhất ( phần 2 )','Font việt hóa UTM Cooper Font Việt hóa UTM Candombe Font việt hóa UTM Alpine KT Font việt hóa UTM A&S Graceland Font việt hóa…','https://fonttiengviet.com/wp-content/uploads/2023/03/Font-viet-hoa-UTM-AS-Graceland.jpg',1,NULL,NULL,'2023-11-23','2023-12-05'),(5,'Top font Việt hóa UTM được yêu thích nhất ( phần 1 )','Font Việt hóa UTM Hanzel Font Việt hóa UTM Gradoo Font Việt hóa UTM Flamenco Font Việt hóa UTM Flavour Font việt hóa UTM Cookies','http://localhost:3001/images/file-1701790487830',1,NULL,NULL,'2023-12-01','2023-12-05'),(6,'Tổng hợp font Việt hóa Halloween đẹp','Font việt hóa Sayuri Halloween Island Font việt hóa 1FTV Spider Home Font việt hóa 1FTV VIP Ghoulies Font việt hóa 1FTV Henny Penny','http://localhost:3001/images/file-1701792378411',1,NULL,NULL,'2023-12-01','2023-12-05'),(8,'Tải font việt hóa viết tay tay đẹp phần 2','Tải font việt hóa viết tay tay đẹp phần 2 Font Việt hóa 1FTV VIP Milton One Bold Font Việt hóa 1FTV VIP Ermia Font…','http://localhost:3001/images/file-1701792423726',1,NULL,NULL,'2023-12-01','2023-12-05'),(9,'Tải font việt hóa viết tay tay đẹp phần 1','Tổng hợp font chữ Việt hóa font viết tay đẹp phần 1 Font Việt hóa 1FTV Photograph Signature Font Việt hóa 1FTV Autography Font việt…','http://localhost:3001/images/file-1701792475332',1,NULL,NULL,'2023-12-01','2023-12-05'),(10,'Top 5 font Việt hóa cho thiết kế ẩm thực','Font Việt hóa 1FTV VIP Komika Axis Font Việt hóa 1FTV VIP Super Funtime Font Việt hóa 1FTV Vip Cheesecake Combo font thiết kế ẩm…','http://localhost:3001/images/file-1701792532851.png',1,NULL,NULL,'2023-12-04','2023-12-05'),(11,'Top 10 font Việt hóa tiếng Việt UVN quen mặt phần 2','Font Việt Hóa UVN Moi Hong Font Việt Hóa UVN Mua Thu Font Việt Hóa UVN Con Thuy Font Việt Hóa UVN Ben Xuan Font…','http://localhost:3001/images/file-1701792607696',1,NULL,NULL,'2023-12-04','2023-12-05'),(12,'Top 10 font Việt hóa tiếng Việt UVN quen mặt phần 1','Font Việt Hóa UVN Phuong Tay Font Việt Hóa UVN Thang Vu Font Việt Hóa UVN Ly Do Font Việt Hóa UVN But Long 2…','http://localhost:3001/images/file-1701792661406',1,NULL,NULL,'2023-12-04','2023-12-05'),(13,'Top font việt hóa độc lạ nhất','Font chữ có rất nhiều hình dạng khác nhau, bài này fonttiengviet.net sẽ giới thiệu bạn những font việt hóa mà đội ngũ admin thấy…','http://localhost:3001/images/file-1701792700661',1,NULL,NULL,'2023-12-05','2023-12-05'),(14,'Top font brush việt hóa được yêu thích nhất','Font chữ việt hóa font brush đẹp được fonttiengviet.com tổng hợp cho các bạn Font việt hóa tiếng việt SVN Kingston Font việt hóa SVN…','http://localhost:3001/images/file-1701792812004',1,NULL,NULL,'2023-12-05','2023-12-05'),(15,'Tổng hợp font viết tay việt hóa tuyệt đẹp ( phần 2 )','Phần 2 các font viết tay việt hóa đẹp nhất dành cho thiết kế Font việt hóa MTD Houstoner Script Font việt hóa 1FTV The…','http://localhost:3001/images/file-1701792844520',1,NULL,NULL,'2023-12-05','2024-02-28'),(16,'Tổng hợp font viết tay việt hóa đẹp nhất ( phần 1 )','Tổng hợp font viết tay việt hóa tuyệt đẹp có thể dùng trong nhiều thiết kế khác nhau: Thiết kế quảng cáo, thiết kế thiệp…','http://localhost:3001/images/file-1701792952115',1,NULL,NULL,'2023-12-05','2023-12-05'),(17,'Top font tiếng việt đẹp do người Việt thiết kế ( phần 2 )','Fonttiengviet.com tiếp tục giới thiệu đến các bạn phần 2 các font tiếng việt tuyệt đẹp do người việt thiết kế. Font việt hoá Bong…','http://localhost:3001/images/file-1701792991072',1,NULL,NULL,'2023-12-05','2024-03-09'),(18,'Top font tiếng việt đẹp do người Việt thiết kế ( phần 1 )','Người Việt Nam rất tài năng và có rất nhiều nhà thiết kế font thiết kế nên nhiều font chữ tuyệt đẹp. Các bạn có…','http://localhost:3001/images/file-1701793041550',1,NULL,NULL,'2023-12-05','2023-12-05'),(19,'Tổng hợp font việt hóa nhật bản đẹp cho thiết kế','Nếu bạn cần những font việt hóa phong cách nhật bản thì đây sẽ là các font dành cho bạn Font việt hóa 1FTV Hirosaki…','http://localhost:3001/images/file-1701793093393',1,NULL,NULL,'2023-12-05','2024-03-30');
/*!40000 ALTER TABLE `blog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_category`
--

DROP TABLE IF EXISTS `blog_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `blog_category` (
  `id` int NOT NULL AUTO_INCREMENT,
  `blogId` int NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `thumbnail` text,
  `createdById` int DEFAULT NULL,
  `updatedById` int DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`,`blogId`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_category`
--

LOCK TABLES `blog_category` WRITE;
/*!40000 ALTER TABLE `blog_category` DISABLE KEYS */;
INSERT INTO `blog_category` VALUES (1,1,'Font việt hóa UTM Arruba KT','https://fonttiengviet.com/wp-content/uploads/2023/03/Font-viet-hoa-UTM-Arruba-KT.jpg',NULL,NULL,'2023-11-23 09:57:28','2023-12-05 16:40:42'),(2,1,'Font việt hóa UTM Showcard','https://fonttiengviet.com/wp-content/uploads/2023/02/font-viet-hoa-UTM-Showcard.jpg',NULL,NULL,'2023-11-23 10:08:46','2023-11-23 04:25:57'),(3,1,'Font việt hóa UTM Androgyne','https://fonttiengviet.com/wp-content/uploads/2023/03/Font-viet-hoa-UTM-Androgyne.jpg',NULL,NULL,'2023-11-23 04:16:30','2023-11-23 04:16:30'),(5,1,'Font việt hoá UTM Impact','https://fonttiengviet.com/wp-content/uploads/2022/05/Font-viet-hoa-IMPACT.jpg',NULL,NULL,'2023-11-23 04:46:04','2023-11-23 04:46:04'),(6,1,'Font việt hóa UTM Thanh Nhac TL','https://fonttiengviet.com/wp-content/uploads/2023/02/font-viet-hoa-UTM-Thanh-Nhac-TL.jpg',NULL,NULL,'2023-11-23 04:46:29','2023-11-23 04:46:29'),(7,3,'Font việt hóa UTM Cooper','https://fonttiengviet.com/wp-content/uploads/2023/06/Font-viet-hoa-UTM-Cooper-100.jpg',NULL,NULL,'2023-11-23 04:47:21','2023-11-23 04:51:47'),(8,3,'Font Việt hóa UTM Candombe','https://fonttiengviet.com/wp-content/uploads/2023/06/Font-Viet-hoa-UTM-Candombe.jpg',NULL,NULL,'2023-11-23 04:47:56','2023-11-23 04:52:06'),(9,3,'Font việt hóa UTM Alpine KT','https://fonttiengviet.com/wp-content/uploads/2023/03/Font-viet-hoa-UTM-Alpine-KT.jpg',NULL,NULL,'2023-11-23 04:48:25','2023-11-23 04:52:10'),(10,3,'Font việt hóa UTM A&S Graceland','https://fonttiengviet.com/wp-content/uploads/2023/03/Font-viet-hoa-UTM-AS-Graceland.jpg',NULL,NULL,'2023-11-23 04:48:49','2023-11-23 04:52:14'),(11,3,'Font việt hóa UTM Staccato','https://fonttiengviet.com/wp-content/uploads/2023/02/font-viet-hoa-UTM-Staccato.jpg',NULL,NULL,'2023-11-23 04:49:12','2023-12-05 15:29:19'),(13,5,'Font Việt hóa UTM Hanzel','http://localhost:3001/images/file-1701790569845',NULL,NULL,'2023-12-05 15:36:13','2023-12-05 15:36:13'),(14,5,'Font Việt hóa UTM Gradoo','http://localhost:3001/images/file-1701791945951',NULL,NULL,'2023-12-05 15:59:08','2023-12-05 15:59:08'),(18,5,'Font Việt hóa UTM Flamenco','http://localhost:3001/images/file-1701795979077',NULL,NULL,'2023-12-05 17:06:20','2023-12-05 17:06:20');
/*!40000 ALTER TABLE `blog_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `categorySlug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `createdById` int DEFAULT NULL,
  `updatedById` int DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categorySlug_UNIQUE` (`categorySlug`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Font việt hóa','font-viet-hoa',1,NULL,NULL,'2023-11-25 10:25:51','2023-11-25 10:25:51'),(2,'Font quảng cáo','font-quang-cao',1,NULL,NULL,'2023-11-25 10:25:51','2023-11-25 10:25:51'),(11,'van linh','van-linh',1,NULL,NULL,'2024-03-30 04:47:36','2024-03-30 04:47:36'),(12,'Shop','Shop',1,NULL,NULL,'2024-04-05 04:29:56','2024-04-05 04:29:56');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `comment` (
  `id` int NOT NULL AUTO_INCREMENT,
  `userId` int NOT NULL,
  `productId` int NOT NULL,
  `note` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `createdById` int DEFAULT NULL,
  `updatedById` int DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
INSERT INTO `comment` VALUES (2,12,14,'font raat dat biett s',1,NULL,NULL,'2024-03-23 08:32:55','2024-03-25 02:10:16'),(3,10,11,'font tuyet vowii',1,NULL,NULL,'2024-03-23 08:52:28','2024-03-23 13:18:47'),(10,7,20,'font tuyet aaaaa',1,NULL,NULL,'2024-04-05 09:28:54','2024-04-05 09:28:54'),(11,7,22,'font tuyet quá là hợp d',1,NULL,NULL,'2024-04-05 09:39:30','2024-04-05 17:59:54'),(13,8,15,'font tuyet quá là hợp',1,NULL,NULL,'2024-04-12 08:44:00','2024-04-12 08:44:00');
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contract`
--

DROP TABLE IF EXISTS `contract`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contract` (
  `id` int NOT NULL AUTO_INCREMENT,
  `fullName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `phoneNumber` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `status` tinyint(1) DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contract`
--

LOCK TABLES `contract` WRITE;
/*!40000 ALTER TABLE `contract` DISABLE KEYS */;
INSERT INTO `contract` VALUES (1,'van linh','linh@gmail.com','0787945995','rat la vuuii hay ',1,'2023-12-11 07:27:57','2024-03-01 09:37:02'),(2,'Chau long','longchau@gmail.com','0787945855','rat la vuuii sd',1,'2023-12-11 10:05:07','2024-04-06 03:50:14');
/*!40000 ALTER TABLE `contract` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `download_font`
--

DROP TABLE IF EXISTS `download_font`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `download_font` (
  `idDownload` int NOT NULL,
  `id` int NOT NULL,
  `productId` int NOT NULL,
  `userId` int NOT NULL,
  `nameDownload` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `createdById` int DEFAULT NULL,
  `updatedById` int DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idDownload`,`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `download_font`
--

LOCK TABLES `download_font` WRITE;
/*!40000 ALTER TABLE `download_font` DISABLE KEYS */;
INSERT INTO `download_font` VALUES (1,1,44,7,'SVN-Vantom.zip',1,NULL,NULL,'2024-04-10 02:54:54','2024-04-10 02:54:54'),(1,2,44,7,'SVN-Vantom.zip',1,NULL,NULL,'2024-04-10 02:55:49','2024-04-10 04:28:59'),(2,1,11,7,'SVN-Rush-Hour.zip',1,NULL,NULL,'2024-04-10 02:57:01','2024-04-10 04:28:00'),(2,2,11,15,'SVN-Rush-Hour.zip',1,NULL,NULL,'2024-04-10 02:57:24','2024-04-10 03:19:16');
/*!40000 ALTER TABLE `download_font` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_data`
--

DROP TABLE IF EXISTS `master_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `master_data` (
  `idMaster` int NOT NULL,
  `id` int NOT NULL,
  `nameMaster` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `note` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `createdById` int DEFAULT NULL,
  `updatedById` int DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idMaster`,`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_data`
--

LOCK TABLES `master_data` WRITE;
/*!40000 ALTER TABLE `master_data` DISABLE KEYS */;
INSERT INTO `master_data` VALUES (1,1,'Cấp bậc người dùng','Cộng tác viên','Cộng tác viên ',1,NULL,NULL,'2024-03-25 04:21:35','2024-03-25 04:21:35'),(1,2,'Cấp bậc người dùng','SPA Home s','SPA Home s',1,NULL,NULL,'2024-03-25 04:57:20','2024-03-25 04:57:20'),(1,3,'Cấp bậc người dùng','SPA VIP','SPA VIP',1,NULL,NULL,'2024-03-25 09:22:10','2024-04-04 05:01:38'),(2,1,'Tình trạng đơn hàng','Chờ kích hoạt','Đơn hàng  chờ kích hoạt',1,NULL,NULL,'2024-03-25 04:25:18','2024-03-28 09:33:40'),(2,2,'Tình trạng đơn hàng','Đã kích hoạt','Đơn hàng đã kích hoạt',1,NULL,NULL,'2024-03-25 04:55:59','2024-03-28 09:34:25'),(3,1,'Phân quyền','ADMIN','Quản lý tổng quan hệ thống font',1,NULL,NULL,'2024-03-26 08:08:39','2024-03-26 08:08:39'),(3,2,'Phân quyền','SALE','Người up font',1,NULL,NULL,'2024-03-26 08:09:27','2024-03-26 08:09:27'),(3,3,'Phân quyền','USER','Khách hàng',1,NULL,NULL,'2024-03-26 08:09:49','2024-03-26 08:09:49'),(4,1,'Tình trạng FONT','Tải các font bình thường','Tải các font bình thường',1,NULL,NULL,'2024-03-29 05:00:43','2024-03-29 05:00:43'),(4,2,'Tình trạng FONT','Tải tất cả các font vs vip','Tải tất cả các font vs vip',1,NULL,NULL,'2024-03-29 05:01:10','2024-03-29 05:01:10');
/*!40000 ALTER TABLE `master_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order` (
  `id` int NOT NULL AUTO_INCREMENT,
  `paymentId` int DEFAULT NULL,
  `userId` int DEFAULT NULL,
  `orderPackageId` int NOT NULL,
  `fullName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `note` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `identification` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `total` decimal(10,0) DEFAULT NULL,
  `orderCode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `orderDate` datetime DEFAULT NULL,
  `telephone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `orderStatus` tinyint(1) DEFAULT NULL,
  `createdById` int DEFAULT NULL,
  `updatedById` int DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` VALUES (109,86,10,1,'aphan','aphan@gmail.com','Gói bắt đầu','USER',99000,'SANFONT000088','2024-03-21 04:33:40','0787945995','08 Trần lựu, Cẩm lệ, Đà Nẵng',2,NULL,NULL,'2024-03-21 04:33:40','2024-03-28 09:46:25'),(110,87,10,2,'aphan','aphan@gmail.com','Gói bắt đầu','USER',99000,'SANFONT000089','2024-03-21 04:41:35','0787945995','08 Trần lựu, Cẩm lệ, Đà Nẵng',2,NULL,NULL,'2024-03-21 04:41:35','2024-03-28 09:42:26'),(112,89,24,2,'Long chauu','tonlongchau@gmail.com','Gói 6 tháng','USER',99000,'SANFONT000091','2024-03-21 04:47:55','0787945995','08 Trần lựu, Cẩm lệ, Đà Nẵng',2,NULL,NULL,'2024-03-21 04:47:55','2024-03-28 09:42:10'),(113,90,10,2,'van linh','linhvanle130@gmail.com','Gói 6 tháng','USER',99000,'SANFONT000092','2024-03-21 04:57:57','0787945995','08 Trần lựu, Cẩm lệ, Đà Nẵng',2,NULL,NULL,'2024-03-21 04:57:57','2024-03-28 09:41:54'),(114,91,10,2,'van linh','linhvanle130@gmail.com','Gói 6 tháng','USER',99000,'SANFONT000093','2024-03-21 04:58:37','0787945995','08 Trần lựu, Cẩm lệ, Đà Nẵng',2,NULL,NULL,'2024-03-21 04:58:37','2024-04-12 08:24:26'),(117,94,24,2,'d','ds@gmail.com','Gói 6 tháng','USER',8525,'SANFONT000094','2024-03-22 04:21:11','0787945915','08 Trần lựu, Cẩm lệ, Đà Nẵng',2,NULL,NULL,'2024-03-22 04:21:11','2024-03-28 09:42:03'),(118,95,23,3,'văn linh','vanlinh@gmail.com','s','USER',5822,'SANFONT000095','2024-03-22 04:23:00','0787945995','407 Tran Luu',2,NULL,NULL,'2024-03-22 04:23:00','2024-03-28 09:41:47'),(119,96,17,5,'văn linh','vanlinh@gmail.com','s','USER',5822,'SANFONT000096','2024-03-22 04:36:08','0787945995','407 Tran Luu',2,NULL,NULL,'2024-03-22 04:36:08','2024-03-28 09:41:38'),(120,97,10,2,'van linh','linhvanle130@gmail.com','Gói 6 tháng','USER',99000,'SANFONT000097','2024-03-22 07:41:47','0787945995','08 Trần lựu, Cẩm lệ, Đà Nẵng',2,NULL,NULL,'2024-03-22 07:41:47','2024-04-12 08:24:38'),(121,99,8,3,'văn linh','admin@gmail.com','dsd','USER',5822,'SANFONT000098','2024-03-23 09:43:31','0787945995','407 Tran Luu',1,NULL,NULL,'2024-03-23 09:43:31','2024-03-23 09:43:31'),(122,100,8,3,'văn linh','vanlinh@gmail.com','sđssd','USER',5822,'SANFONT000099','2024-03-23 09:47:02','0787945995','407 Tran Luu',1,NULL,NULL,'2024-03-23 09:47:02','2024-03-23 09:47:02'),(123,101,8,5,'văn linh','vanlinh@gmail.com','ads','USER',99000,'SANFONT000100','2024-03-23 09:47:32','0787945995','407 Tran Luu',1,NULL,NULL,'2024-03-23 09:47:32','2024-03-23 09:47:32'),(124,102,8,5,'văn linh','vanlinh@gmail.com','ads','USER',99000,'SANFONT000101','2024-03-23 09:48:08','0787945995','407 Tran Luu',1,NULL,NULL,'2024-03-23 09:48:08','2024-03-23 09:48:08'),(125,103,8,3,'văn linh','vanlinh@gmail.com','dsds','USER',5822,'SANFONT000102','2024-03-23 09:50:12','0787945995','407 Tran Luu',1,NULL,NULL,'2024-03-23 09:50:12','2024-03-23 09:50:12'),(127,105,8,5,'văn linh','vanlinh@gmail.com','kljh','USER',5822,'SANFONT000104','2024-03-23 10:17:00','0787945995','407 Tran Luu',2,NULL,NULL,'2024-03-23 10:17:00','2024-04-04 07:40:07'),(129,107,8,3,'văn linh','vanlinh@gmail.com','dsds','USER',5822,'SANFONT000106','2024-03-23 10:22:17','0787945995','407 Tran Luu',2,NULL,NULL,'2024-03-23 10:22:17','2024-04-04 04:38:34'),(132,110,7,3,'văn linh','vanlinh@gmail.com','đsdsds','USER',5822,'SANFONT000109','2024-03-23 10:24:54','0787945995','407 Tran Luu',2,NULL,NULL,'2024-03-23 10:24:54','2024-04-04 07:29:44'),(134,112,10,2,'van linh','linhvanle130@gmail.com','Gói 6 tháng','USER',99000,'SANFONT000111','2024-03-25 03:06:48','0787945995','08 Trần lựu, Cẩm lệ, Đà Nẵng',2,NULL,NULL,'2024-03-25 03:06:48','2024-04-04 07:39:47'),(136,114,9,3,'văn linh','linhvanle130@gmail.com','đs','USER',99000,'SANFONT000113','2024-03-29 02:22:45','0787945995','407 Tran Luu',2,NULL,NULL,'2024-03-29 02:22:45','2024-04-12 08:23:02'),(137,115,7,2,'van linh','linhvanle130@gmail.com','Gói 6 tháng','USER',99000,'SANFONT000114','2024-04-01 04:55:35','0787945995','08 Trần lựu, Cẩm lệ, Đà Nẵng',2,NULL,NULL,'2024-04-01 04:55:35','2024-04-12 08:25:55');
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_content`
--

DROP TABLE IF EXISTS `order_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_content` (
  `idContent` int NOT NULL,
  `id` int NOT NULL,
  `orderPackageId` int NOT NULL,
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idContent`,`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_content`
--

LOCK TABLES `order_content` WRITE;
/*!40000 ALTER TABLE `order_content` DISABLE KEYS */;
INSERT INTO `order_content` VALUES (1,1,1,'KHÔNG TẢI ĐƯỢC FONT VIP','2024-04-12 03:53:21','2024-04-13 04:51:50'),(1,2,1,'Tải font ngay ( Không chờ 30s )','2024-04-12 03:54:48','2024-04-12 03:54:48'),(1,3,1,'Ủng hộ duy trì website','2024-04-12 03:55:12','2024-04-12 03:55:12'),(1,4,1,'Tự động kích hoạt sau đăng ký','2024-04-12 03:55:19','2024-04-13 04:41:09'),(1,5,1,'Không tự động gia hạn','2024-04-12 03:55:26','2024-04-13 04:40:06'),(1,6,1,'Quy định gói thành viên','2024-04-12 03:55:37','2024-04-13 04:41:28'),(2,1,2,'TẢI ĐƯỢC FONT VIP','2024-04-12 03:54:00','2024-04-12 04:22:53'),(2,2,2,'Tải font ngay lập tức ','2024-04-12 08:05:07','2024-04-12 08:05:07'),(2,3,2,'Ủng hộ duy trì website','2024-04-12 08:05:25','2024-04-12 08:05:25'),(2,4,2,'Tự động kích hoạt sau đăng ký','2024-04-12 08:05:33','2024-04-12 08:05:33'),(2,5,2,'Không tự động gia hạn','2024-04-12 08:05:42','2024-04-12 08:05:42'),(2,6,2,'Quy định gói thành viên','2024-04-12 08:05:51','2024-04-12 08:05:51'),(2,7,2,'nmnm','2024-04-15 02:52:26','2024-04-15 02:52:26'),(3,1,3,'KHÔNG TẢI ĐƯỢC FONT VIP','2024-04-12 04:02:46','2024-04-12 04:23:54'),(3,2,3,'Tải font ngay ( Không chờ 30s )','2024-04-12 08:06:14','2024-04-12 08:06:14'),(3,3,3,'Ủng hộ duy trì website','2024-04-12 08:06:23','2024-04-12 08:06:23'),(3,4,3,'Tự động kích hoạt sau đăng ký','2024-04-12 08:06:31','2024-04-12 08:06:31'),(3,5,3,'Không tự động gia hạn','2024-04-12 08:06:38','2024-04-12 08:06:38'),(3,6,3,'Quy định gói thành viên','2024-04-12 08:06:47','2024-04-12 08:06:47');
/*!40000 ALTER TABLE `order_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_package`
--

DROP TABLE IF EXISTS `order_package`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_package` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `pricePrevious` decimal(10,0) DEFAULT NULL,
  `price` decimal(10,0) DEFAULT NULL,
  `note` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `createdById` int DEFAULT NULL,
  `updatedById` int DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_package`
--

LOCK TABLES `order_package` WRITE;
/*!40000 ALTER TABLE `order_package` DISABLE KEYS */;
INSERT INTO `order_package` VALUES (1,'Gói 1 tháng',36000,18000,'Thanh toán lại sau 1 tháng',1,NULL,NULL,'2024-04-12 08:29:11','2024-04-12 08:29:11'),(2,'Gói 12 tháng',200000,99000,'Thanh toán lại sau 12 tháng',1,NULL,NULL,'2024-04-12 08:30:05','2024-04-12 08:30:05'),(3,'Gói 6 tháng',160000,79000,'Thanh toán lại sau 6 tháng',1,NULL,NULL,'2024-04-12 08:30:40','2024-04-12 08:30:40');
/*!40000 ALTER TABLE `order_package` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_payment`
--

DROP TABLE IF EXISTS `order_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_payment` (
  `id` int NOT NULL AUTO_INCREMENT,
  `paymentMethod` int DEFAULT NULL,
  `createdById` int DEFAULT NULL,
  `updatedById` int DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_payment`
--

LOCK TABLES `order_payment` WRITE;
/*!40000 ALTER TABLE `order_payment` DISABLE KEYS */;
INSERT INTO `order_payment` VALUES (1,1,NULL,NULL,'2024-03-12 15:46:24','2024-03-12 15:46:24'),(2,0,NULL,NULL,'2024-03-12 08:56:15','2024-03-12 08:56:15'),(3,0,NULL,NULL,'2024-03-12 08:56:56','2024-03-12 08:56:56'),(4,0,NULL,NULL,'2024-03-12 08:58:26','2024-03-12 08:58:26'),(5,2,NULL,NULL,'2024-03-12 09:18:33','2024-03-12 09:18:33'),(6,1,NULL,NULL,'2024-03-12 09:36:20','2024-03-12 09:36:20'),(7,1,NULL,NULL,'2024-03-12 09:38:17','2024-03-12 09:38:17'),(8,1,NULL,NULL,'2024-03-12 09:39:55','2024-03-12 09:39:55'),(9,3,NULL,NULL,'2024-03-12 09:43:46','2024-03-12 09:43:46'),(10,3,NULL,NULL,'2024-03-12 09:44:03','2024-03-12 09:44:03'),(11,1,NULL,NULL,'2024-03-12 09:44:24','2024-03-12 09:44:24'),(12,1,NULL,NULL,'2024-03-12 09:45:38','2024-03-12 09:45:38'),(13,1,NULL,NULL,'2024-03-12 09:50:04','2024-03-12 09:50:04'),(14,1,NULL,NULL,'2024-03-12 09:51:08','2024-03-12 09:51:08'),(15,1,NULL,NULL,'2024-03-12 09:52:18','2024-03-12 09:52:18'),(16,1,NULL,NULL,'2024-03-12 09:55:07','2024-03-12 09:55:07'),(17,0,NULL,NULL,'2024-03-12 09:55:38','2024-03-12 09:55:38'),(18,1,NULL,NULL,'2024-03-12 09:58:07','2024-03-12 09:58:07'),(19,1,NULL,NULL,'2024-03-12 10:08:22','2024-03-12 10:08:22'),(20,1,NULL,NULL,'2024-03-12 10:24:13','2024-03-12 10:24:13'),(21,1,NULL,NULL,'2024-03-12 10:28:13','2024-03-12 10:28:13'),(22,1,NULL,NULL,'2024-03-12 10:30:48','2024-03-12 10:30:48'),(23,1,NULL,NULL,'2024-03-12 10:31:50','2024-03-12 10:31:50'),(24,1,NULL,NULL,'2024-03-12 10:35:26','2024-03-12 10:35:26'),(25,1,NULL,NULL,'2024-03-12 10:36:16','2024-03-12 10:36:16'),(26,1,NULL,NULL,'2024-03-12 10:37:27','2024-03-12 10:37:27'),(27,1,NULL,NULL,'2024-03-13 08:41:03','2024-03-13 08:41:03'),(28,1,NULL,NULL,'2024-03-14 08:02:28','2024-03-14 08:02:28'),(29,1,NULL,NULL,'2024-03-14 08:11:53','2024-03-14 08:11:53'),(30,1,NULL,NULL,'2024-03-14 08:25:09','2024-03-14 08:25:09'),(31,1,NULL,NULL,'2024-03-14 08:55:59','2024-03-14 08:55:59'),(32,1,NULL,NULL,'2024-03-14 09:13:15','2024-03-14 09:13:15'),(33,1,NULL,NULL,'2024-03-14 10:16:47','2024-03-14 10:16:47'),(34,1,NULL,NULL,'2024-03-15 04:21:51','2024-03-15 04:21:51'),(35,0,NULL,NULL,'2024-03-15 04:43:35','2024-03-15 04:43:35'),(36,1,NULL,NULL,'2024-03-15 07:49:37','2024-03-15 07:49:37'),(37,1,NULL,NULL,'2024-03-15 08:21:06','2024-03-15 08:21:06'),(38,1,NULL,NULL,'2024-03-15 08:23:58','2024-03-15 08:23:58'),(39,1,NULL,NULL,'2024-03-15 08:24:11','2024-03-15 08:24:11'),(40,1,NULL,NULL,'2024-03-15 08:36:14','2024-03-15 08:36:14'),(41,1,NULL,NULL,'2024-03-15 08:36:17','2024-03-15 08:36:17'),(42,1,NULL,NULL,'2024-03-20 17:11:51','2024-03-20 17:11:51'),(43,1,NULL,NULL,'2024-03-20 17:17:22','2024-03-20 17:17:22'),(44,1,NULL,NULL,'2024-03-20 17:30:54','2024-03-20 17:30:54'),(45,1,NULL,NULL,'2024-03-20 18:37:30','2024-03-20 18:37:30'),(46,1,NULL,NULL,'2024-03-20 18:38:13','2024-03-20 18:38:13'),(47,1,NULL,NULL,'2024-03-20 18:42:30','2024-03-20 18:42:30'),(48,1,NULL,NULL,'2024-03-20 18:43:53','2024-03-20 18:43:53'),(49,1,NULL,NULL,'2024-03-21 02:12:02','2024-03-21 02:12:02'),(50,1,NULL,NULL,'2024-03-21 02:18:08','2024-03-21 02:18:08'),(51,1,NULL,NULL,'2024-03-21 02:22:31','2024-03-21 02:22:31'),(52,1,NULL,NULL,'2024-03-21 02:23:25','2024-03-21 02:23:25'),(53,1,NULL,NULL,'2024-03-21 02:24:24','2024-03-21 02:24:24'),(54,1,NULL,NULL,'2024-03-21 02:24:55','2024-03-21 02:24:55'),(55,1,NULL,NULL,'2024-03-21 02:26:15','2024-03-21 02:26:15'),(56,1,NULL,NULL,'2024-03-21 02:26:43','2024-03-21 02:26:43'),(57,1,NULL,NULL,'2024-03-21 02:27:06','2024-03-21 02:27:06'),(58,1,NULL,NULL,'2024-03-21 02:28:57','2024-03-21 02:28:57'),(59,1,NULL,NULL,'2024-03-21 02:33:10','2024-03-21 02:33:10'),(60,1,NULL,NULL,'2024-03-21 02:35:21','2024-03-21 02:35:21'),(61,1,NULL,NULL,'2024-03-21 02:36:47','2024-03-21 02:36:47'),(62,1,NULL,NULL,'2024-03-21 02:43:56','2024-03-21 02:43:56'),(63,1,NULL,NULL,'2024-03-21 02:46:28','2024-03-21 02:46:28'),(64,1,NULL,NULL,'2024-03-21 03:01:08','2024-03-21 03:01:08'),(65,1,NULL,NULL,'2024-03-21 03:01:57','2024-03-21 03:01:57'),(66,1,NULL,NULL,'2024-03-21 03:05:05','2024-03-21 03:05:05'),(67,1,NULL,NULL,'2024-03-21 03:05:07','2024-03-21 03:05:07'),(68,1,NULL,NULL,'2024-03-21 03:07:29','2024-03-21 03:07:29'),(69,1,NULL,NULL,'2024-03-21 03:16:34','2024-03-21 03:16:34'),(70,1,NULL,NULL,'2024-03-21 03:18:23','2024-03-21 03:18:23'),(71,1,NULL,NULL,'2024-03-21 03:19:47','2024-03-21 03:19:47'),(72,1,NULL,NULL,'2024-03-21 03:28:42','2024-03-21 03:28:42'),(73,1,NULL,NULL,'2024-03-21 03:31:19','2024-03-21 03:31:19'),(74,1,NULL,NULL,'2024-03-21 03:32:17','2024-03-21 03:32:17'),(75,1,NULL,NULL,'2024-03-21 03:34:41','2024-03-21 03:34:41'),(76,1,NULL,NULL,'2024-03-21 03:42:09','2024-03-21 03:42:09'),(77,1,NULL,NULL,'2024-03-21 03:49:26','2024-03-21 03:49:26'),(78,1,NULL,NULL,'2024-03-21 03:52:20','2024-03-21 03:52:20'),(79,1,NULL,NULL,'2024-03-21 03:54:09','2024-03-21 03:54:09'),(80,1,NULL,NULL,'2024-03-21 04:10:55','2024-03-21 04:10:55'),(81,1,NULL,NULL,'2024-03-21 04:14:59','2024-03-21 04:14:59'),(82,1,NULL,NULL,'2024-03-21 04:16:28','2024-03-21 04:16:28'),(83,1,NULL,NULL,'2024-03-21 04:22:33','2024-03-21 04:22:33'),(84,1,NULL,NULL,'2024-03-21 04:32:07','2024-03-21 04:32:07'),(85,1,NULL,NULL,'2024-03-21 04:32:57','2024-03-21 04:32:57'),(86,1,NULL,NULL,'2024-03-21 04:33:40','2024-03-21 04:33:40'),(87,1,NULL,NULL,'2024-03-21 04:41:35','2024-03-21 04:41:35'),(88,1,NULL,NULL,'2024-03-21 04:44:42','2024-03-21 04:44:42'),(89,1,NULL,NULL,'2024-03-21 04:47:55','2024-03-21 04:47:55'),(90,1,NULL,NULL,'2024-03-21 04:57:56','2024-03-21 04:57:56'),(91,1,NULL,NULL,'2024-03-21 04:58:37','2024-03-21 04:58:37'),(92,1,NULL,NULL,'2024-03-22 03:31:10','2024-03-22 03:31:10'),(93,2,NULL,NULL,'2024-03-22 03:36:00','2024-03-22 03:36:00'),(94,1,NULL,NULL,'2024-03-22 04:21:10','2024-03-22 04:21:10'),(95,1,NULL,NULL,'2024-03-22 04:23:00','2024-03-22 04:23:00'),(96,1,NULL,NULL,'2024-03-22 04:36:08','2024-03-22 04:36:08'),(97,2,NULL,NULL,'2024-03-22 07:41:47','2024-03-22 07:41:47'),(98,2,NULL,NULL,'2024-03-22 07:48:08','2024-03-22 07:48:08'),(99,1,NULL,NULL,'2024-03-23 09:43:31','2024-03-23 09:43:31'),(100,1,NULL,NULL,'2024-03-23 09:47:02','2024-03-23 09:47:02'),(101,2,NULL,NULL,'2024-03-23 09:47:32','2024-03-23 09:47:32'),(102,2,NULL,NULL,'2024-03-23 09:48:08','2024-03-23 09:48:08'),(103,1,NULL,NULL,'2024-03-23 09:50:12','2024-03-23 09:50:12'),(104,1,NULL,NULL,'2024-03-23 10:13:34','2024-03-23 10:13:34'),(105,2,NULL,NULL,'2024-03-23 10:17:00','2024-03-23 10:17:00'),(106,1,NULL,NULL,'2024-03-23 10:21:38','2024-03-23 10:21:38'),(107,1,NULL,NULL,'2024-03-23 10:22:17','2024-03-23 10:22:17'),(108,1,NULL,NULL,'2024-03-23 10:23:03','2024-03-23 10:23:03'),(109,1,NULL,NULL,'2024-03-23 10:23:52','2024-03-23 10:23:52'),(110,1,NULL,NULL,'2024-03-23 10:24:54','2024-03-23 10:24:54'),(111,1,NULL,NULL,'2024-03-23 10:25:37','2024-03-23 10:25:37'),(112,2,NULL,NULL,'2024-03-25 03:06:47','2024-03-25 03:06:47'),(113,1,NULL,NULL,'2024-03-25 03:43:09','2024-03-25 03:43:09'),(114,1,NULL,NULL,'2024-03-29 02:22:45','2024-03-29 02:22:45'),(115,2,NULL,NULL,'2024-04-01 04:55:35','2024-04-01 04:55:35');
/*!40000 ALTER TABLE `order_payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `otp`
--

DROP TABLE IF EXISTS `otp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `otp` (
  `id` int NOT NULL AUTO_INCREMENT,
  `key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `status` int DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `deletedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `otp`
--

LOCK TABLES `otp` WRITE;
/*!40000 ALTER TABLE `otp` DISABLE KEYS */;
INSERT INTO `otp` VALUES (1,'register_email_ninhle.130301@gmail.com','763982',1,'2023-11-20 07:31:53','2023-11-20 07:31:53',NULL),(2,'forgot_password_ninhle.130301@gmail.com','382239',0,'2023-11-20 07:36:25','2023-11-20 07:37:34','2023-11-20 07:37:34'),(3,'forgot_password_linhvanle130@gmail.com','173192',0,'2023-11-20 08:08:05','2023-11-20 08:09:59','2023-11-20 08:09:59'),(4,'forgot_password_linhvanle130@gmail.com','748536',0,'2023-11-20 08:13:32','2023-11-20 08:14:36','2023-11-20 08:14:36'),(5,'forgot_password_admin@gmail.com','494141',0,'2023-11-20 09:01:51','2023-11-20 09:05:09','2023-11-20 09:05:09'),(6,'forgot_password_admin@gmail.com','567123',0,'2023-11-20 09:05:10','2023-11-20 09:06:29','2023-11-20 09:06:29'),(7,'forgot_password_linhvanle130@gmail.com','312699',0,'2023-11-20 09:20:30','2024-03-06 03:29:17','2024-03-06 03:29:17'),(8,'forgot_password_lethidieuhien121102@gmail.com','843962',1,'2023-11-21 16:48:18','2023-11-21 16:48:18',NULL),(9,'forgot_password_ninhle.130301@gmail.com','752277',0,'2023-12-14 04:50:03','2023-12-14 04:50:39','2023-12-14 04:50:39'),(10,'forgot_password_linhvanle130@gmail.com','182726',0,'2024-03-06 03:29:17','2024-03-06 03:36:33','2024-03-06 03:36:33'),(11,'forgot_password_linhvanle130@gmail.com','723481',0,'2024-03-06 03:36:34','2024-03-06 03:37:43','2024-03-06 03:37:43'),(12,'forgot_password_linhvanle130@gmail.com','661375',0,'2024-03-06 03:37:43','2024-03-06 03:38:37','2024-03-06 03:38:37'),(13,'forgot_password_linhvanle130@gmail.com','541617',0,'2024-03-12 09:47:12','2024-03-12 10:29:17','2024-03-12 10:29:17'),(14,'forgot_password_linhvanle130@gmail.com','168847',0,'2024-03-12 10:29:17','2024-03-21 02:15:38','2024-03-21 02:15:38'),(15,'forgot_password_linhvanle130@gmail.com','787778',0,'2024-03-21 02:15:38','2024-03-21 02:16:24','2024-03-21 02:16:24');
/*!40000 ALTER TABLE `otp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product` (
  `id` int NOT NULL AUTO_INCREMENT,
  `categoryId` int NOT NULL,
  `userId` int NOT NULL,
  `productSlug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `author` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `poster` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `subAuthor` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `numberDownloads` int DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `outstanding` tinyint(1) DEFAULT NULL,
  `status` int DEFAULT NULL,
  `createdById` int DEFAULT NULL,
  `updatedById` int DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `productSlug_UNIQUE` (`productSlug`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (11,2,9,'font-tieng-viet-lac-tu-phien-ban-2023-d','Font tiếng Việt Lạc Tự ( Phiên bản 2023 ) d','Mack Trinh và Harry Dinhd','Quang Đăng','Mack Trinh và Harry Dinhd',6,'21312312d',0,0,NULL,NULL,'2023-11-21 17:20:45','2024-04-03 18:08:30'),(14,2,11,'font-viet-hoa-1ftv-nakana','Font Việt hóa 1FTV Nakana','MJType',' Quang Đăng','fonttiengviet.com',78,'bcvbcvbvbbbvbvb',2,1,NULL,NULL,'2023-11-22 08:35:22','2024-03-20 08:38:15'),(15,3,9,NULL,'Font Việt hóa ','MJType',' Quang Đăng','fonttiengviet.com',407,'21312312',0,1,NULL,NULL,'2023-11-27 16:09:53','2024-03-05 04:56:08'),(16,1,11,'font-tieng-viet-lac-tu-phien-ban-2023','Font tiếng Việt Lạc Tự ( Phiên bản 2023 )','Mack Trinh và Harry Dinh',' Quang Đăng','Mack Trinh và Harry Dinh',1478,'',0,1,NULL,NULL,'2023-11-27 16:48:17','2023-12-16 04:05:39'),(19,1,22,'font-viet-hoa-tekton-pro','Font việt hóa Tekton Pro','David Siegel','Đặng Trung Kiên','Kiên 2k7',573,NULL,0,1,NULL,NULL,'2023-11-29 09:45:09','2024-03-08 09:19:29'),(20,1,9,'font-viet-hoa-plus-jakarta-sans','Font Việt hóa Plus Jakarta Sans',' Tokotype','Nguyễn Văn Dũng',' Tokotype',454,NULL,0,1,NULL,NULL,'2023-12-07 15:53:47','2024-02-27 04:29:02'),(21,5,12,'font-viet-hoa-1ftv-psychoart','Font Việt hóa 1FTV Psychoart','Sigit Dwipa Raharjo','Quang Đăng','fonttiengviet.com',78,NULL,2,1,NULL,NULL,'2023-12-08 03:19:23','2024-03-08 09:19:40'),(22,1,9,'font-tieng-viet-bt-beau-sans-typeface','Font Tiếng Việt BT Beau Sans Typeface','beau.vn','Quang Đăng','beau.vn',1666,'Năm 2022 và nửa đầu 2023',2,1,NULL,NULL,'2023-12-14 09:38:28','2023-12-20 02:17:22'),(23,1,11,'xit-khu-mui','Xịt khử mùi ','Mack Trinh và Harry Dinh','Quang Đăng','fonttiengviet.com',6,'ds',0,1,NULL,NULL,'2023-12-21 02:50:08','2024-03-09 05:01:49'),(24,1,12,'font-viet-hoa-vni-tekon','Font Việt hoá VNI Tekon','Mack Trinh và Harry Dinh','Quang Đăng','fonttiengviet.com',78,'ds',2,1,NULL,NULL,'2024-02-23 02:16:15','2024-03-30 03:04:48'),(25,5,9,'font-viet-hoa-1ftv-the-yoshi','Font Việt hóa 1FTV The Yoshi','Christoph Nino','Quang Đăng','fonttiengviet.com',867,NULL,2,1,NULL,NULL,'2024-03-04 08:52:42','2024-03-04 08:52:42'),(26,9,20,'font-viet-hoa-1ftv-vip-nura-asyifa','Font Việt hóa 1FTV VIP Nura Asyifa','Khurasan','Quang Đăng','fonttiengviet.com',6,NULL,1,1,NULL,NULL,'2024-03-04 10:01:10','2024-03-08 09:43:01'),(28,9,20,'font-viet-hoa-1ftv-vip-daughter-tet','Font Việt hóa 1FTV VIP Daughter – tết','creativeletter','Quang Đăng','fonttiengviet.com',NULL,NULL,1,1,NULL,NULL,'2024-03-05 02:47:16','2024-03-08 09:43:12'),(29,9,9,'font-viet-hoa-1ftv-vip-selly-calligraphy-font-cho-valentine','Font Việt hóa 1FTV VIP Selly Calligraphy – Font cho valentine','AEN Creative Studio 2020','Quang Đăng','fonttiengviet.com',NULL,NULL,1,1,NULL,NULL,'2024-03-05 03:50:39','2024-03-05 03:50:39'),(30,5,9,'font-viet-hoa-1ftv-vip-austria','Font Việt hóa 1FTV VIP Austria','Debut Studio','Quang Đăng','fonttiengviet.com',NULL,NULL,2,1,NULL,NULL,'2024-03-06 03:02:30','2024-03-06 03:02:30'),(31,5,16,'font-viet-hoa-1ftv-huxley-max-extrablack','Font Việt hóa 1FTV Huxley Max ExtraBlack','Suandana Ipandemade',NULL,'fonttiengviet.com',NULL,NULL,0,1,NULL,NULL,'2024-03-06 09:45:09','2024-03-06 09:45:09'),(32,4,22,'font-viet-hoa-uvn-gia-dinh-hep','Font Việt Hóa UVN Gia Dinh Hep','Nguyễn Quỳnh Trâm Anh',NULL,'fonttiengviet.com',NULL,'không rõ',0,1,NULL,NULL,'2024-03-07 03:41:15','2024-03-07 03:41:15'),(33,2,22,'font-viet-hoa-vni-swiss','Font Việt hoá VNI Swiss','Mack Trinh và Harry Dinh',NULL,'fonttiengviet.com',NULL,NULL,0,1,NULL,NULL,'2024-03-07 09:11:23','2024-03-07 09:11:23'),(34,1,8,'dsa','dsa','Mack Trinh và Harry Dinh',NULL,'dsds',NULL,'dsadsa',0,1,NULL,NULL,'2024-03-08 03:29:14','2024-03-08 09:39:55'),(38,2,7,'van-linh','van linh','Mack Trinh và Harry Dinh',NULL,'fonttiengviet.com',NULL,NULL,1,1,NULL,NULL,'2024-03-15 08:15:09','2024-03-15 08:15:09'),(39,2,17,'font-viet-hoa-vni-revue','Font Việt hoá VNI Revue','Mack Trinh và Harry Dinh',NULL,'fonttiengviet.com',NULL,'đsds',2,1,NULL,NULL,'2024-03-15 08:17:26','2024-03-15 08:17:26'),(40,3,11,NULL,'Font Tiếng ','beau',' Quang Đăng','fonttiengviet.com',1504,'<p>21312312</p>',1,1,NULL,NULL,'2024-03-19 02:33:51','2024-03-19 02:33:51'),(41,3,11,NULL,'Font Tiếng ','beau',' Quang Đăng','fonttiengviet.com',1504,'<p>21312312</p>',1,1,NULL,NULL,'2024-03-19 02:37:32','2024-03-19 02:37:32'),(43,3,11,NULL,'Font Tiếng ','beau',' Quang Đăng','fonttiengviet.com',1504,'<p>21312312</p>',1,1,NULL,NULL,'2024-03-19 02:54:56','2024-03-22 03:07:29'),(44,1,7,'font-viet-hoa-tp-born-pink','Font việt hóa TP Born Pink','Tuỳ Pucca dựa theo font Exocet',NULL,'Tuỳ Pucca',NULL,NULL,1,1,NULL,NULL,'2024-03-19 03:54:25','2024-03-19 03:54:25'),(45,1,8,'font-viet-hoa-1ftv-vip-berlin-sans','Font Việt hóa 1FTV VIP Berlin Sans','Mack Trinh và Harry Dinh',NULL,'fonttiengviet.com',NULL,NULL,1,1,NULL,NULL,'2024-03-19 09:14:49','2024-03-19 09:14:49'),(46,1,7,'font-viet-hoa-1ftv-vip-madam-ghea','Font Việt hóa 1FTV VIP Madam Ghea','ibracreative',NULL,'fonttiengviet.com',NULL,NULL,0,1,NULL,NULL,'2024-03-20 04:34:03','2024-03-20 04:34:03'),(47,2,7,'font-viet-hoa-1ftv-vip-royale-brone','Font Việt hóa 1FTV VIP Royale Brone','Mack Trinh và Harry Dinh',NULL,'fonttiengviet.com',NULL,NULL,0,1,NULL,NULL,'2024-03-20 04:42:42','2024-03-20 08:30:05'),(48,3,11,NULL,'Font Tiếng ','beau',' Quang Đăng','fonttiengviet.com',1504,'<p>21312312</p>',1,1,NULL,NULL,'2024-03-20 11:31:20','2024-03-20 11:31:20'),(53,3,13,'font-viet-hoa-pacific-standard','Font Việt hóa Pacific Standard+','Mack Trinh và Harry Dinh',NULL,'fonttiengviet.com',NULL,'SĐS',2,1,NULL,NULL,'2024-03-30 04:45:11','2024-03-30 04:45:11'),(54,2,9,'font-viet-hoa-svn-kelso-medium','Font Việt hóa SVN Kelso (Medium)','Mack Trinh và Harry Dinh',NULL,'fonttiengviet.com',NULL,NULL,0,1,NULL,NULL,'2024-04-01 07:43:53','2024-04-01 07:43:53'),(56,3,10,NULL,'Font Tiếng ','beau',' Quang Đăng','fonttiengviet.com',1504,'<p>21312312</p>',1,1,NULL,NULL,'2024-04-03 17:47:43','2024-04-03 17:47:43'),(57,1,9,'font-viet-hoa-1ftv-vip-bowtaris','Font Việt hóa 1FTV VIP Bowtaris','Mack Trinh và Harry Dinh',NULL,'beau.vn',NULL,NULL,0,1,NULL,NULL,'2024-04-03 18:12:32','2024-04-03 18:12:32'),(58,2,12,'font-viet-hoa-mtd-ariestha','Font Việt hóa MTD Ariestha','Mack Trinh và Harry Dinh',NULL,'beau.vn',NULL,NULL,0,1,NULL,NULL,'2024-04-03 18:13:14','2024-04-08 04:45:19'),(59,1,15,NULL,'Font Việt hóa 1FTV VIP Reglarik','sanfont.com',NULL,'sanfont.com',NULL,'',2,1,NULL,NULL,'2024-04-11 02:14:45','2024-04-11 02:14:45'),(60,2,7,'font-viet-hoa-1ftv-vip-callingstone-font-spa-lam-dep','Font Việt hóa 1FTV VIP Callingstone – Font spa làm đẹp','Nofi Sofyan Hadi',NULL,'fonttiengviet.com',NULL,'',1,1,NULL,NULL,'2024-04-13 08:16:10','2024-04-13 08:16:10');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_category`
--

DROP TABLE IF EXISTS `product_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_category` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `categoryId` int NOT NULL,
  `categorySlug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `createdById` int DEFAULT NULL,
  `updatedById` int DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categorySlug_UNIQUE` (`categorySlug`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_category`
--

LOCK TABLES `product_category` WRITE;
/*!40000 ALTER TABLE `product_category` DISABLE KEYS */;
INSERT INTO `product_category` VALUES (1,'Font tiếng Việt',1,'font-tieng-viet',1,NULL,NULL,'2023-11-22 11:12:08','2023-12-09 04:00:09'),(2,'Font UVF',1,'Font-UVF',1,NULL,NULL,'2023-11-22 11:12:08','2024-02-27 07:46:08'),(3,'Font SFU',1,'Font-SFU',1,NULL,NULL,'2023-11-22 09:15:03','2024-03-01 07:10:28'),(4,'Font UVN',1,'Font-UVN',1,NULL,NULL,'2023-11-22 09:19:11','2024-03-01 07:10:23'),(5,'Font 1FTV',1,'Font-1FTV',1,NULL,NULL,'2023-11-22 09:33:11','2024-02-29 04:46:44'),(6,'Font bất động sản',2,'Font-bat-dong-san',1,NULL,NULL,'2023-11-25 04:15:28','2024-04-15 09:27:34'),(8,'Font mỹ phẩm - Sps',2,'font-my-pham---sps',0,NULL,NULL,'2023-12-09 04:42:20','2024-04-13 09:14:51');
/*!40000 ALTER TABLE `product_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_detail`
--

DROP TABLE IF EXISTS `product_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_detail` (
  `id` int NOT NULL,
  `productId` int NOT NULL,
  `unitId` int DEFAULT NULL,
  `capacityId` int DEFAULT NULL,
  `price` decimal(10,0) DEFAULT NULL,
  `createdById` int DEFAULT NULL,
  `updatedById` int DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`,`productId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_detail`
--

LOCK TABLES `product_detail` WRITE;
/*!40000 ALTER TABLE `product_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_font`
--

DROP TABLE IF EXISTS `product_font`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_font` (
  `id` int NOT NULL AUTO_INCREMENT,
  `productId` int NOT NULL,
  `font` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `isMain` tinyint(1) DEFAULT NULL,
  `createdById` int DEFAULT NULL,
  `updatedById` int DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_font`
--

LOCK TABLES `product_font` WRITE;
/*!40000 ALTER TABLE `product_font` DISABLE KEYS */;
INSERT INTO `product_font` VALUES (1,16,'1FTV-Happy-Christmas-1.otf',0,NULL,NULL,'2024-03-19 09:09:35','2024-03-19 09:09:35'),(2,16,'2FTV-Christmas-2.otf',0,NULL,NULL,'2024-03-19 09:18:25','2024-03-19 09:18:25'),(10,44,'C:\\fakepath\\1FTV-Happy-Christmas-1.otf',1,NULL,NULL,'2024-03-19 03:54:25','2024-03-19 03:54:25'),(11,45,'http://localhost:3001/upfonts/file-1710839669073',1,NULL,NULL,'2024-03-19 09:14:50','2024-03-19 09:14:50'),(12,46,'http://localhost:3001/upfonts/file-1710909227478.otf',1,NULL,NULL,'2024-03-20 04:34:04','2024-03-20 04:34:04'),(14,47,'http://localhost:3001/upfonts/file-1710909754046.txt',1,NULL,NULL,'2024-03-20 08:30:07','2024-03-20 08:30:07'),(16,14,'http://localhost:3001/upfonts/file-1710923893009.zip',1,NULL,NULL,'2024-03-20 08:38:15','2024-03-20 08:38:15'),(17,48,'4FTV-Happy-Christmas-4.otf',1,NULL,NULL,'2024-03-20 11:31:20','2024-03-20 11:31:20'),(18,49,'http://localhost:3001/upfonts/file-1711011231797.otf',1,NULL,NULL,'2024-03-21 08:54:14','2024-03-21 08:54:14'),(19,43,'4FTV-Happy-5.otf',1,NULL,NULL,'2024-03-22 03:07:31','2024-03-22 03:07:31'),(21,51,'http://localhost:3001/upfonts/file-1711620760518.otf',1,NULL,NULL,'2024-03-28 10:12:55','2024-03-28 10:12:55'),(22,52,'4FTV-Happy-Christmas-4.otf',1,NULL,NULL,'2024-03-29 08:34:33','2024-03-29 08:34:33'),(23,50,'http://localhost:3001/upfonts/file-1711704768203.otf',1,NULL,NULL,'2024-03-29 09:32:51','2024-03-29 09:32:51'),(24,24,'http://localhost:3001/upfonts/file-1711767885070.otf',1,NULL,NULL,'2024-03-30 03:04:48','2024-03-30 03:04:48'),(26,53,'http://localhost:3001/upfonts/file-1711773901832.otf',1,NULL,NULL,'2024-03-30 04:45:11','2024-03-30 04:45:11'),(27,54,'http://localhost:3001/upfonts/file-1711957425282.otf',1,NULL,NULL,'2024-04-01 07:43:53','2024-04-01 07:43:53'),(29,56,'4FTV-Happy-Christmas-4.otf',1,NULL,NULL,'2024-04-03 17:47:44','2024-04-03 17:47:44'),(30,42,'3FTV-Happy-3.otf',1,NULL,NULL,'2024-04-03 17:49:11','2024-04-03 17:49:11'),(34,11,'http://localhost:3001/upfonts/file-1712167608690.otf',1,NULL,NULL,'2024-04-03 18:08:30','2024-04-03 18:08:30'),(35,57,'http://localhost:3001/upfonts/file-1712167940929.otf',1,NULL,NULL,'2024-04-03 18:12:32','2024-04-03 18:12:32'),(37,58,'http://localhost:3001/upfonts/file-1712167988878.otf',1,NULL,NULL,'2024-04-03 18:13:20','2024-04-03 18:13:20'),(38,59,'http://localhost:3001/upfonts/file-1712801683103.otf',1,NULL,NULL,'2024-04-11 02:14:46','2024-04-11 02:14:46'),(39,60,'http://localhost:3001/upfonts/file-1712996129642.otf',1,NULL,NULL,'2024-04-13 08:16:10','2024-04-13 08:16:10');
/*!40000 ALTER TABLE `product_font` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_image`
--

DROP TABLE IF EXISTS `product_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_image` (
  `id` int NOT NULL AUTO_INCREMENT,
  `productId` int NOT NULL,
  `image` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `isMain` tinyint(1) DEFAULT NULL,
  `createdById` int DEFAULT NULL,
  `updatedById` int DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=251 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_image`
--

LOCK TABLES `product_image` WRITE;
/*!40000 ALTER TABLE `product_image` DISABLE KEYS */;
INSERT INTO `product_image` VALUES (74,16,'http://localhost:3001/images/file-1702699321798',1,NULL,NULL,'2023-12-16 04:05:39','2023-12-16 04:05:39'),(75,16,'http://localhost:3001/images/file-1702699406351',0,NULL,NULL,'2023-12-16 04:05:39','2023-12-16 04:05:39'),(76,16,'http://localhost:3001/images/file-1702699434492',0,NULL,NULL,'2023-12-16 04:05:39','2023-12-16 04:05:39'),(77,16,'http://localhost:3001/images/file-1702699466961',0,NULL,NULL,'2023-12-16 04:05:39','2023-12-16 04:05:39'),(78,16,'http://localhost:3001/images/file-1702699483721',0,NULL,NULL,'2023-12-16 04:05:39','2023-12-16 04:05:39'),(79,16,'http://localhost:3001/images/file-1702699510824',0,NULL,NULL,'2023-12-16 04:05:39','2023-12-16 04:05:39'),(80,16,'http://localhost:3001/images/file-1702699537575',0,NULL,NULL,'2023-12-16 04:05:39','2023-12-16 04:05:39'),(111,22,'http://localhost:3001/images/file-1702700357902',1,NULL,NULL,'2023-12-20 02:17:23','2023-12-20 02:17:23'),(112,22,'http://localhost:3001/images/file-1702700376817',0,NULL,NULL,'2023-12-20 02:17:23','2023-12-20 02:17:23'),(141,20,'http://localhost:3001/images/file-1702700245610.webp',1,NULL,NULL,'2023-12-27 09:36:57','2023-12-27 09:36:57'),(142,20,'http://localhost:3001/images/file-1702700225918',0,NULL,NULL,'2023-12-27 09:36:57','2023-12-27 09:36:57'),(152,25,'http://localhost:3001/images/file-1709542353861',1,NULL,NULL,'2024-03-04 08:52:42','2024-03-04 08:52:42'),(166,29,'http://localhost:3001/images/file-1709610636041',1,NULL,NULL,'2024-03-05 03:50:39','2024-03-05 03:50:39'),(172,15,'https://fonttiengviet.com/wp-content/uploads/2023/11/Font-Viet-hoa-1FTV-Nakana.jpg',1,NULL,NULL,'2024-03-05 04:56:08','2024-03-05 04:56:08'),(173,30,'http://localhost:3001/images/file-1709693764193',1,NULL,NULL,'2024-03-06 03:02:30','2024-03-06 03:02:30'),(179,31,'http://localhost:3001/images/file-1709718305997',1,NULL,NULL,'2024-03-06 09:45:09','2024-03-06 09:45:09'),(184,32,'http://localhost:3001/images/file-1709782874459',1,NULL,NULL,'2024-03-07 03:41:15','2024-03-07 03:41:15'),(188,33,'http://localhost:3001/images/file-1709802675439',1,NULL,NULL,'2024-03-07 09:11:23','2024-03-07 09:11:23'),(197,19,'http://localhost:3001/images/file-1702700075972',1,NULL,NULL,'2024-03-08 09:19:31','2024-03-08 09:19:31'),(198,21,'http://localhost:3001/images/file-1709803810055',1,NULL,NULL,'2024-03-08 09:19:41','2024-03-08 09:19:41'),(199,21,'http://localhost:3001/images/file-1702025174610',0,NULL,NULL,'2024-03-08 09:19:41','2024-03-08 09:19:41'),(200,21,'http://localhost:3001/images/file-1702025237182',0,NULL,NULL,'2024-03-08 09:19:41','2024-03-08 09:19:41'),(202,34,'http://localhost:3001/images/file-1709868527218',1,NULL,NULL,'2024-03-08 09:39:56','2024-03-08 09:39:56'),(203,26,'http://localhost:3001/images/file-1709545986744',1,NULL,NULL,'2024-03-08 09:43:01','2024-03-08 09:43:01'),(204,28,'http://localhost:3001/images/file-1709606827611',1,NULL,NULL,'2024-03-08 09:43:12','2024-03-08 09:43:12'),(205,23,'http://localhost:3001/images/file-1703126996299',1,NULL,NULL,'2024-03-09 05:01:51','2024-03-09 05:01:51'),(208,38,'http://localhost:3001/images/file-1710490508520',1,NULL,NULL,'2024-03-15 08:15:09','2024-03-15 08:15:09'),(209,39,'http://localhost:3001/images/file-1710490646164.jpg',1,NULL,NULL,'2024-03-15 08:17:26','2024-03-15 08:17:26'),(210,40,'https://fonttiengviet.com/wp-content/uploads/2023/08/beau-sans.jpg',1,NULL,NULL,'2024-03-19 02:33:51','2024-03-19 02:33:51'),(211,41,'https://fonttiengviet.com/wp-content/uploads/2023/08/beau-sans.jpg',1,NULL,NULL,'2024-03-19 02:37:32','2024-03-19 02:37:32'),(219,44,'http://localhost:3001/images/file-1710820455403',1,NULL,NULL,'2024-03-19 03:54:25','2024-03-19 03:54:25'),(220,45,'http://localhost:3001/images/file-1710839673953.jpg',1,NULL,NULL,'2024-03-19 09:14:50','2024-03-19 09:14:50'),(221,46,'http://localhost:3001/images/file-1710909239474',1,NULL,NULL,'2024-03-20 04:34:04','2024-03-20 04:34:04'),(223,47,'http://localhost:3001/images/file-1710909760915.jpg',1,NULL,NULL,'2024-03-20 08:30:07','2024-03-20 08:30:07'),(226,14,'https://fonttiengviet.com/wp-content/uploads/2023/11/Font-Viet-hoa-1FTV-Nakana.jpg',1,NULL,NULL,'2024-03-20 08:38:15','2024-03-20 08:38:15'),(227,48,'https://fonttiengviet.com/wp-content/uploads/2023/08/beau-sans.jpg',1,NULL,NULL,'2024-03-20 11:31:20','2024-03-20 11:31:20'),(229,43,'http://localhost:3001/images/file-1711076846145',1,NULL,NULL,'2024-03-22 03:07:30','2024-03-22 03:07:30'),(234,24,'http://localhost:3001/images/file-1708654564325.jpg',1,NULL,NULL,'2024-03-30 03:04:48','2024-03-30 03:04:48'),(237,53,'http://localhost:3001/images/file-1711773909363.jpg',1,NULL,NULL,'2024-03-30 04:45:11','2024-03-30 04:45:11'),(238,54,'http://localhost:3001/images/file-1711957432012.jpg',1,NULL,NULL,'2024-04-01 07:43:53','2024-04-01 07:43:53'),(240,56,'https://fonttiengviet.com/wp-content/uploads/2023/08/beau-sans.jpg',1,NULL,NULL,'2024-04-03 17:47:44','2024-04-03 17:47:44'),(241,42,NULL,1,NULL,NULL,'2024-04-03 17:49:11','2024-04-03 17:49:11'),(245,11,'http://localhost:3001/images/file-1712167629192',1,NULL,NULL,'2024-04-03 18:08:30','2024-04-03 18:08:30'),(246,57,'http://localhost:3001/images/file-1712167951384',1,NULL,NULL,'2024-04-03 18:12:32','2024-04-03 18:12:32'),(248,58,'http://localhost:3001/images/file-1712167993059',1,NULL,NULL,'2024-04-03 18:13:20','2024-04-03 18:13:20'),(249,59,'http://localhost:3001/images/file-1712801661387',1,NULL,NULL,'2024-04-11 02:14:46','2024-04-11 02:14:46'),(250,60,'http://localhost:3001/images/file-1712996167476',1,NULL,NULL,'2024-04-13 08:16:10','2024-04-13 08:16:10');
/*!40000 ALTER TABLE `product_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_inventory`
--

DROP TABLE IF EXISTS `product_inventory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_inventory` (
  `id` int NOT NULL AUTO_INCREMENT,
  `productId` int NOT NULL,
  `subProductId` int DEFAULT NULL,
  `quantity` int unsigned DEFAULT NULL,
  `cityCode` int DEFAULT NULL,
  `createdById` int DEFAULT NULL,
  `updatedById` int DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`,`productId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_inventory`
--

LOCK TABLES `product_inventory` WRITE;
/*!40000 ALTER TABLE `product_inventory` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_inventory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_save`
--

DROP TABLE IF EXISTS `product_save`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_save` (
  `id` int NOT NULL AUTO_INCREMENT,
  `userId` int NOT NULL,
  `productId` int NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `createdById` int DEFAULT NULL,
  `updatedById` int DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_save`
--

LOCK TABLES `product_save` WRITE;
/*!40000 ALTER TABLE `product_save` DISABLE KEYS */;
INSERT INTO `product_save` VALUES (2,8,19,1,NULL,NULL,'2024-03-20 18:29:44','2024-04-04 05:09:47'),(7,10,47,1,NULL,NULL,'2024-03-21 07:20:24','2024-03-21 09:00:26'),(8,15,46,1,NULL,NULL,'2024-03-29 03:49:53','2024-04-05 07:52:56'),(14,7,44,1,NULL,NULL,'2024-03-31 19:26:02','2024-03-31 19:26:02'),(15,7,44,1,NULL,NULL,'2024-04-05 02:10:17','2024-04-05 02:10:17'),(16,7,46,1,NULL,NULL,'2024-04-05 02:10:23','2024-04-05 02:10:23'),(17,7,46,1,NULL,NULL,'2024-04-05 02:10:39','2024-04-05 02:10:39'),(21,7,47,1,NULL,NULL,'2024-04-05 02:12:58','2024-04-05 02:12:58'),(24,7,24,1,NULL,NULL,'2024-04-05 02:15:00','2024-04-05 02:15:00'),(25,7,38,1,NULL,NULL,'2024-04-05 02:15:08','2024-04-05 02:15:08'),(27,7,44,1,NULL,NULL,'2024-04-05 04:47:26','2024-04-05 04:47:26'),(29,8,44,1,NULL,NULL,'2024-04-05 07:34:01','2024-04-05 07:34:01'),(32,15,45,1,NULL,NULL,'2024-04-11 02:15:05','2024-04-11 02:15:05');
/*!40000 ALTER TABLE `product_save` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `userCode` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `phoneCode` int unsigned DEFAULT NULL,
  `phoneNumber` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `orderStatus` tinyint(1) DEFAULT '1',
  `status` tinyint(1) DEFAULT '1',
  `level` int DEFAULT NULL,
  `role` int DEFAULT NULL,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (7,'SS000001','vanlinh6@gmail.com','vanlinh','$2b$10$zcJDST5Y9Ox20KEynoQ6yu5EpX0HjwH3HOYlmiPt7f.2WcEuMQOHW',84,'787944710',2,1,1,1,'2024-04-12 08:23:59','2023-11-20 05:14:36'),(8,'SS000002','admin@gmail.com','admin','$2b$10$Wi6sz2yLCMZL41e1ntjUIer3nlbs0gMzi1rJtEEprTt2sjssQ.Wq2',84,'787944235',2,1,2,1,'2024-04-04 04:12:41','2023-11-20 05:15:46'),(9,'SS000003','ninhle.130301@gmail.com','Văn Linh','$2b$10$beseBBG2P1Ilqb5a71TF/uxOLBrfKxy5SJDJhCy44RZfqwlBdy7Zq',84,'787944235',2,1,0,3,'2024-03-29 07:56:05','2023-11-20 07:08:22'),(10,'SS000004','linhvanle130@gmail.com','anhlinh','$2b$10$Lj6SbSQMOTgD9msTt1bVY.DKa/2GlJWj9SaVLnf.WkCOYqVAdqZiS',84,'787944235',2,1,1,3,'2024-04-12 08:24:08','2023-11-20 08:07:31'),(11,'SS000005','chaulong10@gmail.com','chau long','$2b$10$m2WaPXMI4kfvyVSmKyj5QurMwnGmx58/WqUaJXwu4ZgKTHoN2y5VO',84,'787944285',1,1,0,2,'2024-04-01 10:25:52','2023-11-21 04:24:29'),(12,'SS000006','dieuhien1211@gmail.com','dieu hien','$2b$10$NUAVP0gPM0GW//Z959hegeoqZ2kenrdsonw0i.0NvHQ2B/CGWTfiu',84,'787944285',1,1,2,3,'2023-12-15 08:04:46','2023-11-21 04:26:25'),(13,'SS000007','lethidieuhien121102@gmail.com','le thi dieu hien','$2b$10$aGePIFdBxMwIocq1rUZMFug9NP7dVM2w62cgGFFc6FhvLtBvm7Wc2',84,'787944285',1,1,1,3,'2024-03-09 03:06:01','2023-11-21 16:47:31'),(14,'SS000008','lin11@gmail.com','linhdasd','$2b$10$pK4w1auPbCTlbPy8Zj5GUe9ssdW8wvcICMmzoJJg33XdMMVL9LgTq',84,'787944285',1,1,0,3,'2023-12-15 08:25:31','2023-11-25 08:25:17'),(15,'SS000009','lenguyen@gmail.com','lên nguyễn','$2b$10$itDRrJTI5nfFvp4BqbYvJeb7JBg96Uf3jAL777vIPm1FoVtkuD/jm',84,'787944710',1,1,2,3,'2024-04-11 02:15:35','2023-11-29 09:57:49'),(16,'SS000010','kenbui02@gmail.com','Ken bùi','$2b$10$O5vLm3PD8OEoBfp.kmAEeel7rSNnY1iRNGzjIN6eLQ3ZQYYNQieEi',NULL,NULL,1,1,0,3,'2024-03-07 03:26:07','2023-11-29 09:58:07'),(17,'SS000011','ngoctu19@gmail.com','Ngọc tú','$2b$10$Xvi1fkDVqqcmXIAHg1MaXedJ9au3j8nZ1RBrFQimXAJArOAru1wgy',84,'787944235',1,1,0,3,'2024-03-07 07:56:52','2023-11-29 09:58:31'),(18,'SS000012','taiphe@gmail.com','Mai Tài Phế','$2b$10$76QXao9iZpJHUutWYXnFJedl4HfLn4Z6DBIRg/pEaRBV.f..xE/ba',NULL,NULL,1,1,1,3,'2023-12-28 09:23:47','2023-12-15 10:12:56'),(19,'SS000013','mck@gmail.com','MCK','$2b$10$V6J7weOBxbzXZQ8Q2amBsej.j8EbT28pG/WPvb1zVSF9xJ4w7cBpu',NULL,NULL,1,1,0,3,'2024-01-31 02:45:12','2024-01-31 02:45:12'),(20,'SS000014','oboto@gmail.com','oboto','$2b$10$r7TkTicl5dCRO0EpRRef1ed6BVJHSiVBPk2jvMIeZx0mEZWYs6pS2',NULL,NULL,1,1,0,3,'2024-02-26 07:40:04','2024-01-31 02:46:38'),(21,'SS000015','low@gmail.com','low','$2b$10$bPuzjzjS/doZZyYd6b8Db.H5bZI5o8Txa.D8SlWEFFVcjwsh97D8W',NULL,NULL,1,1,0,3,'2024-01-31 03:14:11','2024-01-31 03:14:11'),(22,'SS000016','anhphan@gmail.com','anh phan','$2b$10$tBwNVEMr.osv8Nhpwa9uDeDKiUfsF.jLzhlJv./WIribp/eRvT8g.',NULL,NULL,1,1,0,3,'2024-03-01 08:46:41','2024-03-01 08:46:41'),(23,'SS000017','aphan@gmail.com','a phan','$2b$10$YgQ3Tzh5/8FqLHEe4yW6CuDcf4aX1Sh2QnLxX.LhmSNK/Agk6PtGK',NULL,NULL,1,1,3,3,'2024-03-07 05:05:11','2024-03-07 05:05:11'),(24,'SS000018','tonlongchau@gmail.com','Long chauu','$2b$10$Z0ltPW6ZqXMCr93IV7uKBuSUBogon1aNMVzEvYKBQABR4tlwv7jF.',NULL,NULL,1,1,0,3,'2024-03-21 04:46:42','2024-03-21 04:46:42'),(25,'SS000019','tonlongchau85d@gmail.com','Long chauusd','$2b$10$NR0T8uerIDB7O96JW3X5e.27Nk0AFtrHJvIIydy1mYuW3Zc3hQRRC',NULL,NULL,1,1,0,3,'2024-04-10 09:15:26','2024-04-10 09:15:26'),(26,'SS000020','lennguyen0105@gmail.com','lên','$2b$10$psPYUQ9FkxBNi/mBii/VJebxI2AMn/gFUjY.klUR.JX8O1dlJts7m',NULL,NULL,1,1,0,3,'2024-04-11 02:13:11','2024-04-11 02:13:11');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_address`
--

DROP TABLE IF EXISTS `user_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_address` (
  `id` int NOT NULL AUTO_INCREMENT,
  `userId` int DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `cityCode` int DEFAULT NULL,
  `districtCode` int DEFAULT NULL,
  `wardCode` int DEFAULT NULL,
  `telephone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `default` tinyint(1) DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_address`
--

LOCK TABLES `user_address` WRITE;
/*!40000 ALTER TABLE `user_address` DISABLE KEYS */;
INSERT INTO `user_address` VALUES (7,9,'88 Mai Đăng Chơn, Cẩm lệ, Tp Đà Nẵng',47,25,12,'0787947894',1,'2023-11-21 02:30:07','2024-04-10 08:43:06'),(8,10,'Đà Nẵng',456,212,23,'0787945995',1,'2023-11-21 03:11:59','2023-11-21 03:11:59'),(9,10,'Đà Nẵng',456,212,23,'0787945995',0,'2023-12-15 07:41:44','2023-12-15 07:41:44'),(10,7,'Đà Nẵng',456,212,23,'0787945995',1,'2024-04-10 08:49:50','2024-04-10 08:49:50');
/*!40000 ALTER TABLE `user_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_information`
--

DROP TABLE IF EXISTS `user_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_information` (
  `userId` int NOT NULL,
  `avatar` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `cityCode` int DEFAULT NULL,
  `districtCode` int DEFAULT NULL,
  `wardCode` int DEFAULT NULL,
  `lastLogin` datetime DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_information`
--

LOCK TABLES `user_information` WRITE;
/*!40000 ALTER TABLE `user_information` DISABLE KEYS */;
INSERT INTO `user_information` VALUES (7,'https://i.pinimg.com/originals/22/a6/6e/22a66ef9fc1067375df29ee4fd04954b.jpg','Bình an, lộc vĩnh, phú lộc, thừa thiên Huế',47,25,12,'2024-04-16 08:11:14','2023-11-20 05:14:36','2024-04-16 08:11:14'),(8,'https://images.unsplash.com/photo-1700427296131-0cc4c4610fc6?q=80&w=1776&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D','08 Trần Lựu, Cẩm lệ, Tp Đà Nẵng',47,25,10382,'2024-03-30 04:41:38','2023-11-20 05:15:47','2024-03-30 04:41:38'),(9,'https://images.unsplash.com/photo-1599566150163-29194dcaad36?q=80&w=1974&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D','88 Mai Đăng Chơn, Cẩm lệ, Tp Đà Nẵng',47,25,12,'2024-04-02 08:15:00','2023-11-20 07:08:22','2024-04-02 08:15:00'),(10,'https://images.unsplash.com/photo-1599566150163-29194dcaad36?q=80&w=1974&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D','88 Mai Đăng Chơn, Cẩm lệ, Tp Đà Nẵng',47,25,12,'2024-03-07 05:03:22','2023-11-20 08:07:31','2024-03-07 05:03:22'),(11,'https://images.unsplash.com/photo-1438761681033-6461ffad8d80?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D','21 Võ nguyên Giap, Hải Châu, Tp Đà Nẵng',47,25,12,'2024-04-10 04:26:44','2023-11-21 04:24:29','2024-04-10 04:26:44'),(12,'https://images.unsplash.com/photo-1438761681033-6461ffad8d80?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D','Tam Kỳ, Diện Bàn, Tỉnh Quảng Nam',47,25,12,'2023-12-15 08:03:12','2023-11-21 04:26:25','2023-12-15 08:04:46'),(13,'https://images.unsplash.com/photo-1438761681033-6461ffad8d80?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D','Tam Kỳ, Diện Bàn, Tỉnh Quảng Nam',47,25,12,'2023-12-15 08:21:22','2023-11-21 16:47:31','2023-12-15 08:21:53'),(14,'https://images.unsplash.com/photo-1633332755192-727a05c4013d?q=80&w=2080&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D','Bình an, lộc vĩnh, phú lộc, thừa thiên Huế',47,25,12,'2023-12-15 08:24:27','2023-11-25 08:25:17','2023-12-15 08:25:31'),(15,'https://images.unsplash.com/photo-1633332755192-727a05c4013d?q=80&w=2080&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D','Bình an, lộc vĩnh, phú lộc, thừa thiên Huế',47,25,12,'2024-04-11 03:25:38','2023-11-29 09:57:49','2024-04-11 03:25:38'),(16,NULL,NULL,NULL,NULL,NULL,'2024-04-02 08:15:36','2023-11-29 09:58:07','2024-04-02 08:15:36'),(17,'https://images.unsplash.com/photo-1494790108377-be9c29b29330?q=80&w=1974&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D','88 Mai Đăng Chơn, Cẩm lệ, Tp Đà Nẵng',47,25,12,'2023-12-15 07:45:52','2023-11-29 09:58:31','2023-12-15 07:46:05'),(18,NULL,NULL,NULL,NULL,NULL,'2023-12-28 09:03:51','2023-12-15 10:12:56','2023-12-28 09:03:51'),(19,NULL,NULL,NULL,NULL,NULL,NULL,'2024-01-31 02:45:13','2024-01-31 02:45:13'),(20,NULL,NULL,NULL,NULL,NULL,NULL,'2024-01-31 02:46:38','2024-01-31 02:46:38'),(21,NULL,NULL,NULL,NULL,NULL,NULL,'2024-01-31 03:14:11','2024-01-31 03:14:11'),(22,NULL,NULL,NULL,NULL,NULL,'2024-03-01 08:46:53','2024-03-01 08:46:41','2024-03-01 08:46:53'),(23,'https://images.unsplash.com/photo-1527980965255-d3b416303d12?q=80&w=2080&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D','Bình an, lộc vĩnh, phú lộc, thừa thiên Huế',47,25,12,'2024-04-10 08:56:06','2024-03-07 05:05:11','2024-04-10 08:57:10');
/*!40000 ALTER TABLE `user_information` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_referral`
--

DROP TABLE IF EXISTS `user_referral`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_referral` (
  `registerId` bigint NOT NULL,
  `registerCode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `referrerId` bigint DEFAULT NULL,
  `referrerCode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `genealogyPath` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`registerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_referral`
--

LOCK TABLES `user_referral` WRITE;
/*!40000 ALTER TABLE `user_referral` DISABLE KEYS */;
INSERT INTO `user_referral` VALUES (12,'SS000006',7,'SS000001','7.12','2023-11-21 04:26:25'),(13,'SS000007',7,'SS000001','7.13','2023-11-21 16:47:31'),(14,'SS000008',7,'SS000001','7.14','2023-11-25 08:25:18'),(15,'SS000009',7,'SS000001','7.15','2023-11-29 09:57:49'),(16,'SS000010',7,'SS000001','7.16','2023-11-29 09:58:07'),(17,'SS000011',7,'SS000001','7.17','2023-11-29 09:58:31'),(18,'SS000012',9,'SS000003','9.18','2023-12-15 10:12:56'),(19,'SS000013',16,'SS000010','7.16.19','2024-01-31 02:45:13'),(20,'SS000014',16,'SS000010','7.16.20','2024-01-31 02:46:38'),(21,'SS000015',17,'SS000011','7.17.21','2024-01-31 03:14:11'),(22,'SS000016',17,'SS000011','7.17.22','2024-03-01 08:46:41'),(23,'SS000017',17,'SS000011','7.17.23','2024-03-07 05:05:11');
/*!40000 ALTER TABLE `user_referral` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-04-22 11:51:35
