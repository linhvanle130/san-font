import axiosClient from "./axiosClient";
import axiosClientAuth from "./axiosClientAuth";

const Comments = {
  createComment: (body) =>
    axiosClientAuth.post(`/api/comment/create-comment`, body),

  getComment: (params) => axiosClient.get("api/comment", { params }),

  deleteComment: (id) =>
    axiosClient.delete(`/api/comment/delete-comment/${id}`),
};

export default Comments;
