import axiosClientAuth from "./axiosClientAuth";
import axiosClient from "./axiosClient";

const orderApis = {
  createOrder: (payload) =>
    axiosClientAuth.post("/api/order/new-order", payload),

  getOrderPackage: (params) =>
    axiosClient.get("/api/order-package", { params }),

  getListOrder: (params) =>
    axiosClient.get("/api/order/list-order", { params }),

  getOrderCode: (id, params) =>
    axiosClient.get(`api/order/detail-order/${id}`, { params }),
};

export default orderApis;
