import axiosClient from "./axiosClient";

const blogs = {
  getAllBogs: (params) => axiosClient.get("/api/blog", { params }),
};

export default blogs;
