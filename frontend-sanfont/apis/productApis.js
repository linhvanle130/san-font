import axiosClient from "./axiosClient";
import { getAccessToken } from "../utils/getAccessToken";
import axiosClientAuth from "./axiosClientAuth";

const productApis = {
  createProduct: (body) =>
    axiosClientAuth.post(`/api/product/new-product`, body, {
      headers: {
        Authorization: `Bearer ${getAccessToken()}`,
      },
    }),

  productSave: (body) =>
    axiosClientAuth.post(`/api/product/create-save-product`, body, {
      headers: {
        Authorization: `Bearer ${getAccessToken()}`,
      },
    }),

  upFont: (formData) =>
    axiosClientAuth.postForm("/api/font/create-font-url", formData),

  upImage: (formData) =>
    axiosClientAuth.postForm("/api/common/create-presigned-url", formData),

  getAllProducts: (params) => axiosClient.get("/api/product", { params }),

  getProductDetail: (id) =>
    axiosClient.get(`/api/product/detail-product/${id}`),

  productUnSave: (id) =>
    axiosClient.delete(`/api/product/delete-save-product/${id}`, {
      headers: {
        Authorization: `Bearer ${getAccessToken()}`,
      },
    }),

  numberDownload: (body) =>
    axiosClientAuth.post(`/api/download/new-download`, body),
};

export default productApis;
