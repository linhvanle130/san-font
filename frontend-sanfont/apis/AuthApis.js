import axiosClientAuth from "./axiosClientAuth";

const AuthApis = {
  login: ({ email, password }) =>
    axiosClientAuth.post("/api/user/auth/sign-in", {
      email,
      password,
    }),

  signUpUser: ({ email, password, username }) =>
    axiosClientAuth.post("/api/user/auth/register-user-email", {
      username,
      email,
      password,
    }),

  sendOTP: (payload) => axiosClientAuth.post("/api/otp/send", payload),

  resetPassword: ({ email, otpCode, password, rePassword }) =>
    axiosClientAuth.post("/api/user/auth/forgot-password/reset", {
      email,
      otpCode,
      password,
      rePassword,
    }),

  changePass: (payload) =>
    axiosClientAuth.post("/api/user/auth/change-password", payload),
};

export default AuthApis;
