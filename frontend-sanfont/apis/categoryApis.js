import axiosClient from "./axiosClient";

const categoryApis = {
  getAllCategory: (params) =>
    axiosClient.get("/api/product/get-list-category", { params }),
};
export default categoryApis;
