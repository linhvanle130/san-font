import HD from "/public/images/HD.jpg";
import Logo from "/public/images/Logo.png";
import AVT from "/public/images/AVT.png";
import sanfont from "/public/images/sanfont.png";

import Advertisement from "/public/images/Advertisement.jpg";

const imgGlobal = {
  Logo,
  HD,
  AVT,
  Advertisement,
  sanfont,
};

export default imgGlobal;
