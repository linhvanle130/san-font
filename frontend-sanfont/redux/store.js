import { combineReducers, configureStore } from "@reduxjs/toolkit";
import logger from "redux-logger";
import storage from "redux-persist/lib/storage";
import { persistReducer } from "redux-persist";
import thunk from "redux-thunk";
import cartSlice from "./slices/cartSlice";
import accountSlice from "./slices/accountSlice";

const reducer = combineReducers({
  cart: cartSlice,
  account: accountSlice,
});

const loggerMiddleware = (store) => (next) => (action) => {
  next(action);
};

const persistConfig = {
  key: "root",
  storage,
};

const persistedReducer = persistReducer(persistConfig, reducer);

export const store = configureStore({
  reducer: persistedReducer,
  devTools: process.env.NODE_ENV !== "production",
  middleware: [thunk],
  middleware: (gDM) => gDM().concat(logger, loggerMiddleware),
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});
