import { createSlice } from "@reduxjs/toolkit";

const items = [];
const cartInfomation = {};

const initialState = {
  value: items,
  information: cartInfomation,
};

const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    addToCart: (state, action) => {
      const newOrder = action.payload;
      // console.log(newOrder);

      const duplicate = state.value?.filter(
        (e) => e.name === newOrder?.name && e.price === newOrder?.price
      );

      if (duplicate?.length !== 1) {
        state.value?.splice(duplicate, 1, {
          ...newOrder,
        });
      } else {
        state.value?.push({
          ...newOrder,
          id:
            state.value?.length > 0
              ? state.value[state.value.length - 1].id + 1
              : 1,
        });
      }
      localStorage.setItem("cart", JSON.stringify(state.value));
    },
    deleteAll: (state, action) => {
      state.value = [];
    },
  },
});

export const { addToCart, removeItem, deleteAll } = cartSlice.actions;

export default cartSlice.reducer;
