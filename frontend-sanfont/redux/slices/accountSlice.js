import { createSlice } from "@reduxjs/toolkit";
const isClient = typeof window !== "undefined";

const accountSlice = createSlice({
  name: "account",
  initialState: {
    token: isClient ? localStorage.getItem("token") || "" : "",
    info: isClient ? localStorage.getItem("info") || null : "",
  },
  reducers: {
    setInfoLogin: (state, action) => {
      const token = action.payload;
      state.token = token;
    },
    setInfoAccount(state, action) {
      const info = action.payload;
      state.info = info;
    },
    logout: (state) => {
      state.info = "";
      state.token = null;
      localStorage.removeItem("token");
      localStorage.removeItem("info");
    },
    setToken: (state, action) => {
      const token = action.payload;
      state.token = token;
    },
    setProfileAuth: (state, action) => {
      const info = action.payload;
      state.info = info;
    },
    clearInfo: (state) => {
      state.info = null;
      state.token = "";
    },
  },
});

export const {
  setInfoLogin,
  setInfoAccount,
  logout,
  setProfileAuth,
  setToken,
  clearInfo,
} = accountSlice.actions;

export default accountSlice.reducer;
