/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
    "./pages/**/*.{ts,tsx}",
    "./public/**/*.html",
    "./node_modules/flowbite/**/*.js",
  ],
  theme: {
    extend: {
      boxShadow: {
        xxl: "1px 4px 4px rgba(0, 0, 0, 0.4);",
      },
      colors: {
        // oranges: '#ff5d38',
        // oranges: '#ff0000',
        oranges: "#ff5f3a",

        blues: "#0d6bf6",
        blueHover: "#0d0df6",
        rings: "#add7f9",
        whites: "#ffffff",
        blacks: "#000000",
        reds: "#ff0000",
      },
      listStyleImage: {
        checkmark: 'url("/img/checkmark.png")',
      },
    },
  },
  plugins: [require("daisyui"), require("flowbite/plugin")],
};
