/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: [
      "localhost",
      "images.unsplash.com",
      "fonttiengviet.com",
      "imgGlobal.FTV1",
      "res.cloudinary.com",
    ],
  },
};

module.exports = nextConfig;
