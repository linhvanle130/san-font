import React from "react";
import Image from "next/image";
import Link from "next/link";

import imgGlobal from "../../public/images";
const Advertisement = () => {
  return (
    <div className=" h-[150px] md:h-[250px] lg:h-[380px] bg-whites mx-auto mt-8 md:mt-12 lg:mt-16">
      <Link href="https://www.facebook.com/ledmatrixdanang/">
        <Image
          src={imgGlobal.Advertisement}
          className="w-full h-full object-bottom"
          alt="Advertisement"
        />
      </Link>
    </div>
  );
};

export default Advertisement;
