import React, { useRef } from "react";

const CheckButton = (props) => {
  const inputRef = useRef(null);
  const onChange = () => {
    if (props.onChange) {
      props.onChange(inputRef.current);
      console.log(inputRef.current.checked);
    }
  };
  return (
    <label
      htmlFor={props.test}
      className={`${
        inputRef.current?.checked === true
          ? " bg-oranges  text-white"
          : " bg-white text-blacks"
      } text-center flex justify-center items-center cursor-pointer px-4 py-2.5 transition rounded-[30px] font-normal text-[16px]`}
    >
      <input
        id={props.test}
        type="checkbox"
        onChange={onChange}
        checked={props.checked}
        ref={inputRef}
        className=" hidden "
      />
      {props.test}
    </label>
  );
};

export default CheckButton;
