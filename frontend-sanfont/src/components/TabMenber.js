import React from "react";

const TabMenber = ({ tabs }) => {
  return (
    <>
      <div className="py-20 hidden md:block lg:block">
        <div className="mx-auto w-[60%] relative flex ">
          <p className="absolute top-[30%] w-full h-[10px] bg-[#dddddd] z-0"></p>

          <div className="w-full flex justify-between">
            <div className=" z-30">
              {tabs === 1 ? (
                <div className="bg-[#1774fd] text-white  text-xl font-normal w-14 h-14 rounded-[50%]  flex flex-col items-center justify-center">
                  1
                </div>
              ) : (
                <div className="bg-slate-400 text-white  text-xl font-normal w-14 h-14 rounded-[50%]  flex flex-col items-center justify-center">
                  1
                </div>
              )}

              <div>Đăng ký </div>
            </div>

            <div className=" z-30">
              <div>
                {tabs === 2 ? (
                  <div className="bg-[#1774fd] text-white  text-xl font-normal w-14 h-14 rounded-[50%]  flex flex-col items-center justify-center">
                    2
                  </div>
                ) : (
                  <div className="bg-slate-400 text-white  text-xl font-normal w-14 h-14 rounded-[50%]  flex flex-col items-center justify-center">
                    2
                  </div>
                )}
              </div>
              <div>Thanh toán</div>
            </div>

            <div className=" z-30">
              <div>
                {tabs === 3 ? (
                  <div className="bg-[#1774fd] text-white  text-xl font-normal w-14 h-14 rounded-[50%]  flex flex-col items-center justify-center">
                    3
                  </div>
                ) : (
                  <div className="bg-slate-400 text-white  text-xl font-normal w-14 h-14 rounded-[50%]  flex flex-col items-center justify-center">
                    3
                  </div>
                )}
                <div className="-ml-5">Thông báo</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default TabMenber;
