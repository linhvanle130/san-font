import React from "react";

const Title = ({ title, icon }) => {
  return (
    <ul className="font-normal text-sm">
      <li className="flex">{title}</li>
    </ul>
  );
};

export default Title;
