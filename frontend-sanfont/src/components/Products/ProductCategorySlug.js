import Image from "next/image";
import Link from "next/link";
import React from "react";
import moment from "moment";
import { toast } from "react-toastify";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { useState } from "react";
import { GrClose } from "react-icons/gr";
import { createPortal } from "react-dom";
import axios from "axios";
import { FiUser } from "react-icons/fi";
import { getAccessToken } from "../../../utils/getAccessToken";
import productApis from "../../../apis/productApis";
import { useRouter } from "next/router";

const ProductCategorySlug = ({
  CategorySlug,
  productsSaved,
  onReloadProductsSaved,
}) => {
  const router = useRouter();

  const [product, setProduct] = useState();

  const [active, setActive] = useState();
  const dispatch = useDispatch();
  const { token, info } = useSelector((state) => state.account);

  useEffect(() => {
    setProduct({
      id: CategorySlug?.id,
      name: CategorySlug?.name,
      price: CategorySlug?.price,
      author: CategorySlug?.author,
      content: CategorySlug?.content,
      slug: CategorySlug?.productSlug,
      outstanding: CategorySlug?.outstanding,
      subAuthor: CategorySlug?.subAuthor,
      description: CategorySlug?.description,
      user: CategorySlug?.user,
      userId: CategorySlug?.userId,
      createdAt: CategorySlug?.createdAt,
      productCategory: CategorySlug?.productCategory,
      productImage: CategorySlug?.productImage,
      productDetail: CategorySlug?.productDetail,
      productInventory: CategorySlug?.productInventory,
      productFont: CategorySlug?.productFont,
      downloadFont: CategorySlug?.downloadFont,
      productSlug: CategorySlug?.productSlug,
    });
  }, [CategorySlug]);

  // Save product

  const [productSaved, setProductSaved] = useState();
  useEffect(() => {
    const saved = productsSaved?.find(
      (saved) => saved.productId === CategorySlug.id
    );
    setProductSaved(saved);
  }, [productsSaved]);

  const handleSave = async () => {
    try {
      if (token) {
        if (!productSaved) {
          await productApis.productSave({
            userId: info?.id,
            productId: product.id,
          });
          toast.success("Font đã lưu vào tài khoản");
        } else {
          await productApis.productUnSave(`${productSaved?.id}`);

          toast.success("Font đã xóa khỏi tài khoản");
        }

        onReloadProductsSaved();
      } else {
        router.push("/Auth/login");
      }
    } catch (error) {
      console.error("Đã xảy ra lỗi:", error);
    }
  };
  // MumberDownload
  const handleMumberDownload = async () => {
    try {
      await productApis.numberDownload({
        productId: product.id,
        userId: info?.id,
        nameDownload: product?.name,
      });
    } catch (error) {
      console.error("Đã xảy ra lỗi:", error);
    }
  };

  // demo image
  const renderContent = (
    <div
      className={`${
        active ? "" : "hidden"
      } w-full h-full fixed top-0  z-[99] bg-black bg-opacity-30   `}
    >
      <div
        className={`box-check-lick lg:w-[60%] mt-[40%] md:mt-[40%]  lg:mt-[6%] px-8 gap-8 mx-auto  flex relative justify-center bg-white `}
      >
        <span
          className={`absolute cursor-pointer  hover:text-red-600 top-2 right-2`}
          onClick={() => setActive(false)}
        >
          <GrClose className=" text-xl" />
        </span>

        <div className=" w-[98%] py-5 ">
          {product?.productImage.slice(0, 2).map((item) => {
            return (
              <div key={item.id}>
                <Image
                  src={item.image}
                  width={500}
                  alt="image"
                  height={300}
                  className="w-full h-full mt-2 object-cover pb-4"
                />
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );

  return (
    <>
      <div className="bg-whites shadow-md ">
        <div className="relative">
          <div className=" group overflow-hidden">
            <div
              onClick={() => setActive(true)}
              className=" cursor-pointer  duration-500 hover:scale-[1.1] relative "
            >
              {product?.productImage && (
                <div className="w-full h-[185px] md:h-[220px] lg:h-[185px]">
                  <Image
                    priority
                    src={product?.productImage[0]?.image}
                    width={500}
                    height={300}
                    alt="image"
                    className=" w-full h-full object-cover "
                  ></Image>
                </div>
              )}
              <div className="w-full h-full absolute top-0 bg-black/70 cursor-pointer  duration-500  opacity-0 group-hover:opacity-80 flex items-center justify-center">
                <h2 className="text-whites  text-sm font-semibold  ">
                  XEM DEMO
                </h2>
              </div>
            </div>
          </div>
          <button
            onClick={handleSave}
            className=" absolute top-0 left-0 bg-reds py-[3px] px-[6px] text-whit text-whites  text-[12px] shadow-xxl rounded-br-lg  z-1"
          >
            <span className="font-bold ">
              {productSaved ? "Đã lưu" : "Lưu"}
            </span>
          </button>

          {product?.outstanding === 2 ? (
            <button className="btn-vip absolute bg-[#ffa800] py-[3px] px-[6px] text-whites text-[12px] shadow-xxl rounded-bl-lg  z-1">
              <Link href={"/font-chon-loc"} legacyBehavior>
                <a className="font-bold ">Font chọn lọc</a>
              </Link>
            </button>
          ) : null}

          {product?.outstanding === 1 && (
            <button className="btn-vip absolute bg-[#028623] py-[3px] px-[6px] text-whites text-[12px] shadow-xxl rounded-bl-lg  z-1">
              <Link href={"/font-vip"} legacyBehavior>
                <a className="font-bold ">VIP</a>
              </Link>
            </button>
          )}
        </div>

        <div className="pl-3">
          <Link
            href={`${product?.productCategory.categorySlug}/${product?.productSlug}`}
          >
            <h2 className="description  mt-3 font-semibold text-[16px] text-blacks">
              {product?.name}
            </h2>
          </Link>
          <div className=" py-2.5 font-normal text-[12px] leading-7 text-[#818181]">
            <p>
              <span className="font-bold ">Tác giả: </span>
              {product?.author}
            </p>
            <p>
              <span className="font-bold">Người đăng: </span>
              {product?.user?.username}
            </p>
            <p>
              <span className="font-bold">Việt hóa: </span>
              {product?.subAuthor}
            </p>
            <p>
              <span className="font-bold">Ngày đăng: </span>
              {moment(product?.createdAt).format("DD - MM - YYYY")}
            </p>
            <p>
              <span className="font-bold">Số lượt tải font: </span>
              {product?.downloadFont?.length}
            </p>
          </div>
        </div>
        {token ? (
          <div className=" text-sm text-center bg-oranges hover:bg-reds cursor-pointer py-3.5 text-whites">
            {info?.orderStatus === 2 || product?.outstanding !== 1 ? (
              <button onClick={handleMumberDownload}>
                <a href={product?.productFont[0]?.font} download>
                  <span className="font-normal">TẢI FONT NÀY NGAY</span>
                </a>
              </button>
            ) : (
              <div>
                <label
                  htmlFor="my_modal_7"
                  className="font-normal cursor-pointer"
                >
                  TẢI FONT NÀY NGAY
                </label>

                <input
                  type="checkbox"
                  id="my_modal_7"
                  className="modal-toggle h-32 w-60"
                />
                <div className="modal" role="dialog">
                  <div className="modal-box-account modal-box bg-white px-60 rounded-lg flex flex-col items-center">
                    <div className="py-10 flex flex-col gap-3">
                      <h3 className="text-3xl font-bold text-center text-black ">
                        Bạn không thể tải font
                      </h3>

                      <p className="text-black font-normal text-lg">
                        Đăng ký{" "}
                        <Link
                          className="text-blues hover:text-blueHover"
                          href={"/goi-vip"}
                        >
                          gói 12 tháng
                        </Link>{" "}
                        để tải font VIP
                      </p>
                    </div>
                  </div>
                  <label className="modal-backdrop" htmlFor="my_modal_7">
                    Close
                  </label>
                </div>
              </div>
            )}
          </div>
        ) : (
          <div>
            {/* The button to open modal */}
            <div className=" text-sm text-center bg-oranges hover:bg-reds cursor-pointer py-3.5 text-whites w-full">
              <label
                htmlFor="my_modal_7"
                className="font-normal cursor-pointer"
              >
                TẢI FONT NÀY NGAY
              </label>
            </div>

            <input
              type="checkbox"
              id="my_modal_7"
              className="modal-toggle h-32 w-60"
            />
            <div className="modal" role="dialog">
              <div className="modal-box-account modal-box  flex flex-col items-center">
                <h3 className="text-3xl font-bold text-center py-8">
                  Đăng ký tài khoản mới được tải font
                </h3>
                <Link href={"/Auth/register"}>
                  <button className="flex  ml-1 max-sm:mx-3 items-center  px-5  py-3 max-sm:w-28 max-sm:h-8  bg-[#ff8d08] rounded-full  justify-center btn-gradient text-whites">
                    <FiUser className="text-[24px] font-bold max-md:text-sm mr-1" />{" "}
                    Đăng ký
                  </button>
                </Link>
                <div className="text-center text-lg py-8 ">
                  Bạn đã có tài khoản ?{" "}
                  <Link href={"/Auth/login"}>
                    <span className="font-bold">Đăng nhập ngay</span>
                  </Link>
                </div>
              </div>
              <label className="modal-backdrop" htmlFor="my_modal_7">
                Close
              </label>
            </div>
          </div>
        )}

        {active &&
          createPortal(renderContent, document.getElementById("__next"))}
      </div>
    </>
  );
};

export default ProductCategorySlug;
