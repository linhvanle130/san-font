import React, { useEffect, useState } from "react";
import Link from "next/link";
import Image from "next/image";
import { FaFacebook } from "react-icons/fa";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
import moment from "moment";
import { FiUser } from "react-icons/fi";
import productApis from "../../../apis/productApis";
import { useRouter } from "next/router";

const ProductDetail = ({
  dataDetail,
  productsSaved,
  onReloadProductsSaved,
}) => {
  const { token, info } = useSelector((state) => state.account);
  const router = useRouter();

  // sava product
  const [productSaved, setProductSaved] = useState();
  useEffect(() => {
    const saved = productsSaved?.find(
      (saved) => saved.productId === dataDetail.id
    );
    setProductSaved(saved);
  }, [productsSaved]);

  const handleSave = async () => {
    try {
      if (token) {
        if (!productSaved) {
          await productApis.productSave({
            userId: info?.id,
            productId: dataDetail.id,
          });
          toast.success("Font đã lưu vào tài khoản");
        } else {
          await productApis.productUnSave(`${productSaved?.id}`);
          toast.success("Font đã xóa khỏi tài khoản");
        }

        onReloadProductsSaved();
      } else {
        router.push("/Auth/login");
      }
    } catch (error) {
      console.error("Đã xảy ra lỗi:", error);
    }
  };

  // MumberDownload
  const handleMumberDownload = async () => {
    try {
      await productApis.numberDownload({
        productId: dataDetail.id,
        userId: info?.id,
        nameDownload: dataDetail?.name,
      });
    } catch (error) {
      console.error("Đã xảy ra lỗi:", error);
    }
  };

  return (
    <>
      <>
        <div className="w-full bg-whites px-5 md:px-10 lg:px-3 py-3 ">
          <h1 className="text-2xl font-bold">{dataDetail.name}</h1>
          <div className=" grid grid-cols-1 md:grid-cols-1 lg:grid-cols-2  mt-3 gap-3 ">
            <div className="relative">
              <Image
                priority
                src={dataDetail.productImage[0].image}
                width={300}
                alt="image"
                height={500}
                className="w-full h-full "
              />

              <button
                onClick={handleSave}
                className="btn-save absolute bg-reds py-[3px] px-[6px] text-whites text-xs shadow-BShadow  rounded-br-lg  z-1"
              >
                <span className="font-bold ">
                  {productSaved ? "Đã lưu" : "Lưu"}
                </span>
              </button>

              {dataDetail.outstanding === 2 ? (
                <button className="btn-vip absolute bg-[#ffa800] py-[3px] px-[6px] text-whites text-[12px] shadow-xxl rounded-bl-lg  z-1">
                  <Link href={"/font-chon-loc"} legacyBehavior>
                    <a className="font-bold ">Font chọn lọc</a>
                  </Link>
                </button>
              ) : null}

              {dataDetail.outstanding === 1 && (
                <button className="btn-vip absolute bg-[#028623] py-[3px] px-[6px] text-whites text-[12px] shadow-xxl rounded-bl-lg  z-1">
                  <Link href={"/font-vip"} legacyBehavior>
                    <a className="font-bold ">VIP</a>
                  </Link>
                </button>
              )}
            </div>

            <div className=" text-[#818181] flex flex-col gap-1.5 text-[12px] ">
              <p>
                <span className="font-bold">Tác giả: </span>
                {dataDetail.author}
              </p>
              <p>
                <span className="font-bold">Người đăng: </span>
                {dataDetail.user.username}
              </p>
              <p>
                <span className="font-bold">Việt hóa: </span>
                {dataDetail.subAuthor}
              </p>
              <div className="flex gap-14">
                <p>
                  <span className="font-bold">Số lượt tải font: </span>
                  {dataDetail.downloadFont.length}
                </p>
                <p>
                  <span className="font-bold">Ngày đăng: </span>
                  {moment(dataDetail.createdAt).format("DD - MM - YYYY")}
                </p>
              </div>

              <Link href={"https://www.facebook.com/sharer/"}>
                <button className="flex mt-2 text-sm font-normal gap-1 px-2 py-1.5 text-center rounded-[4px] bg-[#0163e0] text-whites">
                  {" "}
                  <FaFacebook className="" />{" "}
                  <span className="text-[12px]">Chia sẽ facebook</span>
                </button>
              </Link>

              {token ? (
                <div className=" mt-2 text-sm w-full text-center bg-oranges hover:bg-reds cursor-pointer  py-3.5   text-whites">
                  {info?.orderStatus === 2 || dataDetail?.outstanding !== 1 ? (
                    <button onClick={handleMumberDownload}>
                      <a href={dataDetail?.productFont[0]?.font} download>
                        <span className="font-normal">TẢI FONT NÀY NGAY</span>
                      </a>
                    </button>
                  ) : (
                    <div>
                      <label
                        htmlFor="my_modal_7"
                        className="font-normal cursor-pointer"
                      >
                        TẢI FONT NÀY NGAY
                      </label>

                      <div className="modal" role="dialog">
                        <div className="modal-box-account modal-box bg-white px-60 rounded-lg flex flex-col items-center">
                          <div className="py-10 flex flex-col gap-3">
                            <h3 className="text-3xl font-bold text-center text-black ">
                              Bạn không thể tải font
                            </h3>

                            <p className="text-black font-normal text-lg">
                              Đăng ký{" "}
                              <Link
                                className="text-blues hover:text-blueHover"
                                href={"/goi-vip"}
                              >
                                gói 12 tháng
                              </Link>{" "}
                              để tải font VIP
                            </p>
                          </div>
                        </div>
                        <label className="modal-backdrop" htmlFor="my_modal_7">
                          Close
                        </label>
                      </div>
                    </div>
                  )}
                </div>
              ) : (
                <div>
                  <div className=" text-sm text-center bg-oranges hover:bg-reds cursor-pointer py-3.5 text-whites w-full">
                    <label
                      htmlFor="my_modal_7"
                      className="font-normal cursor-pointer"
                    >
                      TẢI FONT NÀY NGAY
                    </label>
                  </div>

                  <div className="modal" role="dialog">
                    <div className="modal-box-account modal-box bg-white px-60 rounded-2xl flex flex-col items-center">
                      <h3 className="text-3xl font-bold text-center py-8">
                        Đăng ký tài khoản để tải font
                      </h3>
                      <Link href={"/Auth/register"}>
                        <button className="flex ml-1 max-sm:mx-3 items-center px-5  py-3 max-sm:w-28 max-sm:h-8  bg-[#ff8d08] rounded-full  justify-center btn-gradient text-whites">
                          <FiUser className="text-[24px] font-bold max-md:text-sm mr-1" />{" "}
                          Đăng ký
                        </button>
                      </Link>
                      <div className="text-center text-lg py-8 ">
                        Bạn đã có tài khoản ?{" "}
                        <Link href={"/Auth/login"}>
                          <span className="font-bold">Đăng nhập ngay</span>
                        </Link>
                      </div>
                    </div>
                    <label className="modal-backdrop" htmlFor="my_modal_7">
                      Close
                    </label>
                  </div>
                </div>
              )}
            </div>
          </div>
          <hr className=" w-full mt-2 mx-auto" />
          <p className="mt-3 font-normal text-lg text-blacks">
            <span className="font-bold">Lưu ý: </span>Font này chỉ sử dụng cho
            mục đích cá nhân. Sử dụng cho mục đích thương mại nên mua bản quyền
            gốc từ tác giả.
          </p>
          <div>
            <p className="mt-3 font-normal text-lg text-blacks">
              {dataDetail?.description}
            </p>
          </div>
          <hr className=" w-full  mt-2 mx-auto " />

          <div className="w-full h-90 mt-3 flex flex-col gap-3">
            {dataDetail.productImage.slice(0, 2).map((item) => {
              return (
                <Image
                  key={item.id}
                  src={item.image}
                  width={500}
                  height={500}
                  alt="image"
                  className="w-full h-full"
                />
              );
            })}
          </div>
        </div>

        {token ? (
          <div className="  text-sm  text-center bg-oranges hover:bg-reds cursor-pointer py-3.5 text-whites">
            {info?.orderStatus === 2 || dataDetail?.outstanding !== 1 ? (
              <button onClick={handleMumberDownload}>
                <a href={dataDetail?.productFont[0]?.font} download>
                  <span className="font-normal">TẢI FONT NÀY NGAY</span>
                </a>
              </button>
            ) : (
              <div>
                <label
                  htmlFor="my_modal_7"
                  className="font-normal cursor-pointer"
                >
                  TẢI FONT NÀY NGAY
                </label>

                <input
                  type="checkbox"
                  id="my_modal_7"
                  className="modal-toggle h-32 w-60"
                />
                <div className="modal" role="dialog">
                  <div className="modal-box-account modal-box bg-white px-60 rounded-lg flex flex-col items-center">
                    <div className="py-10 flex flex-col gap-3">
                      <h3 className="text-3xl font-bold text-center text-black ">
                        Bạn không thể tải font
                      </h3>

                      <p className="text-black font-normal text-lg">
                        Đăng ký{" "}
                        <Link
                          className="text-blues hover:text-blueHover"
                          href={"/goi-vip"}
                        >
                          gói 12 tháng
                        </Link>{" "}
                        để tải font VIP
                      </p>
                    </div>
                  </div>
                  <label className="modal-backdrop" htmlFor="my_modal_7">
                    Close
                  </label>
                </div>
              </div>
            )}
          </div>
        ) : (
          <div>
            <div className=" text-sm text-center bg-oranges hover:bg-reds cursor-pointer py-3.5 text-whites w-full">
              <label
                htmlFor="my_modal_7"
                className="font-normal cursor-pointer"
              >
                TẢI FONT NÀY NGAY
              </label>
            </div>

            <input
              type="checkbox"
              id="my_modal_7"
              className="modal-toggle h-32 w-60"
            />
            <div className="modal" role="dialog">
              <div className="modal-box-account modal-box bg-white text-black px-60 rounded-2xl flex flex-col items-center">
                <h3 className="text-3xl font-bold text-center py-8">
                  Đăng ký tài khoản để tải font
                </h3>
                <Link href={"/Auth/register"}>
                  <button className="flex ml-1 max-sm:mx-3 items-center px-5  py-3  max-sm:w-28 max-sm:h-8  bg-[#ff8d08] rounded-full  justify-center btn-gradient text-whites">
                    <FiUser className="text-[24px] font-bold max-md:text-sm mr-1" />{" "}
                    Đăng Ký
                  </button>
                </Link>
                <div className="text-center text-lg py-8 text-black">
                  Bạn đã có tài khoản ?{" "}
                  <Link href={"/Auth/login"}>
                    <span className="font-bold text-black">Đăng nhập ngay</span>
                  </Link>
                </div>
              </div>
              <label className="modal-backdrop" htmlFor="my_modal_7">
                Close
              </label>
            </div>
          </div>
        )}
      </>
    </>
  );
};

export default ProductDetail;
