import React, { useEffect, useState } from "react";
import { MdCheck } from "react-icons/md";
import { useRouter } from "next/navigation";
import { addToCart } from "../../../redux/slices/cartSlice";
import { useDispatch, useSelector } from "react-redux";

const OrderList = ({ dataOrder }) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const [dataOrderList, setdataOrderList] = useState();
  const { token } = useSelector((state) => state.account);

  useEffect(() => {
    setdataOrderList({
      id: dataOrder?.id,
      name: dataOrder?.name,
      note: dataOrder?.note,
      price: dataOrder?.price,
      pricePrevious: dataOrder?.pricePrevious,
      status: dataOrder?.status,
      updatedAt: dataOrder?.updatedAt,
      createdAt: dataOrder?.createdAt,
      orderContent: dataOrder?.orderContent,
    });
  }, [dataOrder]);

  const goToCart = () => {
    let newOrder = {
      id: dataOrderList.id,
      name: dataOrderList.name,
      note: dataOrderList.note,
      price: dataOrderList.price,
      pricePrevious: dataOrderList.pricePrevious,
      status: dataOrderList.status,
      updatedAt: dataOrderList.updatedAt,
      createdAt: dataOrderList.createdAt,
      orderContent: dataOrderList.orderContent,
    };

    dispatch(addToCart(newOrder));
    router.push("/dang-ki-thanh-vien/dang-ki-thong-tin");
  };

  return (
    <>
      <div
        className={` h-2 w-full
      ${dataOrderList?.id === 2 ? "bg-oranges" : "bg-blues"}`}
      ></div>
      <div className={`bg-whites py-10`}>
        <h1 className="font-semibold text-2xl text-center mt-4">
          {dataOrderList?.name}
        </h1>
        <h1 className="font-normal text-gray-500 text-lg line-through text-center mt-6">
          {dataOrderList?.pricePrevious}.vnd
        </h1>
        <h1 className="font-bold text-2xl text-center mt-2">
          {dataOrderList?.price}.vnd
        </h1>
        <div className="text-sm font-normal text-center text-[#cec7c5] mt-2">
          {dataOrderList?.note}
        </div>

        <div className="mt-12 w-[70%] mx-auto">
          {dataOrderList?.orderContent?.map((e) => {
            return (
              <ul key={e.id} className=" py-[2px]">
                <li
                  className={` text-sm text-black flex
                  ${
                    e.id === 1 || e.id === 3
                      ? "font-bold  text-[#f60000] "
                      : "font-normal  text-black "
                  }
                
               `}
                >
                  <MdCheck className="text-[20px] mr-2" />
                  {e.content}
                </li>
              </ul>
            );
          })}
        </div>

        <div className="w-[48%] mx-auto pb-10">
          {token ? (
            <>
              <button
                // disabled={dataOrderList?.id === 1}
                type="submit"
                onClick={() => goToCart()}
                className={
                  dataOrderList?.id === 0
                    ? " mt-6  w-full  cursor-no-drop  h-10 bg-oranges  text-whites"
                    : " mt-6  w-full cursor-pointer hover:bg-reds h-10 bg-oranges  text-whites"
                }
              >
                <span className=" max-md:text-[13px] text-[15px]">Đăng ký</span>
              </button>
            </>
          ) : (
            <>
              <button
                // disabled={dataOrderList?.id === 1}
                type="submit"
                onClick={() => router.push("/Auth/login")}
                className={
                  dataOrderList?.id === 0
                    ? " mt-6  w-full cursor-no-drop  h-10 bg-oranges  text-whites"
                    : " mt-6  w-full cursor-pointer hover:bg-reds h-10 bg-oranges  text-whites"
                }
              >
                <span className=" max-md:text-[13px] text-[15px]">Đăng ký</span>
              </button>
            </>
          )}
        </div>
      </div>
    </>
  );
};

export default OrderList;
