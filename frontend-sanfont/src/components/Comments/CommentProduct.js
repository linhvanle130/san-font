import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { toast } from "react-toastify";
import Link from "next/link";
import { useSelector } from "react-redux";
import Image from "next/image";
import imgGlobal from "../../../public/images";
import moment from "moment";
import axios from "axios";
import * as yup from "yup";

import { MdDelete } from "react-icons/md";
import { useRouter } from "next/navigation";
import { yupResolver } from "@hookform/resolvers/yup";
import { IoWarningOutline } from "react-icons/io5";
import Comments from "../../../apis/commentApis";
const CommentProduct = ({ dataDetail }) => {
  const [comment, setComment] = useState();
  const router = useRouter();
  const [isChange, setIsChange] = useState(false);
  const { token, info } = useSelector((state) => state.account);

  const UpComment = yup.object({
    note: yup.string().required("Vui lòng nhập nội dung").max(255).trim(),
  });
  const {
    register,
    handleSubmit,
    setValue,
    reset,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(UpComment),
  });

  const submitCreate = async (values) => {
    const payload = {
      userId: info?.id,
      productId: dataDetail.id,
      note: values.note,
    };

    try {
      await Comments.createComment(payload);

      setValue("note", "");
      reset();
      setIsChange(!isChange);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    const loadComments = async () => {
      const data = await Comments.getComment();
      const comments = data.rows.filter(
        (item) => item.productId === dataDetail.id
      );

      setComment(comments || []);
    };

    loadComments();
  }, [isChange]);

  const handleDeleteComment = async (id) => {
    try {
      if (token) {
        await Comments.deleteComment(`${id}`);
        alert("Bạn muốn xóa bình luận này ?");
        toast.success("bình luận đã được xóa");
        setIsChange(!isChange);
      } else {
        router.push("/Auth/login");
      }
    } catch (error) {
      console.error("Đã xảy ra lỗi:", error);
    }
  };

  return (
    <>
      <div className="">
        <h3 className="text-lg font-semibold">Bình luận</h3>

        {token ? (
          <div>
            <form onSubmit={handleSubmit(submitCreate)}>
              <p className="mt-2 font-medium">
                Đăng nhập với tên {info?.username}.
                <Link href={"/profile"}>
                  <span className="pl-1 text-blues">
                    Chỉnh sửa hồ sơ của bạn
                  </span>
                  .
                </Link>
                Các trường bắt buộc được đánh dấu *
              </p>

              <h4 className="mt-4 font-normal">Bình luận *</h4>
              <textarea
                {...register("note")}
                id="note"
                rows="4"
                placeholder="Write your comment here..."
                className="block p-2.5 w-full text-sm text-blacks rounded-lg border border-gray-300 focus:outline-none  focus:border-oranges hover:border-oranges dark:bg-gray-700  "
              ></textarea>
              <span className="flex gap-1 mt-1 text-red-600 text-sm">
                {errors?.note?.message}
                {errors?.note?.message && (
                  <IoWarningOutline className="mt-[3px]" />
                )}
              </span>
              <button
                type="submit"
                className="mt-5 font-normal text-[14px] rounded  px-2 py-1 bg-[#fa4921] text-whites"
              >
                Bình luận
              </button>
            </form>

            <div>
              {comment?.map((e) => {
                return (
                  <div key={e.id} className="py-3 flex justify-between mx-5">
                    <div className="flex gap-3 w-full">
                      <Image
                        src={imgGlobal.AVT}
                        className="w-8 h-8  object-cover"
                        alt="avt"
                      ></Image>

                      <div className="w-full">
                        <div className="justify-between flex w-full">
                          <div>
                            <h2 className="pb-0 text-[13px] font-bold">
                              @{e?.user.username}
                            </h2>
                            <div className="text-[11px] italic">
                              {moment(e?.createdAt).format("DD/MM/YYYY HH:mm")}
                            </div>
                          </div>

                          {info?.id === e?.userId ? (
                            <button
                              onClick={() => handleDeleteComment(e?.id)}
                              className=" hover:text-red-600    text-[12px] "
                            >
                              <MdDelete />
                            </button>
                          ) : (
                            ""
                          )}
                        </div>
                        <div>
                          <h2 className="font-light text-[16px]">{e?.note}</h2>
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        ) : (
          <div className="mt-4">
            <p className="flex font-medium">
              Bạn phải
              <Link href={"/Auth/login"} legacyBehavior>
                <a className="px-1 text-[#2270b0] hover:text-[#014d8b]">
                  đăng nhập
                </a>
              </Link>
              để gửi phản hồi.
            </p>
          </div>
        )}
      </div>
    </>
  );
};
export default CommentProduct;
