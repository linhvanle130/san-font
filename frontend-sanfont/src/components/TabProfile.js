import imgGlobal from "../../public/images";
import Image from "next/image";
import Link from "next/link";
import React from "react";
import { useSelector } from "react-redux";

const TabProfile = ({ tabs }) => {
  const { info } = useSelector((state) => state.account);

  return (
    <>
      <div className="col-span-2  mx-auto pt-12  ">
        <div className="w-full">
          <Image
            priority
            src={imgGlobal.AVT}
            className="w-52 h-52 mx-auto object-cover"
            alt="avt"
          ></Image>
          <p className="mt-4 text-center text-blacks font-bold text-md">
            Xin chào <span className="font-extrabold">{info?.username}</span>
          </p>
        </div>
        <div className="mt-12 w-80 md:w-96 lg:w-72">
          <div className="w-full mt-12 mx-auto gap-y-[2px] cursor-pointer font-normal text-[16px]">
            <Link href={"/profile"}>
              <div
                className={` hover:duration-500 py-3 pl-2
            ${
              tabs === 1
                ? "bg-oranges text-white "
                : "bg-whites text-black hover:bg-oranges hover:text-white "
            }
            
            `}
              >
                Thông tin tài khoản
              </div>
            </Link>

            <hr />
            <Link href={"/quan-li-tai-khoan"}>
              <div
                className={` hover:duration-500 py-3 pl-2
            ${
              tabs === 2
                ? "bg-oranges text-white"
                : "bg-whites text-black hover:bg-oranges hover:text-white "
            }
            
            `}
              >
                Quản lý tài khoản
              </div>
            </Link>

            <hr />
            <Link href={"/font-yeu-thich"}>
              <div
                className={`focus:bg-oranges hover:duration-500  focus:text-whites  py-3 pl-2
            ${
              tabs === 3
                ? "bg-oranges text-white"
                : "bg-whites text-black hover:bg-oranges hover:text-white "
            }
            
            `}
              >
                Quản lý font yêu thích
              </div>
            </Link>

            <hr />
            <Link href={"/font-tai-len"}>
              <div
                className={`focus:bg-oranges hover:duration-500  focus:text-whites  py-3 pl-2
            ${
              tabs === 4
                ? "bg-oranges text-white"
                : "bg-whites text-black hover:bg-oranges hover:text-white "
            }
            
            `}
              >
                {" "}
                Font bạn đã tải lên
              </div>
            </Link>

            <hr />
          </div>
        </div>
      </div>
    </>
  );
};

export default TabProfile;
