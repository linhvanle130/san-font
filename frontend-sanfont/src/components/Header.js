import imgGlobal from "./../../public/images";
import Image from "next/image";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import { FiUser } from "react-icons/fi";
import { AiOutlineCloudUpload } from "react-icons/ai";
import { useRouter } from "next/router";
import { FaSignOutAlt } from "react-icons/fa";
import { useDispatch, useSelector } from "react-redux";
import { logout } from "../../redux/slices/accountSlice";
import axios from "axios";

const Header = ({}) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const [pathName, setPathName] = useState("");
  const [toggleMenu, setToggleMenu] = useState(true);
  const [drop1, setDrop1] = useState(true);
  const [drop2, setDrop2] = useState(true);
  const [categoryVH, setCategoryVH] = useState([]);
  const [categoryQC, setCategoryQC] = useState([]);
  const { token, info } = useSelector((state) => state.account);

  useEffect(() => {
    const allCategory = async () => {
      const response = await axios.get(
        "http://localhost:3001/api/product/get-list-category"
      );

      const category = response.data;
      const categoryVH = category.filter((e) => e.categoryId == 1);
      const categoryQc = category.filter((e) => e.categoryId == 2);

      setCategoryVH(categoryVH);
      setCategoryQC(categoryQc);
    };
    allCategory();
  }, []);

  useEffect(() => {
    setPathName(router.pathname);
  }, [router.pathname]);

  const signOut = () => {
    dispatch(logout());
    router.push("/");
  };
  return (
    <header>
      <nav className="bg-whites shadow-lg  py-6 fixed top-0 left-0 right-0 z-[10]">
        <div className="container flex flex-wrap justify-between items-center mx-auto ">
          <Link href={"/"} className="max-lg:ml-3">
            <Image
              priority
              src={imgGlobal.Logo}
              width={150}
              height={150}
              // className="max-md:w-32 w-40"
              alt="logo"
            ></Image>
          </Link>

          <button
            onClick={() => setToggleMenu(!toggleMenu)}
            data-collapse-toggle="mobile-menu-2"
            type="button"
            className="inline-flex max-lg:mr-3 items-center p-2 ml-1 text-sm text-gray-500 rounded-lg xl:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600"
            aria-controls="mobile-menu-2"
            aria-expanded="false"
          >
            <svg
              className="w-6 h-6"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                clipRule="evenodd"
              />
            </svg>
            <svg
              className="hidden w-6 h-6"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                clipRule="evenodd"
              />
            </svg>
          </button>

          <div
            className={`${
              toggleMenu ? "hidden" : ""
            } justify-between items-center w-full xl:flex xl:w-auto xl:order-1`}
            id="mobile-menu-2"
          >
            <ul className="flex flex-col font-bold  xl:flex-row max-xl:text-center xl:space-x-8 xl:mt-0">
              <li
                className={`max-xl:py-2  hover:text-oranges hover:duration-300 max-xl:hover:font-semibold  max-xl:pl-4 cursor-pointer ${
                  pathName === "/" ? "lg:relative" : " "
                }`}
              >
                <Link href={"/"}>Home</Link>
                {pathName === "/" ? (
                  <div
                    className={`xl:absolute top-[30px] xl:h-[3px] bg-oranges xl:w-[calc(100%)]`}
                  ></div>
                ) : (
                  ""
                )}
              </li>

              <li
                className={`hidden md:hidden lg:hidden xl:block test-demo dropdown dropdown-hover max-xl:py-2 hover:text-oranges hover:duration-300 max-xl:hover:font-semibold max-xl:pl-4 cursor-pointer ${
                  pathName === "/font-viet-hoa" ? "xl:relative" : ""
                }`}
              >
                <Link tabIndex={0} href={"/font-viet-hoa"}>
                  Font việt hóa
                </Link>
                {pathName === "/font-viet-hoa" ? (
                  <div
                    className={`xl:absolute top-[30px] left-0 xl:h-[3px] bg-oranges xl:w-[calc(100%)]`}
                  ></div>
                ) : (
                  ""
                )}

                <ul className=" dropdown-content mt-[35px] py-2 text-blacks menu font-normal leading-3 shadow-xl bg-base-100 w-52">
                  {categoryVH?.map((e) => {
                    return (
                      <li key={e.id} className="py-[2px]">
                        <Link
                          href={`/font-viet-hoa/${e.categorySlug}`}
                          legacyBehavior
                        >
                          <a>{e.name}</a>
                        </Link>
                      </li>
                    );
                  })}
                </ul>
              </li>
              {/* mobile */}
              <li
                className={`block md:block lg:block xl:hidden dropdown   max-xl:py-2 hover:text-oranges hover:duration-300 max-xl:hover:font-semibold max-xl:pl-4 cursor-pointer ${
                  pathName === "/font-viet-hoa" ? "xl:relative" : ""
                }`}
              >
                <Link
                  data-collapse-toggle="drop-menu-1"
                  onClick={() => setDrop1(!drop1)}
                  tabIndex={0}
                  className="flex justify-center"
                  href={"/font-viet-hoa"}
                >
                  Font việt hóa
                  <svg
                    className="w-2.5 h-2.5 ms-3 mt-2"
                    aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 10 6"
                  >
                    <path
                      stroke="currentColor"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="m1 1 4 4 4-4"
                    />
                  </svg>
                </Link>
                {pathName === "/font-viet-hoa" ? (
                  <div
                    className={`xl:absolute top-[30px] left-0 xl:h-[3px] bg-oranges xl:w-[calc(100%)]`}
                  ></div>
                ) : (
                  ""
                )}

                <ul
                  id="drop-menu-1"
                  className={`${
                    drop1 ? "hidden" : ""
                  }  py-2 text-blacks font-normal leading-3  w-52 mx-auto`}
                >
                  {categoryVH?.map((e) => {
                    return (
                      <li key={e.id} className="py-2 ">
                        <Link
                          href={`/font-viet-hoa/${e.categorySlug}`}
                          legacyBehavior
                        >
                          <a>{e.name}</a>
                        </Link>
                      </li>
                    );
                  })}
                </ul>
              </li>

              <li
                className={`hidden md:hidden lg:hidden xl:block test-demo dropdown dropdown-hover max-xl:py-2 hover:text-oranges hover:duration-300 max-xl:hover:font-semibold max-xl:pl-4 cursor-pointer ${
                  pathName === "/font-quang-cao" ? "xl:relative" : ""
                }`}
              >
                <Link tabIndex={0} href={"/font-quang-cao"}>
                  Font quảng cáo
                </Link>
                {pathName === "/font-quang-cao" ? (
                  <div
                    className={`xl:absolute top-[30px] left-0 xl:h-[3px] bg-oranges xl:w-[calc(100%)]`}
                  ></div>
                ) : (
                  ""
                )}

                <ul className=" dropdown-content mt-[35px] py-2 text-blacks menu font-normal leading-3 shadow-xl bg-base-100 w-52">
                  {categoryQC?.map((e) => {
                    return (
                      <li key={e.id} className="py-[2px]">
                        <Link
                          href={`/font-quang-cao/${e.categorySlug}`}
                          legacyBehavior
                        >
                          <a>{e.name}</a>
                        </Link>
                      </li>
                    );
                  })}
                </ul>
              </li>
              {/* mobile */}
              <li
                className={`block md:block lg:block xl:hidden dropdown   max-xl:py-2 hover:text-oranges hover:duration-300 max-xl:hover:font-semibold max-xl:pl-4 cursor-pointer ${
                  pathName === "/font-quang-cao" ? "xl:relative" : ""
                }`}
              >
                <Link
                  data-collapse-toggle="drop-menu-2"
                  onClick={() => setDrop2(!drop2)}
                  tabIndex={0}
                  className="flex justify-center"
                  href={"/font-quang-cao"}
                >
                  Font quảng cáo
                  <svg
                    className="w-2.5 h-2.5 ms-3 mt-2"
                    aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 10 6"
                  >
                    <path
                      stroke="currentColor"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="m1 1 4 4 4-4"
                    />
                  </svg>
                </Link>
                {pathName === "/font-quang-cao" ? (
                  <div
                    className={`xl:absolute top-[30px] left-0 xl:h-[3px] bg-oranges xl:w-[calc(100%)]`}
                  ></div>
                ) : (
                  ""
                )}

                <ul
                  id="drop-menu-2"
                  className={`${
                    drop2 ? "hidden" : ""
                  } py-2 text-blacks  font-normal leading-3  w-52 mx-auto`}
                >
                  {categoryQC?.map((e) => {
                    return (
                      <li key={e.id} className="py-2">
                        <Link
                          href={`/font-quang-cao/${e.categorySlug}`}
                          legacyBehavior
                        >
                          <a>{e.name}</a>
                        </Link>
                      </li>
                    );
                  })}
                </ul>
              </li>

              <li
                className={`max-xl:py-2 hover:text-oranges hover:duration-300 max-xl:hover:font-semibold  max-xl:pl-4 cursor-pointer ${
                  pathName === "/goi-vip" ? "xl:relative" : ""
                }`}
              >
                <Link href={"/goi-vip"}>Gói VIP</Link>
                {pathName === "/goi-vip" ? (
                  <div
                    className={`xl:absolute top-[30px] left-0 xl:h-[3px] bg-oranges xl:w-[calc(100%)]`}
                  ></div>
                ) : (
                  ""
                )}
              </li>
              <li
                className={`max-xl:py-2  hover:duration-300 max-xl:hover:font-semibold  max-xl:pl-4 cursor-pointer ${
                  pathName === "/blog" ? "xl:relative" : ""
                }`}
              >
                <Link href={"/blog"}>Blog</Link>
                {pathName === "/blog" ? (
                  <div
                    className={`xl:absolute top-[30px] left-0 xl:h-[3px] bg-oranges  xl:w-[calc(100%)]`}
                  ></div>
                ) : (
                  ""
                )}
              </li>
            </ul>

            <div className="flex ml-10 items-center gap-3 xl:order-2 max-lg:justify-center max-lg:mx-auto max-lg:mt-5">
              <Link href={"/upfont"}>
                <button className="flex items-center py-2.5 bg-oranges rounded justify-center btn-gradient text-whites px-2  max-sm:w-24 max-sm:h-8">
                  <AiOutlineCloudUpload className="font-extrabold text-[24px] max-md:text-sm" />
                  <span className="ml-2 text-[15px]  max-md:text-[13px]">
                    Up font
                  </span>
                </button>
              </Link>
              {token ? (
                <div className="flex gap-2">
                  <Link href={"/profile"}>
                    <button className="flex items-center py-2.5 bg-[#1876f2] rounded  justify-center btn-gradient text-whites px-2  max-sm:w-24 max-sm:h-8">
                      <FiUser className="text-[24px] font-bold mr-1 max-md:text-sm" />
                      <span className="ml-1 text-[15px] font-normal max-md:text-[13px]">
                        {info && info?.username}
                      </span>
                    </button>
                  </Link>

                  <Link href={""}>
                    <button
                      onClick={() => signOut()}
                      className="flex ml-1  items-center px-2 py-2.5 max-sm:w-28 max-sm:h-8  bg-[#ff8d08] rounded  justify-center btn-gradient text-whites"
                    >
                      <FaSignOutAlt className="text-[20px] font-bold max-md:text-sm mr-1" />{" "}
                      Đăng xuất
                    </button>
                  </Link>
                </div>
              ) : (
                <Link href={"/Auth/login"}>
                  <button className="flex ml-1 max-sm:mx-3 items-center px-2 py-2.5 max-sm:w-28 max-sm:h-8  bg-[#ff8d08] rounded  justify-center btn-gradient text-whites">
                    <FiUser className="text-[24px] font-bold max-md:text-sm mr-1" />{" "}
                    Đăng nhập
                  </button>
                </Link>
              )}
            </div>
          </div>
        </div>
      </nav>
    </header>
  );
};

export default Header;
