import React from "react";
import Head from "next/head";
import imgGlobal from "../../../public/images";
const SEO = (props) => {
  const {
    title = " Chia sẽ font chữ miễn phí. ",
    description = "Chia sẽ font chữ miễn phí.",
    href = "favicon.ico",
    image = window.location.origin + "/sanfont.png",
    info = "Email: info@sanfont.com",
    children,
  } = props;
  return (
    <Head>
      <link rel="icon" className="object-cover" href={href} />
      <title>{title}</title>
      <meta charSet="UTF-8" />
      <meta name="title" content={title} />
      <meta name="description" content={description} />
      <meta name="author" content={info} />
      <link rel="canonical" href={window.location.href} />
      <meta property="og:title" content={title} />
      <meta property="og:description" content={description} />
      <meta property="og:image" content={image} />
      <meta property="og:url" content={window.location.href} />
      {children}
    </Head>
  );
};

export default SEO;
