import React from "react";
import axios from "axios";
import lodash from "lodash";
import Link from "next/link";
import Image from "next/image";
import blogApi from "../../../apis/blogApis";
import SEO from "./../../components/SEO/index";
const BlogDetail = ({ dataDetail, AllBlog }) => {
  const randomBlogRelate = lodash.sampleSize(AllBlog, 3);

  return (
    <>
      <SEO title={` ${dataDetail.title}`}></SEO>
      <div className="container mx-auto pt-32 ">
        <div
          className="xl:mx-52 lg:mx-20 px-8 py-8 bg-whites "
          key={dataDetail.id}
        >
          <h1 className="text-[28px] text-center text-blacks font-bold">
            {dataDetail.title}
          </h1>

          <div className="w-20 rounded-[30px] bg-[#eeee] text-center py-2 mx-auto mt-5">
            <Link href={"/blog"}>
              <button className="text-[16px] font-normal">Design</button>
            </Link>
          </div>
          <hr className="mt-3" />
          <p className="font-normal text-[16px] text-blacks mt-6">
            {dataDetail.description}
          </p>
          <div className="w-full h-[1px] bg-gray-400 mt-3"></div>

          <Image
            priority
            src={dataDetail.thumbnail}
            alt="logo"
            width={500}
            height={400}
            className="w-full object-cover mt-5"
          ></Image>
          <div className="w-full h-[1px] bg-gray-400 mt-3"></div>

          <div>
            {dataDetail.blogCategory.map((e) => {
              return (
                <div key={e.id}>
                  <h1 className="text-[30px] font-bold text-[#0d6efd]">
                    {e.title}
                  </h1>
                  <Image
                    priority
                    src={e.thumbnail}
                    alt="logo"
                    width={500}
                    height={400}
                    className="w-full object-cover mt-5"
                  ></Image>
                  <div className="w-full h-[1px] bg-gray-400 mt-3"></div>
                </div>
              );
            })}
          </div>
        </div>
      </div>

      <div className="">
        <h1 className=" mt-10 font-extrabold text-[36px] text-center">
          BÀI VIẾT LIÊN QUAN
        </h1>
        <div className="container mt-10 grid grid-cols-1 md:grid-cols-1 lg:grid-cols-3 gap-6 mx-auto  ">
          {randomBlogRelate?.map((dataDetail) => (
            <div key={dataDetail.id} className="bg-whites shadow-md rounded  ">
              <div className=" overflow-hidden relative w-full ">
                <Link href={`/blog/${dataDetail.id}`}>
                  <Image
                    src={dataDetail.thumbnail}
                    alt="logo"
                    width={500}
                    height={400}
                    className="w-full h-60 object-cover  duration-500 hover:scale-[1.1] "
                  ></Image>
                </Link>
              </div>
              <div className="p-4">
                <Link href={`/blog/${dataDetail.id}`}>
                  <h2 className="text-blacks  font-extrabold text-xl ">
                    {dataDetail.title}
                  </h2>
                </Link>
                <div className="description-blog text-blacks  font-normal text-[16px] pt-2">
                  <p>{dataDetail.description}</p>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </>
  );
};

export default BlogDetail;

export async function getStaticPaths() {
  const blog = await blogApi.getAllBogs();
  const detailPath = blog.rows.map((ev) => {
    return {
      params: {
        blog: ev.title,
        blogDetail: ev.id.toString(),
      },
    };
  });

  return {
    paths: detailPath,
    fallback: false,
  };
}

export async function getStaticProps(context) {
  const id = context.params.blogDetail;
  const dataDetails = await axios.get(
    `http://0.0.0.0:3001/api/blog/detail-blog/${id}`
  );

  const AllBlog = await blogApi.getAllBogs();
  return {
    props: {
      AllBlog: AllBlog.rows,
      dataDetail: dataDetails.data,
    },
  };
}
