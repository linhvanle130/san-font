import Link from "next/link";
import Image from "next/image";
import { useState } from "react";
import SEO from "./../../components/SEO";
import ReactPaginate from "react-paginate";
import blogApi from "../../../apis/blogApis";
import Advertisement from "@/components/Advertisement";

const BlogPage = ({ DataBlog }) => {
  const [blogs, setBlog] = useState(DataBlog);
  const [pageNumber, setPageNumber] = useState(0);

  const blogsPerPage = 6;
  const pagesVisited = pageNumber * blogsPerPage;
  const displayUsers = blogs
    .slice(pagesVisited, pagesVisited + blogsPerPage)
    .map((item, id) => {
      return (
        <div className="bg-whites shadow-md" key={id}>
          <div className=" overflow-hidden relative w-full ">
            <Link href={`/blog/${item.id}`}>
              <Image
                priority
                src={item?.thumbnail}
                alt="logo"
                width={500}
                height={400}
                className="w-full h-60 object-cover  duration-500 hover:scale-[1.1]"
              ></Image>
            </Link>
          </div>
          <div className="p-4">
            <Link href={`/blog/${item.id}`}>
              <h2 className="text-blacks font-extrabold text-[21px]">
                {item.title}
              </h2>
            </Link>
            <div className="description-blog text-blacks font-normal text-[16px] pt-2">
              <p>{item.description}</p>
            </div>
          </div>
        </div>
      );
    })
    .reverse();

  const pageCount = Math.ceil(blogs.length / blogsPerPage);

  const changePage = ({ selected }) => {
    setPageNumber(selected);
  };
  return (
    <>
      <SEO title={" Blog."}></SEO>
      <div className="container mx-auto pt-32  md:pt-32 lg:pt-32">
        <h1 className="text-[36px] font-bold text-center ">Blog</h1>
        <div className="w-20 rounded-[30px] bg-whites text-center py-2 mx-auto mt-5">
          <button className="text-[16px] font-normal">Design</button>
        </div>

        <div className=" mx-auto mt-10 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6  ">
          {displayUsers}
        </div>
        <div className="mt-10  w-[30%] mx-auto ">
          <ReactPaginate
            previousLabel={"Previous"}
            nextLabel={"Next"}
            pageCount={pageCount}
            onPageChange={changePage}
            containerClassName={"paginationBttns"}
            previousLinkClassName={"previousBttn"}
            nextLinkClassName={"nextBttn"}
            disabledClassName={"paginationDisabled"}
            activeClassName={"paginationActive"}
          />
        </div>
        <Advertisement />
      </div>
    </>
  );
};

export default BlogPage;

export async function getStaticProps() {
  const blog = await blogApi.getAllBogs();

  return {
    props: {
      DataBlog: blog.rows,
    },
  };
}
