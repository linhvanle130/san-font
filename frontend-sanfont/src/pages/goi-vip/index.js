import React from "react";
import SEO from "./../../components/SEO";
import { useSelector } from "react-redux";

import orderApis from "../../../apis/orderApis";
import OrderList from "./../../components/Order/OrderList";

const Vip = ({ dataOrder }) => {
  return (
    <>
      <SEO title={" Gói thành viên"}></SEO>
      <div className="container mx-auto pt-20">
        <h1 className="text-[40px] font-semibold text-center mt-14">
          GÓI THÀNH VIÊN
        </h1>

        <div className="px-5 md:px-40 lg:px-20 mx-auto grid grid-cols-1 md:grid-cols-1 lg:grid-cols-3 gap-14 pt-14   ">
          {dataOrder.map((Order) => {
            return (
              <div key={Order.id}>
                <OrderList dataOrder={Order} />
              </div>
            );
          })}
        </div>
      </div>
    </>
  );
};

export default Vip;

export async function getStaticProps() {
  const order = await orderApis.getOrderPackage();

  return {
    props: {
      dataOrder: order.rows,
    },
  };
}
