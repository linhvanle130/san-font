import SEO from "../../components/SEO";
import TabProfile from "../../components/TabProfile";
import React, { useEffect, useMemo } from "react";
import { useState } from "react";
import { AiOutlineEye, AiOutlineEyeInvisible } from "react-icons/ai";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { IoWarningOutline } from "react-icons/io5";
import { useRouter } from "next/router";
import AuthApis from "../../../apis/AuthApis";

const TaiKhoan = () => {
  const router = useRouter();
  const [isLoading, setIsLoading] = useState(false);
  const { info } = useSelector((state) => state.account);

  //eye input1
  const [eye1, setEye1] = useState(false);
  const toggle1 = () => {
    setEye1(!eye1);
  };

  //eye input2
  const [eye2, setEye2] = useState(false);
  const toggle2 = () => {
    setEye2(!eye2);
  };

  //eye input3
  const [eye3, setEye3] = useState(false);
  const toggle3 = () => {
    setEye3(!eye3);
  };

  const changePass = useMemo(
    () =>
      yup
        .object()
        .shape({
          oldPassword: yup
            .string()
            .required("Trường bắt buộc")
            .min(6, "Tối thiểu 6 kí tự")
            .max(30, "Tối đa 30 kí tự")
            .trim(),
          password: yup
            .string()
            .required("Trường bắt buộc")
            .min(6, "Tối thiểu 6 kí tự")
            .max(30, "Tối đa 30 kí tự")
            .trim(),
          newPassword: yup
            .string()
            .when("password", {
              is: (val) => (val && val.length > 0 ? true : false),
              then: () =>
                yup
                  .string()
                  .oneOf([yup.ref("password")], "Mật khẩu không giống nhau"),
            })
            .required("Trường bắt buộc")
            .trim(),
        })
        .required(),
    []
  );
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
    setValue,
  } = useForm({
    resolver: yupResolver(changePass),
  });

  const submitCreate = (values) => {
    const { email, oldPassword, password, newPassword } = values;

    const payload = {
      email: email,
      oldPassword: oldPassword,
      password: password,
      newPassword: newPassword,
    };
    setIsLoading(true);
    return AuthApis.changePass(payload)
      .then(() => {
        toast.success("thay đổi thành công");

        reset();
      })
      .catch((err) => {
        console.log(err);

        toast.error("THẤT BẠI");
      })
      .finally(() => {
        setIsLoading(false);
      });
  };
  useEffect(() => {
    setValue("email", info?.email);
  }, []);

  return (
    <>
      <SEO title={"Quản lý tài khoản"}></SEO>

      <div className="pt-20 md:pt-20 lg:pt-32">
        <div className="container gap-0 md:gap-0 lg:gap-5 mx-auto grid grid-cols-1 md:grid-cols-1 lg:grid-cols-7 flex-col md:flex-col">
          <TabProfile tabs={2} />

          <form
            onSubmit={handleSubmit(submitCreate)}
            className=" col-span-5 mt-12 bg-whites rounded-3xl shadow-md py-8 px-5 lg:px-10 md:px-5"
          >
            <div className="">
              <label
                className="text-[16px] font-normal text-blacks"
                htmlFor="text"
              >
                Họ tên
              </label>
              <input
                disabled
                defaultValue={info?.username}
                id="text"
                type="text"
                className="bg-[#e9ecef]  font-normal border-gray-300 text-blacks rounded border   w-full py-1.5 px-3"
              />
            </div>

            <div className="mt-6">
              <label
                className="mt-10 text-[16px] font-normal text-blacks"
                htmlFor="text"
              >
                Địa chỉ email
              </label>
              <input
                disabled
                defaultValue={info?.email}
                id="email"
                type="email"
                name="email"
                className="bg-[#e9ecef]  font-normal border-gray-300 text-blacks rounded border   w-full py-1.5 px-3"
              />
            </div>

            <h1 className="text-2xl mt-5 font-normal">Thay đổi mật khẩu</h1>

            <div className=" mt-4 relative ">
              <div>
                <label
                  className="mt-10 text-[16px] font-normal text-blacks"
                  htmlFor="text"
                >
                  Mật khẩu hiện tại (bỏ trống nếu không đổi)
                </label>
                <input
                  {...register("oldPassword")}
                  type={eye1 === false ? "password" : "text"}
                  id="oldPassword"
                  name="oldPassword"
                  autoComplete="on"
                  className={`test relative bg-whites  border-gray-300 text-gray-900  rounded border focus:outline-none  focus:border-oranges focus:ring-4 focus:ring-red-200  placeholder:text-[#6d767e]  block w-full py-1.5 p-3
                  ${
                    errors?.oldPassword?.message
                      ? "focus:ring-2 focus:ring-red-300 border border-red-500 "
                      : "border border-slate-300 hover:border hover:border-slate-500"
                  } `}
                />
                <span className="flex gap-1 mt-1 text-red-600 text-sm">
                  {errors?.oldPassword?.message}
                  {errors?.oldPassword?.message && (
                    <IoWarningOutline className="mt-[3px]" />
                  )}
                </span>
              </div>
              <div className="text-2xl cursor-pointer text-[#6a6870] absolute top-8 right-2 max-md:top-14 max-md:text-lg">
                {eye1 === false ? (
                  <AiOutlineEye onClick={toggle1} />
                ) : (
                  <AiOutlineEyeInvisible onClick={toggle1} />
                )}
              </div>
            </div>

            <div className=" mt-4 relative ">
              <div>
                <label
                  className="mt-10 text-[16px] font-normal text-blacks"
                  htmlFor="text"
                >
                  Mật khẩu mới (bỏ trống nếu không đổi)
                </label>
                <input
                  {...register("password")}
                  type={eye2 === false ? "password" : "text"}
                  id="password"
                  name="password"
                  autoComplete="on"
                  className={`test relative bg-whites  border-gray-300 text-gray-900  rounded border focus:outline-none  focus:border-oranges focus:ring-4 focus:ring-red-200  placeholder:text-[#6d767e]  block w-full py-1.5 p-3
                  ${
                    errors?.password?.message
                      ? "focus:ring-2 focus:ring-red-300 border border-red-500 "
                      : "border border-slate-300 hover:border hover:border-slate-500"
                  } `}
                />
                <span className="flex gap-1 mt-1 text-red-600 text-sm">
                  {errors?.password?.message}
                  {errors?.password?.message && (
                    <IoWarningOutline className="mt-[3px]" />
                  )}
                </span>
              </div>
              <div className="text-2xl cursor-pointer text-[#6a6870] absolute top-8 right-2 max-md:top-14 max-md:text-lg">
                {eye2 === false ? (
                  <AiOutlineEye onClick={toggle2} />
                ) : (
                  <AiOutlineEyeInvisible onClick={toggle2} />
                )}
              </div>
            </div>
            <div className=" mt-4 relative ">
              <div>
                <label
                  className="mt-10 text-[16px] font-normal text-blacks"
                  htmlFor="text"
                >
                  Xác nhận mật khẩu mới
                </label>
                <input
                  type={eye3 === false ? "password" : "text"}
                  id="newPassword"
                  name="newPassword"
                  autoComplete="on"
                  {...register("newPassword")}
                  className={`test relative bg-whites  border-gray-300 text-gray-900  rounded border focus:outline-none  focus:border-oranges focus:ring-4 focus:ring-red-200  placeholder:text-[#6d767e]  block w-full py-1.5 p-3
                  ${
                    errors?.newPassword?.message
                      ? "focus:ring-2 focus:ring-red-300 border border-red-500 "
                      : "border border-slate-300 hover:border hover:border-slate-500"
                  } `}
                />
                <span className="flex gap-1 mt-1 text-red-600 text-sm">
                  {errors?.newPassword?.message}
                  {errors?.newPassword?.message && (
                    <IoWarningOutline className="mt-[3px]" />
                  )}
                </span>
              </div>
              <div className="text-2xl cursor-pointer text-[#6a6870] absolute top-8 right-2 max-md:text-lg">
                {eye3 === false ? (
                  <AiOutlineEye onClick={toggle3} />
                ) : (
                  <AiOutlineEyeInvisible onClick={toggle3} />
                )}
              </div>
            </div>

            <button
              type="submit"
              className="flex mt-5 mx-auto  items-center w-[140px] h-10 bg-oranges hover:bg-opacity-80 rounded-[30px]  justify-center btn-gradient text-whites"
            >
              {isLoading ? (
                <>
                  <svg
                    aria-hidden="true"
                    role="status"
                    className="inline w-5 h-5 mr-3 text-white animate-spin"
                    viewBox="0 0 100 101"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                      fill="#E5E7EB"
                    />
                    <path
                      d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                      fill="currentColor"
                    />
                  </svg>
                  Lưu thay đổi
                </>
              ) : (
                <>
                  <a className="ml-2 text-[16px] font-normal"> Lưu thay đổi</a>
                </>
              )}
            </button>
          </form>
        </div>
      </div>
    </>
  );
};
export default TaiKhoan;
