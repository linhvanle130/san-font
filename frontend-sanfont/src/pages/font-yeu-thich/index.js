import React, { useEffect, useState } from "react";
import TabProfile from "../../components/TabProfile";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import SEO from "../../components/SEO";
import moment from "moment";
import Link from "next/link";
import Image from "next/image";
import { createPortal } from "react-dom";
import { GrClose } from "react-icons/gr";
import axios from "axios";
import productApis from "../../../apis/productApis";
import { FiUser } from "react-icons/fi";

const FontLike = () => {
  const { info, token } = useSelector((state) => state.account);

  const [product, setProduct] = useState();

  const [isSavedChange, setIsSavedChange] = useState(false);
  const [active, setActive] = useState();
  const [orderStatus, setOrderStatus] = useState();

  useEffect(() => {
    if (info?.id) {
      const loadProductSave = async () => {
        const { data } = await axios.get(
          `http://localhost:3001/api/user/user-info/detail/${info?.id}`
        );
        setOrderStatus(data.orderStatus);

        setProduct(data?.productSave || []);
      };
      loadProductSave();
    }
  }, [isSavedChange]);

  const handleUnSave = async (id) => {
    try {
      if (token) {
        await productApis.productUnSave(`${id}`);

        toast.success("Font đã xóa khỏi tài khoản");
        onReloadProductsSaved();
      } else {
        router.push("/Auth/login");
      }
    } catch (error) {
      console.error("Đã xảy ra lỗi:", error);
    }
  };

  const onReloadProductsSaved = () => {
    setIsSavedChange(!isSavedChange);
  };

  // MumberDownload
  const handleMumberDownload = async () => {
    try {
      await productApis.numberDownload({
        productId: product?.id,
        userId: info?.id,
        nameDownload: product?.name,
      });
    } catch (error) {
      console.error("Đã xảy ra lỗi:", error);
    }
  };
  //demo image
  const renderContent = (
    <div
      className={`${
        active ? "" : "hidden"
      } w-full h-full fixed top-0  z-[99] bg-black bg-opacity-30 px-5 md:px-10 lg:px-0`}
    >
      <div
        className={`box-check-lick lg:w-[60%] mt-[40%] md:mt-[40%]  lg:mt-[6%] px-8 gap-8 mx-auto  flex relative justify-center bg-white`}
      >
        <span
          className={`absolute cursor-pointer  hover:text-red-600 top-2 right-2`}
          onClick={() => setActive(false)}
        >
          <GrClose className=" text-xl" />
        </span>

        {product?.slice(0, 1).map((e) => {
          return (
            <div key={e.id} className=" w-[98%] py-5">
              {e.product?.productImage?.slice(0, 1).map((item) => {
                return (
                  <div key={item.id}>
                    <Image
                      src={item.image}
                      width={500}
                      alt="image"
                      height={300}
                      className="w-full h-full mt-2 object-cover pb-4"
                    />
                  </div>
                );
              })}
            </div>
          );
        })}
      </div>
    </div>
  );

  return (
    <>
      <SEO title={"Font yêu thích"}></SEO>
      <div className="pt-20 md:pt-20 lg:pt-32">
        <div className="container grid grid-cols-1 md:grid-cols-1 lg:grid-cols-7 gap-0 md:gap-0 lg:gap-5 mx-auto flex-col md:flex-col">
          <TabProfile tabs={3} />

          <div className="col-span-5 mt-12">
            {product?.length === 0 ? (
              <div className="text-center font-semibold text-lg">
                Chưa có font nào
              </div>
            ) : (
              <div className=" w-full grid gap-6 grid-cols-1 md:grid-cols-2 lg:grid-cols-3 ">
                {product?.map((e) => {
                  return (
                    <div key={e.id} className="bg-whites shadow-md ">
                      <div className="relative">
                        <div className=" group overflow-hidden">
                          <div
                            onClick={() => setActive(true)}
                            className=" cursor-pointer  duration-500 hover:scale-[1.1] relative "
                          >
                            {e.product?.productImage && (
                              <div className="w-full h-[185px] md:h-[220px] lg:h-[185px]">
                                <Image
                                  priority
                                  src={e.product?.productImage[0]?.image}
                                  width={500}
                                  height={300}
                                  alt="image"
                                  className=" w-full h-full object-cover "
                                ></Image>
                              </div>
                            )}
                            <div className="w-full h-full absolute top-0 bg-black/70 cursor-pointer duration-500  opacity-0 group-hover:opacity-80 flex items-center justify-center">
                              <h2 className="text-whites text-sm font-semibold  ">
                                XEM DEMO
                              </h2>
                            </div>
                          </div>
                        </div>

                        <div>
                          <button
                            onClick={() => handleUnSave(e?.id)}
                            className=" absolute top-0 left-0 bg-reds py-[3px] px-[6px] text-whit text-whites  text-[12px] shadow-xxl rounded-br-lg  z-1"
                          >
                            <span className="font-bold ">Đã lưu</span>
                          </button>
                        </div>

                        {e.product?.outstanding === 2 ? (
                          <button className="btn-vip absolute top-0 right-0 bg-[#ffa800] py-[3px] px-[6px] text-whites text-[12px] shadow-xxl rounded-bl-lg  z-1">
                            <Link href={"/font-chon-loc"} legacyBehavior>
                              <a className="font-bold ">Font chọn lọc</a>
                            </Link>
                          </button>
                        ) : null}

                        {e.product?.outstanding === 1 && (
                          <button className="btn-vip absolute top-0 right-0 bg-[#028623] py-[3px] px-[6px] text-whites text-[12px] shadow-xxl rounded-bl-lg  z-1">
                            <Link href={"/font-vip"} legacyBehavior>
                              <a className="font-bold ">VIP</a>
                            </Link>
                          </button>
                        )}
                      </div>

                      <div className="pl-3">
                        <Link
                          href={`${e.product?.productCategory.category.categorySlug}/${e.product?.productCategory.categorySlug}/${e.product?.id}`}
                        >
                          <h2 className="description mt-3 font-normal text-[16px] text-blacks">
                            {e.product?.name}
                          </h2>
                        </Link>
                        <div className=" py-2.5 font-normal text-[12px] leading-7 text-[#818181]">
                          <p>
                            <span className="font-bold">Tác giả: </span>
                            {e.product?.author}
                          </p>
                          <p>
                            <span className="font-bold">Người đăng: </span>
                            {e.user?.username}
                          </p>
                          <p>
                            <span className="font-bold">Việt hóa: </span>
                            {e.product?.subAuthor}
                          </p>
                          <p>
                            <span className="font-bold">Ngày đăng: </span>
                            {moment(e.product?.createdAt).format(
                              "DD - MM - YYYY"
                            )}
                          </p>
                          <p>
                            <span className="font-bold">
                              Số lượt tải font:{" "}
                            </span>
                            {e.product?.downloadFont?.length}
                          </p>
                        </div>
                      </div>
                      {token ? (
                        <div className=" text-sm text-center bg-oranges hover:bg-reds cursor-pointer py-3.5 text-whites">
                          {orderStatus === 2 || e.product?.outstanding !== 1 ? (
                            <button onClick={handleMumberDownload}>
                              <a
                                href={e.product?.productFont[0]?.font}
                                download
                              >
                                <span className="font-normal">
                                  TẢI FONT NÀY NGAY
                                </span>
                              </a>
                            </button>
                          ) : (
                            <div>
                              <label
                                htmlFor="my_modal_7"
                                className="font-normal cursor-pointer"
                              >
                                TẢI FONT NÀY NGAY
                              </label>

                              <input
                                type="checkbox"
                                id="my_modal_7"
                                className="modal-toggle h-32 w-60"
                              />
                              <div className="modal" role="dialog">
                                <div className="modal-box-account modal-box bg-white px-60 rounded-lg flex flex-col items-center">
                                  <div className="py-10 flex flex-col gap-3">
                                    <h3 className="text-3xl font-bold text-center text-black ">
                                      Bạn không thể tải font
                                    </h3>

                                    <p className="text-black font-normal text-lg">
                                      Đăng ký{" "}
                                      <Link
                                        className="text-blues hover:text-blueHover"
                                        href={"/goi-vip"}
                                      >
                                        gói 12 tháng
                                      </Link>{" "}
                                      để tải font VIP
                                    </p>
                                  </div>
                                </div>
                                <label
                                  className="modal-backdrop"
                                  htmlFor="my_modal_7"
                                >
                                  Close
                                </label>
                              </div>
                            </div>
                          )}
                        </div>
                      ) : (
                        <div>
                          <div className=" text-sm text-center bg-oranges hover:bg-reds cursor-pointer py-3.5 text-whites w-full">
                            <label
                              htmlFor="my_modal_7"
                              className="font-normal cursor-pointer"
                            >
                              TẢI FONT NÀY NGAY
                            </label>
                          </div>

                          <input
                            type="checkbox"
                            id="my_modal_7"
                            className="modal-toggle h-32 w-60"
                          />
                          <div className="modal" role="dialog">
                            <div className="modal-box-account modal-box bg-white px-60 rounded-2xl flex flex-col items-center">
                              <h3 className="text-3xl font-bold text-center py-8">
                                Đăng ký tài khoản để tải font
                              </h3>
                              <Link href={"/Auth/register"}>
                                <button className="flex ml-1 max-sm:mx-3 items-center px-5  py-3 max-sm:w-28 max-sm:h-8  bg-[#ff8d08] rounded-full  justify-center btn-gradient text-whites">
                                  <FiUser className="text-[24px] font-bold max-md:text-sm mr-1" />{" "}
                                  Đăng Ký
                                </button>
                              </Link>
                              <div className="text-center text-lg py-8 ">
                                Bạn đã có tài khoản ?{" "}
                                <Link href={"/Auth/login"}>
                                  <span className="font-bold">
                                    Đăng nhập ngay
                                  </span>
                                </Link>
                              </div>
                            </div>
                            <label
                              className="modal-backdrop"
                              htmlFor="my_modal_7"
                            >
                              Close
                            </label>
                          </div>
                        </div>
                      )}
                      {/* <div className=" text-sm text-center bg-oranges hover:bg-reds cursor-pointer py-3.5 text-whites">
                        <button onClick={handleMumberDownload}>
                          <a href={e.product?.productFont[0]?.font} download>
                            <span className="font-normal">
                              TẢI FONT NÀY NGAY
                            </span>
                          </a>
                        </button>
                      </div> */}

                      {active &&
                        createPortal(
                          renderContent,
                          document.getElementById("__next")
                        )}
                    </div>
                  );
                })}
              </div>
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default FontLike;
