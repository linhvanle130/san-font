import React, { useEffect, useState } from "react";
import Link from "next/link";
import { AiOutlineEye, AiOutlineEyeInvisible } from "react-icons/ai";
import { IoWarningOutline } from "react-icons/io5";
import { yupResolver } from "@hookform/resolvers/yup";
import { BiLeftArrowCircle } from "react-icons/bi";
import { useRouter } from "next/router";
import * as yup from "yup";
import AuthApis from "../../../../apis/AuthApis";
import { toast } from "react-toastify";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import {
  setInfoAccount,
  setInfoLogin,
} from "../../../../redux/slices/accountSlice";
import axiosClient from "../../../../apis/axiosClient";
import SEO from "./../../../components/SEO";

const Login = () => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");
  const router = useRouter();
  const dispatch = useDispatch();

  const LoginSchema = yup.object({
    email: yup
      .string()
      .email("Vui lòng nhập email hợp lệ")
      .required("Trường bắt buộc")
      .max(255)
      .matches(
        /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
        "Email không đúng định đạng"
      )
      .trim(),

    password: yup
      .string()
      .required("Trường bắt buộc")
      .min(6, "Tối thiểu 6 kí tự")
      .max(30, "Tối đa 30 kí tự")
      .trim(),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
    control,
  } = useForm({
    resolver: yupResolver(LoginSchema),
    mode: "onChange",
  });

  const onSubmit = (data) => {
    const { email, password } = data;

    setLoading(true);

    AuthApis.login({ email, password })
      .then(({ user, token }) => {
        axiosClient.defaults.headers.common = {
          Authorization: `Bearer ${token}`,
        };
        localStorage.setItem("info", JSON.stringify(user));
        localStorage.setItem("token", token);
        dispatch(setInfoLogin(token));
        dispatch(setInfoAccount(user));

        router.push("/profile");
        toast.success("Đăng nhập thành công");
      })

      .catch((err) => {
        {
          setLoading(false);
          setError("Email hoặc Mật khẩu của bạn không chính sát.");
        }
      });
  };

  //eye password
  const [eye, setEye] = useState(false);
  const toggle = () => {
    setEye(!eye);
  };

  return (
    <>
      <SEO title={" Đăng nhập"}></SEO>
      <div className=" flex w-full pt-28 ">
        <div className="container mx-auto ">
          <Link className="gap-1 flex font-normal text-[16px]" href={"/"}>
            <BiLeftArrowCircle className="text-xl mt-[3px]" />
            Về trang chủ
          </Link>
        </div>
      </div>

      <form
        onSubmit={handleSubmit(onSubmit)}
        className="container mx-auto mt-10 "
      >
        <div className="md:w-[60%] lg:w-[45%] mx-auto bg-white rounded-xl shadow-xl">
          <div className="w-full px-3 lg:px-10 pt-8">
            <h1 className="font-normal text-2xl">Đăng Nhập</h1>
            <hr className="mt-4 border-[#cccccc]" />
            <div className="mt-5  ">
              <label
                className="text-[16px] font-normal text-blacks "
                htmlFor="text"
              >
                Địa chỉ Email
              </label>
              <input
                id="email"
                type="text"
                placeholder="Email "
                {...register("email")}
                className={`bg-white test text-gray-900 rounded border focus:outline-none border-slate-300 hover:border-oranges focus:border-oranges  placeholder:font-normal placeholder:text-[16px] placeholder:text-[#6d767e] w-full p-3 ${
                  errors?.email?.message
                    ? "focus:ring-2 focus:ring-red-300 border border-red-500 "
                    : " focus:ring-0   "
                }`}
              />

              <span className="flex gap-1 mt-1 text-red-600 text-sm">
                {errors?.email?.message}
                {errors?.email?.message && (
                  <IoWarningOutline className="mt-[3px]" />
                )}
              </span>
            </div>

            <div className=" mt-4 relative ">
              <div className="">
                <label className="text-[16px] font-normal text-blacks ">
                  Mật khẩu
                </label>
                <input
                  autoComplete="on"
                  id="password"
                  type={eye === false ? "password" : "text"}
                  placeholder="password"
                  {...register("password")}
                  className={`bg-white test  text-gray-900 rounded border border-slate-300 focus:outline-none hover:border-oranges focus:border-oranges  placeholder:font-normal placeholder:text-[16px] placeholder:text-[#6d767e] w-full p-3 ${
                    errors?.password?.message
                      ? "focus:ring-2 focus:ring-red-300 border border-red-500 "
                      : "focus:ring-0   "
                  }`}
                />
                <span className="flex gap-1 mt-1 text-red-600 text-sm">
                  {errors?.password?.message}
                  {errors?.password?.message && (
                    <IoWarningOutline className="mt-[3px]" />
                  )}
                </span>
                <span className="flex gap-1 mt-1 text-red-600 text-sm">
                  {error}
                </span>
              </div>
              <div className="text-2xl cursor-pointer text-[#6a6870] absolute top-9 right-2 max-md:text-lg">
                {eye === false ? (
                  <AiOutlineEye onClick={toggle} />
                ) : (
                  <AiOutlineEyeInvisible onClick={toggle} />
                )}
              </div>
            </div>

            <div className="text-center text-[16px]">
              <Link href={"/Auth/resetpass"}>
                <span className="font-normal">Quên mật khẩu đăng nhập ?</span>
              </Link>
            </div>
            <div className="text-center mt-4">
              <button
                type="submit"
                className=" bg-oranges hover:bg-opacity-80 rounded-[30px] px-5 py-2 text-white   "
              >
                {loading ? (
                  <>
                    <svg
                      aria-hidden="true"
                      role="status"
                      className="inline w-4 h-4 mr-3 text-white animate-spin"
                      viewBox="0 0 100 101"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                        fill="#E5E7EB"
                      />
                      <path
                        d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                        fill="currentColor"
                      />
                    </svg>
                    Đăng Nhập
                  </>
                ) : (
                  "Đăng Nhập"
                )}
              </button>
            </div>

            <div className="text-center  py-5 text-lg">
              Bạn chưa có tài khoản ?{" "}
              <Link href={"/Auth/register"}>
                <span className="font-bold">Đăng ký ngay</span>
              </Link>
            </div>
          </div>
        </div>
      </form>
    </>
  );
};

export default Login;
