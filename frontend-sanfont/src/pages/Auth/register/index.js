import React, { useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { toast } from "react-toastify";
import { AiOutlineEye, AiOutlineEyeInvisible } from "react-icons/ai";
import { BiLeftArrowCircle } from "react-icons/bi";
import { IoWarningOutline } from "react-icons/io5";
import { FiUser } from "react-icons/fi";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import { useMemo } from "react";
import * as yup from "yup";
import AuthApis from "../../../../apis/AuthApis";
import SEO from "./../../../components/SEO";
const Register = () => {
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  const [error, setError] = useState("");

  const SignUpSchema = useMemo(
    () =>
      yup
        .object()
        .shape({
          username: yup
            .string()
            .required("Trường bắt buộc")
            .min(3, "Tối thiểu 3 kí tự")
            .max(50, "Tối đa 50 kí tự")
            .trim(),
          email: yup
            .string()
            .email("Email không hợp lệ")
            .required("Trường bắt buộc")
            .max(255)
            .matches(
              /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
              "Vui lòng nhập email hợp lệ"
            )
            .trim(),
          password: yup
            .string()
            .required("Trường bắt buộc")
            .min(6, "Tối thiểu 6 kí tự")
            .max(30, "Tối đa 30 kí tự")
            .trim(),
          confirmPassword: yup
            .string()
            .when("password", {
              is: (val) => (val && val.length > 0 ? true : false),
              then: () =>
                yup
                  .string()
                  .oneOf([yup.ref("password")], "Mật khẩu không giống nhau"),
            })
            .required("Trường bắt buộc")
            .trim(),
        })
        .required(),
    []
  );

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(SignUpSchema),
    mode: "onChange",
  });

  const onSubmit = (values) => {
    const { email, password, referralCode, username } = values;
    setLoading(true);
    AuthApis.signUpUser({ email, password, referralCode, username })
      .then(() => {
        router.push("/Auth/login");
        toast.success("Đăng ký thành công");
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);

        setError("Email đã được sử dụng.");
      })
      .finally(() => {});
  };
  //eye password
  const [eyeOne, setEyeOne] = useState(false);
  const toggle = () => {
    setEyeOne(!eyeOne);
  };

  //eye confirmPassword
  const [eye, setEye] = useState(false);
  const toggleOne = () => {
    setEye(!eye);
  };
  return (
    <>
      <SEO title={" Đăng ký"}></SEO>

      <div className="w-full  flex pt-28">
        <div className="container mx-auto ">
          <Link className="gap-1 flex font-normal text-[16px]" href={"/"}>
            <BiLeftArrowCircle className="text-xl mt-[3px]" />
            Về trang chủ
          </Link>
        </div>
      </div>

      <form
        onSubmit={handleSubmit(onSubmit)}
        className="container mx-auto mt-10  "
      >
        <div className=" md:w-[60%] lg:w-[45%] mx-auto bg-white rounded-xl shadow-xl">
          <div className="w-full px-3 lg:px-10 pt-8">
            {" "}
            <h1 className="font-normal text-2xl">Đăng ký</h1>
            <hr className="mt-4  border-[#cccccc]" />
            <div className="mt-5  ">
              <input
                type="text"
                id="username"
                placeholder="user"
                {...register("username")}
                className={`bg-white test  text-gray-900 rounded border border-slate-300 focus:outline-none hover:border-oranges focus:border-oranges  placeholder:font-normal placeholder:text-[16px] placeholder:text-[#6d767e] w-full p-3 ${
                  errors?.username?.message
                    ? "focus:ring-2 focus:ring-red-300 border border-red-500 "
                    : "focus:ring-0   "
                }`}
              />
              <span className="flex gap-1 mt-1 text-red-600 text-sm">
                {errors?.username?.message}
                {errors?.username?.message && (
                  <IoWarningOutline className="mt-[3px]" />
                )}
              </span>
            </div>
            <div className="mt-4">
              <input
                type="Email"
                id="email"
                placeholder="Email"
                {...register("email")}
                className={`bg-white test  text-gray-900 rounded border border-slate-300 focus:outline-none hover:border-oranges focus:border-oranges  placeholder:font-normal placeholder:text-[16px] placeholder:text-[#6d767e] w-full p-3 ${
                  errors?.email?.message
                    ? "focus:ring-2 focus:ring-red-300 border border-red-500 "
                    : "focus:ring-0  "
                }`}
              />
              <span className="flex gap-1 mt-1 text-red-600 text-sm">
                {errors?.email?.message}
                {errors?.email?.message && (
                  <IoWarningOutline className="mt-[3px]" />
                )}
              </span>
              <span className="flex gap-1 mt-1 text-red-600 text-sm">
                {error}
              </span>
            </div>
            <p className="mt-4 font-normal text-[12px] text-[#8d9399]">
              * Không sử dụng email Yahoo, email trường CĐ, ĐH khi đăng ký
            </p>
            <div className=" mt-4 relative ">
              <div>
                <input
                  type={eyeOne === false ? "password" : "text"}
                  placeholder="Mật khẩu"
                  id="password"
                  autoComplete="on"
                  {...register("password")}
                  className={`bg-white test  text-gray-900 rounded border border-slate-300 focus:outline-none hover:border-oranges focus:border-oranges  placeholder:font-normal placeholder:text-[16px] placeholder:text-[#6d767e] w-full p-3 ${
                    errors?.password?.message
                      ? "focus:ring-2 focus:ring-red-300 border border-red-500 "
                      : "focus:ring-0  "
                  }`}
                />
                <span className="flex gap-1 mt-1 text-red-600 text-sm">
                  {errors?.password?.message}
                  {errors?.password?.message && (
                    <IoWarningOutline className="mt-[3px]" />
                  )}
                </span>
              </div>
              <div className="text-2xl cursor-pointer text-[#6a6870] absolute top-3 right-2 max-md:text-lg">
                {eyeOne === false ? (
                  <AiOutlineEye onClick={toggle} />
                ) : (
                  <AiOutlineEyeInvisible onClick={toggle} />
                )}
              </div>
            </div>
            <div className=" mt-4 relative ">
              <div>
                <input
                  type={eye === false ? "password" : "text"}
                  id="confirmPassword"
                  autoComplete="on"
                  placeholder="Nhập lại mật khẩu"
                  {...register("confirmPassword")}
                  className={`bg-white test  text-gray-900 rounded border border-slate-300 focus:outline-none hover:border-oranges focus:border-oranges  placeholder:font-normal placeholder:text-[16px] placeholder:text-[#6d767e] w-full p-3 ${
                    errors?.confirmPassword?.message
                      ? "focus:ring-2 focus:ring-red-300 border border-red-500 "
                      : "focus:ring-0   "
                  }`}
                />
                <span className="flex gap-1 mt-1 text-red-600 text-sm">
                  {errors?.confirmPassword?.message}
                  {errors?.confirmPassword?.message && (
                    <IoWarningOutline className="mt-[3px]" />
                  )}
                </span>
              </div>
              <div className="text-2xl cursor-pointer text-[#6a6870] absolute top-3 right-2 max-md:text-lg">
                {eye === false ? (
                  <AiOutlineEye onClick={toggleOne} />
                ) : (
                  <AiOutlineEyeInvisible onClick={toggleOne} />
                )}
              </div>
            </div>
            <div className="w-full  ">
              <button
                type="submit"
                className="flex mt-5 mx-auto px-5 h-10 bg-oranges  items-center justify-center text-whites  hover:bg-opacity-80 rounded-[30px]"
              >
                {loading ? (
                  <>
                    <svg
                      aria-hidden="true"
                      role="status"
                      className="inline w-4 h-4 mr-3 text-white animate-spin"
                      viewBox="0 0 100 101"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                        fill="#E5E7EB"
                      />
                      <path
                        d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                        fill="currentColor"
                      />
                    </svg>
                    Đăng ký
                  </>
                ) : (
                  <>
                    <FiUser className="font-extrabold text-[20px]" />
                    <a className="ml-2 text-[16px] font-normal">Đăng ký</a>
                  </>
                )}
              </button>
            </div>
            <div className="text-center text-lg py-8 ">
              Bạn đã có tài khoản ?{" "}
              <Link href={"/Auth/login"}>
                <span className="font-bold">Đăng nhập ngay</span>
              </Link>
            </div>
          </div>
        </div>
      </form>
    </>
  );
};

export default Register;
