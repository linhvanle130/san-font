import imgGlobal from "../../../../public/images";
import Image from "next/image";
import SEO from "./../../../components/SEO";
const instruct = () => {
  return (
    <>
      <SEO title={" Hướng dẫn cài font chữ cho máy tính"}></SEO>

      <div className=" container pt-28 mx-auto bg-white ">
        <div className="w-full py-7 font-bold text-[30px] text-blacks text-center">
          Hướng dẫn cài font chữ cho máy tính
        </div>
        <hr className="bg-gray-500 h-[1px]" />

        <div className="flex flex-col items-center ">
          <div className="text-[30px] font-extrabold text-blacks mt-5">
            Cài cho Windows
          </div>
          <div className="text-2xl font-semibold text-blacks mt-4">
            Bước 1: Tải font chữ mà bạn muốn sử dụng từ <br /> website
            Sanfont.com
            <li className="text-[16px] font-normal pl-4 mt-2">
              Nếu file có định dạng .ttf hoặc otf thì bạn sẽ qua bước 2
            </li>
            <li className="text-[16px] font-normal mt-1 pl-4">
              Nếu file có định dạng .rar hoặc .zip thì bạn cần phải giải nén ra
              để có lấy font
            </li>
          </div>
          <div className="text-2xl font-semibold text-blacks mt-5">
            Bước 2: Cài đặt
            <p className="text-[16px] font-normal mt-2">
              Chọn các font mà bạn muốn cài, sau đó chuột phải và chọn
            </p>
            <Image
              priority
              src={imgGlobal.HD}
              className="max-md:w-32 w-90 py-6"
              alt="logo"
            ></Image>
          </div>
        </div>
      </div>
    </>
  );
};
export default instruct;
