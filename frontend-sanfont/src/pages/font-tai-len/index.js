import React, { useEffect, useState } from "react";
import TabProfile from "../../components/TabProfile";
import { useSelector } from "react-redux";
import SEO from "../../components/SEO";
import productApis from "../../../apis/productApis";
import ProductItem from "../../components/Products/ProductItem";
import axios from "axios";

const FontTaiLen = ({ data }) => {
  const { info } = useSelector((state) => state.account);

  const [product, setProduct] = useState();
  const [orderStatus, setOrderStatus] = useState();

  const [isSavedChange, setIsSavedChange] = useState(false);
  const [productsSaved, setProductsSaved] = useState();

  // filter product userId
  useEffect(() => {
    const filteredProducts = data.filter((e) => e.userId === info?.id);
    setProduct(filteredProducts);
  }, [info?.id]);

  // product
  useEffect(() => {
    if (info?.id) {
      const loadProductSave = async () => {
        const { data } = await axios.get(
          `http://localhost:3001/api/user/user-info/detail/${info?.id}`
        );
        setOrderStatus(data.orderStatus);

        setProductsSaved(data?.productSave || []);
      };
      loadProductSave();
    }
  }, [isSavedChange]);

  const onReloadProductsSaved = () => {
    setIsSavedChange(!isSavedChange);
  };

  return (
    <>
      <SEO title={"Font tải lên"}></SEO>

      <div className="pt-20 md:pt-20 lg:pt-32">
        <div className="container grid grid-cols-1 md:grid-cols-1 lg:grid-cols-7 gap-0 md:gap-0 lg:gap-5 mx-auto flex-col md:flex-col">
          <TabProfile tabs={4} />

          <div className="col-span-5  mt-12">
            {product?.length === 0 ? (
              <div className="text-center font-semibold text-lg">
                Chưa có font nào
              </div>
            ) : (
              <div className=" w-full grid gap-6 grid-cols-1 md:grid-cols-2 lg:grid-cols-3 ">
                {product?.map((item) => (
                  <div className="bg-whites shadow-md " key={item.id}>
                    <ProductItem
                      productData={item}
                      orderStatus={orderStatus}
                      productsSaved={productsSaved}
                      onReloadProductsSaved={onReloadProductsSaved}
                    />
                  </div>
                ))}
              </div>
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default FontTaiLen;

export async function getStaticProps() {
  const data = await productApis.getAllProducts();

  return {
    props: {
      data: data.rows,
    },
  };
}
