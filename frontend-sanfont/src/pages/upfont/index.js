import { yupResolver } from "@hookform/resolvers/yup";
import Link from "next/link";
import React, { useEffect, useRef, useState } from "react";
import { useSelector } from "react-redux";
import * as yup from "yup";
import { toast } from "react-toastify";
import { useForm, Controller, useWatch } from "react-hook-form";
import categoryApis from "../../../apis/categoryApis";
import { GLOBAL_STATUS } from "../../../constants";
import Compressor from "compressorjs";
import { Radio, Image, Spin } from "antd";
import { MdOutlineUploadFile } from "react-icons/md";

import { CiCamera } from "react-icons/ci";
import { useRouter } from "next/navigation";
import { IoWarningOutline } from "react-icons/io5";
import productApis from "../../../apis/productApis";
import { convertToSlug } from "@/utils/funcs";

const UpFont = ({ AllCategory }) => {
  const router = useRouter();
  const { token, info } = useSelector((state) => state.account);

  const UpFontSchema = yup.object({
    name: yup.string().required("Trường bắt buộc").max(255).trim(),
    slug: yup.string().required("Trường bắt buộc").max(255).trim(),

    subAuthor: yup
      .string()
      .required("Trường bắt buộc")
      .max(255)
      .nullable()
      .trim(),
    author: yup.string().required("Trường bắt buộc").max(255).nullable().trim(),
    outstanding: yup.number().max(2).required().nullable(),
    description: yup.string().trim().nullable().trim(),
    thumbnail: yup
      .string()
      .max(255)
      .required("Trường bắt buộc")
      .nullable()
      .trim(),
    mainFont: yup
      .string()
      .max(255)
      .required("Trường bắt buộc")
      .nullable()
      .trim(),
  });

  const {
    register,
    control,
    handleSubmit,
    formState: { errors },
    setValue,
  } = useForm({
    resolver: yupResolver(UpFontSchema),
  });

  const submitCreate = (values) => {
    const {
      name,
      categoryId,
      userId,
      slug,
      subAuthor,
      author,
      description,
      subImage,
      thumbnail,
      mainFont,
      outstanding,
    } = values;

    const payload = {
      name: name,
      productSlug: slug,
      categoryId: categoryId,
      userId: info?.id,
      description: description,
      subAuthor: subAuthor,
      outstanding: outstanding,
      description: description,
      author: author,
      mainImage: thumbnail,
      mainFont: mainFont,
    };

    return productApis
      .createProduct(payload)
      .then(() => {
        router.push("/");
        toast.success("Đăng tải font thành công");
      })
      .catch((err) => {
        console.log(err);
        toast.error("THẤT BẠI");
      })
      .finally(() => {});
  };

  const handleChange = (value) => {
    setValue("categoryId", value);
  };

  const onChange_Radio = (e) => {
    setValue(e.target.value);
  };

  useEffect(() => {
    setValue("outstanding", 0);
  }, []);

  const handleSetSlug = (value) => {
    setValue("name", value, {
      shouldValidate: true,
      shouldDirty: true,
    });
    setValue("slug", convertToSlug(value), {
      shouldValidate: true,
      shouldDirty: true,
    });
  };

  //file font
  const [isUpdateFont, setIsUpdateFont] = useState(false);
  const { mainFont } = useWatch({ control });

  const refFont = useRef();

  const onchangeFont = (e) => {
    const files = e.target.files;
    if (files.length > 0) {
      onUploadFont(files[0])
        .then((response) => {
          setValue("mainFont", response, {
            shouldValidate: true,
            shouldDirty: true,
          });
        })
        .finally(() => {
          setIsUpdateFont(false);
        });
    }
    e.target.value = null;
  };

  const onUploadFont = async (file) => {
    setIsUpdateFont(true);
    const formData = new FormData();
    formData.append("file", file);
    formData.append("fileName", file.name);
    return productApis
      .upFont(formData)
      .then(async (response) => {
        return response;
      })
      .catch((err) => {
        errorHelper(err);
      });
  };

  //file image
  const [isUpdateThumbnail, setIsUpdateThumbnail] = useState(false);
  const { thumbnail } = useWatch({ control });
  const refThumbnail = useRef();

  const onchangeThumbnail = (e) => {
    const files = e.target.files;
    if (files.length > 0) {
      new Compressor(files[0], {
        quality: 0.8,
        success: (compressedImg) => {
          onUploadThumbnail(compressedImg)
            .then((response) => {
              setValue("thumbnail", response, {
                shouldValidate: true,
                shouldDirty: true,
              });
            })
            .finally(() => {
              setIsUpdateThumbnail(false);
            });
        },
      });
    }
    e.target.value = null;
  };

  const onUploadThumbnail = async (file) => {
    setIsUpdateThumbnail(true);
    const formData = new FormData();
    formData.append("file", file);
    formData.append("fileName", file.name);
    return productApis
      .upImage(formData)
      .then(async (response) => {
        return response;
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <>
      {token ? (
        <div className="container mx-auto pt-32 md:pt-36 lg:pt-44">
          <div className=" bg-whites rounded-xl p-3 md:px-5 lg:p-10  shadow-inner">
            <form onSubmit={handleSubmit(submitCreate)}>
              {/* tên font */}
              <div>
                <h3 className="font-bold text-blacks mt-5">
                  Tiêu đề <span className="text-reds">*</span>
                </h3>
                <p className="text-sm text-[#888888]">
                  <span className="font-semibold ">Cách đặt tiêu đề:</span>
                  Font việt hoá + Tên font
                </p>
                <p className="text-sm text-[#888888]">
                  <span className="font-semibold ">Ví dụ: </span>
                  Font việt hóa Goatskin Brush
                </p>

                <input
                  {...register("name")}
                  type="text"
                  id="name"
                  onChange={(event) => handleSetSlug(event.target.value)}
                  className={`bg-white test  text-gray-900 rounded border border-slate-300 focus:outline-none hover:border-oranges focus:border-oranges  placeholder:font-normal placeholder:text-[16px] placeholder:text-[#6d767e] w-full p-3 ${
                    errors?.name?.message
                      ? "focus:ring-2 focus:ring-red-300 border border-red-500 "
                      : "focus:ring-0   "
                  }`}
                />

                <span className="flex gap-1 mt-1 text-red-600 text-sm">
                  {errors?.name?.message}
                  {errors?.name?.message && (
                    <IoWarningOutline className="mt-[3px]" />
                  )}
                </span>
              </div>
              {/* name slug  */}
              <div className="hidden">
                <input
                  {...register("slug")}
                  type="text"
                  id="slug"
                  name="slug"
                  className={`bg-white test  text-gray-900 rounded border border-slate-300 focus:outline-none hover:border-oranges focus:border-oranges  placeholder:font-normal placeholder:text-[16px] placeholder:text-[#6d767e] w-full p-3`}
                />
              </div>

              {/* Hình ảnh */}
              <div>
                <h3 className="text-blacks font-bold mt-3 md:mt-5 lg:mt-10">
                  Ảnh đại diện <span className="text-reds">*</span>
                </h3>

                <div className=" mt-3">
                  {thumbnail ? (
                    <div className="flex">
                      <div className="w-60 h-60">
                        {" "}
                        <Image
                          className="w-60 h-60"
                          src={thumbnail}
                          alt="avt"
                        />
                        <button
                          type="button"
                          disabled={isUpdateThumbnail}
                          onClick={() => refThumbnail.current.click()}
                        >
                          <CiCamera className=" text-blues hover:text-blueHover" />
                          <input
                            ref={refThumbnail}
                            type="file"
                            accept="image/*"
                            onChange={onchangeThumbnail}
                            style={{ display: "none" }}
                          />
                        </button>
                      </div>
                    </div>
                  ) : (
                    <div>
                      <div className="flex">
                        <p className=" text-[15px] font-medium text-blacks max-lg:text-xs">
                          Chưa có ảnh nào được chọn.
                        </p>
                        <button
                          type="button"
                          disabled={isUpdateThumbnail}
                          onClick={() => refThumbnail.current.click()}
                        >
                          <div className="flex text-blues hover:text-blueHover  text-[15px] font-normal">
                            <p>chọn ảnh</p> <CiCamera />
                          </div>
                          <input
                            ref={refThumbnail}
                            type="file"
                            id="thumbnail"
                            accept="image/*"
                            onChange={onchangeThumbnail}
                            style={{ display: "none" }}
                          />
                        </button>
                      </div>
                      <span className="flex gap-1 mt-1 text-red-600 text-sm">
                        {errors?.thumbnail?.message}
                        {errors?.thumbnail?.message && (
                          <IoWarningOutline className="mt-[3px]" />
                        )}
                      </span>
                    </div>
                  )}
                </div>
              </div>

              {/* Nội dung */}
              <div>
                <h3 className="text-blacks font-bold mt-3 md:mt-5 lg:mt-10">
                  Nội dung
                </h3>
                <textarea
                  {...register("description")}
                  id="description"
                  rows="4"
                  // onChange={(event) => handleDescription(event.target.value)}
                  className="block p-2.5 w-full text-sm text-blacks rounded-lg border border-gray-300 focus:outline-none focus:ring-0   focus:border-oranges hover:border-oranges dark:bg-gray-700  "
                ></textarea>
              </div>

              {/* Danh mục category */}
              <div>
                <h3 className="text-blacks font-bold mt-3 md:mt-5 lg:mt-10 ">
                  Danh mục <span className="text-reds">*</span>
                </h3>
                <div
                  className={`w-[100%]  mt-2 border border-solid border-gray-300 rounded ${
                    errors?.author?.message
                      ? "focus:ring-2 focus:ring-red-300 border border-red-500 "
                      : "focus:ring-red-300   "
                  }`}
                >
                  <div className="box-check-lick w-full  ">
                    {AllCategory?.map((e, id) => {
                      return (
                        <div key={id}>
                          <ul className="w-full pl-3">
                            <li>
                              <input
                                id="vue-checkbox"
                                type="checkbox"
                                value={e.id}
                                onChange={(event) =>
                                  handleChange(event.target.value)
                                }
                                className="w-3 h-3 text-blues bg-gray-100 border-gray-300 shadow-inner rounded focus:ring-blue-500"
                              />
                              <span
                                htmlFor="vue-checkbox"
                                className="w-full pt-2 ml-2 text-sm font-normal text-blacks"
                              >
                                {e.name}
                              </span>
                            </li>
                          </ul>
                        </div>
                      );
                    })}
                  </div>
                </div>
                <span className="flex gap-1 mt-1 text-red-600 text-sm">
                  {errors?.author?.message}
                  {errors?.author?.message && (
                    <IoWarningOutline className="mt-[3px]" />
                  )}
                </span>
              </div>

              <div>
                <h3 className="text-blacks font-bold mt-3 md:mt-5 lg:mt-10 mb-2">
                  Loại font
                </h3>
                {/* loại font */}
                <Controller
                  name="outstanding"
                  control={control}
                  render={({ field }) => (
                    <Radio.Group onChange={onChange_Radio} {...field}>
                      <Radio value={1}>VIP</Radio>
                      <Radio value={2}>Font chọn lọc</Radio>
                    </Radio.Group>
                  )}
                />
              </div>

              {/* Tác giả */}
              <div>
                <h3 className="font-bold text-blacks mt-3 md:mt-5 lg:mt-10">
                  Tác giả <span className="text-reds">*</span>
                </h3>
                <input
                  {...register("author")}
                  type="text"
                  id="author"
                  // onChange={(event) => handleSetAuthor(event.target.value)}
                  className={`bg-whites mt-4  border-gray-300 text-gray-900 rounded border focus:outline-none hover:border-oranges focus:border-oranges  placeholder:font-medium placeholder:text-base placeholder:text-[#6d767e] w-full p-3 
              ${
                errors?.author?.message
                  ? "focus:ring-2 focus:ring-red-300 border border-red-500 "
                  : "focus:ring-0 "
              }    `}
                />
                <span className="flex gap-1 mt-1 text-red-600 text-sm">
                  {errors?.author?.message}
                  {errors?.author?.message && (
                    <IoWarningOutline className="mt-[3px]" />
                  )}
                </span>
              </div>

              {/* việt hóa */}
              <div>
                <h3 className="font-bold text-blacks mt-3 md:mt-5 lg:mt-10">
                  Việt hóa bởi <span className="text-reds">*</span>
                </h3>
                <p className="text-sm mt-3 text-[#888888]">
                  Hãy cố gắng ghi thông tin người việt hóa hoặc team việt hóa để
                  tôn trọng họ nha
                </p>

                <input
                  {...register("subAuthor")}
                  type="text"
                  id="subAuthor"
                  // onChange={(event) => handleSetSubAuthor(event.target.value)}
                  className={`bg-whites mt-4  border-gray-300 text-gray-900 rounded border focus:outline-none hover:border-oranges focus:border-oranges  placeholder:font-medium placeholder:text-base placeholder:text-[#6d767e] w-full p-3 
              ${
                errors?.subAuthor?.message
                  ? "focus:ring-2 focus:ring-red-300 border border-red-500 "
                  : "focus:ring-0"
              }    `}
                />
                <span className="flex gap-1 mt-1 text-red-600 text-sm">
                  {errors?.subAuthor?.message}
                  {errors?.subAuthor?.message && (
                    <IoWarningOutline className="mt-[3px]" />
                  )}
                </span>
              </div>

              {/* file font */}
              <div>
                <h3 className="font-bold text-blacks mt-3 md:mt-5 lg:mt-10">
                  Font tải lên <span className="text-reds">*</span>
                </h3>
                <p className="text-sm mt-3 text-[#888888]">
                  Có thể tải lên định dạng .ttf, .otf hoặc .zip (Nếu nhiều font
                  nên nén zip lại)
                </p>

                <div
                  className={`w-[50%] border-[1px] border-solid border-gray-300 rounded p-3 hover:border-oranges focus:border-oranges
                ${
                  errors?.mainFont?.message
                    ? "focus:ring-2 focus:ring-red-300 border border-red-500 "
                    : "focus:ring-0"
                }`}
                >
                  <div className=" flex gap-5">
                    <div>{mainFont?.slice(30) || "không có tệp được chọn"}</div>
                    <button
                      type={"button"}
                      disabled={isUpdateFont}
                      onClick={() => refFont.current.click()}
                    >
                      {isUpdateFont ? (
                        <Spin />
                      ) : (
                        <MdOutlineUploadFile className="w-6 h-6 text-blues hover:text-blueHover" />
                      )}
                      <input
                        ref={refFont}
                        type="file"
                        id="mainFont"
                        accept=".zip,.otf,.ttf"
                        onChange={onchangeFont}
                        style={{ display: "none" }}
                      />
                    </button>
                  </div>
                </div>
                <span className="flex gap-1 mt-1 text-red-600 text-sm">
                  {errors?.mainFont?.message}
                  {errors?.mainFont?.message && (
                    <IoWarningOutline className="mt-[3px]" />
                  )}
                </span>
              </div>

              {/* button đăng font */}
              <button
                type="submit"
                className="flex mt-8 max-sm:mx-3 items-center w-[120px] max-sm:w-28 max-sm:h-8 h-11 bg-oranges hover:bg-opacity-80 rounded-[30px]  justify-center btn-gradient text-whites  max-md:text-[13px] text-base font-normal"
              >
                Đăng bài
              </button>
            </form>
          </div>
        </div>
      ) : (
        // đăng nhập trước khi đăng font
        <div className="container mx-auto pt-48">
          <div className=" bg-whites rounded-xl p-10 shadow-inner">
            <h2 className="text-blacks font-bold text-[24px]">ĐĂNG TẢI FONT</h2>
            <hr className="mt-4" />

            <p className="mt-6 font-normal text-blacks text-[16px]">
              Vui lòng
              <Link
                className="text-blues hover:text-blueHover px-1"
                href={"/Auth/login"}
              >
                đăng nhập
              </Link>
              để đăng tải font.
            </p>
          </div>
        </div>
      )}
    </>
  );
};

export default UpFont;

export async function getStaticProps() {
  const AllCategory = await categoryApis.getAllCategory({
    status: GLOBAL_STATUS.ACTIVE,
  });

  return {
    props: {
      AllCategory: AllCategory,
    },
  };
}
