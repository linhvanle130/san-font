import SEO from "./../../../components/SEO";
import TabMenber from "./../../../components/TabMenber";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import { HiOutlineCircleStack, HiOutlineUser } from "react-icons/hi2";
import { IoMdTime } from "react-icons/io";
import orderApis from "../../../../apis/orderApis";
import { useSelector } from "react-redux";
import moment from "moment";

const index = ({ ListOrder }) => {
  const { token, info } = useSelector((state) => state.account);

  const [list, setList] = useState();

  useEffect(() => {
    const orderAccount = ListOrder.filter((e) => e.userId === info?.id);

    setList(orderAccount);
  }, []);

  const Status = list?.slice(0, 1).map((value) => {
    if (value.orderStatus === 1) {
      return "Chờ kích hoạt";
    } else if (value.orderStatus === 2) {
      return "đã kích hoạt";
    } else {
      return value;
    }
  });

  return (
    <>
      <SEO title={"Thông báo"}></SEO>

      <div className="container mx-auto bg-white pt-32">
        <div className="py-8 md:py-10 lg:py-14 mx-auto flex flex-col gap-5">
          <h1 className="text-3xl md:text-4xl lg:text-5xl  text-center font-bold">
            Đăng ký gói thành viên
          </h1>
          <TabMenber className="pt-12" tabs={3} />

          <div className="text-center flex flex-col gap-3 md:gap-5 lg:gap-8">
            <h1 className="text-base md:text-xl lg:text-[24px]  text-center font-bold">
              Cảm ơn bạn đã đăng ký thành công
            </h1>

            {list?.slice(0, 1).map((e) => {
              return (
                <div
                  key={e.id}
                  className="grid grid-cols-1 md:grid-cols-1 lg:grid-cols-3 gap-3 text-center justify-items-center"
                >
                  <div className="flex py-1">
                    {" "}
                    <HiOutlineUser className="mr-2 font-normal text-xl mt-[2px]" />
                    <p className="text-[16px] text-blacks font-extrabold ">
                      Loại tài khoản:
                    </p>{" "}
                    <span className="text-gray-400 ">
                      {e.orderPackage?.name.slice(4)}
                    </span>
                  </div>
                  <div className="flex py-1 text-[16px] text-blacks font-extrabold ">
                    <HiOutlineCircleStack className="mr-2 text-lg mt-[2px] " />
                    Trạng thái tài khoản:
                    <span className="text-[#85f00c] ">{Status}</span>
                  </div>
                  <div className="flex py-1">
                    {" "}
                    <IoMdTime className="mr-2 text-lg mt-[2px] " />
                    <p className="text-[16px] text-blacks font-extrabold ">
                      Hạn dùng:
                    </p>{" "}
                    <span className="text-gray-400 ">
                      {moment(e?.createdAt)
                        .add(e.orderPackage?.name.slice(3, 6), "months")
                        .format("DD - MM - YYYY")}
                    </span>
                  </div>
                </div>
              );
            })}

            <div className=" mx-auto text-[16px] text-black mb-10">
              Cảm ơn bạn đã đăng ký gói thành viên. Chúng tôi sẽ kích hoạt ngay
              sau khi bạn thanh toán (8h sáng - 22h tối) hoặc ib page để được hổ
              trợ nhanh
            </div>
          </div>
          <div className="w-full mx-auto">
            <Link href={"/profile"}>
              <button className="w-full py-3 bg-[#1774fd] text-white font-normal text-[18px] rounded">
                Quay lại tài khoản
              </button>
            </Link>
          </div>
        </div>
      </div>
    </>
  );
};

export default index;

export async function getStaticProps() {
  const order = await orderApis.getListOrder();

  return {
    props: {
      ListOrder: order.rows,
    },
  };
}
