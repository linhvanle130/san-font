import SEO from "./../../../components/SEO";
import TabMenber from "./../../../components/TabMenber";
import { deleteAll } from "../../../../redux/slices/cartSlice";
import * as yup from "yup";
import Link from "next/link";
import React, { useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import { useRouter } from "next/navigation";
import { set, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { IoWarningOutline } from "react-icons/io5";
import orderApis from "../../../../apis/orderApis";

const index = () => {
  const router = useRouter();
  const { info } = useSelector((state) => state.account);
  const cart = useSelector((state) => state.cart.value);

  const dispath = useDispatch();

  const UpFontSchema = useMemo(
    () =>
      yup.object().shape({
        fullName: yup.string().required("Dữ liệu này là bắt buộc").trim(),
        email: yup
          .string()
          .email("Email không hợp lệ")
          .required("Trường bắt buộc")
          .max(255),
        address: yup
          .string()
          .required("Trường bắt buộc")
          .max(255)
          .nullable()
          .trim(),
        telephone: yup
          .string()
          .required("Trường bắt buộc")
          .max(255)
          .nullable()
          .trim(),
      }),
    []
  );

  const {
    register,
    control,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(UpFontSchema),
  });

  const submitCreate = (values) => {
    const {
      fullName,
      email,
      orderPackageId,
      userId,
      paymentMethod,
      telephone,
      address,
      total,
    } = values;

    const payload = {
      fullName: fullName,
      email: email,
      orderPackageId: orderPackageId,
      userId: userId,
      paymentMethod: paymentMethod,
      address: address,
      telephone: telephone,
      total: total,
    };

    return orderApis
      .createOrder(payload)
      .then(() => {
        router.push("/dang-ki-thanh-vien/thanh-toan");
        localStorage.setItem("userData", JSON.stringify(payload));
        toast.success(" thành công");
      }, 5000)
      .catch((err) => {
        console.log(err);
        toast.error("THẤT BẠI");
      })
      .finally(() => {});
  };

  useEffect(() => {
    const setOrderPackageId = () => {
      {
        cart?.map((e) => setValue("orderPackageId", e.id));
      }
    };

    const setTotall = () => {
      {
        cart?.map((e) => setValue("total", e.price));
      }
    };
    setValue("userId", info?.id);
    setValue("fullName", info?.username);
    setValue("email", info?.email);
    setValue("paymentMethod", "2");

    setOrderPackageId();
    setTotall();
  }, []);

  return (
    <>
      <SEO title={"Đăng ký thông tin"}></SEO>

      <div className="container mx-auto bg-white   lg:pt-32">
        <div className="py-8 md:py-10 lg:py-14">
          <h1 className="text-3xl md:text-4xl lg:text-5xl  text-center font-bold">
            Đăng ký gói thành viên
          </h1>
          <TabMenber tabs={1} />
          <form onSubmit={handleSubmit(submitCreate)} action="">
            <div className="flex flex-col gap-8">
              <div className=" flex flex-col gap-2 md:gap-4 lg:gap-5 py-3">
                <h1 className="text-black text-lg md:text-xl lg:text-2xl mb-[14px] font-semibold">
                  Thông tin thanh toán
                </h1>
                <div>
                  <label
                    className="text-[16px] font-normal text-blacks"
                    htmlFor="text"
                  >
                    Họ tên
                  </label>
                  <input
                    disabled
                    defaultValue={info?.username}
                    id="text"
                    type="text"
                    className="bg-[#e9ecef] font-normal border-gray-300 text-blacks rounded border focus:outline-none  focus:border-oranges focus:ring-4 focus:ring-red-200  placeholder:font-normal placeholder:text-[16px] placeholder:text-[#6d767e] w-full py-3 px-3"
                  />
                </div>
                <div>
                  <label
                    className="text-[16px] font-normal text-blacks"
                    htmlFor="text"
                  >
                    Email
                  </label>
                  <input
                    disabled
                    defaultValue={info?.email}
                    id="email"
                    type="email"
                    className="bg-[#e9ecef]  font-normal border-gray-300 text-blacks rounded border   w-full py-3 px-3"
                  />
                </div>

                <div>
                  <label
                    className="text-[16px] font-normal text-blacks"
                    htmlFor="text"
                  >
                    Số điện thoại
                  </label>
                  <input
                    {...register("telephone")}
                    id="telephone"
                    type="text"
                    // onChange={(event) => handlePhone(event.target.value)}
                    className={`bg-white test text-gray-900 rounded border focus:outline-none hover:border-oranges focus:border-oranges  placeholder:font-normal placeholder:text-[16px] placeholder:text-[#6d767e] w-full p-3 ${
                      errors?.telephone?.message
                        ? "focus:ring-2 focus:ring-red-300 border border-red-500 "
                        : "focus:ring-red-300"
                    }`}
                  />
                  <span className="flex gap-1 mt-1 text-red-600 text-sm">
                    {errors?.telephone?.message}
                    {errors?.telephone?.message && (
                      <IoWarningOutline className="mt-[3px]" />
                    )}
                  </span>
                </div>
                <div>
                  <label
                    className="text-[16px] font-normal text-blacks"
                    htmlFor="text"
                  >
                    Địa chỉ
                  </label>
                  <input
                    {...register("address")}
                    id="address"
                    type="text"
                    // onChange={(event) => handleAddress(event.target.value)}
                    className={`bg-white test text-gray-900 rounded border  focus:outline-none hover:border-oranges focus:border-oranges  placeholder:font-normal placeholder:text-[16px] placeholder:text-[#6d767e] w-full p-3 ${
                      errors?.address?.message
                        ? "focus:ring-2 focus:ring-red-300 border border-red-500 "
                        : "focus:ring-red-300"
                    }`}
                  />
                  <span className="flex gap-1 mt-1 text-red-600 text-sm">
                    {errors?.address?.message}
                    {errors?.address?.message && (
                      <IoWarningOutline className="mt-[3px]" />
                    )}
                  </span>
                </div>
              </div>

              {cart?.map((e) => {
                return (
                  <div key={e.id}>
                    <ul className="text-[16px] text-black flex flex-col gap-3">
                      <h1 className="text-black text-2xl mb-[14px] font-semibold ">
                        Thông tin gói thành viên
                      </h1>
                      <li className="font-normal">
                        Gói thành viên:{" "}
                        <strong className="font-bold">{e.name.slice(4)}</strong>
                      </li>
                      <li className="font-normal">
                        Tổng:
                        <strong className="font-bold"> {e.price} vnd</strong>
                      </li>

                      <li className="font-normal">
                        Phương thức thanh toán:{" "}
                        <strong className="font-bold">
                          Thanh toán qua tài khoản ngân hàng
                        </strong>
                      </li>
                    </ul>

                    <div className=" flex flex-col gap-3 py-3">
                      <button
                        type="submit"
                        className="w-full py-3 bg-[#1774fd] rounded text-white font-normal text-lg"
                      >
                        đăng ký
                      </button>

                      <Link href={"/goi-vip"}>
                        <button
                          onClick={() => dispath(deleteAll())}
                          className="w-full py-3 bg-white border border-[#8d8d8d] text-[#8d8d8d] rounded font-normal text-lg hover:border-black hover:text-black"
                        >
                          Hủy đăng ký
                        </button>
                      </Link>
                    </div>
                  </div>
                );
              })}
            </div>
          </form>
        </div>
      </div>
    </>
  );
};

export default index;
