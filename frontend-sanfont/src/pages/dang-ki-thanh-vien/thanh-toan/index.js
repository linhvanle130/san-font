import React, { useEffect, useState } from "react";
import SEO from "../../../components/SEO";
import TabMenber from "../../../components/TabMenber";
import orderApis from "../../../../apis/orderApis";
import { useSelector } from "react-redux";
import moment from "moment";
import Link from "next/link";
import imgGlobal from "../../../../public/images";
import Image from "next/image";

const index = ({ ListOrder }) => {
  const { token, info, user } = useSelector((state) => state.account);

  const [list, setList] = useState();

  useEffect(() => {
    const orderAccount = ListOrder.filter((e) => e.userId === info?.id);
    orderAccount.sort((a, b) => new Date(b.postTime) - new Date(a.postTime));
    setList(orderAccount);
  }, []);

  return (
    <>
      <SEO title={"Thanh toán"}></SEO>

      <div className="container mx-auto bg-white pt-32">
        <div className="py-8 md:py-10 lg:py-14">
          <h1 className="text-3xl md:text-4xl lg:text-5xl  text-center font-bold">
            Đăng ký gói thành viên
          </h1>
          <TabMenber className="pt-12" tabs={2} />

          <div className="text-center text-[16px] py-4">
            {list?.slice(0, 1).map((e) => {
              return (
                <div
                  key={e.id}
                  className="flex flex-col gap-2 md:gap-3 lg:gap-4"
                >
                  <div className=" text-base md:text-xl lg:text-[24px] text-[#8d8d8d] font-semibold">
                    <strong className="text-black">
                      Thông tin thanh toán gói:{" "}
                    </strong>
                    {e.orderPackage?.name.slice(4)}
                  </div>

                  <div className="font-bold ">
                    <strong className="font-normal">
                      Ngày bắt đầu đăng ký:{" "}
                    </strong>
                    {moment(e.createdAt).format("DD - MM - YYYY")}
                  </div>
                  <div className=" text-[16px] text-black">
                    Số tiền bạn cần thanh toán:{" "}
                    <strong className="text-red-500">
                      {e.orderPackage?.price} vnd
                    </strong>
                  </div>
                  <div className=" text-[16px] text-black">
                    Nội dung chuyển khoản:{" "}
                    <strong className="text-red-500">{e.orderCode}</strong>
                  </div>
                </div>
              );
            })}

            <p className="text-[16px] text-black py-5 px-5 md:px-32 lg:px-32">
              Sau khi chuyển khoản xong. Chúng tôi sẽ kích hoạt ngay sau khi bạn
              thanh toán ( 8h sáng - 22h tối ) hoặc ib page để được hỗ trợ nhanh
              Ib page sau khi đã thanh toán xong Chat ngay
            </p>
          </div>

          <div className="flex justify-center mx-auto gap-20">
            <div className=" text-[16px]  flex flex-col items-center text-center text-black">
              <h3 className="font-bold mb-2">
                Thanh toán qua tài khoản ngân hàng
              </h3>
              <div className="mb-1">Ngân hàng Vietcombank</div>
              <div className="mb-1">Số tài khoản: *************</div>
              <Image
                src={imgGlobal.AVT}
                width={400}
                height={400}
                alt="image"
                className=" w-full h-full object-cover "
              />
              <div className="mb-3">Tên tài khoản: Ken Bùi</div>
            </div>
          </div>
          <div className="  md:flex lg:flex gap-10  mx-auto mt-5">
            <div className="w-full md:w-[50%] lg:w-[50%]">
              <Link href={"/dang-ki-thanh-vien/dang-ki-thong-tin"}>
                <button className="w-full py-3 bg-white border border-[#8d8d8d] text-[#8d8d8d] rounded font-normal text-lg hover:border-black hover:text-black">
                  Quay lại
                </button>
              </Link>
            </div>
            <div className="w-full md:w-[50%] lg:w-[50%] mt-2 md:mt-0 lg:mt-0">
              {" "}
              <Link href={"/dang-ki-thanh-vien/thong-bao"}>
                <button className="w-full py-3 bg-[#1774fd] rounded text-white font-normal text-lg">
                  Xác nhận đã thanh toán xong
                </button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default index;

export async function getStaticProps() {
  const order = await orderApis.getListOrder();

  return {
    props: {
      ListOrder: order.rows,
    },
  };
}
