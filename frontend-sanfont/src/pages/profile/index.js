import React, { useEffect, useState } from "react";
import Link from "next/link";
import { HiOutlineUser } from "react-icons/hi2";
import { IoMdTime } from "react-icons/io";
import { HiOutlineCircleStack } from "react-icons/hi2";
import TabProfile from "../../components/TabProfile";
import { useSelector } from "react-redux";
import SEO from "../../components/SEO";
import orderApis from "../../../apis/orderApis";
import moment from "moment";
import axios from "axios";

const Profile = ({ ListOrder }) => {
  const { token, info } = useSelector((state) => state.account);

  const [account, setAcount] = useState([""]);

  useEffect(() => {
    if (info?.id) {
      const loadAcount = async () => {
        const { data } = await axios.get(
          `http://localhost:3001/api/user/user-info/detail/${info?.id}`
        );

        setAcount(data);
      };
      loadAcount();
    }
  }, []);

  const [list, setList] = useState();

  useEffect(() => {
    const orderAccount = ListOrder.filter((e) => e.userId === info?.id);

    setList(orderAccount);
  }, []);

  const Status = list?.slice(0, 1).map((value) => {
    if (value.orderStatus === 1) {
      return "Chờ kích hoạt";
    } else if (value.orderStatus === 2) {
      return "đã kích hoạt";
    } else {
      return value;
    }
  });

  return (
    <>
      <SEO title={"Tài khoản"}></SEO>

      <div className="pt-20 md:pt-20 lg:pt-32">
        {token ? (
          <div className="container grid grid-cols-1 md:grid-cols-1 lg:grid-cols-7 gap-0 md:gap-0 lg:gap-5 mx-auto flex-col md:flex-col">
            <TabProfile tabs={1} />
            <div className=" col-span-5 mt-12">
              <div>
                <div className=" w-full  bg-whites rounded-3xl shadow-md py-8  px-10">
                  <h1 className="text-2xl font-normal ">Thông tin tài khoản</h1>

                  {account?.order?.length === 0 ? (
                    <div className="justify-between block md:block lg:flex">
                      <div className="flex py-1">
                        {" "}
                        <HiOutlineUser className="mr-2 font-normal text-xl mt-[2px]" />
                        <p className="text-[16px] text-blacks font-extrabold max-xl:text-sm">
                          Loại tài khoản:
                        </p>{" "}
                        <span className="text-gray-400 max-xl:text-sm">
                          Free
                        </span>
                      </div>
                      <div className="flex py-1 text-[16px] text-blacks font-extrabold max-xl:text-sm">
                        <HiOutlineCircleStack className="mr-2 text-lg mt-[2px] " />
                        Trạng thái tài khoản:
                        <span className="text-[#85f00c] max-xl:text-sm">
                          Đã kích hoạt
                        </span>
                      </div>
                      <div className="flex py-1">
                        {" "}
                        <IoMdTime className="mr-2 text-lg mt-[2px] " />
                        <p className="text-[16px] text-blacks font-extrabold max-xl:text-sm">
                          Hạn dùng:
                        </p>{" "}
                        <span className="text-gray-400 max-xl:text-sm">
                          Vĩnh viễn
                        </span>
                      </div>
                    </div>
                  ) : (
                    <>
                      {list?.slice(0, 1).map((e) => {
                        return (
                          <div
                            key={e.id}
                            className="justify-between block md:block lg:flex"
                          >
                            <div className="flex py-1">
                              {" "}
                              <HiOutlineUser className="mr-2 font-normal text-xl mt-[2px]" />
                              <p className="text-[16px] text-blacks font-extrabold max-xl:text-sm">
                                Loại tài khoản:
                              </p>
                              <span className="text-gray-400 max-xl:text-sm">
                                {e.orderPackage?.name.slice(4)}
                              </span>
                            </div>
                            <div className="flex py-1 text-[16px] text-blacks font-extrabold max-xl:text-sm">
                              <HiOutlineCircleStack className="mr-2 text-lg mt-[2px] " />
                              Trạng thái tài khoản:
                              <span className="text-[#85f00c] max-xl:text-sm">
                                {Status}
                              </span>
                            </div>
                            <div className="flex py-1">
                              {" "}
                              <IoMdTime className="mr-2 text-lg mt-[2px] " />
                              <p className="text-[16px] text-blacks font-extrabold max-xl:text-sm">
                                Hạn dùng:
                              </p>{" "}
                              <span className="text-gray-400 max-xl:text-sm">
                                {moment(e?.createdAt)
                                  .add(
                                    e.orderPackage?.name.slice(3, 6),
                                    "months"
                                  )
                                  .format("DD - MM - YYYY")}
                              </span>
                            </div>
                          </div>
                        );
                      })}
                    </>
                  )}

                  <div className=" mt-4 w-full h-[1px] bg-slate-300"></div>
                  <p className="font-normal text-lg text-blacks mt-4 max-lg:text-sm">
                    Nếu bạn chưa thanh toán, vui lòng đặt lại gói thành viên để
                    lấy mã thanh toán.
                    <Link href={"/goi-vip"} className="text-[#0d6efd] ml-1">
                      Đặt lại
                    </Link>
                  </p>
                </div>
              </div>
            </div>
          </div>
        ) : (
          ""
        )}
      </div>
    </>
  );
};

export default Profile;

export async function getStaticProps() {
  const order = await orderApis.getListOrder();

  return {
    props: {
      ListOrder: order.rows,
    },
  };
}
