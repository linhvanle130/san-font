import React from "react";
import { BsSearch } from "react-icons/bs";
import { useState } from "react";
import productsApis from "../../apis/productApis";
import categoryApis from "../../apis/categoryApis";
import CheckButton from "./../components/CheckButton";
import { useEffect } from "react";
import { useCallback } from "react";
import { useRouter } from "next/router";
import SEO from "./../components/SEO";
import { useSelector } from "react-redux";
import axios from "axios";
import ProductItem from "../components/Products/ProductItem";
import Advertisement from "@/components/Advertisement";

const initFilter = {
  category: [],
};

function Home({ data, AllCategory, dataSelective, dataVip }) {
  const router = useRouter();
  const { info } = useSelector((state) => state.account);
  const [searchQuery, setSearchQuery] = useState(router.query.name || "");

  const [filter, setFilter] = useState(initFilter);
  const [productCategory, setProductCategory] = useState(data);
  const [productsOptions, setProductsOptions] = useState(data);

  const [productsSaved, setProductsSaved] = useState();
  const [isSavedChange, setIsSavedChange] = useState(false);
  const [orderStatus, setOrderStatus] = useState();

  const [seeMore, setSeeMore] = useState(12);

  // search
  const handleSearchChange = (event) => {
    const query = event.target.value;
    setSearchQuery(query);

    const filtered = data.filter((product) =>
      product.name.toLowerCase().includes(query.toLowerCase())
    );

    setProductCategory(filtered);
  };

  // danh mục category
  const updateProducts = useCallback(() => {
    let temp = productsOptions;
    if (filter.category.length > 0) {
      temp = temp?.filter((e) =>
        filter.category.includes(e.productCategory.categorySlug)
      );
    }
    setProductCategory(temp);
  }, [filter, productsOptions]);

  useEffect(() => {
    updateProducts();
  }, [updateProducts]);

  const filterSelect = (type, checked, item) => {
    if (checked) {
      switch (type) {
        case "CATEGORY":
          setFilter({
            ...filter,
            category: [...filter.category, item.categorySlug],
          });
          break;
        default:
      }
    } else {
      switch (type) {
        case "CATEGORY":
          const newCategory = filter.category.filter(
            (e) => e !== item.categorySlug
          );
          setFilter({ ...filter, category: newCategory });
          break;
        default:
      }
    }
  };

  // save product
  useEffect(() => {
    if (info?.id) {
      const loadProductSave = async () => {
        const { data } = await axios.get(
          `http://localhost:3001/api/user/user-info/detail/${info?.id}`
        );
        setOrderStatus(data.orderStatus);

        setProductsSaved(data?.productSave || []);
      };
      loadProductSave();
    }
  }, [isSavedChange]);

  // load lại product sau save- unsave
  const onReloadProductsSaved = () => {
    setIsSavedChange(!isSavedChange);
  };

  // xem thêm
  const showMore =
    productCategory?.length > 4
      ? productCategory.slice(0, seeMore)
      : productCategory;

  const loadMore = () => {
    setSeeMore((prevValue) => prevValue + 4);
  };

  return (
    <>
      <SEO></SEO>
      <div className="container mx-auto ">
        {/* search */}
        <div className="px-10 md:px-20 lg:px-48 pt-32">
          <div className="relative mx-auto ">
            <input
              type="text"
              value={searchQuery}
              onChange={handleSearchChange}
              placeholder="Tìm font tại đây"
              className="pl-4 font-normal w-full py-3 border border-whites focus:ring-0 focus:ring-red-300 hover:border-oranges rounded-[30px] focus:outline-none focus:border-oranges text-[16px] "
            />

            <div className="absolute top-3 right-6 cursor-pointer">
              <BsSearch className="w-7 h-6 text-[#9ca3af] " />
            </div>
          </div>
        </div>

        {/* Menu font */}
        <div className="mt-10 px-10 md:px-20 lg:px-48 flex flex-wrap gap-3.5 justify-center">
          {/* <button
            onClick={() => setProductCategory(data)}
            className="py-3 px-4 mx-1.5  cursor-pointer bg-whites rounded-[30px] font-normal focus:bg-oranges focus:text-whites text-blacks"
          >
            All font
          </button> */}
          {AllCategory?.length > 0 &&
            AllCategory?.map((e) => {
              return (
                <ul key={e.id}>
                  <li>
                    <CheckButton
                      test={e.name}
                      onChange={(button) =>
                        filterSelect("CATEGORY", button.checked, e)
                      }
                      checked={filter.category.includes(e.categorySlug)}
                    ></CheckButton>
                  </li>
                </ul>
              );
            })}

          <ul className="flex gap-3">
            <li onClick={() => setProductCategory(dataSelective)}>
              <button className="text-center flex justify-center items-center cursor-pointer px-3 py-2.5 rounded-[30px] focus:bg-oranges font-normal text-[16px] bg-[#ffa800] text-white">
                Font chọn lọc
              </button>
            </li>
            <li onClick={() => setProductCategory(dataVip)}>
              <button className="text-center flex justify-center items-center cursor-pointer px-3 py-2.5 rounded-[30px] focus:bg-oranges font-normal text-[16px] bg-[#34a853] text-white">
                VIP
              </button>
            </li>
          </ul>
        </div>

        {/* product */}
        <div className="flex gap-3  ">
          <h2 className="font-normal text-[30px] mt-16">
            {productCategory.length} font
          </h2>
        </div>
        <div className="mx-auto mt-6 grid gap-6 grid-cols-1 md:grid-cols-2 lg:grid-cols-4  ">
          {showMore.map((item) => (
            <div key={item.id}>
              <ProductItem
                productData={item}
                productsSaved={productsSaved}
                orderStatus={orderStatus}
                onReloadProductsSaved={onReloadProductsSaved}
              />
            </div>
          ))}
        </div>
        {/* xem thêm */}
        <div className="w-full text-center mt-6">
          <button
            onClick={loadMore}
            className="bg-oranges  hover:bg-reds py-2 px-6 font-normal text-[16px] text-whites rounded-[8px]"
          >
            Xem thêm
          </button>
        </div>
        <Advertisement />
      </div>
    </>
  );
}

export default Home;

export async function getStaticProps() {
  const product = await productsApis.getAllProducts();

  const AllCategory = await categoryApis.getAllCategory();

  const dataVip = product.rows.filter((e) => e.outstanding === 1);
  const dataSelective = product.rows.filter((e) => e.outstanding === 2);

  return {
    props: {
      data: product.rows,
      AllCategory: AllCategory,
      dataSelective: dataSelective,
      dataVip: dataVip,
    },
  };
}
