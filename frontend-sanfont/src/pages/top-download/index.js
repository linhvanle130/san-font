import SEO from "./../../components/SEO";
import React, { useEffect, useState } from "react";
import productsApis from "../../../apis/productApis";
import ProductItem from "@/components/Products/ProductItem";
import { useSelector } from "react-redux";
import axios from "axios";

const TopDownload = ({ data }) => {
  const { info } = useSelector((state) => state.account);

  const [productsSaved, setProductsSaved] = useState();
  const [isSavedChange, setIsSavedChange] = useState(false);
  const [orderStatus, setOrderStatus] = useState();

  const sortedList = [...data].sort(
    (a, b) => b.downloadFont.length - a.downloadFont.length
  );

  // save product
  useEffect(() => {
    if (info?.id) {
      const loadProductSave = async () => {
        const { data } = await axios.get(
          `http://localhost:3001/api/user/user-info/detail/${info?.id}`
        );
        setOrderStatus(data.orderStatus);

        setProductsSaved(data?.productSave || []);
      };
      loadProductSave();
    }
  }, [isSavedChange]);

  // load lại product sau save- unsave
  const onReloadProductsSaved = () => {
    setIsSavedChange(!isSavedChange);
  };
  return (
    <>
      <SEO title={"Top Download"}></SEO>
      <div className="container mx-auto pt-28 ">
        <h2 className="font-medium text-[36px] ">Top Download</h2>
        {/* <div className=" text-center py-5">Đang cập nhật...</div> */}
        <div className=" mt-6 gap-6 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4">
          {sortedList.map((item) => (
            <div key={item.id}>
              <ProductItem
                productData={item}
                productsSaved={productsSaved}
                orderStatus={orderStatus}
                onReloadProductsSaved={onReloadProductsSaved}
              />
            </div>
          ))}
        </div>
      </div>
    </>
  );
};

export default TopDownload;

export async function getStaticProps() {
  const product = await productsApis.getAllProducts();

  return {
    props: {
      data: product.rows,
    },
  };
}
