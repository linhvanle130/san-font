import React, { useEffect, useState } from "react";
import axios from "axios";
import lodash from "lodash";
import SEO from "./../../../components/SEO";
import productApis from "../../../../apis/productApis";
import ProductDetail from "./../../../components/Products/productDetail";
import CommentProduct from "../../../components/Comments/CommentProduct";
import { useSelector } from "react-redux";
import ProductItem from "../../../components/Products/ProductItem";
import Advertisement from "@/components/Advertisement";
const Detail = ({ dataDetail, productRelate }) => {
  // const randomRelate = lodash.sampleSize(productRelate, 6);
  const { info } = useSelector((state) => state.account);
  const [isSavedChange, setIsSavedChange] = useState(false);
  const [productsSaved, setProductsSaved] = useState();
  const [orderStatus, setOrderStatus] = useState();

  // save product
  useEffect(() => {
    if (info?.id) {
      const loadProductSave = async () => {
        const { data } = await axios.get(
          `http://localhost:3001/api/user/user-info/detail/${info?.id}`
        );

        setProductsSaved(data?.productSave || []);
      };
      loadProductSave();
    }
  }, [isSavedChange]);

  const onReloadProductsSaved = () => {
    setIsSavedChange(!isSavedChange);
  };
  return (
    <>
      <SEO title={`${dataDetail.name}`}></SEO>

      <div className="flex  container pt-32 mx-auto gap-5">
        <div className="w-[50%]  ">
          <ProductDetail
            dataDetail={dataDetail}
            productsSaved={productsSaved}
            onReloadProductsSaved={onReloadProductsSaved}
          />
          <div className="mt-6 bg-whites px-3 py-3 text-blacks">
            <CommentProduct dataDetail={dataDetail} />
          </div>
        </div>

        <div className="w-[50%] ">
          <h1 className="text-2xl font-bold max-xl:mt-5"> Font liên quan </h1>
          <div className="w-full mt-2 grid grid-cols-2 gap-5  max-md:grid max-md:grid-cols-1">
            {productRelate?.slice(0, 6).map((e) => {
              return (
                <div key={e.id}>
                  <ProductItem
                    productData={e}
                    orderStatus={orderStatus}
                    productsSaved={productsSaved}
                    onReloadProductsSaved={onReloadProductsSaved}
                  />
                </div>
              );
            })}
          </div>
        </div>
      </div>
      <div className="container mx-auto">
        <Advertisement />
      </div>
    </>
  );
};

export default Detail;

export async function getStaticPaths() {
  const product = await productApis.getAllProducts();

  const allPaths = product.rows.map((ev) => {
    return {
      
      params: {
        fontSmall: ev.productCategory.categorySlug,
        detail: ev.productSlug ,
      },
    };
  });
  console.log("allPaths", allPaths);

  return {
    paths: allPaths,
    fallback: true,
  };
}

export async function getStaticProps(context) {
  const slug = context.params.detail;
  const dataDetails = await axios.get(
    `http://0.0.0.0:3001/api/product/get-product-by-slug?productSlug=${slug}`
  );

  const product = await productApis.getAllProducts();

  return {
    props: {
      productRelate: product.rows,
      dataDetail: dataDetails.data,
    },
  };
}
