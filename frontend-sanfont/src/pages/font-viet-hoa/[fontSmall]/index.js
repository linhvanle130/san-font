import { GLOBAL_STATUS } from "../../../../constants";
import ProductCategorySlug from "./../../../components/Products/ProductCategorySlug";
import React, { useEffect, useState } from "react";
import productApis from "../../../../apis/productApis";
import axios from "axios";
import SEO from "./../../../components/SEO";
import { useSelector } from "react-redux";
import Advertisement from "@/components/Advertisement";

const SlugVH = ({ categorySlug }) => {
  const { info } = useSelector((state) => state.account);

  const [isSavedChange, setIsSavedChange] = useState(false);
  const [productsSaved, setProductsSaved] = useState();

  // save product
  useEffect(() => {
    if (info?.id) {
      const loadProductSave = async () => {
        const { data } = await axios.get(
          `http://localhost:3001/api/user/user-info/detail/${info?.id}`
        );

        setProductsSaved(data?.productSave || []);
      };
      loadProductSave();
    }
  }, [isSavedChange]);

  const onReloadProductsSaved = () => {
    setIsSavedChange(!isSavedChange);
  };

  return (
    <>
      <SEO
        title={` ${categorySlug?.rows
          ?.slice(0, 1)
          .map((e) => e.productCategory.name)} `}
      ></SEO>
      <div className="container mx-auto pt-32 md:pt-32 lg:pt-32">
        <h2 className="font-bold text-2xl ">
          {categorySlug?.rows?.slice(0, 1)?.map((e) => e.productCategory.name)}(
          {categorySlug?.rows?.length} font)
        </h2>

        <div className=" mt-6 grid gap-6 grid-cols-1 md:grid-cols-2 lg:grid-cols-4 ">
          {categorySlug?.rows
            ?.map((item, id) => {
              return (
                <div key={id}>
                  <ProductCategorySlug
                    CategorySlug={item}
                    productsSaved={productsSaved}
                    onReloadProductsSaved={onReloadProductsSaved}
                  />
                </div>
              );
            })
            .reverse()}
        </div>
        <Advertisement />
      </div>
    </>
  );
};

export default SlugVH;

export async function getStaticPaths() {
  const product = await productApis.getAllProducts({
    status: GLOBAL_STATUS.ACTIVE,
  });

  const allPaths = product.rows.map((ev) => {
    return {
      params: {
        fontSmall: ev.productCategory.categorySlug,
      },
    };
  });
  return {
    paths: allPaths,
    fallback: true,
  };
}
export async function getStaticProps(context) {
  const categorySlug = context.params.fontSmall;
  const productCategorySlug = await axios.get(
    `http://0.0.0.0:3001/api/product/get-by-slug?categorySlug=${categorySlug}`
  );

  return {
    props: {
      categorySlug: productCategorySlug.data,
    },
  };
}
