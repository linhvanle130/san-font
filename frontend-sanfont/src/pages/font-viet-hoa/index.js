import SEO from "./../../components/SEO";
import React, { useEffect, useState } from "react";
import ReactPaginate from "react-paginate";
import productApis from "../../../apis/productApis";
import { useSelector } from "react-redux";
import axios from "axios";
import ProductItem from "../../components/Products/ProductItem";
import Advertisement from "@/components/Advertisement";

const fontVH = ({ categoryFont }) => {
  const { info } = useSelector((state) => state.account);

  const [isSavedChange, setIsSavedChange] = useState(false);
  const [productsSaved, setProductsSaved] = useState();
  const [orderStatus, setOrderStatus] = useState();

  const [products, setProduct] = useState(categoryFont);
  const [pageNumber, setPageNumber] = useState(0);

  // save product
  useEffect(() => {
    if (info?.id) {
      const loadProductSave = async () => {
        const { data } = await axios.get(
          `http://localhost:3001/api/user/user-info/detail/${info?.id}`
        );
        setOrderStatus(data.orderStatus);

        setProductsSaved(data?.productSave || []);
      };
      loadProductSave();
    }
  }, [isSavedChange]);

  const onReloadProductsSaved = () => {
    setIsSavedChange(!isSavedChange);
  };

  // navigator
  const productsPerPage = 20;
  const pagesVisited = pageNumber * productsPerPage;
  const displayProduct = products
    .slice(pagesVisited, pagesVisited + productsPerPage)
    .map((item, id) => {
      return (
        <div key={item.id}>
          <ProductItem
            orderStatus={orderStatus}
            productData={item}
            productsSaved={productsSaved}
            onReloadProductsSaved={onReloadProductsSaved}
          />
        </div>
      );
    })
    .reverse();

  const pageCount = Math.ceil(products.length / productsPerPage);

  const changePage = ({ selected }) => {
    setPageNumber(selected);
  };

  return (
    <>
      <SEO
        title={"Font việt hóa"}
        description={"chia sẻ font việt hóa miễn phí"}
      ></SEO>

      <div className="container mx-auto pt-32 md:pt-32 lg:pt-32">
        <h2 className="font-semibold text-[28px] ">Font việt hóa</h2>

        <div className="mt-6 grid gap-6 grid-cols-1 md:grid-cols-2 lg:grid-cols-4 ">
          {displayProduct}
        </div>
        <div className="mt-10  w-[30%] mx-auto ">
          <ReactPaginate
            nextLabel={"Next"}
            pageCount={pageCount}
            onPageChange={changePage}
            containerClassName={"paginationBttns"}
            previousLinkClassName={"previousBttn"}
            nextLinkClassName={"nextBttn"}
            disabledClassName={"paginationDisabled"}
            activeClassName={"paginationActive"}
            onClick={() => window.scroll(0, 0)}
          />
        </div>
        <Advertisement />
      </div>
    </>
  );
};

export default fontVH;

export async function getStaticProps() {
  const product = await productApis.getAllProducts();

  const datas = product.rows.filter((e) => e.productCategory.categoryId === 1);

  return {
    props: {
      categoryFont: datas,
    },
  };
}
