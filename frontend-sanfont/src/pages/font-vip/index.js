import React, { useEffect, useState } from "react";
import productApis from "../../../apis/productApis";
import SEO from "./../../components/SEO";
import { useSelector } from "react-redux";
import axios from "axios";
import ProductItem from "../../components/Products/ProductItem";
import Advertisement from "@/components/Advertisement";

const FontVip = ({ data }) => {
  const { info } = useSelector((state) => state.account);

  const [isSavedChange, setIsSavedChange] = useState(false);
  const [productsSaved, setProductsSaved] = useState();

  // save product
  useEffect(() => {
    if (info?.id) {
      const loadProductSave = async () => {
        const { data } = await axios.get(
          `http://localhost:3001/api/user/user-info/detail/${info?.id}`
        );

        setProductsSaved(data?.productSave || []);
      };
      loadProductSave();
    }
  }, [isSavedChange]);

  const onReloadProductsSaved = () => {
    setIsSavedChange(!isSavedChange);
  };

  const [seeMore, setSeeMore] = useState(20);
  const showMore = data.length > 10 ? data.slice(0, seeMore) : data;
  const loadMore = () => {
    setSeeMore((prevValue) => prevValue + 10);
  };
  return (
    <>
      <SEO title={"Font Vip "}></SEO>

      <div className="container mx-auto pt-32 px-5 md:px-10 lg:px-0  ">
        <h2 className="font-bold text-[30px]   ">
          Font Vip ( {data.length} font)
        </h2>
        <div className=" mt-6 gap-6 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4">
          {showMore
            ?.map((item, id) => {
              return (
                <div key={id}>
                  <ProductItem
                    productData={item}
                    productsSaved={productsSaved}
                    onReloadProductsSaved={onReloadProductsSaved}
                  />
                </div>
              );
            })
            .reverse()}
        </div>
        {/* xem thêm */}
        <div className="w-full text-center mt-6">
          <button
            onClick={loadMore}
            className="bg-oranges  hover:bg-reds py-2 px-6 font-normal text-[16px] text-whites rounded-[8px]"
          >
            Xem thêm
          </button>
        </div>
        <Advertisement />
      </div>
    </>
  );
};

export default FontVip;

export async function getStaticProps() {
  const product = await productApis.getAllProducts();
  const datas = product.rows.filter((e) => e.outstanding === 1);

  return {
    props: { data: datas },
  };
}
